<?php
defined('BASEPATH') or exit('No direct script access allowed');
ini_set('max_execution_time', -1);

class Data extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// $this->authentification->check_user_authentification();

		$this->load->model('Libur_model');
		$this->load->model('Jam_model');
		$this->load->model('Jadwal_model');
		$this->load->model('Presensi_model');

		$this->nipPns = $this->session->userdata('nip');
		$this->roleId = $this->session->userdata('role');
		$this->dataPns = getDataPns($this->nipPns);
	}

	public function index()
	{
		redirect('dashboard');
	}

	public function presensi()
	{
		$th = $this->input->post('tahun');
		if ($th == '') {
			$year = date('Y');
		} else {
			$year = $th;
		}

		$bl = $this->input->post('tahun');
		if ($th == '') {
			$month = date('Y');
		} else {
			$month = $bl;
		}
		$nipPns = $this->session->userdata('nip');
		$roleId = $this->session->userdata('role');
		$dataPns = getDataPns($nipPns);
		if ($roleId == 1 || $roleId == 2) {
			$A_01 = '';
		} else {
			$A_01 = $dataPns->A_01;
		}
		$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);
		$data['keterangan'] = $this->Jam_model->get_all_data();
		$data['year'] = $year;
		$data['month'] = $month;
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('data/presensi/presensi_index', $data);
	}

	public function presensi_cari()
	{
		$opd = $this->input->post('opd');
		$unit = $this->input->post('unit');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$tb = $tahun . $bulan;

		// $a_01 = substr($unit, 0, 2);
		// $a_02 = substr($unit, 2, 2);
		// $a_03 = substr($unit, 4, 2);
		// $a_04 = substr($unit, 6, 2);
		// $a_05 = substr($unit, 8, 2);

		$get = $this->db->query("SELECT a.id_presensi, b.opd, b.unit, a.*
					   FROM  presensi a 
					   INNER JOIN parent_presensi b ON a.id_parent = b.id 
					   WHERE b.opd = '$opd' AND a.tahun_bulan = '$tb'");

		
		$data['presensi_data'] = $get;
		$data['keterangan'] = $this->Jam_model->get_all_data();
		$data['jam_kerja'] = $this->Jam_model->get_jam_kerja();
		$data['izin_data'] = $this->Presensi_model->get_izin($unit,$tahun,$bulan);
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;

		if($get->num_rows() > 0)
		{
		$pgw = array();
		foreach($get->result() as $peg)
		{
			$pgw[] = $peg->nip;
		}
		}
		$data['user_info'] = $this->Presensi_model->get_userinfo($pgw);

		// echo json_encode($data);
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('data/presensi/presensi_list', $data);
	}

	public function set_jadwal_awal()
	{
		$opd = $this->input->post('opd');
		$unit = $this->input->post('unit');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$tb = $tahun . $bulan;

		$a_01 = substr($unit, 0, 2);
		$a_02 = substr($unit, 2, 2);
		$a_03 = substr($unit, 4, 2);
		$a_04 = substr($unit, 6, 2);
		$a_05 = substr($unit, 8, 2);

		$cek = $this->Jadwal_model->count_presensi_by_tahun_bulan($opd, $unit, $bulan, $tahun);
		// echo json_encode($cek);
		if ($cek->hit > 0) {
			$get = $this->db->query("SELECT a.id_presensi, b.opd, b.unit, a.*
					   FROM  presensi a 
					   INNER JOIN parent_presensi b ON a.id_parent = b.id 
					   WHERE b.opd = '$opd' AND a.tahun_bulan = '$tb'")->result();
			$status = 'add';
			$status = 'update';
		} else {

			$nip_sudah = $this->Jadwal_model->sudah_dijadwal($opd, $tahun, $bulan);
			if ($nip_sudah) {
				foreach ($nip_sudah as $sd) {
					$array_sudah = array(
						$sd->nip,
					);
				}
			} else {

				$array_sudah = array(
					1
				);
			}

			// echo json_encode($id_sudah);
			// echo json_encode($sudah);
			if ($unit == '') {
				$a_01 = substr($opd, 0, 2);
				// $gets = $this->Jadwal_model->get_pegawai_opd($a_01);
				$gets = $this->Jadwal_model->get_pegawai_opd_v2($a_01, $array_sudah);
			} else {
				// $gets = $this->Jadwal_model->get_pegawai_unit($a_01, $a_02, $a_03, $a_04, $a_05);
				$gets = $this->Jadwal_model->get_pegawai_unit_v2($a_01, $a_02, $a_03, $a_04, $a_05, $array_sudah);
			}

			$generate_format = array(
				'opd' => $opd,
				'unit' => $unit,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'generate_by' => $this->nipPns,
				'created_at' => date('Y-m-d H:i:s'),

			);
			$add_parent = $this->Jadwal_model->insert_parent_data($generate_format);
			if ($add_parent != FALSE) {
				$data = array();
				foreach ($gets as $pegawai) {
					$data[] = array(
						'tahun_bulan' => $tahun . $bulan,
						'id'		  => $tahun . $bulan . '_' . $pegawai->nip,
						'id_parent'   => $add_parent,
						'nip' 		  => $pegawai->nip,
						'nama_lengkap' => $pegawai->nama_lengkap,
						'created_at'  => date('Y-m-d H:i:s'),
						'created_by'  => $this->nipPns,
					);
				}

				// echo json_encode($data);

				$this->Jadwal_model->insert_batch_jadwal($data);
				$get = $this->db->query("SELECT a.id_presensi, b.opd, b.unit, a.*
					   FROM  presensi a 
					   INNER JOIN parent_presensi b ON a.id_parent = b.id 
					   WHERE b.opd = '$opd' AND a.tahun_bulan = '$tb'")->result();
				$status = 'add';
				//echo json_encode($data);
			}
		}

		$data['data_pegawai'] = $get;
		$data['status'] = $status;
		$data['keterangan'] = $this->Jam_model->get_all_data();
		$data['jamkerja'] = $this->Jam_model->get_jam_kerja();
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/kerja/kerja_add', $data);
	}
}
