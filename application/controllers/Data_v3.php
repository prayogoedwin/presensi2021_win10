<?php
defined('BASEPATH') or exit('No direct script access allowed');
ini_set('max_execution_time', -1);

class Data_v3 extends CI_Controller
{
	var $maxDate = 30;
	public function __construct()
	{
		parent::__construct();
		// $this->authentification->check_user_authentification();

		// untuk di tembah cronjob => http://localhost:81/presensi2021/data_v3/presensi/presensi_save?year=2021&month=09

		$this->load->model('Libur_model');
		$this->load->model('Jam_model');
		$this->load->model('Jadwal_model');
		$this->load->model('Presensi_model');

		$this->nipPns = $this->session->userdata('nip');
		$this->roleId = $this->session->userdata('role');
		$this->dataPns = getDataPns($this->nipPns);
	}

	public function index()
	{
		// redirect('dashboard');
		exec('echo -e "`crontab -l`n30 9 * * * /path/to/script" | crontab -');
	}

	public function presensi($action = "")
	{
		switch($action)
		{
			case "harian" :
				$data['title'] = $title = 'Edit Presensi';
				$data['nip'] = $nip = $this->uri->segment(4);
				$data['tanggal'] = $tanggal = $this->uri->segment(5);
				$data['process'] = $process = $this->uri->segment(6);
				
				if($nip && $tanggal)
				{
				$data['tgl'] = $tgl = substr($tanggal,8,2);
				$data['bln'] = $bln = substr($tanggal,5,2);
				$data['thn'] = $thn = substr($tanggal,0,4);
				
				switch($process)
				{
					case "simpan" :
						$this->load->library('form_validation');
						$this->form_validation->set_rules('absen_masuk_jam', 'Absen Masuk', 'trim|required');
						$this->form_validation->set_rules('absen_masuk_menit', 'Absen Masuk', 'trim|required');
						$this->form_validation->set_rules('absen_pulang_jam', 'Absen Pulang', 'trim|required');
						$this->form_validation->set_rules('absen_pulang_menit', 'Absen Pulang', 'trim|required');
						$this->form_validation->set_rules('status', 'Status', 'trim|required');
						
						if ($this->form_validation->run() == FALSE)
						{
							echo json_encode(array('success'=>false,'message'=>'Lengkapi Formulir'));		
							
						}
						else
						{
							if($this->Presensi_model->simpan_harian($nip,$thn,$bln,$tgl))
							{
								echo json_encode(array('success'=>true,'message'=>"Data Presensi berhasil diubah"));
							}
							else
							{
								echo json_encode(array('success'=>false,'message'=>"Data Presensi gagal diubah"));		
							}
							
						}
					break;
					default :
						$data['form_action'] = $form_action = site_url('data_v3/presensi/harian/'.$nip.'/'.$tanggal.'/simpan');
						$data['status'] = $status = $this->Presensi_model->get_status();
						$data['form_data'] = $form_data = $this->Presensi_model->get_presensi_harian($nip,$thn,$bln,$tgl)->row_array();
						$this->load->view('data_v3/presensi/harian', $data);
					break;
				}
				
				
				
				}
				else
				{
					redirect('data/presensi');
				}
			break;
			case "delete" : 
					$id = $this->uri->segment(4);
																
					if($this->Presensi_model->delete_data($id))
					{
					echo json_encode(array('success'=>true,'message'=>"Data Presensi Berhasil Dihapus"));
					}
					else
					{
					echo json_encode(array('success'=>false,'message'=>"Data Presensi Gagal Dihapus"));
					}
				
				break;
			case "save" :
				$data['unit_kerja'] = $unit_kerja = $this->uri->segment(4);
				$data['tahun'] = $tahun = $this->uri->segment(5);
				$data['bulan'] = $bulan = $this->uri->segment(6);
				if($unit_kerja && $tahun && $bulan)
				{
					$this->Presensi_model->save_parent_data($tahun,$bulan,$unit_kerja);
					$pegawai_data = $this->Presensi_model->get_pegawai($unit_kerja,$tahun,$bulan);
					foreach($pegawai_data->result() as $pegawai)
					{
							$id = $tahun.''.$bulan.'_'.$pegawai->nip;
							$this->Presensi_model->update_data($id,$tahun,$bulan,$pegawai->nip);	
					}
					echo json_encode(array('success'=>true,'message'=>"Data Presensi Berhasil Disimpan"));
				}
				else
				{
					echo json_encode(array('success'=>false,'message'=>"Data Presensi Gagal Disimpan"));
				}
				
				
			break;

			case 'check' :
				$th = $this->input->post('year');
				$bl = $this->input->post('month');
				
				$nipPns = $this->session->userdata('nip');
				$roleId = $this->session->userdata('role');
				$dataPns = getDataPns($nipPns);
				if ($roleId == 1 || $roleId == 2) {
					$A_01 = '';
				} else {
					$A_01 = $dataPns->A_01;
				}
				$data['year'] = $th;
				$data['month'] = $bl;
				$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);
				$table =$this->Presensi_model->get_presensi_all($th, $bl);
				$group_table = [];
				foreach($table as $k => $v){
					$group_table[$v->opd] = $v;
				}
				#echo '<pre>';print_r($data['allopd']);print_r($table);die();
				$data['table'] = $group_table;
				$this->load->view('data_v3/presensi/table', $data);

			break;
	
			case 'all' :
				$th = $this->input->get('year');
				if ($th == '') {
					$year = date('Y');
				} else {
					$year = $th;
				}

				$bl = $this->input->get('month');
				if ($bl == '') {
					$month = date('m');
				} else {
					$month = $bl;
				}
				$nipPns = $this->session->userdata('nip');
				$roleId = $this->session->userdata('role');
				$dataPns = getDataPns($nipPns);
				if ($roleId == 1 || $roleId == 2) {
					$A_01 = '';
				} else {
					$A_01 = $dataPns->A_01;
				}
				#$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);
				#$data['keterangan'] = $this->Jam_model->get_all_data();
				$data['year'] = $year;
				$data['month'] = $month;
				$this->load->view('template/head');
				$this->load->view('template/header');
				$this->load->view('template/sidebar');
				$this->load->view('data_v3/presensi/presensi_all', $data);
			break;
	
			default :
				$th = $this->input->post('tahun');
				if ($th == '') {
					$year = date('Y');
				} else {
					$year = $th;
				}

				
				$bl = $this->input->post('bulan');
				if ($bl == '') {
					$month = date('m');
				} else {
					$month = $bl;
				}
				$nipPns = $this->session->userdata('nip');
				$roleId = $this->session->userdata('role');
				$dataPns = getDataPns($nipPns);
				if ($roleId == 1 || $roleId == 2) {
					$A_01 = '';
				} else {
					$A_01 = $dataPns->A_01;
				}
				$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);
				$data['keterangan'] = $this->Jam_model->get_all_data();
				$data['year'] = $year;
				$data['month'] = $month;
				$this->load->view('template/head');
				$this->load->view('template/header');
				$this->load->view('template/sidebar');
				$this->load->view('data_v3/presensi/presensi_index', $data);
			break;
		}
	}

	public function presensi_save()
	{
			error_reporting(0);
		#echo '<pre>';
		#print_r($this->input->get());
		$bulan = $this->input->get('bulan');
		$tahun = $this->input->get('tahun');
		$nipPns = $this->session->userdata('nip');
		$roleId = $this->session->userdata('role');
		$dataPns = getDataPns($nipPns);
		if ($roleId == 1 || $roleId == 2) {
			$A_01 = '';
		} else {
			$A_01 = $dataPns->A_01;
		}

		$A_01 = '';
		$allopd = $this->Fungsi_model->get_opd_list($A_01);
		// Save Employee & attendance
		
		#print_r($allopd);
		#die();
		$params = [];
		$params['bulan'] = $bulan;
		$params['tahun'] = $tahun;
		$params['jam_kerja'] = $this->Jam_model->get_jam_kerja();

		#check by max date (ignore)
		$excludeopd = $this->Presensi_model->get_presensi_all($tahun, $bulan, $this->maxDate);
		$excludeopdlist = [];
		foreach($excludeopd as $k => $v){
			$excludeopdlist[] = $v->opd;
		}
		
		#echo '<pre>';print_r($excludeopdlist);die();
		$tb = $tahun.''.$bulan;
		foreach($allopd as $key_opd => $value_opd){
			$opd = $value_opd->KOLOK;
			$opd_id = $value_opd->A_01;
			#-----cek max date & parent
			#$is_exist = $this->Presensi_model->get_presensi_all($tahun, $bulan, null, $opd);
			#if (!in_array($opd, $excludeopdlist) || count($is_exist) > 0){
				
			#-----cek max date
			if (!in_array($opd, $excludeopdlist)){
				$presensi_parent_id = $this->Presensi_model->save_parent_data($tahun,$bulan,$opd);
				#echo ']]';print_r($presensi_parent_id);
				$allpns = $this->Fungsi_model->get_pns_list($opd_id);
				#echo ']]';print_r($allpns);
				$params['izin_data'] = $this->Presensi_model->get_izin($opd_id,$tahun,$bulan);
				foreach($allpns as $key_pns => $value_pns){
					$nip = $value_pns->B_02B;
					$fullname = $value_pns->L_NAMA_NIK;
					$presensi_id = $this->Presensi_model->save_data($presensi_parent_id,$tahun,$bulan,$nip,$fullname,$params);
					#echo'@@';print_r($presensi_id);
					#die();
				}
				$get = $this->db->query("SELECT a.id_presensi, b.opd, b.unit, a.*
							FROM  presensi a 
							INNER JOIN parent_presensi b ON a.id_parent = b.id 
							WHERE b.opd = '$opd' AND a.tahun_bulan = '$tb' ");
				$params['presensi_data'] = $get->result();
				if($get->num_rows() > 0)
				{
					$pgw = array();
					foreach($get->result() as $peg)
					{
						$pgw[] = $peg->nip;
					}
				}
				$user_info = $this->Presensi_model->get_userinfo($pgw);
				foreach ($user_info->result() as $user) {
					$params['userinfo'][trim($user->street, ' ')] = $user->userid;
					$params['users'][] = $user->userid;
				}
				#echo '<pre>';print_r($params);
				#die();
				$generated = $this->Presensi_model->generate_data($params);
				#echo '<pre>';print_r($generated);
				
				$updated = $this->Presensi_model->updated_data($generated);
				#die();
			}
		}
		#die();
		
		/*
		// Save Employee & attendance
		$tb = $tahun.''.$bulan;
		foreach($allopd as $key_opd => $value_opd){
			$opd = $value_opd->KOLOK;
			$cek = $this->Jadwal_model->count_presensi_by_tahun_bulan($opd, '', $bulan, $tahun);
			// echo json_encode($cek);
			if ($cek->hit == 0) {
				$nip_sudah = $this->Jadwal_model->sudah_dijadwal($opd, $tahun, $bulan);
				if ($nip_sudah) {
					foreach ($nip_sudah as $sd) {
						$array_sudah = array(
							$sd->nip,
						);
					}
				} else {

					$array_sudah = array(
						1
					);
				}

				$a_01 = substr($opd, 0, 2);
				// $gets = $this->Jadwal_model->get_pegawai_opd($a_01);
				$gets = $this->Jadwal_model->get_pegawai_opd_v2($a_01, $array_sudah);

				$generate_format = array(
					'opd' => $opd,
					'unit' => '',
					'bulan' => $bulan,
					'tahun' => $tahun,
					'generate_by' => $this->nipPns,
					'created_at' => date('Y-m-d H:i:s'),

				);
				$add_parent = $this->Jadwal_model->insert_parent_data($generate_format);
				if ($add_parent != FALSE) {
					$data = array();
					foreach ($gets as $pegawai) {
						$data[] = array(
							'tahun_bulan' => $tahun . $bulan,
							'id'		  => $tahun . $bulan . '_' . $pegawai->nip,
							'id_parent'   => $add_parent,
							'nip' 		  => $pegawai->nip,
							'nama_lengkap' => $pegawai->nama_lengkap,
							'created_at'  => date('Y-m-d H:i:s'),
							'created_by'  => $this->nipPns,
						);
					}

					// echo json_encode($data);

					$this->Jadwal_model->insert_batch_jadwal($data);
				}
			}
		}
		*/
		redirect(base_url('data_v3/presensi/all?year='.$tahun.'&month='.$bulan));
	}

	public function presensi_cari()
	{
		#print_r($this->input->post());die();
		$opd = $this->input->post('opd');
		$unit = $this->input->post('unit');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$tb = $tahun . $bulan;
		
		$pegawai_data = $this->Presensi_model->get_presensi($opd,$tahun,$bulan);
		
		if($pegawai_data->num_rows() > 0)
		{
			// $a_01 = substr($unit, 0, 2);
			// $a_02 = substr($unit, 2, 2);
			// $a_03 = substr($unit, 4, 2);
			// $a_04 = substr($unit, 6, 2);
			// $a_05 = substr($unit, 8, 2);

			$get = $this->db->query("SELECT a.id_presensi, b.opd, b.unit, a.*
						FROM  presensi a 
						INNER JOIN parent_presensi b ON a.id_parent = b.id 
						WHERE b.opd = '$opd' AND a.tahun_bulan = '$tb' ");

			#$data['presensi_data'] = $get;
			$data['keterangan'] = $this->Jam_model->get_all_data();
			$jam_kerja = $this->Jam_model->get_jam_kerja();
			$izin_data = $this->Presensi_model->get_izin($unit,$tahun,$bulan);
			$data['tahun'] = $tahun;
			$data['bulan'] = $bulan;

			if($get->num_rows() > 0)
			{
				$pgw = array();
				foreach($get->result() as $peg)
				{
					$pgw[] = $peg->nip;
				}
			}
			$user_info = $this->Presensi_model->get_userinfo($pgw);

			$params = array(
				'tahun' => $tahun
				, 'bulan' => $bulan
				, 'presensi_data' => $get
				, 'jam_kerja' => $jam_kerja
				, 'izin_data' => $izin_data
				, 'user_info' => $user_info
			);
			$data['presensi_data'] = $this->generate_data($params);
			//echo json_encode($data);

			$data['form_action'] = site_url('data_v3/presensi/save/'.$opd.'/'.$tahun.'/'.$bulan);
			
			$this->load->view('template/head');
			$this->load->view('template/header');
			$this->load->view('template/sidebar');
			$this->load->view('data_v3/presensi/presensi_list', $data);
		}
		else
		{
			$th = $this->input->post('tahun');
			if ($th == '') {
				$year = date('Y');
			} else {
				$year = $th;
			}

			$bl = $this->input->post('tahun');
			if ($th == '') {
				$month = date('Y');
			} else {
				$month = $bl;
			}
			$nipPns = $this->session->userdata('nip');
			$roleId = $this->session->userdata('role');
			$dataPns = getDataPns($nipPns);
			if ($roleId == 1 || $roleId == 2) {
				$A_01 = '';
			} else {
				$A_01 = $dataPns->A_01;
			}
			$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);
			$data['year'] = $year;
			$data['month'] = $month;
			$data['not_found'] = 'Data tidak ditemukan';
			$this->load->view('template/head');
			$this->load->view('template/header');
			$this->load->view('template/sidebar');
			$this->load->view('data_v3/presensi/presensi_index', $data);
		}
		

	}

	function displayDates($data, $start, $end) {
		$dates = [];
		foreach($data as $k => $v){
			$s = $data[$k]['checktime'];
			$e = isset($data[$k+1]['checktime']) ? $data[$k+1]['checktime'] : $data[$k]['checktime'];
			if($start <= $s && $end >= $e){
				$dates[] = $data[$k]['checktime'];
				goto akhir;
			}
		}
		akhir:
		return isset($dates[0]) ? $dates[0] : null;
	}

	public function generate_data($params){
		$users =[];
		foreach ($params['user_info']->result() as $user) {
			$userinfo[trim($user->street, ' ')] = $user->userid;
			$users[] = $user->userid;
		}
		
		#echo '<pre>';
		$wfh = $this->Presensi_model->get_all_wfh($users,$params['bulan'],$params['tahun']);
		$group_wfh =[];
		foreach($wfh as $k => $v){
			$group_wfh[$v['userid']][$v['checktime']][] = $v;
		}
		#print_r($group_wfh);
		$plg = $this->Presensi_model->get_all_pulang($users,$params['bulan'],$params['tahun']);
		$group_plg =[];
		foreach($plg as $k => $v){
			$group_plg[$v['userid']][] = $v;
		}
		#print_r($group_plg);
		$msk = $this->Presensi_model->get_all_masuk($users,$params['bulan'],$params['tahun']);
		$group_msk =[];
		foreach($msk as $k => $v){
			$group_msk[$v['userid']][] = $v;
		}
		#print_r($group_msk);
		#die();
		foreach ($params['jam_kerja'] as $jam) {
			$absen_buka[$jam->id] = $jam->absen_buka;
			$nama_singkat[$jam->id] = $jam->nama_singkat;
			$jadwal_masuk[$jam->id] = $jam->jadwal_masuk;
			$jadwal_tengah[$jam->id] = $jam->jadwal_tengah;
			$jadwal_keluar[$jam->id] = $jam->jadwal_keluar;
			$absen_tutup[$jam->id] = $jam->absen_tutup;
			$selisih_hari[$jam->id] = $jam->selisih_hari;
			$waktu_kerja[$jam->id] = $jam->waktu_kerja;
		} 
		foreach ($params['izin_data']->result() as $izin) {
			$arr_izin[$izin->nip][$izin->tanggal] = $izin->tipe;
		}
		$tahun = $params['tahun'];
		$bulan = $params['bulan'];
		$arr_data = [];
		foreach ($params['presensi_data']->result() as $presensi) {

			for($tgl=1;$tgl<=jumlah_hari($tahun, $bulan);$tgl++){
				$arr_data[$presensi->nip]['data'] = $presensi;
				$idx = str_pad($tgl, 2, '0', STR_PAD_LEFT);
				$j_index = 'j_'.$idx;
				$am_index = 'am_'.$idx;
				$ap_index = 'ap_'.$idx;

				if(!isset($arr_data[$presensi->nip]['date'][$tgl]['presensi'])){
					$arr_data[$presensi->nip]['date'][$tgl] = array(
						'presensi' => ''
						, 'masuk' => ''
						, 'pulang' => ''
						, 'periode' => ''
						, 'absen_buka' => ''
						, 'jadwal_masuk' => ''
						, 'jadwal_tengah' => ''
						, 'jadwal_keluar' => ''
						, 'absen_tutup' => ''
						, 'absen_masuk' => ''
						, 'absen_pulang' => ''
						, 'color' => ''
						, 'status' => ''
						, 'editable' => ''
						, 'waktu_kerja' => ''
					);
				}

				$arr_data[$presensi->nip]['date'][$tgl]['presensi'] = $presensi->$j_index;
				$arr_data[$presensi->nip]['date'][$tgl]['masuk'] = $presensi->$am_index;
				$arr_data[$presensi->nip]['date'][$tgl]['pulang'] = $presensi->$ap_index;
				if(!isset($arr_data[$presensi->nip]['kwk'])){
					$arr_data[$presensi->nip]['kwk'] = 0;
				}
				if(!isset($arr_data[$presensi->nip]['alpha'])){
					$arr_data[$presensi->nip]['alpha'] = 0;
				}

				$tanggal = sprintf("%02d", $tgl);
				$jadwal = $arr_data[$presensi->nip]['date'][$tgl]['presensi'];
				$arr_data[$presensi->nip]['date'][$tgl]['waktu_kerja'] = $waktu_kerja[$jadwal];
				$arr_data[$presensi->nip]['date'][$tgl]['nama_singkat'] = $nama_singkat[$jadwal];

				if ($arr_data[$presensi->nip]['date'][$tgl]['waktu_kerja'] > 0) {
					$arr_data[$presensi->nip]['date'][$tgl]['periode'] = $tahun . '-' . $bulan . '-' . $tanggal;

					if (!empty($arr_izin[$presensi->nip][$tahun . '-' . $bulan . '-' . $tanggal])) {
						if (($arr_izin[$presensi->nip][$tahun . '-' . $bulan . '-' . $tanggal] == 'WFH')) # || ($arr_izin[$presensi->nip][$tahun.'-'.$bulan.'-'.$tanggal] == 'TL')  ==> kalo mau tambahan TL 
						{
							#if ($this->presensi_model->get_check_wfh($userinfo[$presensi->nip], $tahun . '-' . $bulan . '-' . $tanggal) > 0) {
							
								if(isset($group_wfh[$userinfo[$presensi->nip]][$tahun . '-' . $bulan . '-' . $tanggal])){
								if (count($group_wfh[$userinfo[$presensi->nip]][$tahun . '-' . $bulan . '-' . $tanggal]) > 0) {
									//jika absen
								$arr_data[$presensi->nip]['date'][$tgl]['status'] = $arr_izin[$presensi->nip][$tahun . '-' . $bulan . '-' . $tanggal];
								$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'green';
								}
							} else {
								//jika tidak absen
								$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'A'; //'('.'A'.')'
								$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'red';
								$arr_data[$presensi->nip]['alpha'] += 1;
							}
						} else {
							$arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] = ''; //tampilkan tanda - di db
							$arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] = '';
							$arr_data[$presensi->nip]['date'][$tgl]['status'] = $arr_izin[$presensi->nip][$tahun . '-' . $bulan . '-' . $tanggal];
							$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'green';
						}


						// Aktifkan WFH Aplha quote atas hapus.. bawah gantian di quote cuy...
						// quote atas mulai sebelum if( ($arr_izin....

					} else {

						$arr_data[$presensi->nip]['date'][$tgl]['absen_buka'] = $tahun . '-' . $bulan . '-' . $tanggal . ' ' . $absen_buka[$jadwal] . ':00';
						$arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk'] = $tahun . '-' . $bulan . '-' . $tanggal . ' ' . $jadwal_masuk[$jadwal] . ':00';
						$arr_data[$presensi->nip]['date'][$tgl]['jadwal_tengah'] = $tahun . '-' . $bulan . '-' . $tanggal . ' ' . $jadwal_tengah[$jadwal] . ':00';

						$arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'] = date('Y-m-d H:i:s', strtotime($tahun . '-' . $bulan . '-' . $tanggal . ' ' . $jadwal_keluar[$jadwal] . ':00 +' . $selisih_hari[$jadwal] . ' day'));
						$arr_data[$presensi->nip]['date'][$tgl]['absen_tutup'] = date('Y-m-d H:i:s', strtotime($tahun . '-' . $bulan . '-' . $tanggal . ' ' . $absen_tutup[$jadwal] . ':00 +' . $selisih_hari[$jadwal] . ' day'));

						#$arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] = $this->Presensi_model->get_masuk($userinfo[$presensi->nip], $arr_data[$presensi->nip]['date'][$tgl]['absen_buka'], $arr_data[$presensi->nip]['date'][$tgl]['jadwal_tengah']);
						#$arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] = $this->Presensi_model->get_pulang($userinfo[$presensi->nip], $arr_data[$presensi->nip]['date'][$tgl]['jadwal_tengah'], $arr_data[$presensi->nip]['date'][$tgl]['absen_tutup']);
						$arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] = isset($group_msk[$userinfo[$presensi->nip]]) ? $this->displayDates($group_msk[$userinfo[$presensi->nip]], $arr_data[$presensi->nip]['date'][$tgl]['absen_buka'], $arr_data[$presensi->nip]['date'][$tgl]['jadwal_tengah']) : null;
						$arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] = isset($group_plg[$userinfo[$presensi->nip]]) ? $this->displayDates($group_plg[$userinfo[$presensi->nip]], $arr_data[$presensi->nip]['date'][$tgl]['jadwal_tengah'], $arr_data[$presensi->nip]['date'][$tgl]['absen_tutup']) : null;

						if (strlen($arr_data[$presensi->nip]['date'][$tgl]['masuk']) > 0) {
							$arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] = $arr_data[$presensi->nip]['date'][$tgl]['masuk'];
						}

						if (strlen($arr_data[$presensi->nip]['date'][$tgl]['pulang']) > 0) {
							$arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] = $arr_data[$presensi->nip]['date'][$tgl]['pulang'];
						}

						if ((strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) == 0) && (strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang']) == 0)) {
							//alpha
							$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'red';
							$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'A';
							$arr_data[$presensi->nip]['alpha'] += 1;
							$arr_data[$presensi->nip]['date'][$tgl]['editable'] = TRUE;
						} else {
							if ((strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) >= 0) && (strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang']) >= 0)) {
								if (($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] <= $arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk']) && ($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] >= $arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'])) {
									//hadir
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'black';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'H';
								}
								if (($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] > $arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk']) && ($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] > $arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'])) {
									//terlambat
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'T';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang']  = floor((strtotime($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) - strtotime($arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk'])) / 60);
								}
								if (($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] < $arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk']) && ($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] < $arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'])) {
									//pulang awal
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'P';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang']  = floor((strtotime($arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar']) - strtotime($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'])) / 60);
								}
								if (($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] > $arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk']) && ($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] < $arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'])) {
									//terlambat dan pulang awal
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'TP';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang_awal']  = floor((strtotime($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) - strtotime($arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk'])) / 60);
									$arr_data[$presensi->nip]['date'][$tgl]['kurang_akhir']  = floor((strtotime($arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar']) - strtotime($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'])) / 60);
									$arr_data[$presensi->nip]['date'][$tgl]['kurang'] = $arr_data[$presensi->nip]['date'][$tgl]['kurang_awal'] + $arr_data[$presensi->nip]['date'][$tgl]['kurang_akhir'];
									$arr_data[$presensi->nip]['kwk'] = $arr_data[$presensi->nip]['date'][$tgl]['kurang'];
								}
							}
							if ((strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) == 0) && (strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang']) >= 0)) {
								$arr_data[$presensi->nip]['date'][$tgl]['editable'] = TRUE;
								if ($arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'] <= $arr_data[$presensi->nip]['date'][$tgl]['absen_pulang']) {
									//tidak absen masuk
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'TAM';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang'] = floor($waktu_kerja[$jadwal] / 2);
								}
								if ($arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'] >= $arr_data[$presensi->nip]['date'][$tgl]['absen_pulang']) {
									//tidak absen masuk dan pulang cepat
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'TAMP';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang_awal']  = floor($waktu_kerja[$jadwal] / 2);
									$arr_data[$presensi->nip]['date'][$tgl]['kurang_akhir']  = floor((strtotime($arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar']) - strtotime($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'])) / 60);
									$arr_data[$presensi->nip]['date'][$tgl]['kurang'] = $arr_data[$presensi->nip]['date'][$tgl]['kurang_awal'] + $arr_data[$presensi->nip]['date'][$tgl]['kurang_akhir'];
									$arr_data[$presensi->nip]['kwk'] = $arr_data[$presensi->nip]['date'][$tgl]['kurang'];
								}
							}

							if ((strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) >= 0) && (strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang']) == 0)) {
								$arr_data[$presensi->nip]['date'][$tgl]['editable'] = TRUE;
								if ($arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk'] >= $arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) {
									//tidak absen pulang
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'TAP';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang'] = floor($waktu_kerja[$jadwal] / 2);
								}
								if ($arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk'] <= $arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) {
									//tidak absen pulang dan terlambat
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'TAPT';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang_awal']  = floor((strtotime($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) - strtotime($arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk'])) / 60);
									$arr_data[$presensi->nip]['date'][$tgl]['kurang_akhir']  = floor($waktu_kerja[$jadwal] / 2);
									$arr_data[$presensi->nip]['date'][$tgl]['kurang'] = $arr_data[$presensi->nip]['date'][$tgl]['kurang_awal'] + $arr_data[$presensi->nip]['date'][$tgl]['kurang_akhir'];
									$arr_data[$presensi->nip]['kwk'] = $arr_data[$presensi->nip]['date'][$tgl]['kurang'];
								}
							}
						}
					}
				}
			}
		}
		
		#echo '<pre>';
		#print_r($arr_data);die();

		return $arr_data;
	}

	public function set_jadwal_awal()
	{
		$opd = $this->input->post('opd');
		$unit = $this->input->post('unit');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$tb = $tahun . $bulan;

		$a_01 = substr($unit, 0, 2);
		$a_02 = substr($unit, 2, 2);
		$a_03 = substr($unit, 4, 2);
		$a_04 = substr($unit, 6, 2);
		$a_05 = substr($unit, 8, 2);

		$cek = $this->Jadwal_model->count_presensi_by_tahun_bulan($opd, $unit, $bulan, $tahun);
		// echo json_encode($cek);
		if ($cek->hit > 0) {
			$get = $this->db->query("SELECT a.id_presensi, b.opd, b.unit, a.*
					   FROM  presensi a 
					   INNER JOIN parent_presensi b ON a.id_parent = b.id 
					   WHERE b.opd = '$opd' AND a.tahun_bulan = '$tb'")->result();
			$status = 'add';
			$status = 'update';
		} else {

			$nip_sudah = $this->Jadwal_model->sudah_dijadwal($opd, $tahun, $bulan);
			if ($nip_sudah) {
				foreach ($nip_sudah as $sd) {
					$array_sudah = array(
						$sd->nip,
					);
				}
			} else {

				$array_sudah = array(
					1
				);
			}

			// echo json_encode($id_sudah);
			// echo json_encode($sudah);
			if ($unit == '') {
				$a_01 = substr($opd, 0, 2);
				// $gets = $this->Jadwal_model->get_pegawai_opd($a_01);
				$gets = $this->Jadwal_model->get_pegawai_opd_v2($a_01, $array_sudah);
			} else {
				// $gets = $this->Jadwal_model->get_pegawai_unit($a_01, $a_02, $a_03, $a_04, $a_05);
				$gets = $this->Jadwal_model->get_pegawai_unit_v2($a_01, $a_02, $a_03, $a_04, $a_05, $array_sudah);
			}

			$generate_format = array(
				'opd' => $opd,
				'unit' => $unit,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'generate_by' => $this->nipPns,
				'created_at' => date('Y-m-d H:i:s'),

			);
			$add_parent = $this->Jadwal_model->insert_parent_data($generate_format);
			if ($add_parent != FALSE) {
				$data = array();
				foreach ($gets as $pegawai) {
					$data[] = array(
						'tahun_bulan' => $tahun . $bulan,
						'id'		  => $tahun . $bulan . '_' . $pegawai->nip,
						'id_parent'   => $add_parent,
						'nip' 		  => $pegawai->nip,
						'nama_lengkap' => $pegawai->nama_lengkap,
						'created_at'  => date('Y-m-d H:i:s'),
						'created_by'  => $this->nipPns,
					);
				}

				// echo json_encode($data);

				$this->Jadwal_model->insert_batch_jadwal($data);
				$get = $this->db->query("SELECT a.id_presensi, b.opd, b.unit, a.*
					   FROM  presensi a 
					   INNER JOIN parent_presensi b ON a.id_parent = b.id 
					   WHERE b.opd = '$opd' AND a.tahun_bulan = '$tb'")->result();
				$status = 'add';
				//echo json_encode($data);
			}
		}

		$data['data_pegawai'] = $get;
		$data['status'] = $status;
		$data['keterangan'] = $this->Jam_model->get_all_data();
		$data['jamkerja'] = $this->Jam_model->get_jam_kerja();
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/kerja/kerja_add', $data);
	}
}
