<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fungsi extends CI_Controller {

    public function __construct(){
		parent::__construct();
    $this->dbeps = $this->load->database('eps', TRUE);
		//  is_login();
	}

  public function cari_pns($A_01){
    $nip = $this->input->get('nipnya');
    $roleId = $this->session->userdata('role');
    
    if ($roleId == 1 || $roleId == 2) {
      $data = $this->Fungsi_model->getDataPns($nip);
		} else {
      $data = $this->Fungsi_model->getDataPns_skpd($nip, $A_01);
		}
    
    header('Content-Type: application/json');
    echo json_encode($data);
    }

    public function unit_kerja(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $dinas = $this->input->post('opd');
        $opd = substr($dinas,0,2);
        // $dinas = 'B2';
        
        // $lokasis = $this->dbeps->query("SELECT KOLOK, NALOK from TABLOKB08 WHERE A_01 = '$dinas' AND aktif = 1 AND KOLOK <> '$opd'")->result();
        // $lokasis = $this->dbeps->query("SELECT KOLOK, NALOK from TABLOKB08 WHERE A_01 = '$dinas' AND aktif = 1 ")->result();
        $this->dbeps->select("NALOKP, KOLOK");
        $this->dbeps->where('A_01',$opd);
        $this->dbeps->where('AKTIF',1);
        if($opd == 'A2')
        {
          $this->dbeps->select('CONCAT(A_01,A_02,A_03) as kode,NALOKP, KOLOK');
          $this->dbeps->WHERE('ESEL','22');
        }

        
        elseif($opd == 'D0')
        {
          // $this->dbeps->select("IF(A_04<'40',CONCAT(A_01,A_02,A_03),CONCAT(A_01,A_02,A_03,A_04)) as kode,NALOKP, KOLOK");
          $this->dbeps->select("NALOKP, KOLOK");
          $in_array = array('31','32','88');
          $this->dbeps->where_in('ESEL',$in_array);
        }
        elseif($opd == '86')
        {
          // $this->dbeps->select('CONCAT(A_01,A_02,A_03,A_04) as kode,NALOKP,KOLOK');
          $in_array = array('31','41','88');
          $this->dbeps->where_in('ESEL',$in_array);
        }
        else
        {
          // $this->dbeps->select('CONCAT(A_01,A_02,A_03) as kode,NALOKP, KOLOK');
          $in_array = array('22','31','32');
          // $this->dbeps->where_in('ESEL',$in_array);
          $this->dbeps->where('(ESEL="22" or ESEL="31" or ESEL="32" or (ESEL="41" and KOLOK like "%000000") or KOLOK like "%00000000")');
        }
        $lokasis = $this->dbeps->get("TABLOKB08")->result();

        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = '<option value="">Silakan Pilih Unit Kerja</option>';
        
        foreach($lokasis as $loks){
          $lists .= "<option value='".$loks->KOLOK."'>".$loks->NALOKP."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_lokasis'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }

    public function contoh(){
      // $this->DB_simpeg->where('A_01',$unit_kerja);
			// if($unit_kerja == 'A2')
			// {
			// 	$this->DB_simpeg->select('CONCAT(A_01,A_02,A_03) as kode,NALOKP');
			// 	$this->DB_simpeg->WHERE('ESEL','22');
			// }
			// elseif($unit_kerja == 'D0')
			// {
			// 	$this->DB_simpeg->select("IF(A_04<'40',CONCAT(A_01,A_02,A_03),CONCAT(A_01,A_02,A_03,A_04)) as kode,NALOKP");
			// 	$in_array = array('31','32','88');
			// 	$this->DB_simpeg->where_in('ESEL',$in_array);
			// }
			// elseif($unit_kerja == '86')
			// {
			// 	$this->DB_simpeg->select('CONCAT(A_01,A_02,A_03,A_04) as kode,NALOKP');
			// 	$in_array = array('31','41','88');
			// 	$this->DB_simpeg->where_in('ESEL',$in_array);
			// }
			// else
			// {
			// 	$this->DB_simpeg->select('CONCAT(A_01,A_02,A_03) as kode,NALOKP');
			// 	$in_array = array('22','31','32');
			// 	//$this->DB_simpeg->where_in('ESEL',$in_array);
			// 	$this->DB_simpeg->where('(ESEL="22" or ESEL="31" or ESEL="32" or (ESEL="41" and KOLOK like "%000000") or KOLOK like "%00000000")');
			// }
    }



}