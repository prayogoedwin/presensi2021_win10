<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Izin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// $this->authentification->check_user_authentification();

		$this->load->model('Izin_model');
		$this->load->model('Fungsi_model');

		$this->nipPns = $this->session->userdata('nip');
		$this->roleId = $this->session->userdata('role');
		$this->dataPns = getDataPns($this->nipPns);
	}

	public function index()
	{
		redirect('dashboard');
	}

	public function data()
	{

		$dataPns = getDataPns($this->nipPns);
		if ($this->roleId == 1 || $this->roleId == 2) {
			$A_01 = '';
		} else {
			$A_01 = $dataPns->A_01;
		}
		$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);
		$data['cari'] = 0;
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('izin/data/data_index', $data);
	}

	// public function data_cari()
	// {
	//     $dataPns = getDataPns($this->nipPns);
	// 	if ($this->roleId == 1 || $this->roleId == 2) {
	// 		$A_01 = '';
	// 	} else {
	// 		$A_01 = $dataPns->A_01;
	// 	}
	// 	$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);

	//     $opd = $this->session->userdata('opd');
	// 	$unit = $this->session->userdata('unit');
	//     $data['data_izin'] = $this->Izin_model->get_all_by($opd, $unit)->result();
	//     $data['cari'] = 1;
	// 	$this->load->view('template/head');
	// 	$this->load->view('template/header');
	// 	$this->load->view('template/sidebar');
	// 	$this->load->view('izin/data/data_index', $data);
	// }

	public function data_cari()
	{
		$opd = $this->session->userdata('opd');
		$unit = $this->session->userdata('unit');

		// $a_01_opd = substr($opd, 0, 2);

		$data['opd'] = $opd;
		$data['unit'] = $unit;
		$dataPns = getDataPns($this->nipPns);
		if ($this->roleId == 1 || $this->roleId == 2) {
			$A_01 = '';
		} else {
			$A_01 = $dataPns->A_01;
		}

		$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);
		$data['allpns'] = $this->Fungsi_model->get_pns_list($dataPns->A_01);


		// <a  href="' . base_url('setting/privilage_edit/') . $value->id . '" type="button" class="btn bg-primary btn-icon rounded-round"><i class="fa fa-edit"></i></a> &nbsp;

		$get = $this->Izin_model->get_all_by($opd, $unit)->result();
		$data['data_tipe'] = $this->Izin_model->get_all_tipe(3)->result();
		$datatable = array();
		foreach ($get as $key => $value) {
			$action =
				'<div class="row">
                
                <a type="button" class="btn bg-primary btn-icon rounded-round" href="javascript:;"
				data-id="' . $value->id . '"
				data-tipe="' . $value->tipe . '"
				data-tanggal_awal="' . $value->tanggal_awal . '"
				data-tanggal_akhir="' . $value->tanggal_akhir . '"
				data-status="' . $value->status . '"
				data-deskripsi="' . $value->deskripsi . '""
				data-toggle="modal" data-target="#edit-data"
				>
				<i class="fa fa-edit"></i>
				</a>&nbsp;
				
				<a  onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');" href="' . base_url('izin/hapus_izin/') . encode_url($value->id) . '" type="button" class="btn bg-warning btn-icon rounded-round"><i class="fa fa-trash"></i></a>
        
                </div>';


			if ($value->status == 1) {
				$status = "<span class='badge badge-success'>Disetujui</span>";
			} else if ($value->status == 2) {
				$status = "<span class='badge badge-danger'>Ditolak</span>";
			} else {
				$status = "<span class='badge badge-info'>Menunggu</span>";
			}


			$datatable[$key] = array(
				'nama'            => $value->nama,
				'nip'             => $value->nip,
				'tipe'            => $value->tipe,
				'tanggal'         => $value->tanggal_awal . ' <b>s/d</b> ' . $value->tanggal_akhir,
				'description'     => string_add($value->deskripsi, '-<br/>', 45, 0),
				'status'          => $status,
				'action'          => $action
			);
		}
		$data['datatable'] = $datatable;
		$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('izin/data/data_cari', $data);
	}

	public function edit_izin()
	{
		// $dataPns = getDataPns($this->nipPns);
		$nip = $this->nipPns;
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		$catatan = $this->input->post('catatan');

		$data = array(
			'status' => $status,
			'unit_kerja' => $catatan,
			'validation_user' => $nip,
			'validation_datetime' => date('Y-m-d H:i:s'),
		);

		$update = $this->Izin_model->update_data($id, $data);
		if ($update) {
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('message', 'Berhasil Update data izin');
		} else {
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('message', 'Gagal Update data izin');
		}

		redirect('izin/data_cari');
	}


	public function saya()
	{
		// <a  href="' . base_url('setting/privilage_edit/') . $value->id . '" type="button" class="btn bg-primary btn-icon rounded-round"><i class="fa fa-edit"></i></a> &nbsp;
		$get = $this->Izin_model->get_all_saya($this->nipPns)->result();
		$data['data_tipe'] = $this->Izin_model->get_all_tipe(3)->result();
		$datatable = array();
		foreach ($get as $key => $value) {
			$action =
				'<div class="row">

				<a type="button" class="btn bg-primary btn-icon rounded-round" href="javascript:;"
				data-id="' . $value->id . '"
				data-tipe="' . $value->tipe . '"
				data-tanggal_awal="' . $value->tanggal_awal . '"
				data-tanggal_akhir="' . $value->tanggal_akhir . '"
				data-status="' . $value->status . '"
				data-deskripsi="' . $value->deskripsi . '""
				data-toggle="modal" data-target="#edit-data"
				>
				<i class="fa fa-edit"></i>
				</a>&nbsp;
                
                <a  onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');" href="' . base_url('setting/privilage_deleted/') . encode_url($value->id) . '" type="button" class="btn bg-warning btn-icon rounded-round"><i class="fa fa-trash"></i></a>
        
                </div>';


			if ($value->status == 1) {
				$status = "<span class='badge badge-success'>Disetujui</span>";
			} else if ($value->status == 2) {
				$status = "<span class='badge badge-danger'>Ditolak</span>";
			} else {
				$status = "<span class='badge badge-info'>Menunggu</span>";
			}


			$datatable[$key] = array(
				'tipe'            => $value->tipe,
				'tanggal'         => $value->tanggal_awal . ' <b>s/d</b> ' . $value->tanggal_akhir,
				'description'     => string_add($value->deskripsi, '-<br/>', 45, 0),
				'status'          => $status,
				'action'          => $action
			);
		}
		$data['datatable'] = $datatable;

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('izin/saya/saya_index', $data);
	}

	public function add_izin()
	{
		$dataPns = getDataPns($this->nipPns);
		$nip = $this->nipPns;
		$unitKerja = $dataPns->A_01 . $dataPns->A_02 . $dataPns->A_03 . $dataPns->A_04 . $dataPns->A_05;
		$tipe = $this->input->post('tipe');
		$awal = $this->input->post('awal');
		$akhir = $this->input->post('akhir');
		$keterangan = $this->input->post('keterangan');

		$tanggal_h = getDatesFromRange($awal, $akhir, 'implode');
		$tanggal_d = getDatesFromRange($awal, $akhir, 'array');
		$parent = id_izin($nip, $awal);

		$data = array(
			'id' => $parent,
			'nip' => $nip,
			'unit_kerja' => $unitKerja,
			'tipe'  => $tipe,
			'deskripsi' => $keterangan,
			'tanggal_awal' => $awal,
			'tanggal_akhir' => $akhir,
			'create_user' => $nip,
			'create_datetime' => date('Y-m-d H:i:s'),
		);

		if (count($tanggal_d) > 0) {
			$this->Izin_model->insert_data($data);
			$this->Izin_model->delete_data_detail($parent);

			$a = 0;
			foreach ($tanggal_d as $tgl) {
				$a++;
				$id = $parent . '_' . sprintf("%02d", $a);
				$this->Izin_model->insert_detail($id, $parent, $tgl);
			}
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('message', 'Tambah Izin Baru Berhasil');
		} else {
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('message', 'Tambah Izin Baru Gagal');
		}
		redirect('izin/saya');
	}

	

	public function add_izin_bulk()
	{
		$tanggal_awal = $this->input->post('awal');
		$tanggal_akhir = $this->input->post('akhir');

		$nip = $this->input->post('pns');

		$tanggal_h = getDatesFromRange($tanggal_awal, $tanggal_akhir, 'implode');
		$tanggal_d = getDatesFromRange($tanggal_awal, $tanggal_akhir, 'array');


		if (count($tanggal_d) > 0) {
			foreach ($nip as $a => $b) {
				$profile_pegawai = $this->Izin_model->get_pegawai_nip($nip[$a])->row_array();
				$unit_kerja = $profile_pegawai['unit_kerja'];
				$parent = id_izin($nip[$a], $tanggal_awal);

				$this->Izin_model->fill_data2('insert', $parent, $nip[$a], $unit_kerja, $tanggal_h);
				$this->Izin_model->insert_data2();
				$this->Izin_model->delete_data_detail($parent);

				$k = 0;
				foreach ($tanggal_d as $tgl) {
					$k++;
					$id = $parent . '_' . sprintf("%02d", $k);
					$this->Izin_model->insert_detail($id, $parent, $tgl);
				}
			}
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('message', 'Tambah Izin Baru Berhasil');
		} else {
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('message', 'Tambah Izin Baru Gagal');
		}
		redirect('izin/data_cari');
	}

	public function hapus_izin($ide){
		$id = decode_url($ide);
		$izin_data = $this->Izin_model->get_data_by($id);
		if($izin_data){
			$izin = $izin_data->row_array();
			$this->Izin_model->delete_data($id);
			$this->Izin_model->delete_data_detail($id);

			$data = array(
				'id_lama' =>  $izin['id'],
				'nip'  =>  $izin['nip'],
				'unit_kerja' =>  $izin['unit_kerja'],
				'tipe' => $izin['tipe'],
				'deskripsi' =>  $izin['deskripsi'],
				'lampiran' =>  $izin['lampiran'],
				'tanggal_awal' =>  $izin['tanggal_awal'],
				'tanggal_akhir' =>  $izin['tanggal_akhir'],
				'tanggal' =>  $izin['tanggal'],
				'create_datetime' => $izin['create_datetime'],
				'create_user' => $izin['create_user'],
				'status' => $izin['status'],
				'catatan' => $izin['catatan'],
				'no_surat' => $izin['no_surat'],
				'created_by' => $this->nipPns,
				'created_at' => date('Y-m-d H:i:s'),
				'aksi' => 'Dihapus'
			);
			$this->Izin_model->insert_log($data);
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('message', 'Hapus Izin Berhasil');

		}else{

			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('message', 'Hapus Izin Gagal');

		}

		redirect('izin/data_cari');
		
		

	}
}
