<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// $this->authentification->check_user_authentification();

		$this->load->model('Libur_model');
		$this->load->model('Jam_model');
		$this->load->model('Jadwal_model');

		$this->nipPns = $this->session->userdata('nip');
		$this->roleId = $this->session->userdata('role');
		$this->dataPns = getDataPns($this->nipPns);
	}

	public function index()
	{
		redirect('dashboard');
	}

	function sample()
	{

		$a = $this->db->query("SELECT id, nama_singkat FROM jam_kerja ORDER BY id ")->result();
		foreach ($a as $d) {
			$data[] = array(
				"id" => "$d->nama_singkat"
			);
		}
		echo json_encode($data);
	}



	public function libur_asli()
	{
		if ($this->uri->segment(3) > 0) {
			$year = $data['year'] = $this->uri->segment(3);
			if ($this->uri->segment(4) > 0) {
				$month = $data['month'] = $this->uri->segment(4);
			} else {
				$month = $data['month'] = date("m");
			}
		} else {
			$year = $data['year'] = date("Y");
			$month = $data['month'] = date("m");
		}

		$data['title'] = "Hari Libur " . namaBulan($month) . ' ' . $year;

		$prefs['template'] = '

			{table_open}
            <div class="table-responsive">
            <table class="table">{/table_open}

			{heading_row_start}<tr class="cyan">{/heading_row_start}

			{heading_previous_cell}<th class="text-center"><a href="{previous_url}" class="link white-text"><i class="fa fa-chevron-left"></i></a></th>{/heading_previous_cell}
			{heading_title_cell}<th colspan="{colspan}" class="text-center"><h5 class="white-text">{heading}</h5></th>{/heading_title_cell}
			{heading_next_cell}<th class="text-center"><a href="{next_url}" class="link white-text"><i class="fa fa-chevron-right"></i></i></a></th>{/heading_next_cell}

			{heading_row_end}</tr>{/heading_row_end}

			{week_row_start}<tr>{/week_row_start}
			{week_day_cell}<td>{week_day}</td>{/week_day_cell}
			{week_row_end}</tr>{/week_row_end}

			{cal_row_start}<tr>{/cal_row_start}
			{cal_cell_start}<td>{/cal_cell_start}
			{cal_cell_start_today}<td>{/cal_cell_start_today}
			{cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

			{cal_cell_content}<a onclick="ubah({day})" data-position="bottom" data-delay="50" data-popup="tooltip" data-placement="bottom" title="{content}" class="btn btn-danger " href="javascript:void(0)">{day}</a>{/cal_cell_content}
			{cal_cell_content_today}<a onclick="ubah({day})" data-position="bottom" data-delay="50" data-popup="tooltip" data-placement="bottom" title="{content}" class="btn btn-warning " href="javascript:void(0)">{day}</a>{/cal_cell_content_today}

			{cal_cell_no_content}<a href="javascript:void(0)" onclick="tambah({day})" class="black-text btn waves-effect waves-light white" href="javascript:void(0)">{day}</a>{/cal_cell_no_content}
			{cal_cell_no_content_today}<a onclick="tambah({day})" data-position="bottom" data-delay="50" data-popup="tooltip" data-placement="bottom" title="Hari Ini" class="btn btn-primary" href="javascript:void(0)">{day}</a>{/cal_cell_no_content_today}

			{cal_cell_blank}&nbsp;{/cal_cell_blank}

			{cal_cell_other}<div class="grey-text text-lighten-3">{day}</div>{/cal_cel_other}

			{cal_cell_end}</td>{/cal_cell_end}
			{cal_cell_end_today}</td>{/cal_cell_end_today}
			{cal_cell_end_other}</td>{/cal_cell_end_other}
			{cal_row_end}</tr>{/cal_row_end}

			{table_close}</table></div>{/table_close}
			';

		$prefs['show_next_prev'] = TRUE;
		$prefs['day_type'] = 'long';
		$prefs['next_prev_url'] = site_url('jadwal/libur');
		$prefs['start_day'] = 'sunday';
		$prefs['show_other_days'] = FALSE;



		$data['table_data'] = site_url('jadwal/libur/data');

		$agenda_data = $data['agenda_data'] = $this->Libur_model->get_libur($year, $month);
		$data['calendar'] = array();
		$items = array();
		foreach ($agenda_data->result() as $row) {
			$items[$row->day] = $row->nama;
		}
		$data["calendar"] = $items;

		$this->load->library('calendar', $prefs);

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/libur/libur_index', $data);
	}

	public function libur($action = '')
	{
		$data['group_module'] = $group_module = $this->uri->segment(1);
		$data['module'] = $module = $this->uri->segment(2);
		$data['act'] = $act = $this->uri->segment(3);

		switch ($action) {
			case "add":
				$data['tanggal_awal'] = $tanggal_awal = $this->uri->segment(4);
				$data['tanggal_akhir'] = $tanggal_akhir = $this->uri->segment(4);
				$data['title'] = "Tambah Hari Libur";
				$data['form_action'] = site_url($group_module . '/' . $module . '/insert');
				$data['form_data'] = false;
				$this->load->view('jadwal/libur/form', $data);

				break;
			default:
				if ($this->uri->segment(3) > 0) {
					$year = $data['year'] = $this->uri->segment(3);
					if ($this->uri->segment(4) > 0) {
						$month = $data['month'] = $this->uri->segment(4);
					} else {
						$month = $data['month'] = date("m");
					}
				} else {
					$year = $data['year'] = date("Y");
					$month = $data['month'] = date("m");
				}

				$data['title'] = "Hari Libur " . namaBulan($month) . ' ' . $year;

				$prefs['template'] = '

				{table_open}
				<div class="table-responsive">
				<table class="table">{/table_open}

				{heading_row_start}<tr class="cyan">{/heading_row_start}

				{heading_previous_cell}<th class="text-center"><a href="{previous_url}" class="link white-text"><i class="fa fa-chevron-left"></i></a></th>{/heading_previous_cell}
				{heading_title_cell}<th colspan="{colspan}" class="text-center"><h5 class="white-text">{heading}</h5></th>{/heading_title_cell}
				{heading_next_cell}<th class="text-center"><a href="{next_url}" class="link white-text"><i class="fa fa-chevron-right"></i></i></a></th>{/heading_next_cell}

				{heading_row_end}</tr>{/heading_row_end}

				{week_row_start}<tr>{/week_row_start}
				{week_day_cell}<td>{week_day}</td>{/week_day_cell}
				{week_row_end}</tr>{/week_row_end}

				{cal_row_start}<tr>{/cal_row_start}
				{cal_cell_start}<td>{/cal_cell_start}
				{cal_cell_start_today}<td>{/cal_cell_start_today}
				{cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

				{cal_cell_content}<a onclick="ubah({day})" data-position="bottom" data-delay="50" data-popup="tooltip" data-placement="bottom" title="{content}" class="btn btn-danger " href="javascript:void(0)">{day}</a>{/cal_cell_content}
				{cal_cell_content_today}<a onclick="ubah({day})" data-position="bottom" data-delay="50" data-popup="tooltip" data-placement="bottom" title="{content}" class="btn btn-warning " href="javascript:void(0)">{day}</a>{/cal_cell_content_today}

				{cal_cell_no_content}<a href="javascript:void(0)" onclick="tambah({day})" class="black-text btn waves-effect waves-light white" href="javascript:void(0)">{day}</a>{/cal_cell_no_content}
				{cal_cell_no_content_today}<a onclick="tambah({day})" data-position="bottom" data-delay="50" data-popup="tooltip" data-placement="bottom" title="Hari Ini" class="btn btn-primary" href="javascript:void(0)">{day}</a>{/cal_cell_no_content_today}

				{cal_cell_blank}&nbsp;{/cal_cell_blank}

				{cal_cell_other}<div class="grey-text text-lighten-3">{day}</div>{/cal_cel_other}

				{cal_cell_end}</td>{/cal_cell_end}
				{cal_cell_end_today}</td>{/cal_cell_end_today}
				{cal_cell_end_other}</td>{/cal_cell_end_other}
				{cal_row_end}</tr>{/cal_row_end}

				{table_close}</table></div>{/table_close}
				';

				$prefs['show_next_prev'] = TRUE;
				$prefs['day_type'] = 'long';
				$prefs['next_prev_url'] = site_url('jadwal/libur');
				$prefs['start_day'] = 'sunday';
				$prefs['show_other_days'] = FALSE;



				$data['table_data'] = site_url('jadwal/libur/data');

				$agenda_data = $data['agenda_data'] = $this->Libur_model->get_libur($year, $month);
				$data['calendar'] = array();
				$items = array();
				foreach ($agenda_data->result() as $row) {
					$items[$row->day] = $row->nama;
				}
				$data["calendar"] = $items;

				$this->load->library('calendar', $prefs);

				$this->load->view('template/head');
				$this->load->view('template/header');
				$this->load->view('template/sidebar');
				$this->load->view('jadwal/libur/libur_index', $data);
				break;
		}
	}



	public function jam()
	{


		$get = $this->Jam_model->get_all_data();
		$datatable = array();
		foreach ($get as $key => $value) {
			$action =
				'<div class="row">
                
                <a  href="' . base_url('setting/privilage_edit/') . $value->id . '" type="button" class="btn bg-primary btn-icon rounded-round"><i class="fa fa-edit"></i></a> &nbsp;
                <a  onclick="return confirm(\'Are you sure you want to delete this item?\');" href="' . base_url('setting/privilage_deleted/') . $value->id . '" type="button" class="btn bg-warning btn-icon rounded-round"><i class="fa fa-trash"></i></a>
        
                </div>';


			if ($value->status == 1) {
				$status = "<span class='badge badge-success'>Aktif</span>";
			} else {
				$status = "<span class='badge badge-danger'>Tidak Aktif</span>";
			}

			$name = "(" . $value->nama_singkat . ") " . $value->nama;
			$jadwal = $value->jadwal_masuk . " - " . $value->jadwal_keluar;



			$datatable[$key] = array(
				'name'            => $name,
				'schedule'        => $jadwal,
				'description'     => string_add($value->deskripsi, '-<br/>', 45, 0),
				'status'          => $status,
				'action'          => $action
			);
		}
		$data['datatable'] = $datatable;

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/jam/jam_index', $data);
	}

	public function saya()
	{
		if ($this->uri->segment(3) > 0) {
			$year = $this->uri->segment(3);
			$data['year'] = $year;
			if ($this->uri->segment(4) > 0) {
				$month = $this->uri->segment(4);
				$data['month'] = $month;
			} else {
				$month = date("m");
				$data['month'] = $month;
			}
		} else {
			$year = date("Y");
			$month =  date("m");

			$data['year'] = $year;
			$data['month'] = $month;
		}
		$nip = $this->authentification->get_user('nip');
		$tb_nip = $year . $month . '_' . $nip;

		$data['title'] = "Hari Libur " . namaBulan($month) . ' ' . $year;
		$data['nip'] = $nip;
		$data['jadwal_data'] = $this->Jadwal_model->get_jadwal_perorangan($tb_nip);
		$data['jam_kerja'] = $this->Jam_model->get_jam_kerja();

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/saya/saya_index', $data);
	}


	public function kerja()
	{
		$th = $this->input->post('tahun');
		if ($th == '') {
			$year = date('Y');
		} else {
			$year = $th;
		}

		$bl = $this->input->post('tahun');
		if ($th == '') {
			$month = date('Y');
		} else {
			$month = $bl;
		}
		$nipPns = $this->session->userdata('nip');
		$roleId = $this->session->userdata('role');
		$dataPns = getDataPns($nipPns);
		if ($roleId == 1 || $roleId == 2) {
			$A_01 = '';
		} else {
			$A_01 = $dataPns->A_01;
		}
		$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);
		$data['keterangan'] = $this->Jam_model->get_all_data();
		$data['year'] = $year;
		$data['month'] = $month;
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/kerja/kerja_index', $data);
	}

	public function cari_pegawai()
	{
		$opd = $this->input->post('opd');
		$unit = $this->input->post('unit');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');

		$a_01 = substr($unit, 0, 2);
		$a_02 = substr($unit, 2, 2);
		$a_03 = substr($unit, 4, 2);
		$a_04 = substr($unit, 6, 2);
		$a_05 = substr($unit, 8, 2);

		if ($unit == '') {
			$a_01 = substr($opd, 0, 2);
			$get = $this->Jadwal_model->get_pegawai_opd($a_01);
		} else {
			$get = $this->Jadwal_model->get_pegawai_unit($a_01, $a_02, $a_03, $a_04, $a_05);
		}

		$data['data_pegawai'] = $get;
		$data['keterangan'] = $this->Jam_model->get_all_data();
		$data['jamkerja'] = $this->Jam_model->get_jam_kerja();
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/kerja/kerja_add', $data);



		// echo json_encode($data);


		// echo $a_01;
		// echo "<br/>";
		// echo $a_02;
		// echo "<br/>";
		// echo $a_03;
		// echo "<br/>";
		// echo $a_04;
		// echo "<br/>";
		// echo $a_05;




	}

	public function set_jadwal_awal()
	{
		$opd = $this->input->post('opd');
		$unit = $this->input->post('unit');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$tb = $tahun . $bulan;

		$a_01 = substr($unit, 0, 2);
		$a_02 = substr($unit, 2, 2);
		$a_03 = substr($unit, 4, 2);
		$a_04 = substr($unit, 6, 2);
		$a_05 = substr($unit, 8, 2);

		$cek = $this->Jadwal_model->count_presensi_by_tahun_bulan($opd, $unit, $bulan, $tahun);
		// echo json_encode($cek);
		if ($cek->hit > 0) {
			// echo "a;";
			$get_parent = $this->Jadwal_model->get_presensi_by_tahun_bulan($opd, $unit, $bulan, $tahun);
			$get = $this->Jadwal_model->get_presensi_by_parent($get_parent->id);
			$tb = $tahun . $bulan;
			// $get = $this->db->query("SELECT a.id_presensi, b.opd, b.unit, a.id, a.nip, a.nama_lengkap FROM  presensi a INNER JOIN parent_presensi b ON a.id_parent = b.id  WHERE b.opd = '$opd' AND a.tahun_bulan = '$tb'")->result();
			$status = 'update';
		} else {
			// echo "an";
			// $tb = $tahun.$bulan.'_';
			// $id_sudah = $this->db->query("SELECT b.opd, b.unit, a.id, a.nip FROM  presensi a INNER JOIN parent_presensi b ON a.id_parent = b.id  WHERE b.opd = '$opd' AND a.id LIKE '$tb%'")->result();
			
			
			$nip_sudah = $this->Jadwal_model->sudah_dijadwal($opd, $tahun, $bulan);
			if ($nip_sudah) {
				foreach ($nip_sudah as $sd) {
					$array_sudah = array(
						$sd->nip,
					);
				}
			} else {

				$array_sudah = array(
					1
				);
			}

			// echo json_encode($id_sudah);
			// echo json_encode($sudah);
			if ($unit == '') {
				$a_01 = substr($opd, 0, 2);
				// $gets = $this->Jadwal_model->get_pegawai_opd($a_01);
				$gets = $this->Jadwal_model->get_pegawai_opd_v2($a_01, $array_sudah);
			} else {
				// $gets = $this->Jadwal_model->get_pegawai_unit($a_01, $a_02, $a_03, $a_04, $a_05);
				$gets = $this->Jadwal_model->get_pegawai_unit_v2($a_01, $a_02, $a_03, $a_04, $a_05, $array_sudah);
			}

			$generate_format = array(
				'opd' => $opd,
				'unit' => $unit,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'generate_by' => $this->nipPns,
				'created_at' => date('Y-m-d H:i:s'),

			);
			$add_parent = $this->Jadwal_model->insert_parent_data($generate_format);
			if ($add_parent != FALSE) {
				$data = array();
				foreach ($gets as $pegawai) {
					$data[] = array(
						'tahun_bulan' => $tahun . $bulan,
						'id'		  => $tahun . $bulan . '_' . $pegawai->nip,
						'id_parent'   => $add_parent,
						'nip' 		  => $pegawai->nip,
						'nama_lengkap'=> $pegawai->nama_lengkap,
						'created_at'  => date('Y-m-d H:i:s'),
						'created_by'  => $this->nipPns,
					);
				}

				echo json_encode($array_sudah);
				// echo json_encode($data);

				// $this->Jadwal_model->insert_batch_jadwal($data);

				// if ($unit == '') {
				// 	$get = $this->db->query("SELECT a.id_presensi, b.opd, b.unit, a.*
				// 		FROM  presensi a INNER JOIN parent_presensi b ON a.id_parent = b.id  WHERE b.opd = '$opd' AND a.tahun_bulan = '$tb'")->result();
				// } else {
				// 	$get = $this->Jadwal_model->get_presensi_by_parent($add_parent);
				// }
				// // $tb = $tahun.$bulan;
				// // $get = $this->db->query("SELECT b.opd, b.unit, a.id, a.nip FROM  presensi a INNER JOIN parent_presensi b ON a.id_parent = b.id  WHERE b.opd = '$opd' AND a.tahun_bulan = '$tb'")->result();
				// $status = 'add';
				// //echo json_encode($data);
			}
		}

		// $data['data_pegawai'] = $get;
		// $data['status'] = $status;
		// $data['keterangan'] = $this->Jam_model->get_all_data();
		// $data['jamkerja'] = $this->Jam_model->get_jam_kerja();
		// $data['tahun'] = $tahun;
		// $data['bulan'] = $bulan;

		// $this->load->view('template/head');
		// $this->load->view('template/header');
		// $this->load->view('template/sidebar');
		// $this->load->view('jadwal/kerja/kerja_add', $data);

		// echo json_encode($data);


		// echo $a_01;
		// echo "<br/>";
		// echo $a_02;
		// echo "<br/>";
		// echo $a_03;
		// echo "<br/>";
		// echo $a_04;
		// echo "<br/>";
		// echo $a_05;




	}


	public function update_jadwal_satuan()
	{

		$id = $this->input->post('id');
		$tanggal = $this->input->post('tanggal');
		$jadwal_baru = $this->input->post('jadwal_baru');



		if ($tanggal < 10) {
			$tgl = 'J_0' . $tanggal;
		} else {
			$tgl = 'J_' . $tanggal;
		}

		if ($jadwal_baru != '') {
			$data = array(
				$tgl => $jadwal_baru,
			);

			$a = $this->Jadwal_model->update_jadwal($id, $data);
			$result = $a;
			// if($a){
			// 	$result = 'berhasil';
			// }else{
			// 	$result = 'gagal';
			// }

		} else {
			$result = 'tidak ada perubahan';
		}

		echo json_encode($result);
	}
}
