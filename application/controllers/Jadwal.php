<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// $this->authentification->check_user_authentification();

		$this->load->model('Libur_model');
		$this->load->model('Jam_model');
		$this->load->model('Jadwal_model');

		$this->nipPns = $this->session->userdata('nip');
		$this->roleId = $this->session->userdata('role');
		$this->dataPns = getDataPns($this->nipPns);
	}

	public function index()
	{
		redirect('dashboard');
	}

	function sample()
	{

		$a = $this->db->query("SELECT id, nama_singkat FROM jam_kerja ORDER BY id ")->result();
		foreach ($a as $d) {
			$data[] = array(
				"id" => "$d->nama_singkat"
			);
		}
		echo json_encode($data);
	}



	public function libur_asli()
	{
		if ($this->uri->segment(3) > 0) {
			$year = $data['year'] = $this->uri->segment(3);
			if ($this->uri->segment(4) > 0) {
				$month = $data['month'] = $this->uri->segment(4);
			} else {
				$month = $data['month'] = date("m");
			}
		} else {
			$year = $data['year'] = date("Y");
			$month = $data['month'] = date("m");
		}

		$data['title'] = "Hari Libur " . namaBulan($month) . ' ' . $year;

		$prefs['template'] = '

			{table_open}
            <div class="table-responsive">
            <table class="table">{/table_open}

			{heading_row_start}<tr class="cyan">{/heading_row_start}

			{heading_previous_cell}<th class="text-center"><a href="{previous_url}" class="link white-text"><i class="fa fa-chevron-left"></i></a></th>{/heading_previous_cell}
			{heading_title_cell}<th colspan="{colspan}" class="text-center"><h5 class="white-text">{heading}</h5></th>{/heading_title_cell}
			{heading_next_cell}<th class="text-center"><a href="{next_url}" class="link white-text"><i class="fa fa-chevron-right"></i></i></a></th>{/heading_next_cell}

			{heading_row_end}</tr>{/heading_row_end}

			{week_row_start}<tr>{/week_row_start}
			{week_day_cell}<td>{week_day}</td>{/week_day_cell}
			{week_row_end}</tr>{/week_row_end}

			{cal_row_start}<tr>{/cal_row_start}
			{cal_cell_start}<td>{/cal_cell_start}
			{cal_cell_start_today}<td>{/cal_cell_start_today}
			{cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

			{cal_cell_content}<a onclick="ubah({day})" data-position="bottom" data-delay="50" data-popup="tooltip" data-placement="bottom" title="{content}" class="btn btn-danger " href="javascript:void(0)">{day}</a>{/cal_cell_content}
			{cal_cell_content_today}<a onclick="ubah({day})" data-position="bottom" data-delay="50" data-popup="tooltip" data-placement="bottom" title="{content}" class="btn btn-warning " href="javascript:void(0)">{day}</a>{/cal_cell_content_today}

			{cal_cell_no_content}<a href="javascript:void(0)" onclick="tambah({day})" class="black-text btn waves-effect waves-light white" href="javascript:void(0)">{day}</a>{/cal_cell_no_content}
			{cal_cell_no_content_today}<a onclick="tambah({day})" data-position="bottom" data-delay="50" data-popup="tooltip" data-placement="bottom" title="Hari Ini" class="btn btn-primary" href="javascript:void(0)">{day}</a>{/cal_cell_no_content_today}

			{cal_cell_blank}&nbsp;{/cal_cell_blank}

			{cal_cell_other}<div class="grey-text text-lighten-3">{day}</div>{/cal_cel_other}

			{cal_cell_end}</td>{/cal_cell_end}
			{cal_cell_end_today}</td>{/cal_cell_end_today}
			{cal_cell_end_other}</td>{/cal_cell_end_other}
			{cal_row_end}</tr>{/cal_row_end}

			{table_close}</table></div>{/table_close}
			';

		$prefs['show_next_prev'] = TRUE;
		$prefs['day_type'] = 'long';
		$prefs['next_prev_url'] = site_url('jadwal/libur');
		$prefs['start_day'] = 'sunday';
		$prefs['show_other_days'] = FALSE;



		$data['table_data'] = site_url('jadwal/libur/data');

		$agenda_data = $data['agenda_data'] = $this->Libur_model->get_libur($year, $month);
		$data['calendar'] = array();
		$items = array();
		foreach ($agenda_data->result() as $row) {
			$items[$row->day] = $row->nama;
		}
		$data["calendar"] = $items;

		$this->load->library('calendar', $prefs);

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/libur/libur_index', $data);
	}

	public function libur($action = '')
	{
		$data['group_module'] = $group_module = $this->uri->segment(1);
		$data['module'] = $module = $this->uri->segment(2);
		$data['act'] = $act = $this->uri->segment(3);

		switch ($action) {
			case "add":
				$data['tanggal_awal'] = $tanggal_awal = $this->uri->segment(4);
				$data['tanggal_akhir'] = $tanggal_akhir = $this->uri->segment(4);
				$data['title'] = "Tambah Hari Libur";
				$data['form_action'] = site_url($group_module . '/' . $module . '/insert');
				$data['form_data'] = false;
				$this->load->view('jadwal/libur/form', $data);

				break;
			default:
				if ($this->uri->segment(3) > 0) {
					$year = $data['year'] = $this->uri->segment(3);
					if ($this->uri->segment(4) > 0) {
						$month = $data['month'] = $this->uri->segment(4);
					} else {
						$month = $data['month'] = date("m");
					}
				} else {
					$year = $data['year'] = date("Y");
					$month = $data['month'] = date("m");
				}

				$data['title'] = "Hari Libur " . namaBulan($month) . ' ' . $year;

				$prefs['template'] = '

				{table_open}
				<div class="table-responsive">
				<table class="table">{/table_open}

				{heading_row_start}<tr class="cyan">{/heading_row_start}

				{heading_previous_cell}<th class="text-center"><a href="{previous_url}" class="link white-text"><i class="fa fa-chevron-left"></i></a></th>{/heading_previous_cell}
				{heading_title_cell}<th colspan="{colspan}" class="text-center"><h5 class="white-text">{heading}</h5></th>{/heading_title_cell}
				{heading_next_cell}<th class="text-center"><a href="{next_url}" class="link white-text"><i class="fa fa-chevron-right"></i></i></a></th>{/heading_next_cell}

				{heading_row_end}</tr>{/heading_row_end}

				{week_row_start}<tr>{/week_row_start}
				{week_day_cell}<td>{week_day}</td>{/week_day_cell}
				{week_row_end}</tr>{/week_row_end}

				{cal_row_start}<tr>{/cal_row_start}
				{cal_cell_start}<td>{/cal_cell_start}
				{cal_cell_start_today}<td>{/cal_cell_start_today}
				{cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

				{cal_cell_content}<a onclick="ubah({day})" data-position="bottom" data-delay="50" data-popup="tooltip" data-placement="bottom" title="{content}" class="btn btn-danger " href="javascript:void(0)">{day}</a>{/cal_cell_content}
				{cal_cell_content_today}<a onclick="ubah({day})" data-position="bottom" data-delay="50" data-popup="tooltip" data-placement="bottom" title="{content}" class="btn btn-warning " href="javascript:void(0)">{day}</a>{/cal_cell_content_today}

				{cal_cell_no_content}<a href="javascript:void(0)" onclick="tambah({day})" class="black-text btn waves-effect waves-light white" href="javascript:void(0)">{day}</a>{/cal_cell_no_content}
				{cal_cell_no_content_today}<a onclick="tambah({day})" data-position="bottom" data-delay="50" data-popup="tooltip" data-placement="bottom" title="Hari Ini" class="btn btn-primary" href="javascript:void(0)">{day}</a>{/cal_cell_no_content_today}

				{cal_cell_blank}&nbsp;{/cal_cell_blank}

				{cal_cell_other}<div class="grey-text text-lighten-3">{day}</div>{/cal_cel_other}

				{cal_cell_end}</td>{/cal_cell_end}
				{cal_cell_end_today}</td>{/cal_cell_end_today}
				{cal_cell_end_other}</td>{/cal_cell_end_other}
				{cal_row_end}</tr>{/cal_row_end}

				{table_close}</table></div>{/table_close}
				';

				$prefs['show_next_prev'] = TRUE;
				$prefs['day_type'] = 'long';
				$prefs['next_prev_url'] = site_url('jadwal/libur');
				$prefs['start_day'] = 'sunday';
				$prefs['show_other_days'] = FALSE;



				$data['table_data'] = site_url('jadwal/libur/data');

				$agenda_data = $data['agenda_data'] = $this->Libur_model->get_libur($year, $month);
				$data['calendar'] = array();
				$items = array();
				foreach ($agenda_data->result() as $row) {
					$items[$row->day] = $row->nama;
				}
				$data["calendar"] = $items;

				$this->load->library('calendar', $prefs);

				$this->load->view('template/head');
				$this->load->view('template/header');
				$this->load->view('template/sidebar');
				$this->load->view('jadwal/libur/libur_index', $data);
				break;
		}
	}



	public function jam()
	{


		$get = $this->Jam_model->get_all_data();
		$datatable = array();
		// <a  href="' . base_url('setting/privilage_edit/') . $value->id . '" type="button" class="btn bg-primary btn-icon rounded-round"><i class="fa fa-edit"></i></a> &nbsp;
		foreach ($get as $key => $value) {
			$action =
				'<div class="row">
				<a type="button" class="btn bg-primary btn-icon rounded-round" href="javascript:;"
				data-id="'.$value->id.'"
				data-nama="'.$value->nama.'"
				data-nama_singkat="'.$value->nama_singkat.'"
				data-jadwal_masuk="'.$value->jadwal_masuk.'"
				data-jadwal_keluar="'.$value->jadwal_keluar.'"
				data-deskripsi="'.$value->deskripsi.'""
				data-toggle="modal" data-target="#edit-data"
				>
				<i class="fa fa-edit"></i>
                </a>&nbsp;

                <a  onclick="return confirm(\'Are you sure you want to delete this item?\');" href="' . base_url('setting/privilage_deleted/') . $value->id . '" type="button" class="btn bg-warning btn-icon rounded-round"><i class="fa fa-trash"></i></a>
        
                </div>';


			if ($value->status == 1) {
				$status = "<span class='badge badge-success'>Aktif</span>";
			} else {
				$status = "<span class='badge badge-danger'>Tidak Aktif</span>";
			}

			$name = "(" . $value->nama_singkat . ") " . $value->nama;
			$jadwal = $value->jadwal_masuk . " - " . $value->jadwal_keluar;



			$datatable[$key] = array(
				'name'            => $name,
				'schedule'        => $jadwal,
				'description'     => string_add($value->deskripsi, '-<br/>', 45, 0),
				'status'          => $status,
				'action'          => $action
			);
		}
		$data['datatable'] = $datatable;

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/jam/jam_index', $data);
	}

	public function saya()
	{
		if ($this->uri->segment(3) > 0) {
			$year = $this->uri->segment(3);
			$data['year'] = $year;
			if ($this->uri->segment(4) > 0) {
				$month = $this->uri->segment(4);
				$data['month'] = $month;
			} else {
				$month = date("m");
				$data['month'] = $month;
			}
		} else {
			$year = date("Y");
			$month =  date("m");

			$data['year'] = $year;
			$data['month'] = $month;
		}
		$nip = $this->authentification->get_user('nip');
		$tb_nip = $year . $month . '_' . $nip;

		$data['title'] = "Hari Libur " . namaBulan($month) . ' ' . $year;
		$data['nip'] = $nip;
		$data['jadwal_data'] = $this->Jadwal_model->get_jadwal_perorangan($tb_nip);
		$data['jam_kerja'] = $this->Jam_model->get_jam_kerja();

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/saya/saya_index', $data);
	}


	public function kerja()
	{
		$th = $this->input->post('tahun');
		if ($th == '') {
			$year = date('Y');
		} else {
			$year = $th;
		}

		$bl = $this->input->post('tahun');
		if ($th == '') {
			$month = date('Y');
		} else {
			$month = $bl;
		}
		$nipPns = $this->session->userdata('nip');
		$roleId = $this->session->userdata('role');
		$dataPns = getDataPns($nipPns);
		if ($roleId == 1 || $roleId == 2) {
			$A_01 = '';
		} else {
			$A_01 = $dataPns->A_01;
		}
		$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);
		$data['keterangan'] = $this->Jam_model->get_all_data();
		$data['year'] = $year;
		$data['month'] = $month;
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/kerja/kerja_index', $data);
	}

	public function cari_pegawai()
	{
		$opd = $this->input->post('opd');
		$unit = $this->input->post('unit');
		$eselon = $this->input->post('eselon');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');

		$a_01 = substr($unit, 0, 2);
		$a_02 = substr($unit, 2, 2);
		$a_03 = substr($unit, 4, 2);
		$a_04 = substr($unit, 6, 2);
		$a_05 = substr($unit, 8, 2);

		if ($unit == '') {
			$a_01 = substr($opd, 0, 2);
			$get = $this->Jadwal_model->get_pegawai_opd($a_01);
		} else {
			$get = $this->Jadwal_model->get_pegawai_unit($a_01, $a_02, $a_03, $a_04, $a_05);
		}

		$data['data_pegawai'] = $get;
		$data['keterangan'] = $this->Jam_model->get_all_data();
		$data['jamkerja'] = $this->Jam_model->get_jam_kerja();
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/kerja/kerja_add', $data);



		// echo json_encode($data);


		// echo $a_01;
		// echo "<br/>";
		// echo $a_02;
		// echo "<br/>";
		// echo $a_03;
		// echo "<br/>";
		// echo $a_04;
		// echo "<br/>";
		// echo $a_05;




	}

	public function set_jadwal_awal()
	{
		$u3 = $this->uri->segment(3);
		if($u3 == ''){
			$opd = $this->input->post('opd');
			$opdA_01 = substr($opd, 0, 2);
			$unit = $this->input->post('unit');

			if($unit == 'all'){
				$unit = '';
			}elseif($unit == ''){
				$un = 'all';
			}else{
				$un = $unit;
			}

			$bulan = $this->input->post('bulan');
			$tahun = $this->input->post('tahun');
		}else{
			$opd = $this->uri->segment(3);
			$opdA_01 = substr($opd, 0, 2);
			$unit = $this->uri->segment(4);

			if($unit == 'all'){
				$unit = '';
				$un = 'all';
			}elseif($unit == ''){
				$un = 'all';
			}else{
				$un = $unit;
			}

			$bulan = $this->uri->segment(5);
			$tahun = $this->uri->segment(6);
		}
		
		$tb = $tahun . $bulan;

		$a_01 = substr($unit, 0, 2);
		$a_02 = substr($unit, 2, 2);
		$a_03 = substr($unit, 4, 2);
		$a_04 = substr($unit, 6, 2);
		$a_05 = substr($unit, 8, 2);

		$cek = $this->Jadwal_model->count_presensi_by_tahun_bulan($opd, $unit, $bulan, $tahun);

		// echo json_encode($cek);
		if ($cek->hit > 0) {
			// $get = $this->db->query("SELECT a.id_presensi, b.opd, b.unit, a.*
			// 		   FROM  presensi a 
			// 		   INNER JOIN parent_presensi b ON a.id_parent = b.id 
			// 		   WHERE b.opd = '$opd' AND a.tahun_bulan = '$tb'")->result();
			if ($unit == '') {
				$get = $this->Jadwal_model->terjadwal_opd($opd, $tahun, $bulan);
			} else {
				$get = $this->Jadwal_model->terjadwal_opd($opd, $tahun, $bulan);
			}
			$status = 'update';
			
		} else {
			
			$array_sudah = $this->Jadwal_model->sudah_dijadwal_v3($opd, $tahun, $bulan);
			if ($unit == '') {
				// $gets = $this->Jadwal_model->get_pegawai_opd($a_01);
				$gets = $this->Jadwal_model->get_pegawai_opd_v2($opdA_01, $array_sudah);
			} else {
				// $gets = $this->Jadwal_model->get_pegawai_unit($a_01, $a_02, $a_03, $a_04, $a_05);
				$gets = $this->Jadwal_model->get_pegawai_unit_v2($a_01, $a_02, $a_03, $a_04, $a_05, $array_sudah);
			}
			

			// print_r($gets);

			$generate_format = array(
				'opd' => $opd,
				'unit' => $unit,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'generated' => 0,
				'generate_by' => $this->nipPns,
				'created_at' => date('Y-m-d H:i:s'),

			);
			$add_parent = $this->Jadwal_model->insert_parent_data($generate_format);
			if ($add_parent != FALSE) {

				$datas = array();
				foreach ($gets as $pegawai) {
					$datas[] = array(
						'tahun_bulan' => $tahun . $bulan,
						'id'		  => $tahun . $bulan . '_' . $pegawai->nip,
						'id_parent'   => $add_parent,
						'nip' 		  => $pegawai->nip,
						'nama_lengkap'=> $pegawai->nama_lengkap,
						'created_at'  => date('Y-m-d H:i:s'),
						'created_by'  => $this->nipPns,
					);
				}

				// echo json_encode($data);

				$this->Jadwal_model->insert_batch_jadwal($datas);
				// $get = $this->db->query("SELECT a.id_presensi, b.opd, b.unit, a.*
				// 	   FROM  presensi a 
				// 	   INNER JOIN parent_presensi b ON a.id_parent = b.id 
				// 	   WHERE b.opd = '$opd' AND a.tahun_bulan = '$tb'")->result();
				$status = 'add';

				if ($unit == '') {
					// $gets = $this->Jadwal_model->get_pegawai_opd($a_01);
					$get = $this->Jadwal_model->terjadwal_opd($opd, $tahun, $bulan);
				} else {
					// $gets = $this->Jadwal_model->get_pegawai_unit($a_01, $a_02, $a_03, $a_04, $a_05);
					$get = $this->Jadwal_model->terjadwal_opd($opd, $tahun, $bulan);
				}

				//echo json_encode($data);
			}
		}

		// echo json_encode($array_sudah);
		// echo '<br/>';
		// echo json_encode($gets);
		// // $a = implode(',', $array_sudah);
		// echo '<br/>';
		// // echo $a;

		// foreach($array_sudah as $as){
        //     echo "$as->nip".',';
        // }
		
		
		$data['data_pegawai'] = $get;
		$data['status'] = $status;
		$data['keterangan'] = $this->Jam_model->get_all_data();
		$data['jamkerja'] = $this->Jam_model->get_jam_kerja();
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;
		$data['opd'] = $opd;
		$data['unit'] = $un;

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('jadwal/kerja/kerja_add', $data);

		//  echo json_encode($data);


		// echo $a_01;
		// echo "<br/>";
		// echo $a_02;
		// echo "<br/>";
		// echo $a_03;
		// echo "<br/>";
		// echo $a_04;
		// echo "<br/>";
		// echo $a_05;




	}

	

	public function set_jadwal_umum(){
		$opd = $this->input->post('opd');
		$unit = $this->input->post('unit');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');

		$j_01 = $this->input->post('tanggal_1');
		$j_02 = $this->input->post('tanggal_2');
		$j_03 = $this->input->post('tanggal_3');
		$j_04 = $this->input->post('tanggal_4');
		$j_05 = $this->input->post('tanggal_5');
		$j_06 = $this->input->post('tanggal_6');
		$j_07 = $this->input->post('tanggal_7');
		$j_08 = $this->input->post('tanggal_8');
		$j_09 = $this->input->post('tanggal_9');
		$j_10 = $this->input->post('tanggal_10');
		$j_11 = $this->input->post('tanggal_11');
		$j_12 = $this->input->post('tanggal_12');
		$j_13 = $this->input->post('tanggal_13');
		$j_14 = $this->input->post('tanggal_14');
		$j_15 = $this->input->post('tanggal_15');
		$j_16 = $this->input->post('tanggal_16');
		$j_17 = $this->input->post('tanggal_17');
		$j_18 = $this->input->post('tanggal_18');
		$j_19 = $this->input->post('tanggal_19');
		$j_20 = $this->input->post('tanggal_20');
		$j_21 = $this->input->post('tanggal_21');
		$j_22 = $this->input->post('tanggal_22');
		$j_23 = $this->input->post('tanggal_23');
		$j_24 = $this->input->post('tanggal_24');
		$j_25 = $this->input->post('tanggal_25');
		$j_26 = $this->input->post('tanggal_26');
		$j_27 = $this->input->post('tanggal_27');
		$j_28 = $this->input->post('tanggal_28');
		$j_29 = $this->input->post('tanggal_29');
		$j_30 = $this->input->post('tanggal_30');
		$j_31 = $this->input->post('tanggal_31');


		$id_presensi = $this->input->post('id_presensi');

		for($i = 0; $i <= count($id_presensi); $i++){
			
			$presensi[] = array(
				'id_presensi' => $id_presensi[$i],
				'j_01'	=> $j_01,
				'j_02'	=> $j_02,
				'j_03'	=> $j_03,
				'j_04'	=> $j_04,
				'j_05'	=> $j_05,
				'j_06'	=> $j_06,
				'j_07'	=> $j_07,
				'j_08'	=> $j_08,
				'j_09'	=> $j_09,
				'j_10'	=> $j_10,
				'j_11'	=> $j_11,
				'j_12'	=> $j_12,
				'j_13'	=> $j_13,
				'j_14'	=> $j_14,
				'j_15'	=> $j_15,
				'j_16'	=> $j_16,
				'j_17'	=> $j_17,
				'j_18'	=> $j_18,
				'j_19'	=> $j_19,
				'j_20'	=> $j_20,
				'j_21'	=> $j_21,
				'j_22'	=> $j_22,
				'j_23'	=> $j_23,
				'j_24'	=> $j_24,
				'j_25'	=> $j_25,
				'j_26'	=> $j_26,
				'j_27'	=> $j_27,
				'j_28'	=> $j_28,
				'j_29'	=> $j_29,
				'j_30'	=> $j_30,
				'j_31'	=> $j_31,
			);
		}

		$update = $this->db->update_batch('presensi', $presensi, 'id_presensi');
		if($update){
			redirect('jadwal/set_jadwal_awal/'.$opd.'/'. $unit.'/'. $bulan.'/'. $tahun);
			// echo "success";
		}else{
			redirect('jadwal/set_jadwal_awal/'.$opd.'/'. $unit.'/'. $bulan.'/'. $tahun);
			// echo "gagal";
		}

		

		// echo json_encode($presensi);

		// $merge = array_merge($presensi, $jadwal); 
		// // echo json_encode($merge);
		// print_r($merge);

	
	}


	public function update_jadwal_satuan()
	{

		$id = $this->input->post('id');
		$tanggal = $this->input->post('tanggal');
		$jadwal_baru = $this->input->post('jadwal_baru');



		if ($tanggal < 10) {
			$tgl = 'j_0' . $tanggal;
		} else {
			$tgl = 'j_' . $tanggal;
		}

		if ($jadwal_baru != '') {
			$data = array(
				$tgl => $jadwal_baru,
			);

			$a = $this->Jadwal_model->update_jadwal($id, $data);
			$result = $a;
			// if($a){
			// 	$result = 'berhasil';
			// }else{
			// 	$result = 'gagal';
			// }

		} else {
			$result = 'tidak ada perubahan';
		}

		echo json_encode($result);
	}
}
