<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekap extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// $this->authentification->check_user_authentification();

		$this->load->model('Rekap_model');
		$this->load->model('Fungsi_model');
        $this->load->model('Jam_model');
		$this->load->model('Service_model');

		$this->nipPns = $this->session->userdata('nip');
		$this->roleId = $this->session->userdata('role');
		$this->dataPns = getDataPns($this->nipPns);
	}

	public function index()
	{
		redirect('dashboard');
	}

    public function saya()
	{
        $bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun');

        if($bulan == ''){
             $bln = date('m');
			// $bln = '08';
        }else{
             $bln = $bulan;
			// $bln = '08';
        }	
        if($tahun == ''){
             $th = date('Y');
			// $th ='2021';
        }else{
             $th = $tahun;
			// $th ='2021';
        }

		$data['tahun'] = $th;
		$data['bulan'] = $bln;
        $presensi = $this->Rekap_model->get_presensi_personal($this->nipPns, $th,  $bln,);
		$data['presensi_data'] = $presensi->row_array();
		$data['presensi_data_cek'] = $presensi;
		$data['jam_kerja'] = $this->Jam_model->get_jam_kerja();
        $data['tipe_data'] = $this->Rekap_model->get_tipe();

		// echo json_encode($data);

		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('rekap/saya/saya_index', $data);
	}

	public function personal()
	{

		$nip = $this->input->post('nipbos');
		$nama = $this->input->post('namabos');
        $bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun');

		if($nip == ''){
			$nip_b = '';
		   
	   }else{
			$nip_b = $nip;
		  
	   }	

	   if($nama == ''){
		$nama_b = '';
	   
   }else{
		$nama_b = $nama;
	  
   }	

		
        if($bulan == ''){
             $bln = date('m');
			// $bln = '08';
        }else{
             $bln = $bulan;
			// $bln = '08';
        }	
        if($tahun == ''){
             $th = date('Y');
			// $th ='2021';
        }else{
             $th = $tahun;
			// $th ='2021';
        }

		$data['nip'] = $nip_b;
		$data['nama'] = $nama_b;
		$data['tahun'] = $th;
		$data['bulan'] = $bln;
		$presensi = $this->Rekap_model->get_presensi_personal($nip, $th, $bln);
		
		$data['presensi_data'] = $presensi->row_array();
		$data['presensi_data_cek'] = $presensi;
		$data['jam_kerja'] = $this->Jam_model->get_jam_kerja();
        $data['tipe_data'] = $this->Rekap_model->get_tipe();

		// echo json_encode($data);

		$this->load->view('template/head');
		
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('rekap/personal/personal_index', $data);
	}


	public function skpd()
	{
		$opd = $this->input->post('opd');
		$unit = $this->input->post('unit');
		$eselon = $this->input->post('eselon');

		$th = $this->input->post('tahun');
		if ($th == '') {
			$year = date('Y');
		} else {
			$year = $th;
		}

		$bl = $this->input->post('bulan');
		if ($th == '') {
			$month = date('Y');
		} else {
			$month = $bl;
		}

		$nipPns = $this->session->userdata('nip');
		$roleId = $this->session->userdata('role');
		$dataPns = getDataPns($nipPns);
		if ($roleId == 1 || $roleId == 2) {
			$A_01 = '';
		} else {
			$A_01 = $dataPns->A_01;
		}
		$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);
		$data['year'] = $year;
		$data['month'] = $month;
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');

		$uri3 = $this->uri->segment(3);
		// echo $uri3;
		if($uri3 == 'cari'){
			$data['presensi_data'] = $this->Rekap_model->get_presensi_skpd($opd,$unit,$year,$month,$eselon);
		$data['tipe_data'] = $this->Rekap_model->get_tipe(); 
		$this->load->view('rekap/skpd/skpd_index_cari', $data);
		}else{
			$this->load->view('rekap/skpd/skpd_index', $data);
		}
		

		

	}


	public function bulanan()
	{
			#echo'<pre>'; print_r($this->input->post());die();
		$opd = $this->input->post('opd');
		$unit = $this->input->post('unit');
		$eselon = $this->input->post('eselon');

		$th = $this->input->post('tahun');
		if ($th == '') {
			$year = date('Y');
		} else {
			$year = $th;
		}

		$bl = $this->input->post('bulan');
		if ($th == '') {
			$month = date('Y');
		} else {
			$month = $bl;
		}

		$nipPns = $this->session->userdata('nip');
		$roleId = $this->session->userdata('role');
		$dataPns = getDataPns($nipPns);
		if ($roleId == 1 || $roleId == 2) {
			$A_01 = '';
		} else {
			$A_01 = $dataPns->A_01;
		}
		$data['allopd'] = $this->Fungsi_model->get_opd_list($A_01);
		$data['year'] = $year;
		$data['month'] = $month;
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');

		$uri3 = $this->uri->segment(3);
		// echo $uri3;
		if($uri3 == 'cari'){
			$data['unit_kerja'] = $unit;
			$data['eselon'] = $eselon;
			$data['tahun'] = $year;
			$data['bulan'] = $month;
			switch($month)
			{
				case 'all':
				$period = ' ';
				break;

				case 'smt1':
				$period = ' Semester I';
				break;

				case 'smt2':
				$period = ' Semester II';
				break;
				
				case 'caturwulan1':
				$period = ' Catur Wulan I';
				break;
				
				case 'caturwulan2':
				$period = ' Catur Wulan II';
				break;
				
				case 'caturwulan3':
				$period = ' Catur Wulan III';
				break;
				
				default:
				$period = ' Bulan '.bulan($month);
				break;
			}
			if($opd && $year && $month)
			{
				$data['title'] = 'Rekapitulasi '.get_opd('nama',$opd).''.$period.' Tahun '.$year;
				$data['pegawai_data']  = $this->Rekap_model->get_pegawai($opd,$year,$month,$eselon);
				$data['presensi_data']  = $this->Rekap_model->get_presensi_bulanan($opd,$year,$month,$eselon);
				//$data['presensi_data'] = $presensi_data = $this->rekap_model->get_presensi_bulanan($unit_kerja,$tahun,$bulan,$eselon);
				$data['tipe_data']  = $this->Rekap_model->get_tipe();	
				#echo '<pre>'; print_r($data);die();
				$this->load->view('rekap/bulanan/bulanan_cari', $data);
		
			}
			else
			{
				echo "Lengkapi Form !!!";
			} 
		}else{
			$this->load->view('rekap/bulanan/bulanan_index', $data);
		}
		

		

	}

	function bulanan_backup($action = "")
	{
		$this->authentification->check_modules_user('0504');
		$data['group_module'] = $group_module = $this->uri->segment(1);
		$data['module'] = $module = $this->uri->segment(2);
		$data['act'] = $act = $this->uri->segment(3);
		$data['nip_user'] = $nip_user = $this->authentification->get_user('nip');
		$data['profile'] = $profile = $this->Service_model->get_pegawai_nip($nip_user)->row_array();
		
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		
		switch($action)
		{
			case "result" : 
				$data['unit_kerja'] = $unit_kerja = decode($this->uri->segment(4));
				$data['tahun_bulan'] = $tahun_bulan = $this->uri->segment(5);
				$data['eselon'] = $eselon = $this->uri->segment(6);
				
				$explode = explode('-',$tahun_bulan);
				$data['tahun'] = $tahun = $explode[0];
				$data['bulan'] = $bulan = $explode[1];
				
				switch($bulan)
				{
					case 'all':
					$period = ' ';
					break;

					case 'smt1':
					$period = ' Semester I';
					break;

					case 'smt2':
					$period = ' Semester II';
					break;
					
					case 'caturwulan1':
					$period = ' Catur Wulan I';
					break;
					
					case 'caturwulan2':
					$period = ' Catur Wulan II';
					break;
					
					case 'caturwulan3':
					$period = ' Catur Wulan III';
					break;
					
					default:
					$period = ' Bulan '.bulan($bulan);
					break;
					}
				
				if($unit_kerja && $tahun_bulan)
				{
					$data['unit_kerja'] = $unit_kerja = decode($this->uri->segment(4));
					$data['title'] = 'Rekapitulasi '.get_opd('nama',$unit_kerja).''.$period.' Tahun '.$tahun;
					$data['pegawai_data'] = $pegawai_data = $this->rekap_model->get_pegawai($unit_kerja,$tahun,$bulan,$eselon);
					$data['presensi_data'] = $presensi_data = $this->rekap_model->get_presensi_bulanan($unit_kerja,$tahun,$bulan,$eselon);
					//$data['presensi_data'] = $presensi_data = $this->rekap_model->get_presensi_bulanan($unit_kerja,$tahun,$bulan,$eselon);
					$data['tipe_data'] = $tipe_data = $this->rekap_model->get_tipe();	
					$this->load->view('rekap/bulanan/bulanan_result', $data);
			
				}
				else
				{
					echo "Lengkapi Form !!!";
				}
					
            break;
						
			default:
			$data['title'] = "Rekap Bulanan";
			#echo '<pre>';print_r($data);die();
			$this->load->view('rekap/bulanan/bulanan_default', $data);
			break;
		}
	}

}
?>