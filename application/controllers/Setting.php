<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->authentification->check_user_authentification();

        $this->load->model('Note_model');
        $this->load->model('Konfigurasi_model');
        $this->load->model('Privilege_model');
    }

    public function index()
    {
        redirect('dashboard');
    }

    public function privilege()
    {
        // <a  href="' . base_url('setting/privilage_edit/') . $value->id . '" type="button" class="btn bg-primary btn-icon rounded-round"><i class="fa fa-edit"></i></a> &nbsp;
        $get = $this->Privilege_model->get_all_data();
        $datatable = array();
        foreach ($get as $key => $value) {
           

            if ($value->status == 1) {
                $status = "<span class='badge badge-success'>Aktif</span>";
            } else {
                $status = "<span class='badge badge-danger'>Tidak Aktif</span>";
            }

            $action =
            '<div class="row">
            <div class="row">
            <a type="button" class="btn bg-primary btn-icon rounded-round" href="javascript:;"
            data-id="'.$value->id.'"
            data-nama="'.$value->name.'"
            data-status="'.$value->status.'"
            data-deskripsi="'.$value->description.'""
            data-toggle="modal" data-target="#edit-data"
            >
            <i class="fa fa-edit"></i>
            </a>&nbsp;

            <a  onclick="return confirm(\'Are you sure you want to delete this item?\');" href="' . base_url('setting/privilage_deleted/') . $value->id . '" type="button" class="btn bg-warning btn-icon rounded-round"><i class="fa fa-trash"></i></a>
    
            </div>';




            $datatable[$key] = array(
                'name'            => $value->name,
                'description'     => $value->description,
                'status'          => $status,
                'action'          => $action
            );
        }
        $data['datatable'] = $datatable;

        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('setting/privilege/privilege_index', $data);
    }

    public function konfigurasi()
    {

        $data['config'] = $this->Konfigurasi_model->get_all_data();
        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('setting/konfigurasi/konfigurasi_index',$data);
    }

    public function konfigurasi_edit_act()
    {
        $config_data = $this->Konfigurasi_model->get_config();
        foreach ($config_data->result() as $config)
			{	
				$this->Konfigurasi_model->update($config->id);
			}
        redirect('setting/konfigurasi');
    }

    public function note()
    {

        $data['notes'] = $this->Note_model->get_note()->result();
        // echo json_encode($data);
        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('setting/note/note_index',$data);
    }

    public function note_edit_act()
    {
        foreach ($this->Note_model->get_note()->result() as $note)
			{	
				$this->Note_model->update($note->id);
			}
        redirect('setting/note');
    }

    

    
}
