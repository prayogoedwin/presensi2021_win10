<?php

function getDataPns($nip){
    $ci = get_instance();
    $ci->dbeps = $ci->load->database('eps', TRUE);
    $row = $ci->dbeps->query("SELECT * FROM MASTFIP08 WHERE B_02B = '$nip'")->row();
    if($row){
        return $row;
    }else{
        return array();
    }
    
}

function lokasiKerja($A_01, $A_02 = '00', $A_03 = '00', $A_04 = '00', $A_05 = '00') {
    $ci = get_instance();
    $ci->dbeps = $ci->load->database('eps', TRUE);
    $q="SELECT * FROM TABLOKB08 WHERE A_01='$A_01' AND A_02='$A_02' AND A_03='$A_03' AND A_04='$A_04' AND A_05='$A_05' ";
    $row = $ci->dbeps->query($q)->row();
    return $row;
}
	
function get_opd($field,$kode)
{
    $ci = get_instance();
    $ci->dbeps = $ci->load->database('eps', TRUE);
    $ci->dbeps->select('a.NALOKP as nama');
    $ci->dbeps->like('a.KOLOK',$kode,'AFTER');
    $output = $ci->dbeps->get('TABLOKB08 a')->row_array();
    return $output[$field];
}

function get_module($id)
{
    $ci = get_instance();
    $ci->db->select('module.name,module.description,module.controller,module.icon');
    $ci->db->join('privilege','FIND_IN_SET(module.id, REPLACE(privilege.access,"", "," )) > 0','INNER');
    $ci->db->order_by('module.id', 'ASC');
    $ci->db->where('module.controller !=', '#');
    $ci->db->where('module.type', 'menu');
    $ci->db->where('module.status', '1');
    $ci->db->where('privilege.status', '1');
    $ci->db->where('privilege.id', $id);
    return $ci->db->get('module');
    
}

function randomcolor(){
    $color = array("red","pink","purple","deep-purple","indigo","blue","light-blue","cyan","teal","green","light-green","lime","yellow","amber","orange","deep-orange","brown");
	
	return  $color[array_rand($color)];
 
	}
	
	function safe_b64encode($string) {
	
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }

	function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
	
    function encode($value){ 
		
        $ci =& get_instance();
		$ci->load->database();
		$ci->load->library('authentification');
	    if(!$value){return false;}
        $text = $value;
		$crypttext = $ci->encryption->encrypt($text);
		return trim(safe_b64encode($crypttext)); 
    }
    
    function decode($value){
		
        $ci =& get_instance();
		$ci->load->database();
		$ci->load->library('authentification');
        if(!$value){return false;}
        $crypttext = safe_b64decode($value); 
        return trim($ci->encryption->decrypt($crypttext));
    }

?>