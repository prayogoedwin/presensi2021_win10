<?php
date_default_timezone_set('Asia/Jakarta');
function is_login()
{
    $ci = get_instance();
    if ($ci->session->userdata('id') == null) {
        redirect('login');
    } else {
        return true;
    }
}

function id_bidang_by($var, $value)
{
    $ci = get_instance();
    return $ci->db->query("SELECT * FROM bidang_hibah WHERE $var = '$value'")->row();
}

function id_tipe_by($var, $value)
{
    $ci = get_instance();
    return $ci->db->query("SELECT * FROM tipe WHERE $var = '$value'")->row();
}

function adatidak($kode)
{
    switch ($kode) {
        case '1':
            return 'ADA';
            break;
        case '0':
            return 'Tidak';
            break;
        default:
            return '-';
            break;
    }
}


function roleUser($id)
{
    $ci = get_instance();
    $row = $ci->db->query("SELECT * FROM privilege WHERE  id = '$id'")->row();
    #echo '<pre>'; print_r($id);die();
    if ($row) {
        return $row;
    } else {
        return array();
    }
}

function namaBulan($input)
{
    if ($input == '1') {
        $output = 'Januari';
    }
    if ($input == '2') {
        $output = 'Februari';
    }
    if ($input == '3') {
        $output = 'Maret';
    }
    if ($input == '4') {
        $output = 'April';
    }
    if ($input == '5') {
        $output = 'Mei';
    }
    if ($input == '6') {
        $output = 'Juni';
    }
    if ($input == '7') {
        $output = 'Juli';
    }
    if ($input == '8') {
        $output = 'Agustus';
    }
    if ($input == '9') {
        $output = 'September';
    }
    if ($input == '10') {
        $output = 'Oktober';
    }
    if ($input == '11') {
        $output = 'November';
    }
    if ($input == '12') {
        $output = 'Desember';
    }
    return $output;
}

function string_add($kalimat, $tambahan, $posisi)
{
    $c = strlen($kalimat);
    if ($c >= $posisi) {
        return substr_replace($kalimat, $tambahan, $posisi, 0);
    } else {
        return $kalimat;
    }
}

function active_menu($ctrl, $menu)
{
    if ($ctrl == $menu) {
        return 'nav-item-expanded nav-item-open';
    } else {
        return '';
    }
}

function encode_url($url)
{
    $random1 = substr(sha1(rand()), 0, 40);
    $random2 = substr(md5(rand()), 0, 20);
    $ret = base64_encode($random1 . $url . $random2);

    return strtr(
        $ret,
        array(
            '+' => '.',
            '=' => '-',
            '/' => '~'
        )
    );
}

function decode_url($url)
{
    $a = base64_decode($url);
    $hitung = strlen($a);
    $x = $hitung - 60;
    $y = $x + 20;
    $c = substr($a, -$y);
    return substr($c, 0, $x);
}

function rupiah($uang,$delimiter='.')
    {
        if($uang == '' || $uang <= 0)
        {
            $rupiah = '0';
            return $rupiah;
        }
      
        $rupiah = number_format(round($uang,0),0,',',$delimiter);

        return $rupiah;
    }	

function jumlah_hari($tahun=0,$bulan=0)
{

    $bulan = $bulan > 0 ? $bulan : date("m");
    $tahun = $tahun > 0 ? $tahun : date("Y");

    switch ($bulan) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            return 31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
            break;
        case 2:
            return $tahun % 4 == 0 ? 29 : 28;
            break;
    }
}

function hari($input)
{
    if ($input == 'Sun') {
        $output = 'Minggu';
    }
    if ($input == 'Mon') {
        $output = 'Senin';
    }
    if ($input == 'Tue') {
        $output = 'Selasa';
    }
    if ($input == 'Wed') {
        $output = 'Rabu';
    }
    if ($input == 'Thu') {
        $output = 'Kamis';
    }
    if ($input == 'Fri') {
        $output = 'Jumat';
    }
    if ($input == 'Sat') {
        $output = 'Sabtu';
    }
    return $output;
}

function namahari_tanggal_($in,$show_time=false)
    {
        $tgl = substr($in,8,2);
        $bln = substr($in,5,2);
        $thn = substr($in,0,4);
       
            $hour = substr($in,11,2);
            $min = substr($in,14,2);
            $sec = substr($in,17,2);
      
        $timestmp = mktime($hour,$min,$sec,$bln,$tgl,$thn);
        $output = hari(date('D',$timestmp)).', '.$tgl.'  '.bulan($bln).' '.$thn;
        if($show_time) $output .= ', pukul '.$hour.' : '.$min;
        return $output;
    }

    function hari_2($input)
    {
        if($input=='Sun'){$output='Minggu';}
        if($input=='Mon'){$output='Senin';}
        if($input=='Tue'){$output='Selasa';}
        if($input=='Wed'){$output='Rabu';}
        if($input=='Thu'){$output='Kamis';}
        if($input=='Fri'){$output='Jumat';}
        if($input=='Sat'){$output='Sabtu';}
        return $output;
    }

    function namahari_tanggal($tahun,$bulan,$tanggal)
    {
        $tgl = $tanggal;
        $bln = $bulan;
        $thn = $tahun;
        $timestmp = mktime('00','00','00',$bln,$tgl,$thn);
        $output = hari_2(date('D',$timestmp)).', '.$tgl.'  '.bulan($bln).' '.$thn;
        return $output;
    }

    function nama_hari($in)
    {
        $tgl = substr($in, 8, 2);
        $bln = substr($in, 5, 2);
        $thn = substr($in, 2, 2);

        $timestmp = mktime('00', '00', '00', $bln, $tgl, $thn);
        $output = hari(date('D', $timestmp));
        return $output;
    }

function hari2($input)
{
    if ($input == '1') {
        $output = 'Minggu';
    }
    if ($input == '2') {
        $output = 'Senin';
    }
    if ($input == '3') {
        $output = 'Selasa';
    }
    if ($input == '4') {
        $output = 'Rabu';
    }
    if ($input == '5') {
        $output = 'Kamis';
    }
    if ($input == '6') {
        $output = 'Jumat';
    }
    if ($input == '7') {
        $output = 'Sabtu';
    }
    return $output;
}

function jam_kerjas($jadwal)
{

    $ci = get_instance();
    $row = $ci->db->query("SELECT * FROM jam_kerja WHERE  id = '$jadwal'")->row();
    if ($row) {
        return $row->nama_singkat;
    } else {
        return '-';
    }
}

function bulan($input)
{
    if ($input == '1') {
        $output = 'Januari';
    }
    if ($input == '2') {
        $output = 'Februari';
    }
    if ($input == '3') {
        $output = 'Maret';
    }
    if ($input == '4') {
        $output = 'April';
    }
    if ($input == '5') {
        $output = 'Mei';
    }
    if ($input == '6') {
        $output = 'Juni';
    }
    if ($input == '7') {
        $output = 'Juli';
    }
    if ($input == '8') {
        $output = 'Agustus';
    }
    if ($input == '9') {
        $output = 'September';
    }
    if ($input == '10') {
        $output = 'Oktober';
    }
    if ($input == '11') {
        $output = 'November';
    }
    if ($input == '12') {
        $output = 'Desember';
    }
    return $output;
}

function bulan2($input)
{
    if ($input == '1') {
        $output = 'Jan';
    }
    if ($input == '2') {
        $output = 'Feb';
    }
    if ($input == '3') {
        $output = 'Mar';
    }
    if ($input == '4') {
        $output = 'Apr';
    }
    if ($input == '5') {
        $output = 'Mei';
    }
    if ($input == '6') {
        $output = 'Jun';
    }
    if ($input == '7') {
        $output = 'Jul';
    }
    if ($input == '8') {
        $output = 'Ags';
    }
    if ($input == '9') {
        $output = 'Sep';
    }
    if ($input == '10') {
        $output = 'Okt';
    }
    if ($input == '11') {
        $output = 'Nov';
    }
    if ($input == '12') {
        $output = 'Des';
    }
    return $output;
}

function getDatesFromRange($start, $end, $output, $format = 'Y-m-d')
{
    $array = array();
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

    foreach ($period as $date) {
        $array[] = $date->format($format);
    }

    switch ($output) {
        case 'array':
            return $array;
            break;

        case 'implode':
            return implode(",", $array);
            break;

        default:
            return $array;
            break;
    }
}

function id_izin($nip, $in)
{
    $ci = get_instance();
    $bln = substr($in, 5, 2);
    $thn = substr($in, 0, 4);
    $prefix = $nip . '_' . $thn . '' . $bln;

    $ci->db->select_max('id', 'id');
    $ci->db->like('id', $prefix, 'after');
    $query =  $ci->db->get('izin')->row_array();

    if($query){
        $id = sprintf("%02d", (substr($query['id'], -2) + 1));
        $out =    strtoupper($prefix . '' . $id);
    }else{
        $out = '-';
    }
    return $out;
}

function get_konfigurasi($id)
    {
        $ci = get_instance();
		$ci->db->where('id', $id);
		$config = $ci->db->get('konfigurasi')->row_array();
		$return = $config['value'];
		return $return;
    }
    
    function jam_menit($in)
    {
        $tgl = substr($in,8,2);
        $bln = substr($in,5,2);
        $thn = substr($in,0,4);
       
        $hour = substr($in,11,2);
        $min = substr($in,14,2);
        $sec = substr($in,17,2);
		
		if($hour && $min)
		{
			$output = $hour.':'.$min;
		}
		else
		{
			$output = '-';
		}
      
        
        return $output;
    }

    function select_jadwal($tanggal, $value){
        $ci = get_instance();
        $ci->load->model('Jam_model');
        $jamkerja = $ci->Jam_model->get_jam_kerja();

        $html = '
        <div class="col-xs-6">
        <div class="form-group">
        <select class="form-control " style="width:100px;" name="tanggal_'.$tanggal.'" id="tanggal_'.$tanggal.'">';
          
            foreach ($jamkerja as $ja)
                {
                if($ja->id == $value){
                    $x = 'selected';
                }else{
                    $x = '';
                }
                $html .= '<option '.$x.' value="'.$ja->id.'">'.$ja->nama_singkat.'</option>';
                }
           
        $html .= '</select>
            </div>
        </div>
        ';

        return $html;
    }

