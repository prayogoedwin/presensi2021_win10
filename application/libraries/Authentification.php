<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Authentification {

	// var $CI = null;

    protected $CI;
    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI =& get_instance();
		$this->CI->load->database();
		$this->dbeps = $this->CI->load->database('eps', TRUE);
    }

	
	
	
	function Login($login = NULL)
	{
	    $this->CI->load->library('user_agent'); 				
	    $username = $login['username'];
	    $password = $login['password'];
	
		$this->dbeps->select('id,nip,role');
		$this->dbeps->where('username', $username);
		$this->dbeps->where('password', md5($password));
		$this->dbeps->where('flag', '1');
		$query = $this->dbeps->get('USER_NEW');

        if ($query->num_rows() == 1)
        {
            
            $data =  $query->row_array();

            $newdata = array(
            'user_id'	=> $data['id'],
            'nip'	=> $data['nip'],
            'role'	=> $data['role'],
            'logged_in' => TRUE      
            );
                
        $this->CI->session->set_userdata($newdata);	  
        return TRUE;
        }
		else 
		{
			return FALSE;
		}
	}

	
	
	function get_user($field)
    {
		$CI =& get_instance();
		$CI->load->library('session');
				
		switch($field)
		{
			case 'id':
				$data = $CI->session->userdata('user_id');
			break;
			
			case 'nip':
				$data = $CI->session->userdata('nip');
			break;
			
			case 'role':
				$data = $CI->session->userdata('role');
			break;
			
			default:
				$data = $CI->session->userdata('user_id');
			break;
		}
		
		return $data;
			
	}
 
	
    function Logout() 
	{
		
		//$delete_prev_month = $this->CI->db->where('datetime <', date("Y-m"));
		//$delete_prev_month = $this->CI->db->delete('token');
		
		//$delete_token = $this->CI->db->where('id', $this->CI->session->userdata('token'));
		//$delete_token = $this->CI->db->delete('token');
		
		$this->CI->session->sess_destroy();	
		return TRUE;
	}
	
	
	function Check_user_authentification()
    {
		$CI =& get_instance();
		$CI->load->library('session');
		
		$this->dbeps->where('id',  $this->get_user('id'));
		$this->dbeps->where('flag', '1');
		$count = $this->dbeps->get('USER_NEW')->num_rows();
		
		if(!($CI->session->userdata('user_id') && $CI->session->userdata('logged_in') && $count == 1))
		{
			
			$this->logout();
			$data = array(
				'error' => 'Anda harus login terlebih dahulu !'
			);
			$CI->session->set_flashdata($data);
			redirect('login');
 		}
		
    }
	
	function Check_modules_user($module)
    {
		$CI =& get_instance();
		$CI->load->library('session');
		
		$role = $this->get_user('role');
		
		$this->CI->db->select('privilege.access');
		$this->CI->db->where('id', $role);
		$this->CI->db->where('status', '1');
		$result = $this->CI->db->get('privilege')->row_array();
			
		if( !(in_array($module,explode(",",$result['access']))) ) 
			{
				$data = array(
					'error' => "Anda tidak berhak mengakses halaman ini"
				);
				$CI->session->set_flashdata($data);
				redirect('home');
			}
			
			else
		{
		return TRUE;	
			
		}
    }
	
	
	
}

?>
