<?php

class Fungsi_model extends CI_Model {


	
	function get_opd_list_($A_01)
	{	
        if($A_01 == ''){
            return $this->dbeps->query("SELECT * FROM TABLOKB08 WHERE A_02 = '00' AND A_03 = '00' AND A_04 = '00' AND A_05 = '00' AND AKTIF = 1 and A_01 in ('B2','C0','E0')")->result();
        }else{
            return $this->dbeps->query("SELECT * FROM TABLOKB08 WHERE A_01 = '$A_01' AND A_02 = '00' AND A_03 = '00' AND A_04 = '00' AND A_05 = '00' AND AKTIF = 1 and A_01 in ('B2','C0','E0')")->result();
        }
		
	}

    function get_opd_lis__t($A_01)
	{	
        if($A_01 == ''){
            return $this->dbeps->query("SELECT * FROM TABLOKB08 WHERE A_02 = '00' AND A_03 = '00' AND A_04 = '00' AND A_05 = '00' AND AKTIF = 1 and A_01 in ('B2','C0','E0', 'D5')")->result();
        }else{
            return $this->dbeps->query("SELECT * FROM TABLOKB08 WHERE A_01 = '$A_01' AND A_02 = '00' AND A_03 = '00' AND A_04 = '00' AND A_05 = '00' AND AKTIF = 1 and A_01 in ('B2','C0','E0', 'D5')")->result();
        }
		
	}

    function get_opd_list($A_01)
	{	
        if($A_01 == ''){
            return $this->dbeps->query("SELECT * FROM TABLOKB08 WHERE A_02 = '00' AND A_03 = '00' AND A_04 = '00' AND A_05 = '00' AND AKTIF = 1 and A_01 in ('B2','E0', 'D5')")->result();
        }else{
            return $this->dbeps->query("SELECT * FROM TABLOKB08 WHERE A_01 = '$A_01' AND A_02 = '00' AND A_03 = '00' AND A_04 = '00' AND A_05 = '00' AND AKTIF = 1 and A_01 in ('D5')")->result();
        }
		
	}

    function get_pns_list($A_01)
	{	
       
            return $this->dbeps->query("SELECT * FROM MASTFIP08 WHERE A_01 = '$A_01'AND I_JB NOT LIKE 'LOKASI PENSIUN%' ")->result();
        
		
	}


        public function getDataPns($nip){
            $x = '%'.$nip.'%';
            $sql =
            "SELECT CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`) full_A, mastfip.B_02B id, CONCAT(mastfip.`B_03A`, ' ', mastfip.`B_03`, ' ', mastfip.`B_03B`) name, mastfip.`I_5A`, mastfip.`I_05`, pangkat.`KODE` kode_gol, pangkat.`NAMAY` pkt, pangkat.`NAMAX` gol, tablok.`NALOKP` lokasi, tablok8.`nm` unit_kerja, mastfip.`I_06` esel
            FROM MASTFIP08 mastfip
            JOIN `TABPKT` pangkat ON mastfip.`F_03` = pangkat.`KODE`
            JOIN `TABLOKB08` tablok ON mastfip.`A_01` = tablok.`A_01`
            JOIN `TABLOK08` tablok8 ON mastfip.`A_01` = tablok8.`kd`
            WHERE 
            (mastfip.`B_02B` = ? OR mastfip.`B_03` LIKE ?)
            AND tablok.`KOLOK` = CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`)";
    
            $query = $this->dbeps->query($sql, array($nip, $x));
            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }
    
            return $data;
        }

        public function getDataPns_skpd($nip, $A_01){
            $x = '%'.$nip.'%';
            $sql =
            "SELECT CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`) full_A, mastfip.B_02B id, CONCAT(mastfip.`B_03A`, ' ', mastfip.`B_03`, ' ', mastfip.`B_03B`) name, mastfip.`I_5A`, mastfip.`I_05`, pangkat.`KODE` kode_gol, pangkat.`NAMAY` pkt, pangkat.`NAMAX` gol, tablok.`NALOKP` lokasi, tablok8.`nm` unit_kerja, mastfip.`I_06` esel
            FROM MASTFIP08 mastfip
            JOIN `TABPKT` pangkat ON mastfip.`F_03` = pangkat.`KODE`
            JOIN `TABLOKB08` tablok ON mastfip.`A_01` = tablok.`A_01`
            JOIN `TABLOK08` tablok8 ON mastfip.`A_01` = tablok8.`kd`
            WHERE 
            (mastfip.`B_02B` = ? OR mastfip.`B_03` LIKE ?)
            AND mastfip.`A_01` = ?
            AND tablok.`KOLOK` = CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`)";
    
            $query = $this->dbeps->query($sql, array($nip, $x, $A_01));
            if ($query->num_rows() > 0) {
                $data = $query->result();
            } else {
                $data = array();
            }
    
            return $data;
        }

	
	
}
?>
