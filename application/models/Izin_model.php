<?php

class Izin_model extends CI_Model {

	var $data;

    public function __construct(){
		parent::__construct();
        $this->dbeps = $this->load->database('eps', TRUE);
		//  is_login();
	}

    function get_all_by($opd, $unit)
	{
		$this->db->select('c.B_03 as nama, b.nama as tipe,a.nip,a.id,a.deskripsi,IFNULL(a.lampiran,"") as lampiran,DATE_FORMAT(a.tanggal_awal,"%d/%m/%Y") as tanggal_awal,DATE_FORMAT(a.tanggal_akhir,"%d/%m/%Y") as tanggal_akhir,a.status,a.validation_user,a.validation_datetime,a.status,a.no_surat, a.create_datetime');
		$this->db->JOIN('tipe b','a.tipe = b.id','INNER');
		$this->db->JOIN('eps.MASTFIP08 c','a.nip = c.B_02B','LEFT');
		
		if($unit != ''){
            $this->db->where('a.unit_kerja',$unit);
        }else{
            $a_01 = substr($opd, 0, 2);
            $this->db->like('a.unit_kerja',$a_01,'BEFORE');
        }

        return $this->db->get('izin a');
		
	}

    function get_all_saya($nip)
	{
		$this->db->select('c.B_03 as nama, b.nama as tipe,a.nip,a.id,a.deskripsi,IFNULL(a.lampiran,"") as lampiran,DATE_FORMAT(a.tanggal_awal,"%d/%m/%Y") as tanggal_awal,DATE_FORMAT(a.tanggal_akhir,"%d/%m/%Y") as tanggal_akhir,a.status,a.validation_user,a.validation_datetime,a.status,a.no_surat, a.create_datetime');
		$this->db->JOIN('tipe b','a.tipe = b.id','INNER');
		$this->db->JOIN('eps.MASTFIP08 c','a.nip = c.B_02B','LEFT');
        $this->db->where('a.nip',$nip);
        return $this->db->get('izin a');	
	}

	function get_data_by_id($id)
	{
		$this->db->select('a.tipe as tipe, b.nama as tipe_l,a.nip,a.id,a.deskripsi,a.lampiran,DATE_FORMAT(a.tanggal_awal,"%d/%m/%Y") as tanggal_awal,DATE_FORMAT(a.tanggal_akhir,"%d/%m/%Y") as tanggal_akhir,a.status,a.validation_user,a.validation_datetime,a.status,a.catatan,a.no_surat');
		$this->db->JOIN('tipe b','a.tipe = b.id','INNER');
		$this->db->where('a.id', $id);
		//$this->db->where('a.status', '0');
		return $this->db->get('izin a');
		
	}

	function get_data_by($id)
	{
		$this->db->where('a.id', $id);
		//$this->db->where('a.status', '0');
		return $this->db->get('izin a');
		
	}


    function get_all_tipe($group)
	{
        $this->db->where('group', $group);
        $this->db->where('status', 1);
        return $this->db->get('tipe');	
	}

    function insert_data($data)
	{
		$insert = $this->db->insert('izin', $data);
		return $insert;
	}

	function insert_log($data)
	{
		$insert = $this->db->insert('izin_log', $data);
		return $insert;
	}

	

	
    function update_data($id, $data)
    {
        $this->db->where('id', $id);
        $update = $this->db->update('izin', $data);
        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    function delete_data($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('izin');
		return $delete;
	}
	
	function delete_data_detail($parent_id){
		$this->db->where('parent_id', $parent_id);
		$delete = $this->db->delete('izin_detail');
		return $delete;
	}

    function insert_detail($id,$parent_id,$tanggal)
	{
		$this->data = array(
			'id' => $id,
			'parent_id' => $parent_id,
			'tanggal' => $tanggal
		);
		$insert = $this->db->insert('izin_detail', $this->data);
		return $insert;
	}

	function fill_data2($act,$id='',$nip,$unit_kerja,$tanggal)
	{
		$this->data = array(
			'tanggal_awal' => $this->input->post('awal'),
			'tanggal_akhir' => $this->input->post('akhir'),
			'tanggal' => $tanggal,
			'status' => '1',
			'tipe' => $this->input->post('tipe'),
			'lampiran' => $this->input->post('lampiran'),
			'deskripsi' => $this->input->post('deskripsi'),
			'update_datetime' => date("Y-m-d H:i:s"),
			'update_user' => $this->authentification->get_user('nip'),
			'validation_user' => $this->authentification->get_user('nip'),
			'validation_datetime' => date("Y-m-d H:i:s"),
			'no_surat' => $this->input->post('no_surat')
			
		);
		if ($act == 'insert')
		{
			$this->data['id'] = $id;
			$this->data['unit_kerja'] = $unit_kerja;
			$this->data['nip'] = $nip;
			$this->data['create_user'] = $this->authentification->get_user('nip');
			$this->data['create_datetime'] =  date("Y-m-d H:i:s");
		}
	}

	function insert_data2()
	{
		$insert = $this->db->insert('izin', $this->data);
		return $insert;
	}

	function get_pegawai_nip($nip)
	{
		$this->dbeps->select('b.nm as  instansi,
		a.A_01 as opd,
		a.I_JB as jabatan,
		CONCAT(a.A_01,a.A_02) as upt,
		CONCAT(a.A_01,a.A_02,a.A_03) as ruang,
		CONCAT(a.A_01,a.A_02,a.A_03,a.A_04) as satker,
		CONCAT(a.A_01,a.A_02,a.A_03,a.A_04,a.A_05) as unit_kerja,
		a.B_02B as nip,CONCAT(a.B_03A," ",a.B_03," ",a.B_03B) as nama');
		$this->dbeps->JOIN('TABLOK08 b','a.A_01 = b.kd','INNER');
		$this->dbeps->WHERE('a.B_02B',$nip);
		
		 return $this->dbeps->get('MASTFIP08 a');
	}

   

}
?>