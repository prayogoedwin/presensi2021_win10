<?php

class Jadwal_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->dbeps = $this->load->database('eps', TRUE);
        //  is_login();
    }



    function contoh()
    {
        // $this->DB_simpeg->select('B_02B as nip,CONCAT(A_01,A_02,A_03,A_04,A_05) as unit_kerja,CONCAT(B_03A,IF(B_03A!="",". ",""),B_03,IF(B_03B!="",", ",""),B_03B) as nama');
        // $this->DB_simpeg->like('CONCAT(A_01,A_02,A_03,A_04,A_05)',$unit_kerja,'AFTER');
        // if (substr($unit_kerja,0,2) == 'D0' && substr($unit_kerja,6,2) < '40') 
        // 	$this->DB_simpeg->where('A_04 <','40');
        // //$this->DB_simpeg->limit(10);
        // //$this->DB_simpeg->group_by('B_02B');
        // $this->DB_simpeg->order_by('I_06,A_01,A_02,A_03,A_04,A_05,F_03,B_02B desc');
        // if ($limit != '') $this->DB_simpeg->limit($limit,$offset);
        // return $this->DB_simpeg->get('MASTFIP08');

        // if($unit_kerja == 'A2')
        // 	{
        // 		$this->DB_simpeg->select('CONCAT(A_01,A_02,A_03) as kode,NALOKP as nama');
        // 		$this->DB_simpeg->WHERE('ESEL','22');
        // 	}
        // 	elseif($unit_kerja == 'D0')
        // 	{
        // 		$this->DB_simpeg->select("IF(A_04<'40',CONCAT(A_01,A_02,A_03),CONCAT(A_01,A_02,A_03,A_04)) as kode,NALOKP as nama");
        // 		$in_array = array('31','32','88');
        // 		$this->DB_simpeg->where_in('ESEL',$in_array);
        // 	}
        // 	elseif($unit_kerja == '86')
        // 	{
        // 		$this->DB_simpeg->select('CONCAT(A_01,A_02,A_03,A_04) as kode,NALOKP as nama');
        // 		$in_array = array('31','41','88');
        // 		$this->DB_simpeg->where_in('ESEL',$in_array);
        // 	}
        // 	else
        // 	{
        // 		$this->DB_simpeg->select('CONCAT(A_01,A_02,A_03) as kode,NALOKP as nama');
        // 		$in_array = array('22','31','32');
        // 		//$this->DB_simpeg->where_in('ESEL',$in_array);
        // 		$this->DB_simpeg->where('(ESEL="22" or ESEL="31" or ESEL="32" or (ESEL="41" and KOLOK like "%000000") or KOLOK like "%00000000")');
        // 	}
    }

    function get_pegawai_opd($a_01)
    {
        $this->dbeps->select('B_02B as nip, CONCAT(A_01,A_02,A_03,A_04,A_05) as unit_kerja, CONCAT(B_03A,IF(B_03A!="",". ",""),B_03,IF(B_03B!="",", ",""),B_03B) as nama_lengkap');
        $this->dbeps->where('A_01', $a_01);
        if ($a_01 == 'D0') {
            $this->dbeps->where('A_04 <', 40);
        }
        $this->dbeps->order_by('I_06,A_01,A_02,A_03,A_04,A_05,F_03,B_02B desc');
        return $this->dbeps->get('MASTFIP08')->result();
    }

    function get_pegawai_unit($a_01, $a_02, $a_03, $a_04, $a_05)
    {
        // $unit_kerja = $a_01.$a_02.$a_03.$a_04.$a_05;
        $a_05;
        $this->dbeps->select('B_02B as nip, CONCAT(A_01,A_02,A_03,A_04,A_05) as unit_kerja, CONCAT(B_03A,IF(B_03A!="",". ",""),B_03,IF(B_03B!="",", ",""),B_03B) as nama_lengkap');
        if ($a_01 == 'A2') {
            $this->dbeps->where('A_01', $a_01);
            $this->dbeps->where('A_02', $a_02);
            $this->dbeps->where('A_03', $a_03);
            $this->dbeps->WHERE('I_06', '22');
        } elseif ($a_01 == 'D0') {
            if ($a_04 < 40) {
                $this->dbeps->where('A_01', $a_01);
                $this->dbeps->where('A_02', $a_02);
                $this->dbeps->where('A_03', $a_03);
                $this->dbeps->where('A_04 <', 40);
            } else {
                $this->dbeps->where('A_01', $a_01);
                $this->dbeps->where('A_02', $a_02);
                $this->dbeps->where('A_03', $a_03);
                $this->dbeps->where('A_04 >=', 40);
            }
            $in_array = array('31', '32', '88');
            $this->dbeps->where_in('I_06', $in_array);
        } elseif ($a_01 == '86') {
            $this->dbeps->where('A_01', $a_01);
            $this->dbeps->where('A_02', $a_02);
            $this->dbeps->where('A_03', $a_03);
            $this->dbeps->where('A_04', $a_04);
            $in_array = array('31', '41', '88');
            $this->dbeps->where_in('I_06', $in_array);
        } else {
            $this->dbeps->where('A_01', $a_01);
            $this->dbeps->where('A_02', $a_02);
            $this->dbeps->where('A_03', $a_03);
        }

        $this->dbeps->order_by('A_01,A_02,A_03,A_04,A_05,I_06,F_03,B_02B desc');
        return $this->dbeps->get('MASTFIP08')->result();
    }

    function get_pegawai_opd_v2($a_01, $array_sudah)
    {

        $this->dbeps->select('B_02B as nip, CONCAT(A_01,A_02,A_03,A_04,A_05) as unit_kerja, CONCAT(B_03A,IF(B_03A!="",". ",""),B_03,IF(B_03B!="",", ",""),B_03B) as nama_lengkap');
        if ($array_sudah != 0) {
            foreach ($array_sudah as $as) {
                $this->dbeps->where('B_02B !=', $as->nip);
            }
        }
        $this->dbeps->where('A_01', $a_01);
        if ($a_01 == 'D0') {
            $this->dbeps->where('A_04 <', 40);
        }

        $this->dbeps->order_by('I_06,A_01,A_02,A_03,A_04,A_05,F_03,B_02B desc');
        return $this->dbeps->get('MASTFIP08')->result();
    }

    function get_pegawai_unit_v2($a_01, $a_02, $a_03, $a_04, $a_05, $array_sudah)
    {
        // $a = implode(',', $array_sudah);
        // $unit_kerja = $a_01.$a_02.$a_03.$a_04.$a_05;
        $this->dbeps->select('B_02B as nip, CONCAT(A_01,A_02,A_03,A_04,A_05) as unit_kerja, CONCAT(B_03A,IF(B_03A!="",". ",""),B_03,IF(B_03B!="",", ",""),B_03B) as nama_lengkap');

        if ($array_sudah != 0) {
            foreach ($array_sudah as $as) {
                $this->dbeps->where('B_02B !=', $as->nip);
            }
        }
        // $this->dbeps->where_not_in('B_02B', $a);


        if ($a_01 == 'A2') {
            $this->dbeps->where('A_01', $a_01);
            $this->dbeps->where('A_02', $a_02);
            $this->dbeps->where('A_03', $a_03);
            $this->dbeps->WHERE('I_06', '22');
        } elseif ($a_01 == 'D0') {
            if ($a_04 < 40) {
                $this->dbeps->where('A_01', $a_01);
                $this->dbeps->where('A_02', $a_02);
                $this->dbeps->where('A_03', $a_03);
                $this->dbeps->where('A_04 <', 40);
            } else {
                $this->dbeps->where('A_01', $a_01);
                $this->dbeps->where('A_02', $a_02);
                $this->dbeps->where('A_03', $a_03);
                $this->dbeps->where('A_04 >=', 40);
            }
            $in_array = array('31', '32', '88');
            $this->dbeps->where_in('I_06', $in_array);
        } elseif ($a_01 == '86') {
            $this->dbeps->where('A_01', $a_01);
            $this->dbeps->where('A_02', $a_02);
            $this->dbeps->where('A_03', $a_03);
            $this->dbeps->where('A_04', $a_04);
            $in_array = array('31', '41', '88');
            $this->dbeps->where_in('I_06', $in_array);
        } else {
            $this->dbeps->where('A_01', $a_01);
            $this->dbeps->where('A_02', $a_02);
            $this->dbeps->where('A_03', $a_03);
        }


        $this->dbeps->order_by('A_01,A_02,A_03,A_04,A_05,I_06,F_03,B_02B desc');
        return $this->dbeps->get('MASTFIP08')->result();
    }

    function count_presensi_by_tahun_bulan($opd, $unit=null, $bulan, $tahun)
    {
        if(empty($unit)){
            return $this->db->query("SELECT  COUNT(id) as hit FROM parent_presensi WHERE opd = '$opd' AND bulan = '$bulan' AND tahun = '$tahun'")->row();
        } else{
            return $this->db->query("SELECT  COUNT(id) as hit FROM parent_presensi WHERE opd = '$opd' AND unit = '$unit' AND bulan = '$bulan' AND tahun = '$tahun'")->row();
    
        }    
    }

    function get_presensi_by_tahun_bulan($opd, $unit, $bulan, $tahun)
    {
        return $this->db->query("SELECT  * FROM parent_presensi WHERE opd = '$opd' AND unit = '$unit' AND bulan = '$bulan' AND tahun = '$tahun'")->row();
    }

    function get_presensi_by_parent($id_parent)
    {
        return $this->db->query("SELECT a.*, b.nama_singkat as j1, c.nama_singkat j2 FROM presensi a 
        INNER JOIN jam_kerja b ON b.id = a.j_01
        INNER JOIN jam_kerja c ON c.id = a.j_02
        WHERE a.id_parent = '$id_parent' ")->result();
    }



    function sudah_dijadwal($opd, $tahun, $bulan)
    {
        $tb = $tahun . $bulan . '_';
        return $this->db->query("SELECT b.opd, b.unit, a.id, a.nip FROM  presensi a INNER JOIN parent_presensi b ON a.id_parent = b.id  WHERE b.opd = '$opd' AND a.id LIKE '$tb%'")->result();
    }

    function sudah_dijadwal_v2($tahun, $bulan)
    {
        $tb = $tahun . $bulan . '_';
        return $this->db->query("SELECT a.id, a.nip FROM  presensi a  WHERE a.id LIKE '$tb%'")->result();
    }

    function sudah_dijadwal_v3($opd, $tahun, $bulan)
    {
        $a_01 = substr($opd, 0, 2);
        $tb = $tahun . $bulan . '_';
        $tb1 = $tahun . $bulan;

        // FROM A.table1 t1 JOIN B.table2 t2 ON t2.column2 = t1.column1;

        return $this->db->query("SELECT presensi2021.presensi.id_presensi, presensi2021.presensi.nip as nip
        FROM presensi2021.presensi JOIN eps.MASTFIP08 ON eps.MASTFIP08.B_02B = presensi2021.presensi.nip
        WHERE eps.MASTFIP08.A_01 = '$a_01' AND presensi2021.presensi.tahun_bulan = '$tb1'
         ")->result();
    }

    function terjadwal_opd($opd, $tahun, $bulan)
    {
        $a_01 = substr($opd, 0, 2);
        $tb = $tahun . $bulan . '_';
        $tb1 = $tahun . $bulan;

        // FROM A.table1 t1 JOIN B.table2 t2 ON t2.column2 = t1.column1;

        return $this->db->query("SELECT presensi2021.presensi.*
        FROM presensi2021.presensi JOIN eps.MASTFIP08 ON eps.MASTFIP08.B_02B = presensi2021.presensi.nip
        WHERE eps.MASTFIP08.A_01 = '$a_01' AND presensi2021.presensi.tahun_bulan = '$tb1'
         ")->result();
    }






    function insert_parent_data($generate)
    {
        $insert = $this->db->insert('parent_presensi', $generate);
        $inser_id = $this->db->insert_id();
        if ($insert) {
            return  $inser_id;
        } else {
            return FALSE;
        }
    }

    // insert batch
    function insert_batch_jadwal($data)
    {
        $add = $this->db->insert_batch('presensi', $data);
        if ($add) {
            return true;
        } else {
            return false;
        }
    }

    // insert batch
    function insert_batch_jadwal_v2($data, $array_sudah)
    {
        $this->dbeps->where_not_in('B_02B', $array_sudah);
        $add = $this->db->insert_batch('presensi', $data);
        if ($add) {
            return true;
        } else {
            return false;
        }
    }

    function update_jadwal($id, $data)
    {
        $this->db->where('id_presensi', $id);
        $update = $this->db->update('presensi', $data);
        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    function get_jadwal_perorangan($tb_nip)
    {
        return $this->db->query("SELECT a.* FROM presensi a WHERE a.id = '$tb_nip'");
    }
}
