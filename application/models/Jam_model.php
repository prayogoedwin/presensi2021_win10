<?php

class Jam_model extends CI_Model {


	
	function get_all_data()
	{		
		return $this->db->query("SELECT * FROM jam_kerja")->result();
	}
	
	function get_jam_kerja($opd = '')
	{
		$this->db->select('id,nama_singkat,nama,deskripsi,jadwal_masuk,jadwal_keluar,jadwal_tengah,absen_buka,absen_tutup,selisih_hari,waktu_kerja');
		$this->db->where('status', '1');
		if ($opd != '') {
			$this->db->where('skpd', '');
			$this->db->or_like('skpd',$opd);
		}else{
			$this->db->where('skpd', '');
		}
		return $this->db->get('jam_kerja')->result();	
	}
	
	
}
?>