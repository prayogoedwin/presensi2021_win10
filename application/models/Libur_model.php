<?php

class Libur_model extends CI_Model {

	var $data;
	
	function get_libur($year,$month)
	{
				
		$this->db->select('DAY(a.tanggal) as day,DATE_FORMAT(a.tanggal,"%Y-%m-%d") as tanggal, b.nama');
		$this->db->JOIN('hari_libur b','a.parent_id = b.id','INNER');
		$this->db->group_by('DAY(a.tanggal)');
		$this->db->where('YEAR(a.tanggal)',$year);
		$this->db->where('MONTH(a.tanggal)',$month);
		return $this->db->get('hari_libur_detail a');
	}
	
	function get_detail_by_date($tanggal)
	{
		$this->db->select('b.id,b.tanggal_awal,b.tanggal_akhir,b.nama,b.keterangan');
		$this->db->JOIN('hari_libur b','a.parent_id = b.id','INNER');
		$this->db->where('DATE_FORMAT(a.tanggal,"%Y-%m-%d")', $tanggal);
		return $this->db->get('hari_libur_detail a');
		
	}
	
	function fill_data($act,$id='',$tanggal)
	{
		$this->data = array(
			'id' => $id,
			'tanggal_awal' => $this->input->post('tanggal_awal'),
			'tanggal_akhir' => $this->input->post('tanggal_akhir'),
			'tanggal' => $tanggal,
			'nama' => $this->input->post('nama'),
			'keterangan' => $this->input->post('keterangan'),
			'create_datetime' => date("Y-m-d H:i:s"),
			'create_user' => $this->authentification->get_user('nip'),
			'update_datetime' => date("Y-m-d H:i:s"),
			'update_user' => $this->authentification->get_user('nip')
		);
		if ($act == 'insert')
		{
			$this->data['id'] = $id;
		}
	}
	
	function insert_data()
	{
		$insert = $this->db->insert('hari_libur', $this->data);
		return $insert;
	}
	
	function update_data($id)
	{
		$this->db->where('id', $id);
		$update = $this->db->update('hari_libur', $this->data);
		return $update;
	}
	
	
	function insert_detail($id,$parent_id,$tanggal)
	{
		$this->data = array(
			'id' => $id,
			'parent_id' => $parent_id,
			'tanggal' => $tanggal
		);
		$insert = $this->db->insert('hari_libur_detail', $this->data);
		return $insert;
	}
	
	function delete_data($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('hari_libur');
		return $delete;
	}
	
	function delete_data_detail($parent_id){
		$this->db->where('parent_id', $parent_id);
		$delete = $this->db->delete('hari_libur_detail');
		return $delete;
	}

	function delete_detail($tanggal){
		$this->db->where('tanggal', $tanggal);
		$delete = $this->db->delete('hari_libur_detail');
		return $delete;
	}

	

}

