<?php

class Presensi_model extends CI_Model {


	public function __construct(){
		parent::__construct();
    	$this->dbeps = $this->load->database('eps', TRUE);
		$this->dbadms = $this->load->database('adms', TRUE);
		//  is_login();
	}

	
	function get_all_data()
	{		
		return $this->db->query("SELECT * FROM presensi")->result();
	}

	function get_check_wfh($userid,$date)
	{
		$this->dbadms->select('a.checktime');		
		$this->dbadms->where('a.userid',$userid);
		$this->dbadms->where('DATE_FORMAT(a.checktime,"%Y-%m-%d")',$date);
		$query = $this->dbadms->get('checkinout a')->num_rows();
		return $query;
	}

	function get_userinfo($pegawai)
	{
		$this->dbadms->select('a.name,a.street,a.userid');		
		$this->dbadms->where_in('a.street',$pegawai);		
		return $this->dbadms->get('userinfo a');
	}

	function get_izin($unit_kerja,$tahun,$bulan)
	{
		$a_01 = substr($unit_kerja, 0, 2);
		// $a_01 = 'B2';
		$this->db->select('a.nip,a.tipe,b.tanggal');
		$this->db->join('izin_detail b','a.id = b.parent_id ','INNER');		
		//$this->db->like('b.id', $tahun.''.$bulan,'BOTH');
		$this->db->like('a.unit_kerja', $unit_kerja,'AFTER');
		return $this->db->get('izin a');

		// return $this->db->query("SELECT presensi2021.izin.id, presensi2021.izin.nip, presensi2021.izin.unit_kerja, presensi2021.izin.tipe, presensi2021.izin_detail.tanggal FROM presensi2021.izin 
		// JOIN eps.MASTFIP08 ON eps.MASTFIP08.B_02B = presensi2021.izin.nip 
		// JOIN presensi2021.izin_detail ON presensi2021.izin_detail.parent_id = presensi2021.izin.id
		// WHERE eps.MASTFIP08.A_01 = 'B2' AND YEAR(presensi2021.izin_detail.tanggal) = 2021 
		// AND MONTH(presensi2021.izin_detail.tanggal) = 07
		// ");

		

		// return $this->db->query("SELECT presensi2021.izin.*
		// FROM presensi2021.izin 
		// JOIN eps.MASTFIP08 ON eps.MASTFIP08.B_02B = presensi2021.izin.nip
		// JOIN presensi2021.izin_detail ON presensi2021.izin_detail.parent_id = presensi2021.izin.id
		// WHERE eps.MASTFIP08.A_01 = '$a_01'
		// AND YEAR(presensi2021.izin_detail.tanggal) = $tahun
		// AND MONTH(presensi2021.izin_detail.tanggal) = $bulan
		// ");

// return $this->db->query("SELECT presensi2021.presensi.*
// FROM presensi2021.presensi 
// JOIN eps.MASTFIP08 ON eps.MASTFIP08.B_02B = presensi2021.presensi.nip
// WHERE eps.MASTFIP08.A_01 = '$a_01' AND presensi2021.presensi.tahun_bulan = '$tb1'
//  ")->result();


		
	}

	function get_masuk($userid,$absen_buka,$jadwal_tengah)
	{
		$this->dbadms->select('DATE_FORMAT(a.checktime,"%Y-%m-%d %H:%i:%s") as checktime');		
		$this->dbadms->where('a.userid',$userid);
		$this->dbadms->where('a.checktime >=',$absen_buka);
		$this->dbadms->where('a.checktime <=',$jadwal_tengah);
		$this->dbadms->where('a.checktype !=','9');
		$this->dbadms->where('a.Reserved not like','%WFH%');
		$this->dbadms->where('a.Reserved not like','%TL%');
		$this->dbadms->order_by('a.checktime','ASC');
		$this->dbadms->limit(1);			
		$query = $this->dbadms->get('checkinout a')->row_array();
		return $query['checktime'];
	}
	
	function get_pulang($userid,$jadwal_tengah,$absen_tutup)
	{
		$this->dbadms->select('DATE_FORMAT(a.checktime,"%Y-%m-%d %H:%i:%s") as checktime');		
		$this->dbadms->where('a.userid',$userid);
		$this->dbadms->where('a.checktime >=',$jadwal_tengah);
		$this->dbadms->where('a.checktime <=',$absen_tutup);
		$this->dbadms->where('a.checktype !=','9');
		$this->dbadms->where('a.Reserved not like','%WFH%');
		$this->dbadms->where('a.Reserved not like','%TL%');
		$this->dbadms->order_by('a.checktime','DESC');
		$this->dbadms->limit(1);			
		$query = $this->dbadms->get('checkinout a')->row_array();
		return $query['checktime'];
	}
	
	
	
}
?>