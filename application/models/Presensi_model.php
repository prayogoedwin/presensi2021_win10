<?php

class Presensi_model extends CI_Model {


	public function __construct(){
		parent::__construct();
    	$this->dbeps = $this->load->database('eps', TRUE);
		$this->dbadms = $this->load->database('adms', TRUE);
		//  is_login();
	}

	
	function get_all_data()
	{		
		return $this->db->query("SELECT * FROM presensi")->result();
	}

	function get_all_wfh($users =[], $month = null, $year = null)
	{
		$this->dbadms->select('a.userid, DATE_FORMAT(a.checktime,"%Y-%m-%d") as checktime');
		if(count($users) > 0){	
		$this->dbadms->where_in('a.userid',$users);
		}	
		if(!empty($month)){
		$this->dbadms->where('month(a.checktime) =',$month);
		}
		if(!empty($year)){
		$this->dbadms->where('year(a.checktime) =',$year);
		}
		$this->dbadms->distinct();
		$query = $this->dbadms->get('checkinout a')->result_array();
		#echo $this->dbadms->last_query();die();
		return $query;
	}

	function get_check_wfh($userid,$date)
	{
		$this->dbadms->select('a.checktime');		
		$this->dbadms->where('a.userid',$userid);
		$this->dbadms->where('DATE_FORMAT(a.checktime,"%Y-%m-%d")',$date);
		$query = $this->dbadms->get('checkinout a')->num_rows();
		return $query;
	}

	function get_userinfo($pegawai)
	{
		$this->dbadms->select('a.name,a.street,a.userid');		
		$this->dbadms->where_in('a.street',$pegawai);		
		return $this->dbadms->get('userinfo a');
	}
	
	function simpan_harian($nip,$thn,$bln,$tgl)
	{
		$this->data = array(
			'am_'.$tgl => $this->input->post('tanggal_masuk').' '.$this->input->post('absen_masuk_jam').':'.$this->input->post('absen_masuk_menit').':00',
			'ap_'.$tgl => $this->input->post('tanggal_pulang').' '.$this->input->post('absen_pulang_jam').':'.$this->input->post('absen_pulang_menit').':00',
			's_'.$tgl => $this->input->post('status')
		);
		
		$this->db->where('id', $thn.''.$bln.'_'.$nip);
		$update = $this->db->update('presensi', $this->data);
		return $update;
	}
	
	function get_pegawai($unit_kerja,$tahun,$bulan)
	{
		$this->db->select('a.nip');		
		$this->db->JOIN('parent_presensi b','a.id_parent = b.id','INNER');
		$this->db->like('a.id', $tahun.''.$bulan,'AFTER');
		$this->db->like('b.opd', $unit_kerja,'AFTER');
		$this->db->group_by('a.nip');
		$this->db->order_by('b.opd','ASC');
		return $this->db->get('presensi a');
	}
	
	function get_status()
	{
		$array=array('0','1','4');
		$this->db->select('a.id,a.nama');		
		$this->db->where_in('a.group', $array);
		$this->db->order_by('a.urut', 'ASC');
		return $this->db->get('tipe a');
	}
	
	
	function get_presensi($unit_kerja,$tahun,$bulan)
	{
		$this->db->select('a.*');		
		$this->db->JOIN('parent_presensi b','a.id_parent = b.id','INNER');
		$this->db->like('a.id', $tahun.''.$bulan,'AFTER');
		$this->db->like('b.opd', $unit_kerja,'AFTER');
		if (substr($unit_kerja,0,2) == 'D0' && substr($unit_kerja,6,2) < '40') 
			$this->db->where('substring(b.opd,7,2) <','40');
		$this->db->group_by('a.nip');
		$this->db->order_by('b.opd','ASC');
		$d = $this->db->get('presensi a');
		#print_r($this->db->last_query());die();
		return $d;
	}
	
	function get_presensi_harian($nip,$thn,$bln,$tgl)
	{
		$this->db->select('b.selisih_hari,a.j_'.$tgl.' as jadwal,a.am_'.$tgl.' as absen_masuk,a.ap_'.$tgl.' as absen_pulang,a.s_'.$tgl.' as status ');		
		$this->db->JOIN('jam_kerja b','a.j_'.$tgl.' = b.id','INNER');
		$this->db->where('a.id', $thn.''.$bln.'_'.$nip);
		return $this->db->get('presensi a');
	}

	function get_izin($unit_kerja,$tahun,$bulan)
	{
		$a_01 = substr($unit_kerja, 0, 2);
		// $a_01 = 'B2';
		$this->db->select('a.nip,a.tipe,b.tanggal');
		$this->db->join('izin_detail b','a.id = b.parent_id ','INNER');		
		//$this->db->like('b.id', $tahun.''.$bulan,'BOTH');
		$this->db->like('a.unit_kerja', $unit_kerja,'AFTER');
		return $this->db->get('izin a');

		// return $this->db->query("SELECT presensi2021.izin.id, presensi2021.izin.nip, presensi2021.izin.unit_kerja, presensi2021.izin.tipe, presensi2021.izin_detail.tanggal FROM presensi2021.izin 
		// JOIN eps.MASTFIP08 ON eps.MASTFIP08.B_02B = presensi2021.izin.nip 
		// JOIN presensi2021.izin_detail ON presensi2021.izin_detail.parent_id = presensi2021.izin.id
		// WHERE eps.MASTFIP08.A_01 = 'B2' AND YEAR(presensi2021.izin_detail.tanggal) = 2021 
		// AND MONTH(presensi2021.izin_detail.tanggal) = 07
		// ");

		

		// return $this->db->query("SELECT presensi2021.izin.*
		// FROM presensi2021.izin 
		// JOIN eps.MASTFIP08 ON eps.MASTFIP08.B_02B = presensi2021.izin.nip
		// JOIN presensi2021.izin_detail ON presensi2021.izin_detail.parent_id = presensi2021.izin.id
		// WHERE eps.MASTFIP08.A_01 = '$a_01'
		// AND YEAR(presensi2021.izin_detail.tanggal) = $tahun
		// AND MONTH(presensi2021.izin_detail.tanggal) = $bulan
		// ");

// return $this->db->query("SELECT presensi2021.presensi.*
// FROM presensi2021.presensi 
// JOIN eps.MASTFIP08 ON eps.MASTFIP08.B_02B = presensi2021.presensi.nip
// WHERE eps.MASTFIP08.A_01 = '$a_01' AND presensi2021.presensi.tahun_bulan = '$tb1'
//  ")->result();


		
	}

	function get_all_masuk($users =[], $month = null, $year = null)
	{
		$this->dbadms->select('a.userid, DATE_FORMAT(a.checktime,"%Y-%m-%d %H:%i:%s") as checktime');
		if(count($users) > 0){	
		$this->dbadms->where_in('a.userid',$users);
		}	
		if(!empty($month)){
		$this->dbadms->where('month(a.checktime) =',$month);
		}
		if(!empty($year)){
		$this->dbadms->where('year(a.checktime) =',$year);
		}
		$this->dbadms->where('a.checktype !=','9');
		$this->dbadms->where('a.Reserved not like','%WFH%');
		$this->dbadms->where('a.Reserved not like','%TL%');
		$this->dbadms->order_by('a.userid, a.checktime','ASC');
		$query = $this->dbadms->get('checkinout a')->result_array();
		#echo'<pre>'; print_r($this->dbadms->last_query());die();
		return $query;
	}

	function get_masuk($userid,$absen_buka,$jadwal_tengah,$month = null, $year = null)
	{
		$this->dbadms->select('DATE_FORMAT(a.checktime,"%Y-%m-%d %H:%i:%s") as checktime');	
		if(!empty($userid)){	
		$this->dbadms->where('a.userid',$userid);
		}
		if(!empty($absen_buka)){
		$this->dbadms->where('a.checktime >=',$absen_buka);
		}
		if(!empty($jadwal_tengah)){
		$this->dbadms->where('a.checktime <=',$jadwal_tengah);
		}
		if(!empty($month)){
		$this->dbadms->where('month(a.checktime) =',$month);
		}
		if(!empty($year)){
		$this->dbadms->where('year(a.checktime) =',$year);
		}
		$this->dbadms->where('a.checktype !=','9');
		$this->dbadms->where('a.Reserved not like','%WFH%');
		$this->dbadms->where('a.Reserved not like','%TL%');
		$this->dbadms->order_by('a.checktime','ASC');
		$this->dbadms->limit(1);			
		$query = $this->dbadms->get('checkinout a')->row_array();
		echo'<pre>';print_r($this->dbadms->last_query());die();
		return $query['checktime'];
	}
	
	function get_all_pulang($users =[], $month = null, $year = null)
	{
		$this->dbadms->select('a.userid, DATE_FORMAT(a.checktime,"%Y-%m-%d %H:%i:%s") as checktime');
		if(count($users) > 0){	
		$this->dbadms->where_in('a.userid',$users);
		}	
		if(!empty($month)){
		$this->dbadms->where('month(a.checktime) =',$month);
		}
		if(!empty($year)){
		$this->dbadms->where('year(a.checktime) =',$year);
		}
		$this->dbadms->where('a.checktype !=','9');
		$this->dbadms->where('a.Reserved not like','%WFH%');
		$this->dbadms->where('a.Reserved not like','%TL%');
		$this->dbadms->order_by('a.userid, a.checktime','DESC');
		$query = $this->dbadms->get('checkinout a')->result_array();
		return $query;
	}
	
	function get_pulang($userid,$jadwal_tengah,$absen_tutup)
	{
		$this->dbadms->select('DATE_FORMAT(a.checktime,"%Y-%m-%d %H:%i:%s") as checktime');		
		$this->dbadms->where('a.userid',$userid);
		$this->dbadms->where('a.checktime >=',$jadwal_tengah);
		$this->dbadms->where('a.checktime <=',$absen_tutup);
		$this->dbadms->where('a.checktype !=','9');
		$this->dbadms->where('a.Reserved not like','%WFH%');
		$this->dbadms->where('a.Reserved not like','%TL%');
		$this->dbadms->order_by('a.checktime','DESC');
		$this->dbadms->limit(1);			
		$query = $this->dbadms->get('checkinout a')->row_array();
		return $query['checktime'];
	}
	
	
		
	function update_data($id,$tahun,$bulan,$nip)
	{
		#echo '<pre>';print_r($this->input->post());die();
		$this->data = array(
			'kwk' => $this->input->post('kwk_'.$nip),
			'alpha' => $this->input->post('alpha_'.$nip),
			//'terlambat' => $this->input->post('terlambat_'.$nip),
			'updated_at' => date("Y-m-d H:i:s"),
			'updated_by' => $this->authentification->get_user('nip')
		);
		
		for ($tgl = 1; $tgl <=  jumlah_hari($tahun,$bulan); $tgl++)
		{
		$tanggal = sprintf("%02d",$tgl);
		if($this->input->post('absen_masuk_'.$nip.'_'.$tanggal))$this->data['am_'.$tanggal] = $this->input->post('absen_masuk_'.$nip.'_'.$tanggal);
		if($this->input->post('absen_pulang_'.$nip.'_'.$tanggal))$this->data['ap_'.$tanggal] = $this->input->post('absen_pulang_'.$nip.'_'.$tanggal);
		if($this->input->post('status_'.$nip.'_'.$tanggal))$this->data['s_'.$tanggal] = $this->input->post('status_'.$nip.'_'.$tanggal);
		}
		
		$this->db->where('id', $id);
		$update = $this->db->update('presensi', $this->data);
		// return $update;
	}
	function updated_data($all_data)
	{
		$xdata = [];
		foreach($all_data as $k => $v){
			#echo '<pre>';print_r($v);die();
			$xdata = array(
				'kwk' => $v['kwk'],
				'alpha' => $v['alpha'],
				'updated_at' => date("Y-m-d H:i:s"),
				'updated_by' => $this->authentification->get_user('nip')
			);
			foreach($v['date'] as $tgl => $vd){
				#echo '<pre>';print_r($vd);#die();
				$tanggal = sprintf("%02d",$tgl);
				if(!empty($vd['absen_masuk'])){
					$xdata['am_'.$tanggal] = $vd['absen_masuk'];
				}
				if(!empty($vd['absen_pulang'])){
					$xdata['ap_'.$tanggal] = $vd['absen_pulang'];
				}
				if(!empty($vd['status'])){
					$xdata['s_'.$tanggal] = $vd['status'];
				}
		
			}
			#die();
			#echo '<pre>';print_r($xdata);die();
			$this->db->where('id_presensi', $v['data']->id_presensi);
			$update = $this->db->update('presensi', $xdata);
			#echo '<pre>';print_r($this->db->last_query());#die();
		}
		
		// return $update;
	}
	function save_data($parent,$tahun,$bulan,$nip,$fullname)
	{
		$this->db->where('id_parent', $parent);
		$this->db->where('tahun_bulan', $tahun.''.$bulan);
		$this->db->where('nip', $nip);
		$check = $this->db->get('presensi')->row();
		#print_r($check);
		if(empty($check->id_presensi)){
			$this->data = array(
				'id' => $tahun.''.$bulan.'_'.$nip,
				'tahun_bulan' => $tahun.''.$bulan,
				'id_parent' => $parent,
				'nip' => $nip,
				'nama_lengkap' => $fullname,
				'created_at' => date("Y-m-d H:i:s"),
				'created_by' => $this->authentification->get_user('nip')
			);	
			$presensi_id = $this->db->insert('presensi', $this->data);
		}else{
			$presensi_id = $check->id_presensi;
		}
		return $presensi_id;
	}
	public function generate_data($params){
		$userinfo = $params['userinfo'];
		$users = $params['users'];
		
		#echo '<pre>';print_r($users);
		#die();
		$wfh = $this->get_all_wfh($users,$params['bulan'],$params['tahun']);
		$group_wfh =[];
		foreach($wfh as $k => $v){
			$group_wfh[$v['userid']][$v['checktime']][] = $v;
		}
		#print_r($group_wfh);
		$plg = $this->get_all_pulang($users,$params['bulan'],$params['tahun']);
		$group_plg =[];
		foreach($plg as $k => $v){
			$group_plg[$v['userid']][] = $v;
		}
		#print_r($group_plg);
		$msk = $this->get_all_masuk($users,$params['bulan'],$params['tahun']);
		$group_msk =[];
		foreach($msk as $k => $v){
			$group_msk[$v['userid']][] = $v;
		}
		#echo '<pre>';print_r($group_msk);
		#die();
		foreach ($params['jam_kerja'] as $jam) {
			$absen_buka[$jam->id] = $jam->absen_buka;
			$nama_singkat[$jam->id] = $jam->nama_singkat;
			$jadwal_masuk[$jam->id] = $jam->jadwal_masuk;
			$jadwal_tengah[$jam->id] = $jam->jadwal_tengah;
			$jadwal_keluar[$jam->id] = $jam->jadwal_keluar;
			$absen_tutup[$jam->id] = $jam->absen_tutup;
			$selisih_hari[$jam->id] = $jam->selisih_hari;
			$waktu_kerja[$jam->id] = $jam->waktu_kerja;
		} 
		foreach ($params['izin_data']->result() as $izin) {
			$arr_izin[$izin->nip][$izin->tanggal] = $izin->tipe;
		}
		#echo '<pre>';print_r($arr_izin);
		#die();
		$tahun = $params['tahun'];
		$bulan = $params['bulan'];
		$arr_data = [];
		foreach ($params['presensi_data'] as $presensi) {

			for($tgl=1;$tgl<=jumlah_hari($tahun, $bulan);$tgl++){
				$arr_data[$presensi->nip]['data'] = $presensi;
				$idx = str_pad($tgl, 2, '0', STR_PAD_LEFT);
				$j_index = 'j_'.$idx;
				$am_index = 'am_'.$idx;
				$ap_index = 'ap_'.$idx;

				if(!isset($arr_data[$presensi->nip]['date'][$tgl]['presensi'])){
					$arr_data[$presensi->nip]['date'][$tgl] = array(
						'presensi' => ''
						, 'masuk' => ''
						, 'pulang' => ''
						, 'periode' => ''
						, 'absen_buka' => ''
						, 'jadwal_masuk' => ''
						, 'jadwal_tengah' => ''
						, 'jadwal_keluar' => ''
						, 'absen_tutup' => ''
						, 'absen_masuk' => ''
						, 'absen_pulang' => ''
						, 'color' => ''
						, 'status' => ''
						, 'editable' => ''
						, 'waktu_kerja' => ''
					);
				}

				$arr_data[$presensi->nip]['date'][$tgl]['presensi'] = $presensi->$j_index;
				$arr_data[$presensi->nip]['date'][$tgl]['masuk'] = $presensi->$am_index;
				$arr_data[$presensi->nip]['date'][$tgl]['pulang'] = $presensi->$ap_index;
				if(!isset($arr_data[$presensi->nip]['kwk'])){
					$arr_data[$presensi->nip]['kwk'] = 0;
				}
				if(!isset($arr_data[$presensi->nip]['alpha'])){
					$arr_data[$presensi->nip]['alpha'] = 0;
				}

				$tanggal = sprintf("%02d", $tgl);
				$jadwal = $arr_data[$presensi->nip]['date'][$tgl]['presensi'];
				#echo '<pre>';print_r($waktu_kerja);
				#echo '<pre>';print_r($jadwal);die();
				$arr_data[$presensi->nip]['date'][$tgl]['waktu_kerja'] = $waktu_kerja[$jadwal];
				$arr_data[$presensi->nip]['date'][$tgl]['nama_singkat'] = $nama_singkat[$jadwal];

				if ($arr_data[$presensi->nip]['date'][$tgl]['waktu_kerja'] > 0) {
					$arr_data[$presensi->nip]['date'][$tgl]['periode'] = $tahun . '-' . $bulan . '-' . $tanggal;

				#echo '<pre>';print_r('000');
				#die();
					if (!empty($arr_izin[$presensi->nip][$tahun . '-' . $bulan . '-' . $tanggal])) {
						if (($arr_izin[$presensi->nip][$tahun . '-' . $bulan . '-' . $tanggal] == 'WFH')) # || ($arr_izin[$presensi->nip][$tahun.'-'.$bulan.'-'.$tanggal] == 'TL')  ==> kalo mau tambahan TL 
						{
							#if ($this->presensi_model->get_check_wfh($userinfo[$presensi->nip], $tahun . '-' . $bulan . '-' . $tanggal) > 0) {
							if(isset($group_wfh[$userinfo[$presensi->nip]][$tahun . '-' . $bulan . '-' . $tanggal])){
								if (count($group_wfh[$userinfo[$presensi->nip]][$tahun . '-' . $bulan . '-' . $tanggal]) > 0) {
									//jika absen
								$arr_data[$presensi->nip]['date'][$tgl]['status'] = $arr_izin[$presensi->nip][$tahun . '-' . $bulan . '-' . $tanggal];
								$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'green';
								}
							} else {
								//jika tidak absen
								$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'A'; //'('.'A'.')'
								$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'red';
								$arr_data[$presensi->nip]['alpha'] += 1;
							}
						} else {
							$arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] = ''; //tampilkan tanda - di db
							$arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] = '';
							$arr_data[$presensi->nip]['date'][$tgl]['status'] = $arr_izin[$presensi->nip][$tahun . '-' . $bulan . '-' . $tanggal];
							$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'green';
						}


						// Aktifkan WFH Aplha quote atas hapus.. bawah gantian di quote cuy...
						// quote atas mulai sebelum if( ($arr_izin....

						#echo '<pre>';print_r('444');
					} else {

						$arr_data[$presensi->nip]['date'][$tgl]['absen_buka'] = $tahun . '-' . $bulan . '-' . $tanggal . ' ' . $absen_buka[$jadwal] . ':00';
						$arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk'] = $tahun . '-' . $bulan . '-' . $tanggal . ' ' . $jadwal_masuk[$jadwal] . ':00';
						$arr_data[$presensi->nip]['date'][$tgl]['jadwal_tengah'] = $tahun . '-' . $bulan . '-' . $tanggal . ' ' . $jadwal_tengah[$jadwal] . ':00';

						$arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'] = date('Y-m-d H:i:s', strtotime($tahun . '-' . $bulan . '-' . $tanggal . ' ' . $jadwal_keluar[$jadwal] . ':00 +' . $selisih_hari[$jadwal] . ' day'));
						$arr_data[$presensi->nip]['date'][$tgl]['absen_tutup'] = date('Y-m-d H:i:s', strtotime($tahun . '-' . $bulan . '-' . $tanggal . ' ' . $absen_tutup[$jadwal] . ':00 +' . $selisih_hari[$jadwal] . ' day'));

						#$arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] = isset($group_msk[$userinfo[$presensi->nip]]) ? $this->get_masuk($userinfo[$presensi->nip], $arr_data[$presensi->nip]['date'][$tgl]['absen_buka'], $arr_data[$presensi->nip]['date'][$tgl]['jadwal_tengah']) : null;
						#$arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] = isset($group_msk[$userinfo[$presensi->nip]]) ? $this->get_pulang($userinfo[$presensi->nip], $arr_data[$presensi->nip]['date'][$tgl]['jadwal_tengah'], $arr_data[$presensi->nip]['date'][$tgl]['absen_tutup']) : null;
						$arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] = isset($group_msk[$userinfo[$presensi->nip]]) ? $this->displayDates($group_msk[$userinfo[$presensi->nip]], $arr_data[$presensi->nip]['date'][$tgl]['absen_buka'], $arr_data[$presensi->nip]['date'][$tgl]['jadwal_tengah']) : null;
						$arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] = isset($group_plg[$userinfo[$presensi->nip]]) ? $this->displayDates($group_plg[$userinfo[$presensi->nip]], $arr_data[$presensi->nip]['date'][$tgl]['jadwal_tengah'], $arr_data[$presensi->nip]['date'][$tgl]['absen_tutup']) : null;

						if (strlen($arr_data[$presensi->nip]['date'][$tgl]['masuk']) > 0) {
							$arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] = $arr_data[$presensi->nip]['date'][$tgl]['masuk'];
						}

						if (strlen($arr_data[$presensi->nip]['date'][$tgl]['pulang']) > 0) {
							$arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] = $arr_data[$presensi->nip]['date'][$tgl]['pulang'];
						}
						if ((strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) == 0) && (strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang']) == 0)) {
							//alpha
							$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'red';
							$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'A';
							$arr_data[$presensi->nip]['alpha'] += 1;
							$arr_data[$presensi->nip]['date'][$tgl]['editable'] = TRUE;
						} else {
							if ((strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) >= 0) && (strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang']) >= 0)) {
								if (($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] <= $arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk']) && ($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] >= $arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'])) {
									//hadir
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'black';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'H';
								}
								if (($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] > $arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk']) && ($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] > $arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'])) {
									//terlambat
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'T';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang']  = floor((strtotime($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) - strtotime($arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk'])) / 60);
								}
								if (($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] < $arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk']) && ($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] < $arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'])) {
									//pulang awal
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'P';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang']  = floor((strtotime($arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar']) - strtotime($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'])) / 60);
								}
								if (($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk'] > $arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk']) && ($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'] < $arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'])) {
									//terlambat dan pulang awal
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'TP';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang_awal']  = floor((strtotime($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) - strtotime($arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk'])) / 60);
									$arr_data[$presensi->nip]['date'][$tgl]['kurang_akhir']  = floor((strtotime($arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar']) - strtotime($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'])) / 60);
									$arr_data[$presensi->nip]['date'][$tgl]['kurang'] = $arr_data[$presensi->nip]['date'][$tgl]['kurang_awal'] + $arr_data[$presensi->nip]['date'][$tgl]['kurang_akhir'];
									$arr_data[$presensi->nip]['kwk'] = $arr_data[$presensi->nip]['date'][$tgl]['kurang'];
								}
							}
							if ((strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) == 0) && (strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang']) >= 0)) {
								$arr_data[$presensi->nip]['date'][$tgl]['editable'] = TRUE;
								if ($arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'] <= $arr_data[$presensi->nip]['date'][$tgl]['absen_pulang']) {
									//tidak absen masuk
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'TAM';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang'] = floor($waktu_kerja[$jadwal] / 2);
								}
								if ($arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar'] >= $arr_data[$presensi->nip]['date'][$tgl]['absen_pulang']) {
									//tidak absen masuk dan pulang cepat
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'TAMP';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang_awal']  = floor($waktu_kerja[$jadwal] / 2);
									$arr_data[$presensi->nip]['date'][$tgl]['kurang_akhir']  = floor((strtotime($arr_data[$presensi->nip]['date'][$tgl]['jadwal_keluar']) - strtotime($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang'])) / 60);
									$arr_data[$presensi->nip]['date'][$tgl]['kurang'] = $arr_data[$presensi->nip]['date'][$tgl]['kurang_awal'] + $arr_data[$presensi->nip]['date'][$tgl]['kurang_akhir'];
									$arr_data[$presensi->nip]['kwk'] = $arr_data[$presensi->nip]['date'][$tgl]['kurang'];
								}
							}

							if ((strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) >= 0) && (strlen($arr_data[$presensi->nip]['date'][$tgl]['absen_pulang']) == 0)) {
								$arr_data[$presensi->nip]['date'][$tgl]['editable'] = TRUE;
								if ($arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk'] >= $arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) {
									//tidak absen pulang
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'TAP';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang'] = floor($waktu_kerja[$jadwal] / 2);
								}
								if ($arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk'] <= $arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) {
									//tidak absen pulang dan terlambat
									$arr_data[$presensi->nip]['date'][$tgl]['color'] = 'orange';
									$arr_data[$presensi->nip]['date'][$tgl]['status'] = 'TAPT';
									$arr_data[$presensi->nip]['date'][$tgl]['kurang_awal']  = floor((strtotime($arr_data[$presensi->nip]['date'][$tgl]['absen_masuk']) - strtotime($arr_data[$presensi->nip]['date'][$tgl]['jadwal_masuk'])) / 60);
									$arr_data[$presensi->nip]['date'][$tgl]['kurang_akhir']  = floor($waktu_kerja[$jadwal] / 2);
									$arr_data[$presensi->nip]['date'][$tgl]['kurang'] = $arr_data[$presensi->nip]['date'][$tgl]['kurang_awal'] + $arr_data[$presensi->nip]['date'][$tgl]['kurang_akhir'];
									$arr_data[$presensi->nip]['kwk'] = $arr_data[$presensi->nip]['date'][$tgl]['kurang'];
								}
							}
						}
					}
					#echo '<br>@@'.$arr_data[$presensi->nip]['date'][$tgl]['status'];
				}
			}
		}
		
		#echo '<pre>';
		#print_r($arr_data);die();

		return $arr_data;
	}

	function displayDates($data, $start, $end) {
		#echo ' <pre> ';print_r($data);
		#echo ' # '.$start;
		#echo ' # '.$end;
		#die();
		$dates = [];
		foreach($data as $k => $v){
			$s = $data[$k]['checktime'];
			$e = isset($data[$k+1]['checktime']) ? $data[$k+1]['checktime'] : $data[$k]['checktime'];
			if($start <= $s && $end >= $e){
				$dates[] = $data[$k]['checktime'];
				goto akhir;
			}
		}
		akhir:
		return isset($dates[0]) ? $dates[0] : null;
	}
	function save_parent_data($tahun,$bulan,$opd,$generated = 1)
	{
		
		$this->db->where('tahun', $tahun);
		$this->db->where('bulan', $bulan);
		$this->db->where('opd', $opd);
		$check = $this->db->get('parent_presensi')->row();
		if(empty($check->id)){
			$this->data = array(
				'opd' => $opd,
				'tahun' => $tahun,
				'bulan' => $bulan,
				'created_at' => date("Y-m-d H:i:s"),
				'generate_by' => $this->authentification->get_user('nip'),
				'generated' => $generated,
				'saved_at' => date("Y-m-d H:i:s"),
				'saved_by' => $this->authentification->get_user('nip')
			);	
			$this->db->insert('parent_presensi', $this->data);
			$insert_id = $this->db->insert_id();
		}else{
			$insert_id = $check->id;
			$this->data = array(
				'generated' => $generated,
				'saved_at' => date("Y-m-d H:i:s"),
				'saved_by' => $this->authentification->get_user('nip')
			);	
			$this->db->where('id', $insert_id);
			$this->db->update('parent_presensi', $this->data);
		}
		return $insert_id;
	}
	
	function get_presensi_all($tahun, $bulan, $max_date = null, $isparent = null){
		$this->db->where('tahun', $tahun);
		$this->db->where('bulan', $bulan);
		if(empty($max_date)){
			if(!empty($isparent)){
				$this->db->where('opd', $isparent);
			}
			else{
				$this->db->where('generated', 1);
			}
		}
		else{
			$max_date = date('Y-m-d', strtotime($tahun.'-'.$bulan.'-'.$max_date));
			$this->db->where("cast(saved_at as date) <= cast('".$max_date."' as date)", null, false);
		}
		$data = $this->db->get('parent_presensi')->result();
		#echo $this->db->last_query();die();
		return $data;
	}

	function delete_data($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('presensi');
		return $delete;
	}
	
}
?>