<?php

class Privilege_model extends Ci_Model {

	var $data;
	
	function get_all_data()
	{		
		return $this->db->query("SELECT * FROM privilege")->result();
	}
	
		
	
	function get_module($level='',$parent='')
	{
		$this->db->select('a.id,a.name,a.parent,a.level,a.controller,a.icon,a.description');
		$this->db->select('(SELECT GROUP_CONCAT(module.id SEPARATOR " ") FROM module WHERE module.id LIKE CONCAT(REPLACE(a.id,"00","") ,"%") ) AS child');
		$this->db->order_by('a.id','ASC');
		if(strlen($level) > 0) 
		{
			$this->db->where('a.level',$level);
		}
		if(strlen($parent) > 0) 
		{
			$parent = str_replace("00", "",$parent);
			$this->db->LIKE('a.parent',$parent,'AFTER');
		}
		$this->db->where('a.status','1');
		return $this->db->get('module a');
	}
	

	function get_data_by_id($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('privilege');
		
	}
	
	function fill_data()
	{
		
		if($this->input->post('module'))
		{
			$modules = implode(",", $this->input->post('module'));;
		}
		else
		{
			$modules = '';
		}
		
		$this->data = array(
			'id' => $this->input->post('id'),
			'name' => $this->input->post('name'),
			'all' => $this->input->post('all'),
			'access' => $modules,
			'description' => $this->input->post('description'),
			'status' => $this->input->post('status')
		);
		
	}
	
	function check_id($id = '')
	{
		$this->db->where('id', $this->data['id']);
		$this->db->where('status', '1');
		if($id != '') $this->db->where('id !=', $id);
		$query = $this->db->get('privilege');

		if ($query->num_rows() > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	

	function insert_data()
	{
		$insert = $this->db->insert('privilege', $this->data);
		return $insert;
	}

	function update_data($id)
	{
		$this->db->where('id', $id);
		$update = $this->db->update('privilege', $this->data);
		return $update;
	}

	function delete_data($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('privilege');
		return $delete;
	}
	
	
	

}

