<?php

class Rekap_model extends CI_Model {

    public function __construct(){
		parent::__construct();
    	$this->dbeps = $this->load->database('eps', TRUE);
		$this->dbadms = $this->load->database('adms', TRUE);
		//  is_login();
	}


	
	function get_presensi_personal($nip,$tahun,$bulan)
	{
		$this->db->select('a.*');		
		$this->db->where('a.id',$tahun.''.$bulan.'_'.$nip);		
		return $this->db->get('presensi a');
	}


	function get_presensi_skpd($a_01, $unit ,$tahun,$bulan,$eselon)
	{
		$this->db->select('a.*');		
		$this->db->like('a.id',$tahun.$bulan,'AFTER');
		if($unit != ''){
			$a_01 = substr($unit, 0, 2);
			$a_02 = substr($unit, 2, 2);
			$a_03 = substr($unit, 4, 2);
			$a_04 = substr($unit, 6, 2);
			// $a_05 = substr($unit, 8, 2);

			if ($a_01 == 'A2') {
				$this->db->where('b.A_01', $a_01);
				$this->db->where('b.A_02', $a_02);
				$this->db->where('b.A_03', $a_03);
				$this->db->WHERE('b.I_06', '22');
			} elseif ($a_01 == 'D0') {
				if ($a_04 < 40) {
					$this->db->where('b.A_01', $a_01);
					$this->db->where('b.A_02', $a_02);
					$this->db->where('b.A_03', $a_03);
					$this->db->where('b.A_04 <', 40);
				} else {
					$this->db->where('b.A_01', $a_01);
					$this->db->where('b.A_02', $a_02);
					$this->db->where('b.A_03', $a_03);
					$this->db->where('b.A_04 >=', 40);
				}
				$in_array = array('31', '32', '88');
				$this->db->where_in('I_06', $in_array);
			} elseif ($a_01 == '86') {
				$this->db->where('b.A_01', $a_01);
				$this->db->where('b.A_02', $a_02);
				$this->db->where('b.A_03', $a_03);
				$this->db->where('b.A_04', $a_04);
				$in_array = array('31', '41', '88');
				$this->db->where_in('b.I_06', $in_array);
			} else {
				$this->db->where('b.A_01', $a_01);
				$this->db->where('b.A_02', $a_02);
				$this->db->where('b.A_03', $a_03);
			}

		}else{
			// $this->db->like('a.unit_kerja',$opd,'AFTER');
			$this->db->where('A_01', $a_01);
		}
		
		$this->db->join('eps.MASTFIP08 b','a.nip = b.B_02B','INNER');
		
		switch($eselon)
		{
			case "all":
			break;
			
			case "str":
			$this->db->where('b.A_01 !=','99');
			$this->db->where('b.I_5A','1');
			$this->db->where('b.I_06 !=','88');
			$this->db->where('b.I_06 !=','99');
			break;
			
			case "1":
			$this->db->where('b.I_06 like','1%');
			break;
			
			case "2":
			$this->db->where('b.I_06 like','2%');
			break;
			
			case "3":
			$this->db->where('b.I_06 like','3%');
			break;
			
			case "4":
			$this->db->where('b.I_06 like','4%');
			break;
			
			default:
			
			break;
		}
		
		$this->db->group_by('a.nip');
		$this->db->order_by('A_01,A_02,A_03,A_04,A_05,I_06,F_03,B_02B ASC');		
		return $this->db->get('presensi a');
	}

    function get_tipe()
	{
		return $this->db->get('tipe');
	}

	function get_pegawai($unit_kerja,$tahun,$bulan,$eselon)
	{
		$this->db->select('a.nip,a.nama_lengkap nama');		
		
		switch($bulan)
		{
			case 'all':
			$this->db->where('SUBSTR(a.id,1,4)',$tahun);
			break;

			case 'smt1':
			$period = array($tahun.'01',$tahun.'02',$tahun.'03',$tahun.'04',$tahun.'05',$tahun.'06');
			$this->db->where_in('SUBSTR(a.id,1,6)',$period);
			break;

			case 'smt2':
			$period = array($tahun.'07',$tahun.'08',$tahun.'09',$tahun.'10',$tahun.'11',$tahun.'12');
			$this->db->where_in('SUBSTR(a.id,1,6)',$period);
			
			break;
			
			case 'caturwulan1':
			$period = array($tahun.'01',$tahun.'02',$tahun.'03',$tahun.'04');
			$this->db->where_in('SUBSTR(a.id,1,6)',$period);
			break;
			
			case 'caturwulan2':
			$period = array($tahun.'05',$tahun.'06',$tahun.'07',$tahun.'08');
			$this->db->where_in('SUBSTR(a.id,1,6)',$period);
			break;
			
			case 'caturwulan3':
			$period = array($tahun.'09',$tahun.'10',$tahun.'11',$tahun.'12');
			$this->db->where_in('SUBSTR(a.id,1,6)',$period);
			break;
			
			default:
			$this->db->like('SUBSTR(a.id,1,6)',$tahun.''.$bulan,'AFTER');
			break;
		}
		
		
		if($unit_kerja != 'all')
		{
			$this->db->where("(c.unit = '$unit_kerja' or c.opd = '$unit_kerja')", NULL, FALSE);
		}
		
		$this->db->join('eps.MASTFIP08 b','a.nip = b.B_02B','INNER');
		
		switch($eselon)
		{
			case "all":
			break;
			
			case "str":
			$this->db->where('b.A_01 !=','99');
			$this->db->where('b.I_5A','1');
			$this->db->where('b.I_06 !=','88');
			$this->db->where('b.I_06 !=','99');
			break;
			
			case "1":
			$this->db->where('b.I_06 like','1%');
			break;
			
			case "2":
			$this->db->where('b.I_06 like','2%');
			break;
			
			case "3":
			$this->db->where('b.I_06 like','3%');
			break;
			
			case "4":
			$this->db->where('b.I_06 like','4%');
			break;
			
			default:
			
			break;
		}
		$this->db->join('parent_presensi c','c.id = a.id_parent','INNER');
		$this->db->group_by('a.nip');
		$this->db->order_by('c.unit','ASC');		
		$q = $this->db->get('presensi a');
		
		#echo '<pre>'; print_r($this->db->last_query());die();
		return $q;
	}
	
	function get_presensi_bulanan($unit_kerja,$tahun,$bulan,$eselon)
	{
		$this->db->select('SUBSTR(a.id,1,4) as tahun,SUBSTR(a.id,1,6) as period,a.*');		
		
		switch($bulan)
		{
			case 'all':
			$this->db->where('SUBSTR(a.id,1,4)',$tahun);
			//$this->db->where('SUBSTR(a.id,5,2)<>','12');#ngilangiBulanDesember
			break;

			case 'smt1':
			$period = array($tahun.'01',$tahun.'02',$tahun.'03',$tahun.'04',$tahun.'05',$tahun.'06');
			$this->db->where_in('SUBSTR(a.id,1,6)',$period);
			break;

			case 'smt2':
			$period = array($tahun.'07',$tahun.'08',$tahun.'09',$tahun.'10',$tahun.'11',$tahun.'12');
			$this->db->where_in('SUBSTR(a.id,1,6)',$period);
			
			break;
	
			case 'caturwulan1':
			$period = array($tahun.'01',$tahun.'02',$tahun.'03',$tahun.'04');
			$this->db->where_in('SUBSTR(a.id,1,6)',$period);
			break;
			
			case 'caturwulan2':
			$period = array($tahun.'05',$tahun.'06',$tahun.'07',$tahun.'08');
			$this->db->where_in('SUBSTR(a.id,1,6)',$period);
			break;
			
			case 'caturwulan3':
			$period = array($tahun.'09',$tahun.'10',$tahun.'11',$tahun.'12');
			$this->db->where_in('SUBSTR(a.id,1,6)',$period);
			break;
			
			default:
			$this->db->like('SUBSTR(a.id,1,6)',$tahun.''.$bulan,'AFTER');
			break;
		}
		
		
		if($unit_kerja != 'all')
		{
			$this->db->where("(c.unit = '$unit_kerja' or c.opd = '$unit_kerja')", NULL, FALSE);
		}
		
		$this->db->join('eps.MASTFIP08 b','a.nip = b.B_02B','INNER');
		
		switch($eselon)
		{
			case "all":
			break;
			
			case "str":
			$this->db->where('b.A_01 !=','99');
			$this->db->where('b.I_5A','1');
			$this->db->where('b.I_06 !=','88');
			$this->db->where('b.I_06 !=','99');
			break;
			
			case "1":
			$this->db->where('b.I_06 like','1%');
			break;
			
			case "2":
			$this->db->where('b.I_06 like','2%');
			break;
			
			case "3":
			$this->db->where('b.I_06 like','3%');
			break;
			
			case "4":
			$this->db->where('b.I_06 like','4%');
			break;
			
			default:
			
			break;
		}
		$this->db->join('parent_presensi c','c.id = a.id_parent','INNER');
		$this->db->order_by('c.unit','ASC');		
		return $this->db->get('presensi a');
	}

}
?>