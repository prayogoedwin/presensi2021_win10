<?php
class Service_model extends Ci_Model {
	
	var $data;
	function __construct()
	{
	parent::__construct();
	$this->DB_presensi = $this->load->database('default', TRUE);
	$this->DB_simpeg = $this->load->database('eps', TRUE);
	$this->DB_adms = $this->load->database('adms', TRUE);
	}

	function get_module($parent)
	{
		$this->db->select('a.id,a.name,a.parent,a.controller,a.icon,a.description');
		$this->db->select('(SELECT COUNT(module.id) from module WHERE module.parent = a.id) as child');
		$this->db->order_by('a.id','ASC');
		$this->db->where('a.status','1');
		$this->db->where('a.parent',$parent);
		return $this->db->get('module a');
	}
	
	function get_realtime()
	{
	    $this->DB_adms->select('b.street as nip, c.Alias as mesin, a.checktime as jam');
		$this->DB_adms->JOIN('userinfo b','a.userid = b.userid');
		$this->DB_adms->JOIN('iclock c','a.SN = c.SN');
		$this->DB_adms->order_by('a.checktime','DESC');
		$this->DB_adms->limit(12);
		return $this->DB_adms->get('checkinout a');
	}
	
	function get_tipe()
	{
		return $this->db->get('tipe');
	}
	
	function get_unit_kerja($limit = array(),$keyword='')
	{
				
		$this->DB_simpeg->select('kd as kode,nm as nama');
		$this->DB_simpeg->order_by('kd','ASC');
		$this->DB_simpeg->WHERE('kd !=','99');
		$this->DB_simpeg->WHERE('aktif','1');
		
		if($keyword) 
		{
			$this->DB_simpeg->like('nm', $keyword,'BOTH');
		}
		
		if ($limit == null)
            return $this->DB_simpeg->get('TABLOK08');
        else
            return $this->DB_simpeg->limit($limit['rows'], $limit['offset'])->get('TABLOK08'); 
	}
	
	function get_upt($limit = array(),$keyword='',$unit_kerja='')
	{
		$role = $this->authentification->get_user('role');		
		
		$this->DB_simpeg->order_by('KOLOK','ASC');
		$this->DB_simpeg->order_by('ESEL','ASC');
		$this->DB_simpeg->WHERE('AKTIF','1');
		
		
			$this->DB_simpeg->where('A_01',$unit_kerja);
			if($unit_kerja == 'A2')
			{
				$this->DB_simpeg->select('CONCAT(A_01,A_02,A_03) as kode,NALOKP as nama');
				$this->DB_simpeg->WHERE('ESEL','22');
			}
			elseif($unit_kerja == 'D0')
			{
				$this->DB_simpeg->select("IF(A_04<'40',CONCAT(A_01,A_02,A_03),CONCAT(A_01,A_02,A_03,A_04)) as kode,NALOKP as nama");
				$in_array = array('31','32','88');
				$this->DB_simpeg->where_in('ESEL',$in_array);
			}
			elseif($unit_kerja == '86')
			{
				$this->DB_simpeg->select('CONCAT(A_01,A_02,A_03,A_04) as kode,NALOKP as nama');
				$in_array = array('31','41','88');
				$this->DB_simpeg->where_in('ESEL',$in_array);
			}
			else
			{
				$this->DB_simpeg->select('CONCAT(A_01,A_02,A_03) as kode,NALOKP as nama');
				$in_array = array('22','31','32');
				//$this->DB_simpeg->where_in('ESEL',$in_array);
				$this->DB_simpeg->where('(ESEL="22" or ESEL="31" or ESEL="32" or (ESEL="41" and KOLOK like "%000000") or KOLOK like "%00000000")');
			}
		
		
		
		if($keyword) 
		{
			$this->DB_simpeg->like('NALOKP', $keyword,'BOTH');
		}
		
		if ($limit == null)
            return $this->DB_simpeg->get('TABLOKB08');
        else
            return $this->DB_simpeg->limit($limit['rows'], $limit['offset'])->get('TABLOKB08'); 
	}
	
	function get_pegawai($limit = array(),$keyword)
	{
				
		$this->DB_simpeg->select('B_02B as nip,CONCAT(B_03A,IF(B_03A!="",". ",""),B_03,IF(B_03B!="",", ",""),B_03B) as nama');
		$this->DB_simpeg->WHERE('A_01 !=','99');
		
		if($keyword) 
		{
			$this->DB_simpeg->like('B_03', $keyword,'BOTH')->or_like('B_02B', $keyword,'BOTH');
		}
		
		if ($limit == null)
            return $this->DB_simpeg->get('MASTFIP08');
        else
            return $this->DB_simpeg->limit($limit['rows'], $limit['offset'])->get('MASTFIP08'); 
	}
	
	function get_personal_userinfo($nip)
	{
		$this->DB_adms->select('a.name,a.street,a.userid');		
		$this->DB_adms->where_in('a.street',$nip);		
		return $this->DB_adms->get('userinfo a');
	}
	
	function get_personal_presensi($nip,$tahun,$bulan)
	{
		$this->DB_presensi->select('a.*');		
		$this->DB_presensi->where('a.id', $tahun.''.$bulan.'_'.$nip);
		//$this->DB_presensi->where('a.nip',$nip);
		return $this->DB_presensi->get('presensi a');
	}
	
	function get_service_jam_kerja()
	{
		$this->DB_presensi->select('id,nama_singkat,nama,deskripsi,jadwal_masuk,jadwal_keluar,jadwal_tengah,absen_buka,absen_tutup,selisih_hari,waktu_kerja');
		$this->DB_presensi->where('status', '1');
		return $this->DB_presensi->get('jam_kerja');	
	}
	
	function get_personal_izin($nip,$tahun,$bulan)
	{
		$this->DB_presensi->select('a.nip,a.tipe,b.tanggal');
		$this->DB_presensi->where('a.nip',$nip);
		$this->DB_presensi->where('DATE_FORMAT(b.tanggal, "%Y-%m") =',$tahun.'-'.$bulan);
		$this->DB_presensi->join('izin_detail b','a.id = b.parent_id ','INNER');		
		return $this->DB_presensi->get('izin a');
	}
	
	function update_personal_data($nip,$tahun,$bulan,$absen_masuk,$absen_pulang,$status,$kwk,$alpha)
	{
		$id = $tahun.$bulan.'_'.$nip;
		
		$this->data = array(
			'kwk' => $kwk,
			'alpha' => $alpha,
			'update_datetime' => date("Y-m-d H:i:s"),
			'update_user' => '',
			'lock' => '1'
		);
		
		for ($tgl = 1; $tgl <=  $this->maestro->jumlah_hari($tahun,$bulan); $tgl++)
		{
		$tanggal = sprintf("%02d",$tgl);
		if($absen_masuk[$tanggal])$this->data['am_'.$tanggal] = $absen_masuk[$tanggal];
		if($absen_pulang[$tanggal])$this->data['ap_'.$tanggal] = $absen_pulang[$tanggal];
		if($status[$tanggal])$this->data['s_'.$tanggal] = $status[$tanggal];
		}
		
		$this->db->where('id', $id);
		$update = $this->db->update('presensi', $this->data);
		return $update;
	}
	
	
	function get_all_pegawai()
	{
		$this->DB_simpeg->select('B_02B as nip,CONCAT(B_03A,IF(B_03A!="",". ",""),B_03,IF(B_03B!="",", ",""),B_03B) as nama');
		$this->DB_simpeg->WHERE('A_01 !=','99');
		$this->DB_simpeg->limit('10');
		return $this->DB_simpeg->get('MASTFIP08');
	}
	
	function get_pegawai_skpd($limit = array(),$skpd,$keyword)
	{
				
		$this->DB_simpeg->select('B_02B as nip,CONCAT(B_03A,IF(B_03A!="",". ",""),B_03,IF(B_03B!="",", ",""),B_03B) as nama');
		$this->DB_simpeg->WHERE('A_01',$skpd);
		
		if($keyword) 
		{
			$this->DB_simpeg->like('B_03', $keyword,'BOTH');
		}
		
		if ($limit == null)
            return $this->DB_simpeg->get('MASTFIP08');
        else
            return $this->DB_simpeg->limit($limit['rows'], $limit['offset'])->get('MASTFIP08'); 
	}
	
	function get_pegawai_lokker($limit = array(),$skpd,$keyword)
	{
				
		$this->DB_simpeg->select('B_02B as nip,CONCAT(B_03A,IF(B_03A!="",". ",""),B_03,IF(B_03B!="",", ",""),B_03B) as nama');
		$this->DB_simpeg->like('CONCAT(A_01,A_02,A_03,A_04,A_05)',$skpd,'AFTER');
		
		if($keyword) 
		{
			$this->DB_simpeg->like('B_03', $keyword,'BOTH')->or_like('B_02B', $keyword,'BOTH');
		}
		
		if ($limit == null)
            return $this->DB_simpeg->get('MASTFIP08');
        else
            return $this->DB_simpeg->limit($limit['rows'], $limit['offset'])->get('MASTFIP08'); 
	}
	
	function get_pegawai_nip($nip)
	{
		$this->DB_simpeg->select('b.nm as  instansi,
		a.A_01 as opd,
		a.I_JB as jabatan,
		CONCAT(a.A_01,a.A_02) as upt,
		CONCAT(a.A_01,a.A_02,a.A_03) as ruang,
		CONCAT(a.A_01,a.A_02,a.A_03,a.A_04) as satker,
		CONCAT(a.A_01,a.A_02,a.A_03,a.A_04,a.A_05) as unit_kerja,
		a.B_02B as nip,CONCAT(a.B_03A," ",a.B_03," ",a.B_03B) as nama');
		$this->DB_simpeg->JOIN('TABLOK08 b','a.A_01 = b.kd','INNER');
		$this->DB_simpeg->WHERE('a.B_02B',$nip);
		
		 return $this->DB_simpeg->get('MASTFIP08 a');
	}
	
		function get_data($tahun,$bulan,$nip)
	{
		$this->db->select('a.*');		
		$this->db->where('a.id',$tahun.''.$bulan.'_'.$nip);		
		return $this->db->get('presensi a');
	}
	
	function get_jam_kerja($uk = '',$limit = array(),$keyword)
	{
		$this->db->select('id,nama_singkat');
		$this->db->where('status', '1');
		
		if ($keyword) {
			$this->db->like('nama_singkat',$keyword);
		}
		
		if ($uk != '') {
			$this->db->where('skpd', '');
			$this->db->or_like('skpd',$uk);
		}
		
		if ($limit == null)
            return $this->db->get('jam_kerja');
        else
            return $this->db->limit($limit['rows'], $limit['offset'])->get('jam_kerja'); 
		
	}
	
	//service token simpan bulanan 
	
	function get_presensi($unit_kerja,$tahun,$bulan)
	{
		$this->db->select('a.*');		
		$this->db->like('a.id', $tahun.''.$bulan,'AFTER');
		$this->db->like('a.unit_kerja', $unit_kerja,'AFTER');
		if (substr($unit_kerja,0,2) == 'D0' && substr($unit_kerja,6,2) < '40') 
			$this->db->where('substring(a.unit_kerja,7,2) <','40');
		$this->db->group_by('a.nip');
		$this->db->order_by('a.unit_kerja','ASC');
		return $this->db->get('presensi a');
	}
	
	function get_presensi_harian($nip,$thn,$bln,$tgl)
	{
		$this->db->select('b.selisih_hari,a.j_'.$tgl.' as jadwal,a.am_'.$tgl.' as absen_masuk,a.ap_'.$tgl.' as absen_pulang,a.s_'.$tgl.' as status ');		
		$this->db->JOIN('jam_kerja b','a.j_'.$tgl.' = b.id','INNER');
		$this->db->where('a.id', $thn.''.$bln.'_'.$nip);
		return $this->db->get('presensi a');
	}
	
	function simpan_harian($nip,$thn,$bln,$tgl)
	{
		$this->data = array(
			'am_'.$tgl => $this->input->post('tanggal_masuk').' '.$this->input->post('absen_masuk_jam').':'.$this->input->post('absen_masuk_menit').':00',
			'ap_'.$tgl => $this->input->post('tanggal_pulang').' '.$this->input->post('absen_pulang_jam').':'.$this->input->post('absen_pulang_menit').':00',
			's_'.$tgl => $this->input->post('status')
		);
		
		$this->db->where('id', $thn.''.$bln.'_'.$nip);
		$update = $this->db->update('presensi', $this->data);
		return $update;
	}
	
	function get_status()
	{
		$array=array('0','1','4');
		$this->db->select('a.id,a.nama');		
		$this->db->where_in('a.group', $array);
		$this->db->order_by('a.urut', 'ASC');
		return $this->db->get('tipe a');
	}
	
	/*function get_pegawai($unit_kerja,$tahun,$bulan)
	{
		$this->db->select('a.nip,a.nama');		
		$this->db->like('a.id', $tahun.''.$bulan,'AFTER');
		$this->db->like('a.unit_kerja', $unit_kerja,'AFTER');
		$this->db->group_by('a.nip');
		$this->db->order_by('a.unit_kerja','ASC');
		return $this->db->get('presensi a');
	} */
	
	function get_izin($unit_kerja,$tahun,$bulan)
	{
		$this->db->select('a.nip,a.tipe,b.tanggal');
		$this->db->join('izin_detail b','a.id = b.parent_id ','INNER');		
		//$this->db->like('b.id', $tahun.''.$bulan,'BOTH');
		$this->db->like('a.unit_kerja', $unit_kerja,'AFTER');
		return $this->db->get('izin a');
	}
	
	
	function get_userinfo($pegawai)
	{
		$this->DB_adms->select('a.name,a.street,a.userid');		
		$this->DB_adms->where_in('a.street',$pegawai);		
		return $this->DB_adms->get('userinfo a');
	}
	
	function get_masuk($userid,$absen_buka,$jadwal_tengah)
	{
		$this->DB_adms->select('DATE_FORMAT(a.checktime,"%Y-%m-%d %H:%i:%s") as checktime');		
		$this->DB_adms->where('a.userid',$userid);
		$this->DB_adms->where('a.checktime >=',$absen_buka);
		$this->DB_adms->where('a.checktime <=',$jadwal_tengah);
		$this->DB_adms->where('a.checktype !=','9');
		$this->DB_adms->where('a.Reserved not like','%WFH%');
		$this->DB_adms->order_by('a.checktime','ASC');
		$this->DB_adms->limit(1);			
		$query = $this->DB_adms->get('checkinout a')->row_array();
		return $query['checktime'];
	}
	
	function get_pulang($userid,$jadwal_tengah,$absen_tutup)
	{
		$this->DB_adms->select('DATE_FORMAT(a.checktime,"%Y-%m-%d %H:%i:%s") as checktime');		
		$this->DB_adms->where('a.userid',$userid);
		$this->DB_adms->where('a.checktime >=',$jadwal_tengah);
		$this->DB_adms->where('a.checktime <=',$absen_tutup);
		$this->DB_adms->where('a.checktype !=','9');
		$this->DB_adms->where('a.Reserved not like','%WFH%');
		$this->DB_adms->order_by('a.checktime','DESC');
		$this->DB_adms->limit(1);			
		$query = $this->DB_adms->get('checkinout a')->row_array();
		return $query['checktime'];
	}
	
		
	function update_data($id,$tahun,$bulan,$nip)
	{
		
		$this->data = array(
			'kwk' => $this->input->post('kwk_'.$nip),
			'alpha' => $this->input->post('alpha_'.$nip)
			//'terlambat' => $this->input->post('terlambat_'.$nip),
			//'update_datetime' => date("Y-m-d H:i:s"),
			//'update_user' => $this->authentification->get_user('nip')
		);
		
		for ($tgl = 1; $tgl <=  $this->maestro->jumlah_hari($tahun,$bulan); $tgl++)
		{
		$tanggal = sprintf("%02d",$tgl);
		if($this->input->post('absen_masuk_'.$nip.'_'.$tanggal))$this->data['am_'.$tanggal] = $this->input->post('absen_masuk_'.$nip.'_'.$tanggal);
		if($this->input->post('absen_pulang_'.$nip.'_'.$tanggal))$this->data['ap_'.$tanggal] = $this->input->post('absen_pulang_'.$nip.'_'.$tanggal);
		if($this->input->post('status_'.$nip.'_'.$tanggal))$this->data['s_'.$tanggal] = $this->input->post('status_'.$nip.'_'.$tanggal);
		}
		
		$this->db->where('id', $id);		
		if ($this->data['kwk'] != '') $update = $this->db->update('presensi', $this->data);
		//$update = $this->db->update('presensi', $this->data);
		//return $update;
	}

}
