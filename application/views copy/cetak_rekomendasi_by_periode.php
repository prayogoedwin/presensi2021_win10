<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$this->formatter->getDateMonthFormatUser($info->tanggal)?></title>

    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css"/>


    <style type="text/css">

    
		h1 {
			font-size: 18pt;
			font-weight: bold;
			padding: 0px;
			margin: 0px;
		}
		h2 {
			font-size: 16pt;
			font-weight: bold;
			padding: 0px;
			margin: 0px;
		}
		h3 {
			font-size: 14px;
			font-weight: bold;
			padding: 0px;
			margin: 0px;
		}
		h4 {
			font-size: 10pt;
			font-weight: bold;
			padding: 0px;
			margin: 0px;
		}
		body {
			font-size: 9pt;
			font-weight: normal;
			text-align: center;
		}
        @media print{@page {
            height:165mm;
            width:210mm;
            /* size: landscape;  */
            margin:0; }}
        /* 165 x 210 */
	</style>
</head>
<!-- <body onload="window.print()"> -->
<body>
<h1 style="text-align:center">Rekomendasi Hibah Cetak Pada <?=$this->formatter->getDateMonthFormatUser($info->tanggal)?></h1>
<p><b>Periode <?=$this->formatter->getDateMonthFormatUser($info->dari)?> s/d <?=$this->formatter->getDateMonthFormatUser($info->sampai)?></b></p>


<table border="1" cellspacing="0" width="100%" id="example">
<thead>
    <tr>
    <th>NO</th>
   
    <th>NAMA LEMBAGA</th>
    <th>ALAMAT</th>
    <th>RAB LEMBAGA (Rp.)</th>
    <th>REKOMENDASI HIBAH (Rp.)</th>
    <!-- <th>KETERANGAN</th> -->
    <th>KONTAK PERSON</th>
    <th>NOMOR PROPOSAL</th>
    <th>ASPIRATOR</th>
   <th>KETERANGAN</th>
</thead>
<tbody>

<?php 
$no = 0;
foreach($alldata as $all): 
$no++;
?>
    <tr style="text-align:left">
    <td style="text-align:center"><?=$no?></td>
    <td><?=strtoupper($all->klasifikasi)?> <?=ucwords(strtolower($all->nama_lembaga))?> </td>
    <td><?=ucwords(strtolower($all->alamat))?> Kel.<?=ucwords(strtolower($all->kel))?>, Kec.<?=ucwords(strtolower($all->kec))?>, <?=ucwords(strtolower($all->kota))?></td>
    <td style="text-align:right"><?=number_format($all->rab)?></td>
    <td style="text-align:right"><?=number_format($all->usulan_apbd)?></td>
    <!-- <td><?=$all->keterangan?></td> -->
    <td><?=$all->pengurus?> - <?=$all->kontak_pengurus?></td>
    <td style="text-align:left"><?=$all->nomor?></td>
    <td style="text-align:center"><?=$all->asp?></td>
    <td style="text-align:center"><?=$all->keterangan?></td>
    </tr>
<?php endforeach; ?>

                        
                        
						
    </tbody>
</table>

<p style="text-align:right">Dicetak Oleh <?=$info->username?></p>
                    
 
                    
              
                        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
                        <script src=" https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
                        <script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
                        <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>
 



                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
                        <script src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
                        <script src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
                        <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
                        <script>
                            

                            var aa = 'REKAPITULASI PROPOSAL'+ '\n' +'HIBAH BIDANG KEAGAMAAN' + '\n' + 'PROVINSI JAWA TENGAH TAHUN ANGGARAN <?=$this->uri->segment("4")?>'  + '\n' + '</h4>';

                            $(document).ready(function() {
                            $('#example').DataTable( {
                                dom: 'Bfrtip',
                                // 'scrollX'   : true,
                                'bPaginate' : false,
                                // 'scrollCollapse': true,
                                searching: false,
                                info: false,
                                
                                // scrollY: 800,
                                // scrollX: true,
                                // scrollCollapse: true,
                                paging: false,
                                'fixedColumns': {
                                    rightColumns: 1
                                },

                                buttons: [
                                    // 'copyHtml5',
                                    // 'excelHtml5',
                                    // 'csvHtml5',
                                    // 'pdfHtml5'
                                    {
                                    extend: 'excel',
                                    customize: function ( xlsx ) {
                                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                        $('c[r=A1] t', sheet).text( 'REKAPITULASI PROPOSAL HIBAH BIDANG KEAGAMAAN PROVINSI JAWA TENGAH TAHUN ANGGARAN <?=$this->uri->segment("4")?>' );
                                        $('row:first c', sheet).attr( 's', '2' ); // first row is bold
                                    }
                                    },
                                    // {
                                    //     extend: 'excelHtml5',
                                    //     title: 'REKAPITULASI PROPOSAL'+ '\n' +'HIBAH BIDANG PENDIDIKAN KEAGAMAAN' + '\n' + 'PROVINSI JAWA TENGAH TAHUN ANGGARAN <?=$this->uri->segment("4")?>',
                                    //     customize: function(doc) {
                                    //         doc.styles.title = {
                                    //         color: 'red',
                                    //         fontSize: '40',
                                    //         background: 'blue',
                                    //         alignment: 'left'
                                    //         }   
                                    //     }  
                                    // },
                                    

                                    {
                                        text: 'Back',
                                        className: 'export-to-pdf',
                                        action: function ( e, dt, button, config ) {
                                            var query = dt.search();
                                            window.open('<?=base_url("p/rekomendasi")?>','_self');
                                        }
                                    },
                                    // {
                                    // extend: 'print',
                                    // customize: function ( win ) {
                                    //     $(win.document.body)
                                    //         .css( 'font-size', '10pt' )
                                           
                    
                                    //     $(win.document.body).find( 'table' )
                                    //         .addClass( 'compact' )
                                    //         .css( 'font-size', 'inherit' );
                                    // }
                                
                                ]
                            } );
                             $('#example2').DataTable( {
                                dom: 'Bfrtip',
                                // 'scrollX'   : true,
                                'bPaginate' : false,
                                // 'scrollCollapse': true,
                                searching: false,
                                info: false,
                                
                                // scrollY: 800,
                                // scrollX: true,
                                // scrollCollapse: true,
                                paging: false,
                                'fixedColumns': {
                                    rightColumns: 1
                                },

                                buttons: [
                                    // 'copyHtml5',
                                    // 'excelHtml5',
                                    // 'csvHtml5',
                                    // 'pdfHtml5'
                                    {
                                    extend: 'excel',
                                    customize: function ( xlsx ) {
                                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                        $('c[r=A1] t', sheet).text( 'REKAPITULASI PROPOSAL HIBAH BIDANG KEAGAMAAN PROVINSI JAWA TENGAH TAHUN ANGGARAN <?=$this->uri->segment("4")?>' );
                                        $('row:first c', sheet).attr( 's', '2' ); // first row is bold
                                    }
                                    },
                                    // {
                                    //     extend: 'excelHtml5',
                                    //     title: 'REKAPITULASI PROPOSAL'+ '\n' +'HIBAH BIDANG PENDIDIKAN KEAGAMAAN' + '\n' + 'PROVINSI JAWA TENGAH TAHUN ANGGARAN <?=$this->uri->segment("4")?>',
                                    //     customize: function(doc) {
                                    //         doc.styles.title = {
                                    //         color: 'red',
                                    //         fontSize: '40',
                                    //         background: 'blue',
                                    //         alignment: 'left'
                                    //         }   
                                    //     }  
                                    // },
                                    {
                                        text: 'Export to PDF',
                                        className: 'export-to-pdf',
                                        action: function ( e, dt, button, config ) {
                                            var query = dt.search();
                                            window.open('<?=base_url("p/cetak2/bk/".$this->uri->segment('4'))?>','_blank');
                                        }
                                    },

                                    {
                                        text: 'Back to Dashboard',
                                        className: 'export-to-pdf',
                                        action: function ( e, dt, button, config ) {
                                            var query = dt.search();
                                            window.open('<?=base_url("p/rekaps")?>','_self');
                                        }
                                    },
                                    // {
                                    // extend: 'print',
                                    // customize: function ( win ) {
                                    //     $(win.document.body)
                                    //         .css( 'font-size', '10pt' )
                                           
                    
                                    //     $(win.document.body).find( 'table' )
                                    //         .addClass( 'compact' )
                                    //         .css( 'font-size', 'inherit' );
                                    // }
                                
                                ]
                            } );
                        } );

//                         $(document).ready(function() {
//     $('#example').DataTable( {
//         dom: 'Bfrtip',
//         buttons: [
//             {
//                 extend: 'print',
//                 customize: function ( win ) {
//                     $(win.document.body)
//                         .css( 'font-size', '10pt' )
//                         .prepend(
//                             '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
//                         );
 
//                     $(win.document.body).find( 'table' )
//                         .addClass( 'compact' )
//                         .css( 'font-size', 'inherit' );
//                 }
//             }
//         ]
//     } );
// } );
                       
                        </script>

                        

</body>
</html>