<!-- Main content -->
<div class="content-wrapper">

<!-- Page header -->
<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
				<div class="page-title d-flex">
						<h4> HIBAH <span class="font-weight-semibold"><?=id_bidang_by('url',$this->uri->segment('3'))->nama?></span> <?php echo $this->uri->segment('3') != 'kpp' ? '- '.strtoupper($this->uri->segment('4')) : '' ?></h4>
							<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<div class="header-elements d-none mb-3 mb-md-0">
						<!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
					</div>
				</div>
			</div>
            <!-- /page header -->


			<!-- Content area -->
			<div class="content pt-0">

				<!-- Form inputs -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Edit Data</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
					<?php
						$message = $this->session->flashdata('message');
						$info = $this->session->flashdata('info');
                        if (isset($message)) { ?>
                           
						 <!-- Solid alert -->
						 <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
						 <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
						 <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
				    </div>
					<!-- /solid alert -->
					
					<?php 	} ?>
						<p class="mb-4"></p>

						<!-- <form action="#"> -->
						<?php 
						$uri2 = $this->uri->segment('2');      
						$uri3 = $this->uri->segment('3');
						$uri4 = $this->uri->segment('4');
						$uri5 = $this->uri->segment('5');
						echo form_open('p/act_edit') ?>
							<fieldset class="mb-3">

							<input type="hidden" name="uri3" value="<?=$uri3?>" class="form-control">
							<input type="hidden" name="uri4" value="<?=$uri4?>" class="form-control">	
                            <input type="hidden" name="uri5" value="<?=$uri5?>" class="form-control">

							<div class="form-group row">
									<label class="col-form-label col-lg-2">Nomor Proposal</label>
									<div class="col-lg-10">
										<input type="text"  required id="email" name="nomor" value="<?=$get->nomor?>" class="form-control" onkeyup="cek_email()">
										<span id='pesan_email'></span>
									</div>
								</div>

								<?php if($this->uri->segment('3') == 'bpk' ){ ?>
								<div class="form-group row">
									<label class="col-form-label col-lg-2">Nomor Statistik</label>
									<div class="col-lg-10">
										<input type="text" required name="nomor_t" class="form-control" value="<?=$get->nomor_t?>">
									</div>
								</div>
								<?php }elseif($this->uri->segment('3') == 'bk' ){ ?>
								<div class="form-group row">
									<label class="col-form-label col-lg-2">Nomor Identitas</label>
									<div class="col-lg-10">
										<input type="text" required name="nomor_t" class="form-control" value="<?=$get->nomor_t?>">
									</div>
								</div>
								<?php }else{ ?>
									<input type="text" required name="nomor_t" class="form-control" value="<?=$get->nomor_t?>">
								<?php } ?>


								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Tahun Anggaran</label>
		                        	<div class="col-lg-10">
										<select name="tahun" required class="form-control">	
										<option value="<?=$get->tahun?>"><?=$get->tahun?></option>
											<?php
											$thn_skr = date('Y');
											$thn_dpn = date('Y')+1;
											for ($x = $thn_dpn; $x >= 2010; $x--) {
											?>
												<option value="<?php echo $x ?>"><?php echo $x ?></option>
											<?php
											}
											?>
										</select>

		                            </div>
								</div>

								<?php if($this->uri->segment('3') != 'kpp' ){ ?>
								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2"><?php echo $this->uri->segment('3') == 'bpk'  ? 'Klasifikasi Lembaga' : 'Klasifikasi'; ?></label>
		                        	<div class="col-lg-10">	
			                            <select class="form-control" required name="klasifikasi" id="id_klasifikasi">
										<option value="<?=$get->klasifikasi_id?>"><?=$get->klasifikasi?></option>
											<?php foreach($klasifikasi as $klas): ?>
			                                <option value="<?=$klas->id_klasifikasi?>"><?=$klas->nama?></option>
											<?php endforeach;?>
			                            </select>
		                            </div>
								</div>
								<?php }else{ ?>
									<input type="hidden" name="klasifikasi" value="3">
								<?php } ?>


								<div class="form-group row">
									<label class="col-form-label col-lg-2"><?php echo $this->uri->segment('3') == 'bpk'  ? 'Nama Lembaga' : 'Nama Lembaga'; ?></label>
									<div class="col-lg-10">
										<input type="text" name="nama" required class="form-control" value="<?=$get->nama_lembaga?>">
									</div>
								</div>

								<div class="form-group row">
									<label class="col-form-label col-md-2">Kabupaten/Kota</label>
									<div class="col-md-10">
										<select name="kota" id="id_kota" required class="form-control select2" data-fouc> 
											<option value="<?=$get->kota_id?>"><?=$get->kota?></option>
										<?php foreach($kabupaten as $kab): ?>
											
			                                <option value="<?=$kab->id?>"><?=$kab->name?></option>
											<?php endforeach;?> 
                                        </select>
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-form-label col-lg-2">Kecamatan</label>
									<div class="col-lg-10">
									<select required class="form-control select2" id="kecamatan" name="id_kecamatan" style="width: 100%;">
                                    <option value="<?=$get->kec_id?>"><?=$get->kec?></option>
                                	<select>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-form-label col-lg-2">Desa/Kelurahan</label>
									<div class="col-lg-10">
									<select required class="form-control select2" id="kelurahan" name="desa" style="width: 100%;">
                                    <option value="<?=$get->kel_id?>"><?=$get->kel?></option>
                                <select>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-form-label col-lg-2">Alamat</label>
									<div class="col-lg-10">
										<textarea rows="3" required cols="3" name="alamat" class="form-control" placeholder="Alamat Lengkap"><?=$get->alamat?></textarea>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-form-label col-lg-2">Tanggal Proposal</label>
									<div class="col-lg-10">
										<input type="date" required name="tanggal" class="form-control" value="<?=$get->tanggal_proposal?>">
									</div>
								</div>

								<div class="form-group row">
									<label class="col-form-label col-lg-2"><?php echo $this->uri->segment('3') == 'bpk'  ? 'Kepala Lembaga' : 'Kepala Pengurus'; ?></label>
									<div class="col-lg-10">
										<input type="text" required name="pengurus" class="form-control" value="<?=$get->pengurus?>">
									</div>
								</div>

								<div class="form-group row">
									<label class="col-form-label col-lg-2">Telp / No HP</label>
									<div class="col-lg-10">
										<input type="number" required name="hp" class="form-control" value="<?=$get->kontak_pengurus?>">
									</div>
								</div>

								<style>
									#custom_usulan{
										display: none;
									}
									#custom_kegunaan{
										display: none;
									}
								</style>

								<?php if($this->uri->segment('3') != 'kpp' ){ ?>

								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Kegunaan</label>
		                        	<div class="col-lg-10">
			                            <select class="form-control" required name="kegunaan" id="kegunaan" onchange="showDiv('custom_usulan', this)"> 
										<option value="<?=$get->kegunaan_id?>"><?=$get->kegunaan?></option>
										<option value="1">Custom</option>
			                            </select>
									</div>
									

									<label class="col-form-label col-lg-2"></label>
									
									<div class="col-lg-10">
										<br/>
										<input type="text" placeholder="Custom Kegunaan (Diisi jika ada kebutuhan custom)" name="custom_kegunaan" id="custom_kegunaan"   class="form-control" value="<?=$get->custom_kegunaan?>">
									</div>
								</div>

								<div class="form-group row">
									<label class="col-form-label col-lg-2">Usulan APBD</label>
									<div class="col-lg-5">
										<input type="text"  readonly name="usulan" id="usulan"  required class="form-control" value="<?=$get->usulan_apbd?>">
									</div>

									<div class="col-lg-5">
										<!--<input type="text" placeholder="Custom Nilai Usulan (Diisi jika ada kebutuhan custom)" name="custom_usulan" id="custom_usulan"   class="form-control">-->
								        <input type="text"  name="custom_usulanB" id="custom_usulanB"  class="form-control"  value="" data-type="currency" value="<?=$get->custom_usulan_apbd?>" >
										<input type="hidden" id="custom_usulan" name="custom_usulan" class="form-control input-sm" style="text-align:right;margin-bottom:5px;" value="<?=$get->custom_usulan_apbd?>">
									</div>
								</div>

								<script>

									function showDiv(divId, element)
									{
										document.getElementById(divId).style.display = element.value == '1' ? 'block' : 'none';
										document.getElementById('custom_kegunaan').style.display = element.value == '1' ? 'block' : 'none';
								// 		document.getElementById('custom_usulanB').style.display = element.value == '1' ? 'block' : 'none';
										
									}

								</script>

								<?php }else{ ?>

									<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Kegunaan</label>
		                        	<div class="col-lg-10">
										<input type="text" required class="form-control" name="custom_kegunaan" value="<?=$get->custom_kegunaan?>">
									</div>
									

									<label class="col-form-label col-lg-2"></label>
									
									<div class="col-lg-10">
										<br/>
										<input type="hidden" placeholder="Custom Kegunaan (Diisi jika ada kebutuhan custom)" name="kegunaan" id="kegunaan"   class="form-control" value="1">
									</div>
								</div>

								<div class="form-group row">
									<label class="col-form-label col-lg-2">Usulan APBD</label>
									<div class="col-lg-5">
										<input type="text"  name="usulan" id="usulan"  required class="form-control" data-type="currency" value="<?=$get->usulan_apbd?>">
									</div>	
								</div>

								<!--<div class="form-group row" hidden>-->
								<!--	<label class="col-form-label col-lg-2">Usulan APBD</label>-->
								<!--	<div class="col-lg-10">-->
								<!--	    <input type="text"  name="custom_usulan" id="custom_usulan"  class="form-control"  value="" data-type="currency"  >-->
								<!--		<input type="text" id="custom_usulanB" name="custom_usulanB" class="form-control input-sm" style="text-align:right;margin-bottom:5px;">-->
										<!--<input type="text" id="custom_usulan" name="custom_usulan" data-type="currency" class="form-control input-sm" style="text-align:right;margin-bottom:5px;">-->
										<!--<input type="text" id="custom_usulan1" name="custom_usulan1" class="form-control input-sm" style="text-align:right;margin-bottom:5px;">-->
								<!--	</div>-->
								<!--</div>-->

									
								<?php } ?>
								
								<!--<div class="form-group row">-->
								<!--	<label class="col-form-label col-lg-2">Custom Usulan</label>-->
								<!--	<div class="col-lg-10">-->
								<!--		<input type="text"  name="custom_usulanA" id="custom_usulanA"  class="form-control"  value="" data-type="currency"  >-->
									
								<!--	</div>-->
								<!--</div>-->


								<!--<div class="form-group row" >-->
								<!--	<label class="col-form-label col-lg-2">Custom Usulan 1</label>-->
								<!--	<div class="col-lg-10">-->
									
								<!--		<input type="text" id="custom_usulanB" name="custom_usulanB" class="form-control input-sm" style="text-align:right;margin-bottom:5px;">-->
								<!--	</div>-->
								<!--</div>-->

								<div class="form-group row">
									<label class="col-form-label col-lg-2">RAB</label>
									<div class="col-lg-10">
										<input type="text"  name="rab" id="rab"  class="form-control"   data-type="currency" value="<?=$get->rab?>"  >
									
									</div>
								</div>


								<div class="form-group row" hidden>
									<label class="col-form-label col-lg-2">RAB 1</label>
									<div class="col-lg-10">
									
										<input type="text" id="rab1" name="rab1" class="form-control input-sm" style="text-align:right;margin-bottom:5px;" value="<?=$get->rab?>">
									</div>
								</div>

								

								<!-- <div class="form-group row">
									<label class="col-form-label col-lg-2">Usulan APBD</label>
									<div class="col-lg-10">
										<?php if($uri4 == 'reguler'){ ?>
										<input type="text" disabled  value="Ada" class="form-control">
										<input type="hidden" disabled name="usulan" value="1" class="form-control">
										<?php }else{ ?>
										<select class="form-control" required name="usulan">
			                                <option value="1">Ada</option>
			                                <option value="0">Tidak</option>
			                                
										</select>
										<?php } ?>
									</div>
								</div> -->


								<?php if($this->uri->segment('3') != 'kpp' ){ ?>

								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Rekomendasi Kemenag</label>
		                        	<div class="col-lg-10">
			                            <select class="form-control" required name="rekomendasi">
			                                <option value="<?=$get->rekomendasi_kemenag?>"><?=adatidak($get->rekomendasi_kemenag)?></option>
			                                <option value="1">Ada</option>
			                                <option value="0">Tidak</option>
			                                
			                            </select>
		                            </div>
								</div>

								<?php }else{ ?>

								<input type="hidden" id="rekomendasi" name="rekomendasi" value="3">

								<?php }?>


							
								
								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Surat Domisili</label>
		                        	<div class="col-lg-10">
			                            <select class="form-control" required name="surat_domisili">
			                                <option value="<?=$get->surat_domisili?>"><?=adatidak($get->surat_domisili)?></option>
			                                <option value="1">Ada</option>
			                                <option value="0">Tidak</option>
			                                
			                            </select>
		                            </div>
								</div>


								<?php if($this->uri->segment('3') != 'kpp' ){ ?>

								<div class="form-group row">
									<label class="col-form-label col-lg-2"><?php echo $this->uri->segment('3') == 'bpk' ? 'Ijin Operasional' : 'Simas/Dokumen Sejenis';?></label>
									<div class="col-lg-10">
										<!-- <input type="text" name="simas" required class="form-control"> -->
										<select class="form-control" required name="simas">
										    <option value="<?=$get->simas?>"><?=adatidak($get->simas)?></option>
			                                <option value="1">Ada</option>
			                                <option value="0">Tidak</option>
			                                
			                            </select>
									</div>
								</div>

								<?php }else{ ?>

								<input type="hidden" id="simas" name="simas" value="3">

								<?php }?>

								<?php if($this->uri->segment('3') == 'bpk' ){ ?>
								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Profil Lembaga</label>
		                        	<div class="col-lg-10">
			                            <select class="form-control" required name="profil">
			                                <option value="<?=$get->profil?>"><?=adatidak($get->profil)?></option>
			                                <option value="1">Ada</option>
			                                <option value="0">Tidak</option>
			                                
			                            </select>
		                            </div>
								</div>
								<?php }else{ ?>
									<input type="hidden" name="profil" value="3">
								<?php } ?>


								<?php if($this->uri->segment('3') != 'kpp' ){ ?>

								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Susunan Pengurus</label>
		                        	<div class="col-lg-10">
			                            <select class="form-control" required name="susunan_pengurus">
			                                <option value="<?=$get->susunan_pengurus?>"><?=adatidak($get->susunan_pengurus)?></option>
			                                <option value="1">Ada</option>
			                                <option value="0">Tidak</option>
			                            </select>
		                            </div>
								</div>

								<?php }else{ ?>

								<input type="hidden" id="susunan_pengurus" name="susunan_pengurus" value="3">

								<?php }?>
								
								<!-- <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Status</label>
		                        	<div class="col-lg-10">
			                            <select class="form-control" required name="status" >
										    <option >Pilih</option>
			                                <option value="1">Memenuhi Syarat</option>
			                                <option value="0">Tidak Memenuhi Syarat</option>
			                                
			                            </select>
		                            </div>
								</div> -->

								
								
								<div class="form-group row">
									<label class="col-form-label col-lg-2">Keterangan</label>
									<div class="col-lg-10">
										<textarea rows="3" cols="3" required name="ket" class="form-control" placeholder="Keterangan"><?=$get->keterangan?></textarea>
									</div>
								</div>

								<?php if($this->uri->segment('4') == 'Aspirasi'){ ?>

								<div class="form-group row">
									<label class="col-form-label col-md-2">Aspirator</label>
									<div class="col-md-10">
										<select name="aspirator" id="aspirator" required class="form-control select2" data-fouc> 
										 <option value="<?=$get->aspirator?>"><?=$get->asp?></option>
										<?php foreach($dprds as $dprd): ?>
											
			                                <option value="<?=$dprd->id?>"><?=$dprd->nama?> - Komisi <?=$dprd->komisi?> - <?=$dprd->fraksi?></option>
											<?php endforeach;?> 
                                        </select>
									</div>
								</div>

								<?php }else{ ?>

											<input type="hidden"  name="aspirator" id="aspirator" value="0"  >
								<?php } ?>
							</fieldset>


							<div class="text-right">
								<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						<?php echo form_close() ?>
					</div>
				</div>
				<!-- /form inputs -->

			</div>
			<!-- /content area -->

<?php require(__DIR__ . '/template/footer.php') ?>

<script>
		$(function () {
			//Initialize Select2 Elements
			$('.select2').select2()
		});

		$('.input-money').keyup(function () {
			this.value = formatRupiah(this.value, 'Rp. ');
		});
		$(".input-money").focus(function (data) {
			if (this.value == 'Rp. 0') {
				this.value = '';
			}
		});
		$(".input-money").focusout(function (data) {
			if (this.value == '') {
				this.value = 'Rp. 0';
			} else if (this.value == '0') {
				this.value = 'Rp. 0';
			}
		});
		</script>

<script>
  $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
    // Kita sembunyikan dulu untuk loadingnya
    $("#loading").hide();
    
    $("#id_kota").change(function(){ // Ketika user mengganti atau memilih data provinsi
      $("#kecamatan").hide(); // Sembunyikan dulu combobox kota nya
      $("#loading").show(); // Tampilkan loadingnya
    
      $.ajax({
        type: "POST", // Method pengiriman data bisa dengan GET atau POST
        url: "<?php echo base_url("welcome/listKecamatan"); ?>", // Isi dengan url/path file php yang dituju
        data: {id_kota : $("#id_kota").val()}, // data yang akan dikirim ke file yang dituju
        dataType: "json",
        beforeSend: function(e) {
          if(e && e.overrideMimeType) {
            e.overrideMimeType("application/json;charset=UTF-8");
          }
        },
        success: function(response){ // Ketika proses pengiriman berhasil
          $("#loading").hide(); // Sembunyikan loadingnya
          // set isi dari combobox kota
          // lalu munculkan kembali combobox kotanya
          $("#kecamatan").html(response.list_kecamatan).show();
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
    });
  });
  </script>

<script>
  $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
    // Kita sembunyikan dulu untuk loadingnya
    $("#loading2").hide();
    
    $("#kecamatan").change(function(){ // Ketika user mengganti atau memilih data provinsi
      $("#kelurahan").hide(); // Sembunyikan dulu combobox kota nya
      $("#loading2").show(); // Tampilkan loadingnya
    
      $.ajax({
        type: "POST", // Method pengiriman data bisa dengan GET atau POST
        url: "<?php echo base_url("welcome/listDesa"); ?>", // Isi dengan url/path file php yang dituju
        data: {id_kecamatan : $("#kecamatan").val()}, // data yang akan dikirim ke file yang dituju
        dataType: "json",
        beforeSend: function(e) {
          if(e && e.overrideMimeType) {
            e.overrideMimeType("application/json;charset=UTF-8");
          }
        },
        success: function(response){ // Ketika proses pengiriman berhasil
          $("#loading2").hide(); // Sembunyikan loadingnya
          // set isi dari combobox kota
          // lalu munculkan kembali combobox kotanya
          $("#kelurahan").html(response.list_desa).show();
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
    });
  });
  </script>

<script>
  $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
    // Kita sembunyikan dulu untuk loadingnya
    $("#loading").hide();
    
    $("#id_klasifikasi").change(function(){ // Ketika user mengganti atau memilih data provinsi
      $("#kegunaan").hide(); // Sembunyikan dulu combobox kota nya
      $("#loading").show(); // Tampilkan loadingnya
    
      $.ajax({
        type: "POST", // Method pengiriman data bisa dengan GET atau POST
        url: "<?php echo base_url("welcome/listKegunaan"); ?>", // Isi dengan url/path file php yang dituju
        data: {id_klasifikasi : $("#id_klasifikasi").val()}, // data yang akan dikirim ke file yang dituju
        dataType: "json",
        beforeSend: function(e) {
          if(e && e.overrideMimeType) {
            e.overrideMimeType("application/json;charset=UTF-8");
          }
        },
        success: function(response){ // Ketika proses pengiriman berhasil
          $("#loading").hide(); // Sembunyikan loadingnya
          // set isi dari combobox kota
          // lalu munculkan kembali combobox kotanya
          $("#kegunaan").html(response.list_kegunaan).show();
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
    });
  });
  </script>

<script>
  $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
    
    $("#kegunaan").change(function(){ // Ketika user mengganti atau memilih data provinsi
     
      $.ajax({
        type: "POST", // Method pengiriman data bisa dengan GET atau POST
        url: "<?php echo base_url("welcome/dataUsulan"); ?>", // Isi dengan url/path file php yang dituju
        data: {kegunaan : $("#kegunaan").val()}, // data yang akan dikirim ke file yang dituju
        success: function(response){ // Ketika proses pengiriman berhasil
         
		 //   $("#usulan").val(response.data_usulan);
		 //   $("#usulan1").html(response.data_usulan);
		 document.getElementById('usulan').value = response;
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
    });
  });
  </script>

  <script>
	  // Jquery Dependency

$("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
	  rabAsli($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "";
    }
    
    // Limit decimal to only 10 digits
    right_side = right_side.substring(0, 100);

    // join number by .
    input_val = "Rp" + left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "Rp." + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);


}



</script>

<script>

  $('#rab').on("change", function (e) {

    var rab = document.getElementById('rab').value;
	var number = rab.replace(/Rp.|,/gi,"");
    document.getElementById('rab1').value = number;

  });
</script>

<script>

  $('#custom_usulanB').on("change", function (e) {

    var custom_usulanB = document.getElementById('custom_usulanB').value;
	var number = custom_usulanB.replace(/Rp.|,/gi,"");
    document.getElementById('custom_usulan').value = number;

  });
</script>

<script>

  $('#usulan').on("change", function (e) {

    var usulan = document.getElementById('usulan').value;
	var number_u = usulan.replace(/Rp.|,/gi,"");
    document.getElementById('custom_usulan').value = number_u;

  });
</script>

<script type='text/javascript'>

    function cek_email(){
        $("#pesan_email").hide();
 
        var email = $("#email").val();
 
        if(email != ""){
            $.ajax({
                url: "<?php echo site_url() . 'dashboard/cek_status_emails'; ?>", //arahkan pada proses_tambah di controller member
                data: 'email='+email,
                type: "POST",
                success: function(msg){
                    if(msg==1){
                        $("#pesan_email").css("color","#fc5d32");
                        $("#email").css("border-color","#fc5d32");
                        $("#pesan_email").html("&nbsp; Maaf nomor proposal sudah dipakai.");
                        error = 1;
                    }else{
                        $("#pesan_email").css("color","#59c113");
                        $("#email").css("border-color","#59c113");
                        $("#pesan_email").html("&nbsp; Nomor Proposal bisa di daftarkan");
                        error = 0;
                    }
                    console.log(msg);
 
                    $("#pesan_email").fadeIn(1000);
                }
            });
        } 
    }      
         
    

</script>


            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>