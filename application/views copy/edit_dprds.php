<!-- Main content -->
<div class="content-wrapper">

<!-- Page header -->
<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
				<div class="page-title d-flex">
						<h4> EDIT <span class="font-weight-semibold">DPRD</span></h4>
							<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<div class="header-elements d-none mb-3 mb-md-0">
						<!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
					</div>
				</div>
			</div>
            <!-- /page header -->


			<!-- Content area -->
			<div class="content pt-0">

				<!-- Form inputs -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Tambah Data</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
					<?php
						$message = $this->session->flashdata('message');
						$info = $this->session->flashdata('info');
                        if (isset($message)) { ?>
                           
						 <!-- Solid alert -->
						 <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
						 <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
						 <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
				    </div>
					<!-- /solid alert -->
					
					<?php 	} ?>
						<p class="mb-4"></p>

						<!-- <form action="#"> -->
						<?php 
						
						echo form_open('dprds/act_edit') ?>
							<fieldset class="mb-3">

							<input type="hidden" name="id" value="<?=$this->uri->segment('3')?>" class="form-control">
							
								<div class="form-group row">
									<label class="col-form-label col-lg-2">Nama</label>
									<div class="col-lg-10">
										<input type="text" required name="nama" class="form-control" value="<?=$get->nama?>">
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-form-label col-lg-2">Komisi</label>
									<div class="col-lg-10">
										<input type="text" required name="komisi" class="form-control" value="<?=$get->komisi?>">
									</div>
								</div>

								<div class="form-group row">
									<label class="col-form-label col-lg-2">Fraksi</label>
									<div class="col-lg-10">
									<select required class="form-control select2" id="fraksi" name="fraksi" style="width: 100%;">
									
                                    <option value="<?=$get->fraksi?>"><?=$get->fraksi?></option>
                                    <?php foreach($fraksi as $fr):?>
                                    <option value="<?=$fr->nama?>"><?=$fr->nama?></option>
                                    <?php endforeach; ?>
                                <select>
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-form-label col-lg-2">Status</label>
									<div class="col-lg-10">
									<select required class="form-control select2" id="status" name="status" style="width: 100%;">
                                    <option value="1">Aktif</option>
                                    <option value="0">Non AKtif</option>
                                <select>
									</div>
								</div>

								
							
							</fieldset>


							<div class="text-right">
								<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						<?php echo form_close() ?>
					</div>
				</div>
				<!-- /form inputs -->

			</div>
			<!-- /content area -->

<?php require(__DIR__ . '/template/footer.php') ?>

            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>