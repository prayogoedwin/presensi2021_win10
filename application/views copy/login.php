<!DOCTYPE html>
<html lang="en">
<head>
	<title>Halaman Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?=base_url()?>assets/log/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/log/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/log/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/log/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/log/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/log/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/log/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/log/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/log/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/log/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/log/css/main.css">
<!--===============================================================================================-->
</head>
<body>

<?php 

// $satu = (rand(1,9)); 
// $dua = (rand(1,9)); 


//     $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
//     $charactersLength = strlen($characters);
//     $randomString = '';
//     for ($i = 0; $i < 5; $i++) {
//         $randomString .= $characters[rand(0, $charactersLength - 1)];
//     }
//     echo $randomString;

?>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
            <?php echo form_open('login/cek','class="login100-form validate-form"'); ?>
				<!-- <form class="login100-form validate-form"> -->
					<span class="login100-form-title p-b-34">
                    <!-- <img width="100" height="100" src="" alt="" /> -->
						
                    </span>
                    <span class="login100-form-title p-b-34">
                        SIPPRO<br/>
                        
					<p>Sistem Informasi Pengelolaan Proposal</p>
					<p>Biro Kesra Setda <br/>Provinsi Jawa Tengah</p>
					<?php
                        $message = $this->session->flashdata('message');
                        if (isset($message)) {
                            echo "<p style='color:red'>".$message."</p>";
						}
					?>
                    </span>

					
					
					
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input id="first-name" class="input100" type="text" name="username" placeholder="User name">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
                    </div>
                    
                    <!-- <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
                        <input id="first-name" class="input100" type="text"  readonly value="<?php echo $satu.' + '. $dua.' = '; ?>">
                        <input class="input100" type="hidden" name="satu" value="<?php echo $satu ?>">
                        <input class="input100" type="hidden" name="dua" value="<?php echo $dua ?>">
						<span class="focus-input100"></span>
                    </div> -->
                    
					<!-- <div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
						<input class="input100" type="text" name="capt" placeholder="Captcha">
						<span class="focus-input100"></span>
					</div> -->
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Sign in
						</button>
					</div>

					<div class="w-full text-center p-t-27 p-b-239">
						<span class="txt1">
							<!-- Forgot -->
						</span>

						<a href="<?=base_url()?>assets/log/#" class="txt2">
							<!-- User name / password? -->
						</a>
					</div>

					<div class="w-full text-center">
						<a href="<?=base_url()?>assets/log/#" class="txt3">
							<!-- Sign Up -->
						</a>
					</div>
                    <?=form_close(); ?>

				<div class="login100-more" style="background-image: url('<?=base_url()?>assets/tol.jpg');"></div>
			</div>
		</div>
	</div>
	
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?=base_url()?>assets/log/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url()?>assets/log/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url()?>assets/log/vendor/bootstrap/js/popper.js"></script>
	<script src="<?=base_url()?>assets/log/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url()?>assets/log/vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script src="<?=base_url()?>assets/log/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?=base_url()?>assets/log/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url()?>assets/log/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?=base_url()?>assets/log/js/main.js"></script>

</body>
</html>