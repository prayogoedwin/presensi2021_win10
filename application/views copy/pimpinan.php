<!-- Main content -->
<div class="content-wrapper">

<!-- Page header -->
<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
				<div class="page-title d-flex">
						<h4> <span class="font-weight-semibold">Pimpinan</h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<div class="header-elements d-none mb-3 mb-md-0">
						<!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
					</div>
				</div>
			</div>
            <!-- /page header -->


			<!-- Content area -->
			<div class="content pt-0">

				<!-- Form inputs -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Edit Data</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
					<?php
						$message = $this->session->flashdata('message');
						$info = $this->session->flashdata('info');
                        if (isset($message)) { ?>
                           
						 <!-- Solid alert -->
						 <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
						 <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
						 <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
				    </div>
					<!-- /solid alert -->
					<?php 	} ?>
					
						<p class="mb-4"></p>

						<?php echo form_open('dashboard/edit_pimpinan') ?>
						
							<fieldset class="mb-3">

								


								<div class="form-group row">
									<label class="col-form-label col-lg-2">Nama Pimpinan</label>
									<div class="col-lg-10">
									   <input type="hidden" name="id" value="<?=$atasan->id?>" class="form-control">
										<input type="text" name="nama" value="<?=$atasan->nama?>" class="form-control">
									</div>
								</div>

								<div class="form-group row">
									<label class="col-form-label col-lg-2">NIP</label>
									<div class="col-lg-10">
										<input type="text" name="nip" value="<?=$atasan->nip?>" class="form-control">
									</div>
								</div>


								<div class="form-group row">
									<label class="col-form-label col-lg-2">Jabatan</label>
									<div class="col-lg-10">
										<input type="text" name="jabatan" value="<?=$atasan->jabatan?>" class="form-control">
									</div>
								</div>
								


								

							</fieldset>


							<div class="text-right">
								<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						<?php echo form_close() ?>
					</div>
				</div>
				<!-- /form inputs -->

			</div>
			<!-- /content area -->

<?php require(__DIR__ . '/template/footer.php') ?>
            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>