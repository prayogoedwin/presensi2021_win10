<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HIBAH BIDANG PENDIDIKAN KEAGAMAAN</title>

    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css"/>


    <style type="text/css">
		h1 {
			font-size: 18pt;
			font-weight: bold;
			padding: 0px;
			margin: 0px;
		}
		h2 {
			font-size: 16pt;
			font-weight: bold;
			padding: 0px;
			margin: 0px;
		}
		h3 {
			font-size: 14px;
			font-weight: bold;
			padding: 0px;
			margin: 0px;
		}
		h4 {
			font-size: 10pt;
			font-weight: bold;
			padding: 0px;
			margin: 0px;
            text-align: left;
		}
		body {
			font-size: 9pt;
			font-weight: normal;
			text-align: center;
		}
        /* @media print{@page {
            height:165mm;
            width:210mm; */
            /* size: landscape;  */
            /* margin:0; 
            }} */
        /* 165 x 210 */
	</style>
</head>
<body onload="window.print()"> 
<!-- <h1 style="text-align:center">REKAP HIBAH <span class="font-weight-semibold"><?=id_bidang_by('url',$this->uri->segment('3'))->nama?></span></h1><br/> -->

<h4>REKAPITULASI PROPOSAL<br/>
HIBAH BIDANG PENDIDIKAN KEAGAMAAN<br/>
PROVINSI JAWA TENGAH TAHUN ANGGARAN <?=$this->uri->segment('4')?>
</h4>

<table border="1" cellspacing="0" width="100%" id="example">
						<thead style="text-align:center">
							<tr>
                                <th rowspan="2">No</th>
								<th rowspan="2">KAB/KOTA</th>
                                <th colspan="11">JENIS LEMBAGA</th>
                                <th colspan="2">JUMLAH PROPOSAL</th> 
                                <th rowspan="2">TOTAL RAB</th>  
                                <th rowspan="2">USULAN APBD</th> 
                                <th colspan="2">ASAL PROPOSAL</th>  
                            </tr>
                            
                            <tr>
                                <th>RA/BA/TA/DA</th>
                                <th>MI</th>
                                <th>MTS</th>
                                <th>MA</th>
                                <th>TPQ</th>
                                <th>MADIN</th>
                                <th>PONPES</th>
                                <th>SEKOLAH MINGGU</th>
                                <th>VIJALAYA</th>
                                <th>PASRAMAN</th>
                                <th>LAINNYA</th>
                                <th>MS</th>
                                <th>TMS</th>
                                <th>REGULER</th>
                                <th>ASPIRASI</th>
                            </tr>
						</thead>
						<tbody>
                        <?php 
                        $no = 0;
                        foreach($rekap as $rk): 
                        $no++;
                        ?>
                        <tr style="text-align:center">
                        <td><?=$no?></td>
                        <td style="text-align:left"><?=$rk->kota?></td>
                        <td><?=$rk->BA + $rk->RA + $rk->DA + $rk->TA?></td>
                        <td><?=$rk->MI?></td>
                        <td><?=$rk->MTS?></td>
                        <td><?=$rk->MA?></td>
                        <td><?=$rk->TPQ?></td>
                        <td><?=$rk->MADIN?></td>
                        <td><?=$rk->PONPES?></td>
                        <td><?=$rk->SEKOLAH_MINGGU?></td>
                        <td><?=$rk->VIJALAYA?></td>
                        <td><?=$rk->PASRAMAN?></td>
                        <td><?=$rk->LAINNYA?></td>
                        <td><?=$rk->MS?></td>
                        <td><?=$rk->TMS?></td>
                        <td><?=number_format($rk->total_rab)?></td>
                        <td><?=number_format($rk->total_usulan)?></td>
                        <td><?=$rk->REGULER?></td>
                        <td><?=$rk->ASPIRASI?></td>




                        </tr>


                        <?php endforeach; ?>
                        
						
						</tbody>
                    </table>
                    
              
                        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
                        <script src=" https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
                        <script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
                        <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>
 



                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
                        <script src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
                        <script src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
                        <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
                        <script>
                            $(document).ready(function() {
                            $('#example').DataTable( {
                                // dom: 'Bfrtip',
                                // 'scrollX'   : true,
                                'bPaginate' : false,
                                // 'scrollCollapse': true,
                                searching: false,
                                info: false,
                                
                               
                            } );
                        } );
     
                        </script>

                        

</body>
</html>