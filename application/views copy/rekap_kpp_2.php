<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HIBAH KEPADA PEMERINTAH PUSAT</title>

    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css"/>


    <style type="text/css">
		h1 {
			font-size: 18pt;
			font-weight: bold;
			padding: 0px;
			margin: 0px;
		}
		h2 {
			font-size: 16pt;
			font-weight: bold;
			padding: 0px;
			margin: 0px;
		}
		h3 {
			font-size: 14px;
			font-weight: bold;
			padding: 0px;
			margin: 0px;
		}
		h4 {
			font-size: 10pt;
			font-weight: bold;
			padding: 0px;
			margin: 0px;
            text-align: left;
		}
		body {
			font-size: 9pt;
			font-weight: normal;
			text-align: center;
		}
        /* @media print{@page {
            height:165mm;
            width:210mm; */
            /* size: landscape;  */
            /* margin:0; 
            }} */
        /* 165 x 210 */
	</style>
</head>
<body onload="window.print()"> 
<!-- <h1 style="text-align:center">REKAP HIBAH <span class="font-weight-semibold"><?=id_bidang_by('url',$this->uri->segment('3'))->nama?></span></h1><br/> -->

<h4>REKAPITULASI PROPOSAL<br/>
HIBAH HIBAH KEPADA PEMERINTAH PUSAT<br/>
PROVINSI JAWA TENGAH TAHUN ANGGARAN <?=$this->uri->segment('4')?>
</h4>

<table border="1" cellspacing="0" width="100%" id="example">
						<thead style="text-align:center">
							<tr>
                                <th rowspan="2">No</th>
								<th rowspan="2">KAB/KOTA</th>
                                <th colspan="2">JUMLAH PROPOSAL</th> 
                                <th rowspan="2">TOTAL RAB</th>  
                                <th rowspan="2">USULAN APBD</th> 
                               
                            </tr>

                         
                            
                            <tr>
                                
                                <th>MS</th>
                                <th>TMS</th>
                               
                            </tr>
						</thead>
						<tbody>
                        <?php 
                        $no = 0;
                        foreach($rekap as $rk): 
                        $no++;
                        ?>
                        <tr style="text-align:center">


                        
                        <td><?=$no?></td>
                        <td style="text-align:left"><?=$rk->kota?></td>
                       
                        <td><?=$rk->MS?></td>
                        <td><?=$rk->TMS?></td>
                        <td><?=$rk->total_rab?></td>
                        <td><?=$rk->total_usulan?></td>
                       

                        </tr>


                        <?php endforeach; ?>
                        
						
						</tbody>
                    </table>
                    
              
                        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
                        <script src=" https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
                        <script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
                        <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>
 



                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
                        <script src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
                        <script src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
                        <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
                        <script>
                            $(document).ready(function() {
                            $('#example').DataTable( {
                                // dom: 'Bfrtip',
                                // 'scrollX'   : true,
                                'bPaginate' : false,
                                // 'scrollCollapse': true,
                                searching: false,
                                info: false,
                                
                               
                            } );
                        } );
     
                        </script>

                        

</body>
</html>