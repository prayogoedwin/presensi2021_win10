<!-- Main content -->
<div class="content-wrapper">

<!-- Page header -->
<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
				<div class="page-title d-flex">
						<h4> <span class="font-weight-semibold">Rekapitulasi</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<div class="header-elements d-none mb-3 mb-md-0">
						<!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
					</div>
				</div>
			</div>
            <!-- /page header -->


			<!-- Content area -->
			<div class="content pt-0">

				<!-- Form inputs -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Rekapitulasi</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
					<?php
						$message = $this->session->flashdata('message');
						$info = $this->session->flashdata('info');
                        if (isset($message)) { ?>
                           
						 <!-- Solid alert -->
						 <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
						 <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
						 <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
				    </div>
					<!-- /solid alert -->
					
					<?php 	} ?>
						<p class="mb-4"></p>

						<!-- <form action="#"> -->
						<?php 
						$uri2 = $this->uri->segment('2');      
						$uri3 = $this->uri->segment('3');
						$uri4 = $this->uri->segment('4'); 
						echo form_open('p/c_rekap') ?>
							<fieldset class="mb-3">

							<input type="hidden" name="uri3" value="<?=$uri3?>" class="form-control">
							<input type="hidden" name="uri4" value="<?=$uri4?>" class="form-control">	

								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Tahun Anggaran</label>
		                        	<div class="col-lg-10">
										<select name="tahun" required class="form-control">	
										<?php
											$thn_skr = date('Y');
											$thn_dpn = date('Y')+1;
											for ($x = $thn_dpn; $x >= 2010; $x--) {
											?>
												<option value="<?php echo $x ?>"><?php echo $x ?></option>
											<?php
											}
											?>
										</select>

		                            </div>
								</div>

								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Bidang Hibah</label>
		                        	<div class="col-lg-10">
			                            <select class="form-control" required name="bidang">
											
										<?php
										if($this->session->userdata('akses') == 2){ ?>
											<option value="2">BIDANG KEAGAMAAN</option>
											 
										<?php }else{ ?>
											<option value="1">BIDANG PENDIDIKAN KEAGAMAAN</option>
											<option value="2">BIDANG KEAGAMAAN</option>
											<option value="3">KEPADA PEMERINTAH PUSAT</option>
									<?php	} ?>
											
			                            </select>
		                            </div>
		                        </div>

								

							</fieldset>


							<div class="text-right">
								<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						<?php echo form_close() ?>
					</div>
				</div>
				<!-- /form inputs -->

			</div>
			<!-- /content area -->

<?php require(__DIR__ . '/template/footer.php') ?>
            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>