<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css"/>
 <!-- Main content -->
<div class="content-wrapper">

<!-- Page header -->
<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
				<div class="page-title d-flex">
						<h4> <span class="font-weight-semibold">Rekomendasi</span> - Hibah</h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<div class="header-elements d-none mb-3 mb-md-0">
						<!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
					</div>
				</div>
			</div>
            <!-- /page header -->


			<!-- Content area -->
			<div class="content pt-0">

				<!-- Form inputs -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Cetak Rekomendasi</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
					<?php
						$message = $this->session->flashdata('message');
						$info = $this->session->flashdata('info');
                        if (isset($message)) { ?>
                           
						 <!-- Solid alert -->
						 <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
						 <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
						 <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
				    </div>
					<!-- /solid alert -->
					
					<?php 	} ?>
						<p class="mb-4"></p>

						<!-- <form action="#"> -->
						<?php 
						$uri2 = $this->uri->segment('2');      
						$uri3 = $this->uri->segment('3');
						$uri4 = $this->uri->segment('4'); 
						echo form_open('p/preview_rekomendasi') ?>
							<fieldset class="mb-3">

							<input type="hidden" name="uri3" value="<?=$uri3?>" class="form-control">
							<input type="hidden" name="uri4" value="<?=$uri4?>" class="form-control">	

								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Tahun Anggaran</label>
		                        	<div class="col-lg-5">
										<select name="tahun" required class="form-control">	
										<?php
											$thn_skr = date('Y');
											$thn_dpn = date('Y')+1;
											for ($x = $thn_dpn; $x >= 2010; $x--) {
											?>
												<option value="<?php echo $x ?>"><?php echo $x ?></option>
											<?php
											}
											?>
										</select>

		                            </div>
								</div>

								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Bidang Hibah</label>
		                        	<div class="col-lg-5">
			                            <select class="form-control" required name="bidang">
									<?php
										if($this->session->userdata('akses') == 2){ ?>
											<option value="2">BIDANG KEAGAMAAN</option>
											 
										<?php }else{ ?>
											<option value="1">BIDANG PENDIDIKAN KEAGAMAAN</option>
											<option value="2">BIDANG KEAGAMAAN</option>
											<option value="3">KEPADA PEMERINTAH PUSAT</option>
									<?php	} ?>
											
			                                
											
			                            </select>
		                            </div>
								</div>
								
								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Dari</label>
		                        	<div class="col-lg-5">
									<input type="date" name="dari"  class="form-control">	
									</div>
									
									
								</div>

								<div class="form-group row">
		                        	
									
									<label class="col-form-label col-lg-2">Sampai</label>
		                        	<div class="col-lg-5">
									<input type="date" name="sampai"  class="form-control">	
		                            </div>
								</div>

								<div class="form-group row">
		                        	
									
									<label class="col-form-label col-lg-2">Alokasi</label>
		                        	<div class="col-lg-5">
									<select class="form-control" required name="alokasi">
											
			                                <option value="1">Murni</option>
											<option value="2">Perubahan</option>
											
											
			                            </select>	
		                            </div>
								</div>

								

							</fieldset>


							<div class="text-center">
								<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						<?php echo form_close() ?>
					</div>
				</div>
				<!-- /form inputs -->

			</div>
			<!-- /content area -->



			<!-- Content area -->
			<div class="content pt-0">

				<!-- tabel histori -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Histori Cetak Rekomendasi</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					<div class="card-body">


					<table id="example" class="table" border=1>
						<thead style="text-align:left">
							<tr style="text-align:left">
								<th>No</th>
								<th>Tanggal Cetak</th>
								<th>Periode Cetak Dari</th>
								<th>Periode Cetak Hingga</th>
								<th>Dicetak oleh</th>
								<th>Detail</th>
							</tr>
						</thead>
						<tbody>
						<?php 
						$no = 0;
						foreach($periode as $pr):
						$no++; ?>
							<tr>
							<td><?=$no?></td>
							<td><?=$this->formatter->getDateMonthFormatUser($pr->tanggal)?></td>
							<td><?=$this->formatter->getDateMonthFormatUser($pr->dari)?></td>
							<td><?=$this->formatter->getDateMonthFormatUser($pr->sampai)?></td>
							<td><?=$pr->username?></td>
							<td><a href="<?=base_url('p/detail_rekom/'.$pr->kode)?>" class="btn btn-success">Lihat Detal</a></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				
					
				
					

						
					</div>
				</div>
				<!-- tabel histori -->

			</div>
			<!-- /content area -->


<?php require(__DIR__ . '/template/footer.php') ?>


            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->


	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
                        <script src=" https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
                        <script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
                        <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>
 



                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
                        <script src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
                        <script src=" https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
                        <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
                        <script>
                            

                            // var aa = 'REKAPITULASI PROPOSAL'+ '\n' +'HIBAH BIDANG KEAGAMAAN' + '\n' + 'PROVINSI JAWA TENGAH TAHUN ANGGARAN <?=$this->uri->segment("4")?>'  + '\n' + '</h4>';

                            $(document).ready(function() {
                            $('#example').DataTable( {
                                dom: 'Bfrtip',
                                // 'scrollX'   : true,
                                'bPaginate' : false,
                                // 'scrollCollapse': true,
                                searching: true,
                                info: false,
                                
                                // scrollY: 800,
                                // scrollX: true,
                                // scrollCollapse: true,
                                paging: false,
                                'fixedColumns': {
                                    rightColumns: 1
                                },

                                buttons: [
                                    // 'copyHtml5',
                                    // 'excelHtml5',
                                    // 'csvHtml5',
                                    // 'pdfHtml5'
                                
                                ]
                            } );
                            
                        } );

//                         $(document).ready(function() {
//     $('#example').DataTable( {
//         dom: 'Bfrtip',
//         buttons: [
//             {
//                 extend: 'print',
//                 customize: function ( win ) {
//                     $(win.document.body)
//                         .css( 'font-size', '10pt' )
//                         .prepend(
//                             '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
//                         );
 
//                     $(win.document.body).find( 'table' )
//                         .addClass( 'compact' )
//                         .css( 'font-size', 'inherit' );
//                 }
//             }
//         ]
//     } );
// } );
                       
                        </script>

</body>
</html>