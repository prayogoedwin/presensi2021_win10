<!-- Page content -->
<div class="page-content">

<!-- Main sidebar -->
<div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="<?=base_url()?>assets/jateng.png" width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold"><?=strtoupper($this->session->userdata('username'))?></div>
                        <div class="font-size-xs opacity-50">
                            <i class="icon-pin font-size-sm"></i> Biro Kesra Setda Provinsi Jawa Tengah
                        </div>
                    </div>

                    <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
                <!-- <li class="nav-item">
                    <a href="<?=base_url('dashboard')?>" class="nav-link active">
                        <i class="icon-home4"></i>
                        <span>
                            Kota
                        </span>
                    </a>
                </li>
                

                <li class="nav-item">
                    <a href="tablex.html" class="nav-link">
                        <i class="icon-design"></i>
                        <span>
                            Table
                        </span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="formx.html" class="nav-link">
                        <i class="icon-pencil5"></i>
                        <span>
                            Form
                        </span>
                    </a>
                </li> -->

                <?php 
                $uri1 = $this->uri->segment('1');  
                $uri2 = $this->uri->segment('2');      
                $uri3 = $this->uri->segment('3');
                $uri4 = $this->uri->segment('4'); 
                
                if($this->session->userdata('akses') == 2){
                    $menu = $this->db->query("SELECT * FROM bidang_hibah WHERE id_bidang = 2")->result(); 
                }else{
                    $menu = $this->db->query("SELECT * FROM bidang_hibah ")->result(); 
                }
                
                    foreach($menu as $mn):
                ?>

                        <li class="nav-item nav-item-submenu <?php echo $uri3 == $mn->url ? 'nav-item-expanded nav-item-open' : ''; ?> ">
							<a href="#" class="nav-link"><i class="<?=$mn->icon?>"></i> <span>HIBAH <?=$mn->nama?></span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Sidebars">

                            <?php if($mn->id_bidang != 3){ ?>
								<li class="nav-item nav-item-submenu <?php echo $uri2 == 'tambah' && $uri3 == $mn->url ? 'nav-item-expanded nav-item-open' : ''; ?>  ">
									<a href="#" class="nav-link"><i class="<?=$mn->icon?>"></i>Tambah Data</a>
									<ul class="nav nav-group-sub">
                                       
										<li class="nav-item"><a href="<?=base_url('p')?>/tambah/<?=$mn->url?>/reguler" class="nav-link <?php echo $uri4 == 'reguler' && $uri3 == $mn->url ? 'active' : ''; ?>">Reguler</a></li>
                                        <li class="nav-item"><a href="<?=base_url('p')?>/tambah/<?=$mn->url?>/aspirasi" class="nav-link <?php echo $uri4 == 'aspirasi' && $uri3 == $mn->url ? 'active' : ''; ?>">Aspirasi</a></li>
                                    
									</ul>
                                </li>
                            <?php  }else{ ?> 

                                <li class="nav-item">
                                <a href="<?=base_url('p')?>/tambah/<?=$mn->url?>/reguler" class="nav-link <?php echo $uri2 == 'lihat' && $uri3 == $mn->url ? 'nav-item-expanded nav-item-open' : ''; ?>">
                                    <i class="<?=$mn->icon?>"></i>
                                    <span>
                                        Tambah Data
                                </span>
                                </a>
                            </li>
                            <?php } ?>

                            
                                <!-- <li class="nav-item nav-item-submenu <?php echo $uri2 == 'lihat' && $uri3 == $mn->url ? 'nav-item-expanded nav-item-open' : ''; ?>  ">
									<a href="#" class="nav-link"><i class="<?=$mn->icon?>"></i>Lihat Data</a>
									<ul class="nav nav-group-sub">
										<li class="nav-item"><a href="<?=base_url('p')?>/lihat/<?=$mn->url?>/reguler" class="nav-link <?php echo $uri4 == 'reguler' && $uri3 == $mn->url ? 'active' : ''; ?>">Reguler</a></li>
										<li class="nav-item"><a href="<?=base_url('p')?>/lihat/<?=$mn->url?>/aspirasi" class="nav-link <?php echo $uri4 == 'aspirasi' && $uri3 == $mn->url ? 'active' : ''; ?>">Aspirasi</a></li>
										</ul>
                                </li> -->
                                
                                <li class="nav-item">
                                <a href="<?=base_url('p/lihat/'.$mn->url)?>" class="nav-link <?php echo $uri2 == 'lihat' && $uri3 == $mn->url ? 'nav-item-expanded nav-item-open' : ''; ?>">
                                    <i class="<?=$mn->icon?>"></i>
                                    <span>
                                        Lihat Data
                                </span>
                                </a>
                            </li>
								
							</ul>
						</li>

                    <?php endforeach; ?>

               

                <!-- <li class="nav-item nav-item-submenu <?php echo $uri2 == 'rekap' ? 'nav-item-expanded nav-item-open' : ''; ?>">
                    <a href="#" class="nav-link"><i class="icon-stats-bars3"></i> <span>Rekap Input</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Form components">
                        <li class="nav-item <?php echo $uri3 == '1' ? 'active' : ''; ?>"><a href="<?=base_url('p/rekap/1')?>" class="nav-link">Rekap 1</a></li>
                        <li class="nav-item <?php echo $uri3 == '2' ? 'active' : ''; ?>"><a href="<?=base_url('p/rekap/2')?>" class="nav-link">Rekap 2</a></li>
                        <li class="nav-item <?php echo $uri3 == '3' ? 'active' : ''; ?>"><a href="<?=base_url('p/rekap/3')?>" class="nav-link">Rekap 3</a></li>
                        <li class="nav-item <?php echo $uri3 == '4' ? 'active' : ''; ?>"><a href="<?=base_url('p/rekap/4')?>" class="nav-link">Rekap 4</a></li>
                        <li class="nav-item <?php echo $uri3 == '5' ? 'active' : ''; ?>"><a href="<?=base_url('p/rekap/5')?>" class="nav-link">Rekap 5</a></li>     
                    </ul>
                </li> -->

                <?php foreach($menu as $ms): ?>

                <!-- <li class="nav-item">
                    <a target="BLANK" href="<?=base_url('p/rekap/'.$ms->url)?>" class="nav-link <?php echo $uri3 == $ms->url ? 'active' : ''; ?>">
                        <i class="icon-file-excel"></i>
                        <span>
                            REKAP HIBAH <?=$ms->nama?>
                        </span>
                    </a>
                </li> -->

                <?php endforeach; ?>
                

                <?php if($this->session->userdata('akses') == 1){ ?>
                    <?php if($this->session->userdata('role') == 1){ ?>
                    <li class="nav-item">
                    <a href="<?=base_url('p/rekomendasi')?>" class="nav-link <?php echo $uri2 == 'rekomendasi' ? 'active' : ''; ?>">
                        <i class="icon-file-excel"></i>
                        <span>
                            Rekomendasi Hibah
                        </span>
                    </a>
                    </li>
                    <?php } ?>

                <li class="nav-item">
                    <a href="<?=base_url('p/rekaps')?>" class="nav-link <?php echo $uri2 == 'rekaps' ? 'active' : ''; ?>">
                        <i class="icon-pie-chart"></i>
                        <span>
                             Rekapitulasi 
                        </span>
                    </a>
                </li>
                
                <?php if($this->session->userdata('role') == 1){ ?>
                <li class="nav-item">
                    <a href="<?=base_url('dashboard/pimpinan')?>" class="nav-link <?php echo $uri2 == 'pimpinan' ? 'active' : ''; ?>">
                        <i class="icon-user"></i>
                        <span>
                            Pimpinan
                        </span>
                    </a>
                </li>
                <?php } ?>
                
                <li class="nav-item">
                    <a href="<?=base_url('dprds')?>" class="nav-link <?php echo $uri1 == 'dprds' ? 'active' : ''; ?>">
                        <i class="icon-users"></i>
                        <span>
                            DPRD
                        </span>
                    </a>
                </li>

                <?php } ?>



                    <!-- <li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-tree5"></i> <span>Menu levels</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Menu levels">
								
								<li class="nav-item nav-item-submenu">
									<a href="#" class="nav-link"><i class="icon-firefox"></i> Second level with child</a>
									<ul class="nav nav-group-sub">
										
										<li class="nav-item nav-item-submenu">
											<a href="#" class="nav-link"><i class="icon-apple2"></i> Third level with child</a>
											<ul class="nav nav-group-sub">
												<li class="nav-item"><a href="#" class="nav-link"><i class="icon-html5"></i> Fourth level</a></li>
												<li class="nav-item"><a href="#" class="nav-link"><i class="icon-css3"></i> Fourth level</a></li>
											</ul>
										</li>
										
									</ul>
								</li>
								
							</ul>
						</li> -->
                
                <!-- /main -->

                <!-- Forms -->
                <!-- <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Forms</div> <i class="icon-menu" title="Forms"></i></li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-pencil3"></i> <span>Form components</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Form components">
                        <li class="nav-item"><a href="form_inputs.html" class="nav-link">Basic inputs</a></li>
                        <li class="nav-item"><a href="form_checkboxes_radios.html" class="nav-link">Checkboxes &amp; radios</a></li>
                        
                    </ul>
                </li> -->
                
                
                <!-- /forms -->

                
                
                

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->
    
</div>
<!-- /main sidebar -->