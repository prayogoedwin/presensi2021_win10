<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4>
                    <!-- <i class="icon-arrow-left52 mr-2"></i>  -->
                    <span class="font-weight-semibold">Dashboard</span>
                </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                <!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->

        <?php     } ?>


        <!-- Basic pie charts -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Pie Chart</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <!-- <a class="list-icons-item" data-action="collapse"></a>
				                		<a class="list-icons-item" data-action="reload"></a>
				                		<a class="list-icons-item" data-action="remove"></a> -->
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <!-- <p class="mb-3">A <code>pie chart</code> is a divided into sectors, illustrating numerical proportion. In a pie chart, the arc length of each sector (and consequently its central angle and area), is proportional to the quantity it represents. While it is named for its resemblance to a pie which has been sliced, there are variations on the way it can be presented.</p> -->

                        <div class="chart-container has-scroll text-center">
                            <!-- <div class="d-inline-block" id="google-pie"></div> -->
                            <div id="xyz"></div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Pie Chart</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <!-- <a class="list-icons-item" data-action="collapse"></a>
				                		<a class="list-icons-item" data-action="reload"></a>
				                		<a class="list-icons-item" data-action="remove"></a> -->
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <!-- <p class="mb-3">A <code>donut chart</code> is functionally identical to a pie chart, with the exception of a blank center and the ability to support multiple statistics at once. donut charts provide a better data intensity ratio to standard pie charts since the blank center can be used to display additional related data.</p> -->

                        <div class="chart-container has-scroll text-center">
                            <!-- <div class="d-inline-block" id="google-donut"></div> -->
                            <div id="opq"></div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!-- /basic pie charts -->



        <!-- Dashboard content -->
        <div class="row" hidden>
            <div class="col-xl-12">

                <!-- Quick stats boxes -->
                <div class="row">
                    <div class="col-lg-3">


                        <!-- Members online -->
                        <div class="card bg-teal-400">
                            <div class="card-body">
                                <div class="d-flex">
                                    <h3 class="font-weight-semibold mb-0">12</h3>
                                    <!-- <span class="badge bg-teal-800 badge-pill align-self-center ml-auto">+53,6%</span> -->
                                </div>

                                <div>
                                    Bidang Pendidikan Keagamaan
                                    <!-- <div class="font-size-sm opacity-75">489 avg</div> -->
                                </div>
                            </div>

                            <div class="container-fluid">
                                <div id="members-online"></div>
                            </div>
                        </div>
                        <!-- /members online -->

                    </div>

                    <div class="col-lg-3">

                        <!-- Current server load -->
                        <div class="card bg-pink-400">
                            <div class="card-body">
                                <div class="d-flex">
                                    <h3 class="font-weight-semibold mb-0">12</h3>
                                    <div class="list-icons ml-auto">
                                        <div class="dropdown">
                                            <!-- <a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i></a> -->
                                            <!-- <div class="dropdown-menu dropdown-menu-right">
                                        <a href="#" class="dropdown-item"><i class="icon-sync"></i> Update data</a>
                                        <a href="#" class="dropdown-item"><i class="icon-list-unordered"></i> Detailed log</a>
                                        <a href="#" class="dropdown-item"><i class="icon-pie5"></i> Statistics</a>
                                        <a href="#" class="dropdown-item"><i class="icon-cross3"></i> Clear list</a>
                                    </div> -->
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    Bidang Keagamaan
                                    <!-- <div class="font-size-sm opacity-75">34.6% avg</div> -->
                                </div>
                            </div>

                            <div id="server-load"></div>
                        </div>
                        <!-- /current server load -->

                    </div>

                    <div class="col-lg-3">

                        <!-- Current server load -->
                        <div class="card bg-orange-300">
                            <div class="card-body">
                                <div class="d-flex">
                                    <h3 class="font-weight-semibold mb-0">12</h3>
                                    <div class="list-icons ml-auto">
                                        <div class="dropdown">
                                            <a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a href="#" class="dropdown-item"><i class="icon-sync"></i> Update data</a>
                                                <a href="#" class="dropdown-item"><i class="icon-list-unordered"></i> Detailed log</a>
                                                <a href="#" class="dropdown-item"><i class="icon-pie5"></i> Statistics</a>
                                                <a href="#" class="dropdown-item"><i class="icon-cross3"></i> Clear list</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    Bidang Keagamaan
                                    <i class="icon-book"></i>
                                    <!-- <div class="font-size-sm opacity-75">34.6% avg</div> -->
                                </div>
                            </div>

                            <div id="server-load"></div>
                        </div>
                        <!-- /current server load -->

                    </div>

                    <div class="col-lg-3">

                        <!-- Today's revenue -->
                        <div class="card bg-blue-400">
                            <div class="card-body">
                                <div class="d-flex">
                                    <h3 class="font-weight-semibold mb-0">12</h3>
                                    <!-- <div class="list-icons ml-auto">
                                <a class="list-icons-item" data-action="reload"></a>
                            </div> -->
                                </div>

                                <div>
                                    Kepada Pemerintah Pusat
                                    <!-- <div class="font-size-sm opacity-75">$37,578 avg</div> -->
                                </div>
                            </div>

                            <div id="today-revenue"></div>
                        </div>
                        <!-- /today's revenue -->

                    </div>
                </div>
                <!-- /quick stats boxes -->





            </div>


        </div>
        <!-- /dashboard content -->

    </div>
    <!-- /content area -->

    <?php require(__DIR__ . '/template/footer.php') ?>

    <script>
        //Setting Warna Gradasi
        Highcharts.setOptions({
            colors: Highcharts.map(Highcharts.getOptions().colors, function(color) {
                return {
                    radialGradient: {
                        cx: 0.5,
                        cy: 0.3,
                        r: 0.7
                    },
                    stops: [
                        [0, color],
                        [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
                    ]
                };
            })
        });
    </script>

    <script>
        // Build the chart
        Highcharts.chart('xyz', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Share',
                data: [{
                        name: 'Chrome',
                        y: 61.41
                    },
                    {
                        name: 'Internet Explorer',
                        y: 11.84
                    },
                    {
                        name: 'Firefox',
                        y: 10.85
                    },
                    {
                        name: 'Edge',
                        y: 4.67
                    },
                    {
                        name: 'Safari',
                        y: 4.18
                    },
                    {
                        name: 'Other',
                        y: 7.05
                    }
                ]
            }]
        });
    </script>

    <script>
        // Build the chart
        Highcharts.chart('opq', {
            chart: {
                // backgroundColor: '#354053',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            exporting: {
                enabled: false
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Share',
                data: [{
                        name: 'Chrome',
                        y: 61.41
                    },
                    {
                        name: 'Internet Explorer',
                        y: 11.84
                    },
                    {
                        name: 'Firefox',
                        y: 10.85
                    },
                    {
                        name: 'Edge',
                        y: 4.67
                    },
                    {
                        name: 'Safari',
                        y: 4.18
                    },
                    {
                        name: 'Other',
                        y: 7.05
                    }
                ]
            }]
        });
    </script>






</div>
<!-- /main content -->

</div>
<!-- /page content -->

</body>

</html>