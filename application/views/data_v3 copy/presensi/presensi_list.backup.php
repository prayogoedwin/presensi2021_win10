<?php require(__DIR__ . '/../../template/sesi.php') ?>


<?php

foreach ($user_info->result() as $user) {
	$userinfo[trim($user->street, ' ')] = $user->userid;
}
//echo '<pre>';
//print_r($userinfo); 

foreach ($jam_kerja as $jam) {
	$absen_buka[$jam->id] = $jam->absen_buka;
	$nama_singkat[$jam->id] = $jam->nama_singkat;
	$jadwal_masuk[$jam->id] = $jam->jadwal_masuk;
	$jadwal_tengah[$jam->id] = $jam->jadwal_tengah;
	$jadwal_keluar[$jam->id] = $jam->jadwal_keluar;
	$absen_tutup[$jam->id] = $jam->absen_tutup;
	$selisih_hari[$jam->id] = $jam->selisih_hari;
	$waktu_kerja[$jam->id] = $jam->waktu_kerja;
}
//echo '<pre>';
//print_r($izin_data);exit; 

foreach ($izin_data->result() as $izin) {
	$arr_izin[$izin->nip][$izin->tanggal] = $izin->tipe;
}
//echo '<pre>';
//print_r($arr_izin);exit; 


?>

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Data - <span class="font-weight-semibold">Presensi Bulan <?= bulan($bulan) ?> Tahun <?= $tahun ?></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                <div class="d-flex justify-content-center">
                    <a href="#" class="btn btn-link btn-float text-default" data-toggle="modal" data-target="#keterangan"><i class="fa fa-file"></i><span>Keterangan</span></a>
                    <!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a> -->
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->
            <br />

        <?php     } ?>
        <div class="col-md-12">
            <div class="card">
                <!-- <div class="card-header header-elements-inline">
								<h6 class="card-title">Basic tabs</h6>
								<div class="header-elements">
									<div class="list-icons">
				                		<a class="list-icons-item" data-action="collapse"></a>
				                		<a class="list-icons-item" data-action="reload"></a>
				                		<a class="list-icons-item" data-action="remove"></a>
				                	</div>
			                	</div>
							</div> -->

                <div class="card-body">
                    <?php echo form_open('portal/auth');  ?>
                    <table class="table table-striped datatable-button-html5-basic" border="1" id="jadwal">
<thead><tr>
<!-- <th class="center">No.</th> -->
<th class="center" rowspan="3">NIP</th>
<th class="center" rowspan="3">Nama</th>
<?php
for ($tgl = 1; $tgl <=  jumlah_hari($tahun,$bulan); $tgl++){ 
echo '<th class="center" colspan="2">'.$tgl.'<br>'.nama_hari($tahun.'-'.$bulan.'-'.sprintf("%02d",$tgl)).'</th>';
 } ?>
<th class="center" rowspan="3"> KWK (jam:menit) </th>
<th class="center" rowspan="3"> Alpha (hari) </th>
<th class="center" rowspan="3"> Action </th>
 </tr>
 <tr>
 <!-- <th colspan="3" rowspan="2">
<div class="input-field">
  <input name="keyword" id="keyword" type="search" placeholder="Cari Nama">
  <label class="label-icon" for="search"><i class="material-icons">search</i></label>
  <i class="material-icons">close</i>
</div>
 </th> -->
 <?php
for ($tgl = 1; $tgl <=  jumlah_hari($tahun,$bulan); $tgl++){ ?>
<th>Masuk</th><th rowspan="2">Status</th>
<?php } ?></tr>
<?php
for ($tgl = 1; $tgl <=  jumlah_hari($tahun,$bulan); $tgl++){ ?>
<th>Pulang</th>
<?php } ?></tr>

                        </thead>
                        <tbody>

                            <?php
					$no = 0;
					
					foreach ($presensi_data->result() as $presensi) {
						$no++;
						$arr_presensi[$presensi->nip][1] = $presensi->j_01;
						$arr_presensi[$presensi->nip][2] = $presensi->j_02;
						$arr_presensi[$presensi->nip][3] = $presensi->j_03;
						$arr_presensi[$presensi->nip][4] = $presensi->j_04;
						$arr_presensi[$presensi->nip][5] = $presensi->j_05;
						$arr_presensi[$presensi->nip][6] = $presensi->j_06;
						$arr_presensi[$presensi->nip][7] = $presensi->j_07;
						$arr_presensi[$presensi->nip][8] = $presensi->j_08;
						$arr_presensi[$presensi->nip][9] = $presensi->j_09;
						$arr_presensi[$presensi->nip][10] = $presensi->j_10;
						$arr_presensi[$presensi->nip][11] = $presensi->j_11;
						$arr_presensi[$presensi->nip][12] = $presensi->j_12;
						$arr_presensi[$presensi->nip][13] = $presensi->j_13;
						$arr_presensi[$presensi->nip][14] = $presensi->j_14;
						$arr_presensi[$presensi->nip][15] = $presensi->j_15;
						$arr_presensi[$presensi->nip][16] = $presensi->j_16;
						$arr_presensi[$presensi->nip][18] = $presensi->j_18;
						$arr_presensi[$presensi->nip][17] = $presensi->j_17;
						$arr_presensi[$presensi->nip][19] = $presensi->j_19;
						$arr_presensi[$presensi->nip][20] = $presensi->j_20;
						$arr_presensi[$presensi->nip][21] = $presensi->j_21;
						$arr_presensi[$presensi->nip][22] = $presensi->j_22;
						$arr_presensi[$presensi->nip][23] = $presensi->j_23;
						$arr_presensi[$presensi->nip][24] = $presensi->j_24;
						$arr_presensi[$presensi->nip][25] = $presensi->j_25;
						$arr_presensi[$presensi->nip][26] = $presensi->j_26;
						$arr_presensi[$presensi->nip][27] = $presensi->j_27;
						$arr_presensi[$presensi->nip][28] = $presensi->j_28;
						$arr_presensi[$presensi->nip][29] = $presensi->j_29;
						$arr_presensi[$presensi->nip][30] = $presensi->j_30;
						$arr_presensi[$presensi->nip][31] = $presensi->j_31;

						$arr_masuk[$presensi->nip][1] = $presensi->am_01;
						$arr_masuk[$presensi->nip][2] = $presensi->am_02;
						$arr_masuk[$presensi->nip][3] = $presensi->am_03;
						$arr_masuk[$presensi->nip][4] = $presensi->am_04;
						$arr_masuk[$presensi->nip][5] = $presensi->am_05;
						$arr_masuk[$presensi->nip][6] = $presensi->am_06;
						$arr_masuk[$presensi->nip][7] = $presensi->am_07;
						$arr_masuk[$presensi->nip][8] = $presensi->am_08;
						$arr_masuk[$presensi->nip][9] = $presensi->am_09;
						$arr_masuk[$presensi->nip][10] = $presensi->am_10;
						$arr_masuk[$presensi->nip][11] = $presensi->am_11;
						$arr_masuk[$presensi->nip][12] = $presensi->am_12;
						$arr_masuk[$presensi->nip][13] = $presensi->am_13;
						$arr_masuk[$presensi->nip][14] = $presensi->am_14;
						$arr_masuk[$presensi->nip][15] = $presensi->am_15;
						$arr_masuk[$presensi->nip][16] = $presensi->am_16;
						$arr_masuk[$presensi->nip][18] = $presensi->am_18;
						$arr_masuk[$presensi->nip][17] = $presensi->am_17;
						$arr_masuk[$presensi->nip][19] = $presensi->am_19;
						$arr_masuk[$presensi->nip][20] = $presensi->am_20;
						$arr_masuk[$presensi->nip][21] = $presensi->am_21;
						$arr_masuk[$presensi->nip][22] = $presensi->am_22;
						$arr_masuk[$presensi->nip][23] = $presensi->am_23;
						$arr_masuk[$presensi->nip][24] = $presensi->am_24;
						$arr_masuk[$presensi->nip][25] = $presensi->am_25;
						$arr_masuk[$presensi->nip][26] = $presensi->am_26;
						$arr_masuk[$presensi->nip][27] = $presensi->am_27;
						$arr_masuk[$presensi->nip][28] = $presensi->am_28;
						$arr_masuk[$presensi->nip][29] = $presensi->am_29;
						$arr_masuk[$presensi->nip][30] = $presensi->am_30;
						$arr_masuk[$presensi->nip][31] = $presensi->am_31;

						$arr_pulang[$presensi->nip][1] = $presensi->ap_01;
						$arr_pulang[$presensi->nip][2] = $presensi->ap_02;
						$arr_pulang[$presensi->nip][3] = $presensi->ap_03;
						$arr_pulang[$presensi->nip][4] = $presensi->ap_04;
						$arr_pulang[$presensi->nip][5] = $presensi->ap_05;
						$arr_pulang[$presensi->nip][6] = $presensi->ap_06;
						$arr_pulang[$presensi->nip][7] = $presensi->ap_07;
						$arr_pulang[$presensi->nip][8] = $presensi->ap_08;
						$arr_pulang[$presensi->nip][9] = $presensi->ap_09;
						$arr_pulang[$presensi->nip][10] = $presensi->ap_10;
						$arr_pulang[$presensi->nip][11] = $presensi->ap_11;
						$arr_pulang[$presensi->nip][12] = $presensi->ap_12;
						$arr_pulang[$presensi->nip][13] = $presensi->ap_13;
						$arr_pulang[$presensi->nip][14] = $presensi->ap_14;
						$arr_pulang[$presensi->nip][15] = $presensi->ap_15;
						$arr_pulang[$presensi->nip][16] = $presensi->ap_16;
						$arr_pulang[$presensi->nip][18] = $presensi->ap_18;
						$arr_pulang[$presensi->nip][17] = $presensi->ap_17;
						$arr_pulang[$presensi->nip][19] = $presensi->ap_19;
						$arr_pulang[$presensi->nip][20] = $presensi->ap_20;
						$arr_pulang[$presensi->nip][21] = $presensi->ap_21;
						$arr_pulang[$presensi->nip][22] = $presensi->ap_22;
						$arr_pulang[$presensi->nip][23] = $presensi->ap_23;
						$arr_pulang[$presensi->nip][24] = $presensi->ap_24;
						$arr_pulang[$presensi->nip][25] = $presensi->ap_25;
						$arr_pulang[$presensi->nip][26] = $presensi->ap_26;
						$arr_pulang[$presensi->nip][27] = $presensi->ap_27;
						$arr_pulang[$presensi->nip][28] = $presensi->ap_28;
						$arr_pulang[$presensi->nip][29] = $presensi->ap_29;
						$arr_pulang[$presensi->nip][30] = $presensi->ap_30;
						$arr_pulang[$presensi->nip][31] = $presensi->ap_31;

						$kwk[$presensi->nip] = 0;
						$alpha[$presensi->nip] = 0;
						

					?>
						<tr>
						<!-- <td><?php echo $no ?></td> -->
							<td><?php echo "'" . $presensi->nip ?></td>
							<td><?php echo $presensi->nama_lengkap ?></td>
							<?php
							for ($tgl = 1; $tgl <=  jumlah_hari($tahun, $bulan); $tgl++) {
								$tanggal = sprintf("%02d", $tgl);
								$jadwal = $arr_presensi[$presensi->nip][$tgl];

								if ($waktu_kerja[$jadwal] > 0) {
									$periode[$presensi->nip][$tanggal] = $tahun . '-' . $bulan . '-' . $tanggal;

									if (!empty($arr_izin[$presensi->nip][$tahun . '-' . $bulan . '-' . $tanggal])) {
										if (($arr_izin[$presensi->nip][$tahun . '-' . $bulan . '-' . $tanggal] == 'WFH')) # || ($arr_izin[$presensi->nip][$tahun.'-'.$bulan.'-'.$tanggal] == 'TL')  ==> kalo mau tambahan TL 
										{
											if ($this->presensi_model->get_check_wfh($userinfo[$presensi->nip], $tahun . '-' . $bulan . '-' . $tanggal) > 0) {
												//jika absen
												$status[$presensi->nip][$tanggal] = $arr_izin[$presensi->nip][$tahun . '-' . $bulan . '-' . $tanggal];
												$color[$presensi->nip][$tanggal] = 'green';
											} else {
												//jika tidak absen
												$status[$presensi->nip][$tanggal] = 'A'; //'('.'A'.')'
												$color[$presensi->nip][$tanggal] = 'red';
												$alpha[$presensi->nip] += 1;
											}
										} else {
											$absen_masuk[$presensi->nip][$tanggal] = ''; //tampilkan tanda - di db
											$absen_pulang[$presensi->nip][$tanggal] = '';
											$status[$presensi->nip][$tanggal] = $arr_izin[$presensi->nip][$tahun . '-' . $bulan . '-' . $tanggal];
											$color[$presensi->nip][$tanggal] = 'green';
										}


										// Aktifkan WFH Aplha quote atas hapus.. bawah gantian di quote cuy...
										// quote atas mulai sebelum if( ($arr_izin....

                                                                        /*
                                        $absen_masuk[$presensi->nip][$tanggal] = '';//tampilkan tanda - di db
                                        $absen_pulang[$presensi->nip][$tanggal] = '';
                                        $status[$presensi->nip][$tanggal] = $arr_izin[$presensi->nip][$tahun.'-'.$bulan.'-'.$tanggal];
                                        $color[$presensi->nip][$tanggal] = 'green';
                                        */
									} else {

										$absen_buka[$presensi->nip][$tanggal] = $tahun . '-' . $bulan . '-' . $tanggal . ' ' . $absen_buka[$jadwal] . ':00';
										$jadwal_masuk[$presensi->nip][$tanggal] = $tahun . '-' . $bulan . '-' . $tanggal . ' ' . $jadwal_masuk[$jadwal] . ':00';
										$jadwal_tengah[$presensi->nip][$tanggal] = $tahun . '-' . $bulan . '-' . $tanggal . ' ' . $jadwal_tengah[$jadwal] . ':00';

										$jadwal_keluar[$presensi->nip][$tanggal] = date('Y-m-d H:i:s', strtotime($tahun . '-' . $bulan . '-' . $tanggal . ' ' . $jadwal_keluar[$jadwal] . ':00 +' . $selisih_hari[$jadwal] . ' day'));
										$absen_tutup[$presensi->nip][$tanggal] = date('Y-m-d H:i:s', strtotime($tahun . '-' . $bulan . '-' . $tanggal . ' ' . $absen_tutup[$jadwal] . ':00 +' . $selisih_hari[$jadwal] . ' day'));

										$absen_masuk[$presensi->nip][$tanggal] = $this->Presensi_model->get_masuk($userinfo[$presensi->nip], $absen_buka[$presensi->nip][$tanggal], $jadwal_tengah[$presensi->nip][$tanggal]);
										$absen_pulang[$presensi->nip][$tanggal] = $this->Presensi_model->get_pulang($userinfo[$presensi->nip], $jadwal_tengah[$presensi->nip][$tanggal], $absen_tutup[$presensi->nip][$tanggal]);

										if (strlen($arr_masuk[$presensi->nip][$tgl]) > 0) {
											$absen_masuk[$presensi->nip][$tanggal] = $arr_masuk[$presensi->nip][$tgl];
										}

										if (strlen($arr_pulang[$presensi->nip][$tgl]) > 0) {
											$absen_pulang[$presensi->nip][$tanggal] = $arr_pulang[$presensi->nip][$tgl];
										}

										if ((strlen($absen_masuk[$presensi->nip][$tanggal]) == 0) && (strlen($absen_pulang[$presensi->nip][$tanggal]) == 0)) {
											//alpha
											$color[$presensi->nip][$tanggal] = 'red';
											$status[$presensi->nip][$tanggal] = 'A';
											$alpha[$presensi->nip] += 1;
											$editable[$presensi->nip][$tanggal] = TRUE;
										} else {
											if ((strlen($absen_masuk[$presensi->nip][$tanggal]) >= 0) && (strlen($absen_pulang[$presensi->nip][$tanggal]) >= 0)) {
												if (($absen_masuk[$presensi->nip][$tanggal] <= $jadwal_masuk[$presensi->nip][$tanggal]) && ($absen_pulang[$presensi->nip][$tanggal] >= $jadwal_keluar[$presensi->nip][$tanggal])) {
													//hadir
													$color[$presensi->nip][$tanggal] = 'black';
													$status[$presensi->nip][$tanggal] = 'H';
												}
												if (($absen_masuk[$presensi->nip][$tanggal] > $jadwal_masuk[$presensi->nip][$tanggal]) && ($absen_pulang[$presensi->nip][$tanggal] > $jadwal_keluar[$presensi->nip][$tanggal])) {
													//terlambat
													$color[$presensi->nip][$tanggal] = 'orange';
													$status[$presensi->nip][$tanggal] = 'T';
													$kurang[$presensi->nip][$tanggal]  = floor((strtotime($absen_masuk[$presensi->nip][$tanggal]) - strtotime($jadwal_masuk[$presensi->nip][$tanggal])) / 60);
												}
												if (($absen_masuk[$presensi->nip][$tanggal] < $jadwal_masuk[$presensi->nip][$tanggal]) && ($absen_pulang[$presensi->nip][$tanggal] < $jadwal_keluar[$presensi->nip][$tanggal])) {
													//pulang awal
													$color[$presensi->nip][$tanggal] = 'orange';
													$status[$presensi->nip][$tanggal] = 'P';
													$kurang[$presensi->nip][$tanggal]  = floor((strtotime($jadwal_keluar[$presensi->nip][$tanggal]) - strtotime($absen_pulang[$presensi->nip][$tanggal])) / 60);
												}
												if (($absen_masuk[$presensi->nip][$tanggal] > $jadwal_masuk[$presensi->nip][$tanggal]) && ($absen_pulang[$presensi->nip][$tanggal] < $jadwal_keluar[$presensi->nip][$tanggal])) {
													//terlambat dan pulang awal
													$color[$presensi->nip][$tanggal] = 'orange';
													$status[$presensi->nip][$tanggal] = 'TP';
													$kurang_awal[$presensi->nip][$tanggal]  = floor((strtotime($absen_masuk[$presensi->nip][$tanggal]) - strtotime($jadwal_masuk[$presensi->nip][$tanggal])) / 60);
													$kurang_akhir[$presensi->nip][$tanggal]  = floor((strtotime($jadwal_keluar[$presensi->nip][$tanggal]) - strtotime($absen_pulang[$presensi->nip][$tanggal])) / 60);
													$kurang[$presensi->nip][$tanggal] = $kurang_awal[$presensi->nip][$tanggal] + $kurang_akhir[$presensi->nip][$tanggal];
													$kwk[$presensi->nip] = $kurang[$presensi->nip][$tanggal];
												}
											}
											if ((strlen($absen_masuk[$presensi->nip][$tanggal]) == 0) && (strlen($absen_pulang[$presensi->nip][$tanggal]) >= 0)) {
												$editable[$presensi->nip][$tanggal] = TRUE;
												if ($jadwal_keluar[$presensi->nip][$tanggal] <= $absen_pulang[$presensi->nip][$tanggal]) {
													//tidak absen masuk
													$color[$presensi->nip][$tanggal] = 'orange';
													$status[$presensi->nip][$tanggal] = 'TAM';
													$kurang[$presensi->nip][$tanggal] = floor($waktu_kerja[$jadwal] / 2);
												}
												if ($jadwal_keluar[$presensi->nip][$tanggal] >= $absen_pulang[$presensi->nip][$tanggal]) {
													//tidak absen masuk dan pulang cepat
													$color[$presensi->nip][$tanggal] = 'orange';
													$status[$presensi->nip][$tanggal] = 'TAMP';
													$kurang_awal[$presensi->nip][$tanggal]  = floor($waktu_kerja[$jadwal] / 2);
													$kurang_akhir[$presensi->nip][$tanggal]  = floor((strtotime($jadwal_keluar[$presensi->nip][$tanggal]) - strtotime($absen_pulang[$presensi->nip][$tanggal])) / 60);
													$kurang[$presensi->nip][$tanggal] = $kurang_awal[$presensi->nip][$tanggal] + $kurang_akhir[$presensi->nip][$tanggal];
													$kwk[$presensi->nip] = $kurang[$presensi->nip][$tanggal];
												}
											}

											if ((strlen($absen_masuk[$presensi->nip][$tanggal]) >= 0) && (strlen($absen_pulang[$presensi->nip][$tanggal]) == 0)) {
												$editable[$presensi->nip][$tanggal] = TRUE;
												if ($jadwal_masuk[$presensi->nip][$tanggal] >= $absen_masuk[$presensi->nip][$tanggal]) {
													//tidak absen pulang
													$color[$presensi->nip][$tanggal] = 'orange';
													$status[$presensi->nip][$tanggal] = 'TAP';
													$kurang[$presensi->nip][$tanggal] = floor($waktu_kerja[$jadwal] / 2);
												}
												if ($jadwal_masuk[$presensi->nip][$tanggal] <= $absen_masuk[$presensi->nip][$tanggal]) {
													//tidak absen pulang dan terlambat
													$color[$presensi->nip][$tanggal] = 'orange';
													$status[$presensi->nip][$tanggal] = 'TAPT';
													$kurang_awal[$presensi->nip][$tanggal]  = floor((strtotime($absen_masuk[$presensi->nip][$tanggal]) - strtotime($jadwal_masuk[$presensi->nip][$tanggal])) / 60);
													$kurang_akhir[$presensi->nip][$tanggal]  = floor($waktu_kerja[$jadwal] / 2);
													$kurang[$presensi->nip][$tanggal] = $kurang_awal[$presensi->nip][$tanggal] + $kurang_akhir[$presensi->nip][$tanggal];
													$kwk[$presensi->nip] = $kurang[$presensi->nip][$tanggal];
												}
											}
										}
									}

							?>
									<td class="center">
										<input placeholder="Masuk" id="absen_masuk_<?php echo $presensi->nip . '_' . $tanggal ?>" name="absen_masuk_<?php echo $presensi->nip . '_' . $tanggal ?>" type="hidden" value="<?php echo $absen_masuk[$presensi->nip][$tanggal] ?>">
										<?php echo jam_menit($absen_masuk[$presensi->nip][$tanggal])   ?><br>
										<input placeholder="Pulang" id="absen_pulang_<?php echo $presensi->nip . '_' . $tanggal ?>" name="absen_pulang_<?php echo $presensi->nip . '_' . $tanggal ?>" type="hidden" value="<?php echo $absen_pulang[$presensi->nip][$tanggal] ?>">
										<?php echo jam_menit($absen_pulang[$presensi->nip][$tanggal]) ?>
									</td>
									<td class="center">
										<input placeholder="Status" id="status_<?php echo $presensi->nip . '_' . $tanggal ?>" name="status_<?php echo $presensi->nip . '_' . $tanggal ?>" type="hidden" value="<?php echo $status[$presensi->nip][$tanggal] ?>" readonly="readonly">
										<a style="color:<?php echo $color[$presensi->nip][$tanggal] ?>" class="<?php echo $color[$presensi->nip][$tanggal] ?>-text" href="javascript:void(0)"><?php echo $status[$presensi->nip][$tanggal]; ?> </a>
										<!-- <?php if ((!$editable[$presensi->nip][$tanggal] == TRUE) || ($this->authentification->get_user('role') != '1')) { ?> <a class="<?php echo $color[$presensi->nip][$tanggal] ?>-text" href="javascript:void(0)"><?php echo $status[$presensi->nip][$tanggal]; ?> </a> <?php } else { ?> <a class="<?php echo $color[$presensi->nip][$tanggal] ?>-text" href="javascript:void(0)" onclick="ubah('<?php echo $presensi->nip ?>','<?php echo $tahun . '-' . $bulan . '-' . $tanggal ?>','harian')"><?php echo $status[$presensi->nip][$tanggal] ?></a> <?php } ?> -->
									</td>
								<?php
								} else {
									echo '<td class="center blue-text" colspan="2">' . $nama_singkat[$jadwal] . '<input placeholder="Status" id="status_' . $presensi->nip . '_' . $tanggal . '" name="status_' . $presensi->nip . '_' . $tanggal . '" type="hidden" value="' . $nama_singkat[$jadwal] . '" readonly="readonly"></td>';
								}

								?>
							<?php }
							$class = "";
							if ($kwk[$presensi->nip] > get_konfigurasi('batas_kwk')) {
								$class = "yellow";
							}
							?>
							<td class="center <?php echo $class ?>">
								<?php
								echo sprintf("%02d : %02d", floor($kwk[$presensi->nip] / 60), $kwk[$presensi->nip] % 60);
								?>
								<input placeholder="KWK" id="kwk_<?php echo $presensi->nip ?>" name="kwk_<?php echo $presensi->nip ?>" type="hidden" class="right-align <?php echo $class ?>" value="<?php echo $kwk[$presensi->nip]; ?>" readonly="readonly">
							</td>
							<?php
							$class = "";
							if ($alpha[$presensi->nip] > get_konfigurasi('batas_alpha')) {
								$class = "yellow";
							}
							?>
							<td class="right-align <?php echo $class ?>"><?php echo $alpha[$presensi->nip]; ?>
								<input placeholder="Alpha" id="alpha_<?php echo $presensi->nip ?>" name="alpha_<?php echo $presensi->nip ?>" type="hidden" class="right-align <?php echo $class ?>" value="<?php echo $alpha[$presensi->nip]; ?>" readonly="readonly">
							</td>

							<td> <a class="btn-floating waves-effect waves-light red" href="javascript:void(0)" onclick="hapus('<?php echo $presensi->id ?>')" title="Hapus"><i class="material-icons">delete</i></a></td>

							<!-- <td onclick="javascript: return confirm('Anda yakin hapus?')"> <?php echo anchor('data/hapus' . $presensi->id, ' <div class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>Hapus</div>') ?> </td> -->
						</tr>
					<?php } ?>
				</tbody>
                    </table>
                    <br />
                    <br />
                    <div class="text-left">
                        <!-- <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan </button> -->
                    </div>
                    <?php echo form_close();  ?>






                </div>
            </div>
        </div>

    </div>
    <!-- /content area -->

    <?php require(__DIR__ . '/../../template/footer.php') ?>

    <script src="https://cdn.datatables.net/fixedcolumns/3.3.3/js/dataTables.fixedColumns.min.js"></script>
	


    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>



<script>
        /* ------------------------------------------------------------------------------
         *
         *  # Buttons extension for Datatables. HTML5 examples
         *
         *  Demo JS code for datatable_extension_buttons_html5.html page
         *
         * ---------------------------------------------------------------------------- */


        // Setup module
        // ------------------------------

        var DatatableButtonsHtml5 = function() {


            //
            // Setup module components
            //

            // Basic Datatable examples
            var _componentDatatableButtonsHtml5 = function() {
                if (!$().DataTable) {
                    console.warn('Warning - datatables.min.js is not loaded.');
                    return;
                }

                // Setting datatable defaults
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Filter:</span> _INPUT_',
                        searchPlaceholder: 'Type to filter...',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {
                            'first': 'First',
                            'last': 'Last',
                            'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                            'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                        }
                    }
                });


                // Basic initialization


                var tabel = $('.datatable-button-html5-basic').DataTable({
                        buttons: {
                            dom: {
                                button: {
                                    className: 'btn btn-light'
                                }
                            },
                            buttons: [
                                // 'excelHtml5'
                            ]
                        },

                        'scrollY': "500px",
                        'scrollX': true,
                        'scrollCollapse': true,
                        'paging': false,
                        'fixedColumns': {
                            leftColumns: 1,
                        },

                    }

                );

                $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                    $.fn.dataTable.tables({
                        visible: true,
                        api: true
                    }).columns.adjust();
                });



            };

            // Select2 for length menu styling
            var _componentSelect2 = function() {
                if (!$().select2) {
                    console.warn('Warning - select2.min.js is not loaded.');
                    return;
                }

                // Initialize
                $('.dataTables_length select').select2({
                    minimumResultsForSearch: Infinity,
                    dropdownAutoWidth: true,
                    width: 'auto'
                });
            };


            //
            // Return objects assigned to module
            //

            return {
                init: function() {
                    _componentDatatableButtonsHtml5();
                    _componentSelect2();
                }
            }
        }();


        // Initialize module
        // ------------------------------

        document.addEventListener('DOMContentLoaded', function() {
            DatatableButtonsHtml5.init();
        });
    </script>


    <!-- Modal edit di form kerja_add.php -->

    <div id="edit-data" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Jadwal</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form action="#">
                    <div class="modal-body">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>NIP</label>
                                    <input type="hidden" readonly id="id" name="id" class="form-control">
                                    <input type="text" readonly id="nip" name="nip" class="form-control">
                                </div>

                                <div class="col-sm-6">
                                    <label>Nama</label>
                                    <input type="text" readonly id="nama" name="nama" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Hari</label>
                                    <input type="text" readonly id="hari" name="hari" class="form-control">
                                </div>

                                <div class="col-sm-6">
                                    <label>Tanggal</label>
                                    <input type="text" readonly id="tanggal" name="tanggal" class="form-control">
                                    <input type="hidden" readonly id="tgl" name="tgl" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Jadwal Saat Ini</label>
                                    <input type="text" readonly id="jadwal" name="jadwal" class="form-control">
                                </div>
                                <div class="col-sm-6">
                                    <label>Ubah Jadwal</label>
                                    <select class="form-control select2" style="width: 100%;" name="jadwal_baru" id="jadwal_baru">
                                        <option value="">Pilih</option>
                                        <?php foreach ($jamkerja as $ja) : ?>
                                            <option value="<?= $ja->id ?>"><?= $ja->nama_singkat ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>


                            </div>
                        </div>



                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="button" id="btn_update" class="btn bg-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal edit di form kerja_add.php -->


    <script>
        $(document).ready(function() {
            // Untuk sunting
            $('#edit-data').on('show.bs.modal', function(event) {
                var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
                var modal = $(this)
                // Isi nilai pada field
                modal.find('#id').attr("value", div.data('id'));
                modal.find('#nip').attr("value", div.data('nip'));
                modal.find('#nama').attr("value", div.data('nama'));
                modal.find('#hari').attr("value", div.data('hari'));
                modal.find('#tanggal').attr("value", div.data('tanggal'));
                modal.find('#tgl').attr("value", div.data('tgl'));
                modal.find('#jadwal').attr("value", div.data('jadwal'));

            });
        });


        //Update Jadwak
        $('#btn_update').on('click', function() {
            // var id = modal.find('#id').attr("value",div.data('id'));
            var id = $('#id').val();
            var tanggal = $('#tgl').val();
            var jadwal_baru = $('#jadwal_baru').val();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('jadwal/update_jadwal_satuan') ?>",
                dataType: "JSON",
                data: {
                    id: id,
                    tanggal: tanggal,
                    jadwal_baru: jadwal_baru
                },
                success: function(data) {
                    // console.log(data);
                    // $('[name="kobar_edit"]').val("");
                    // $('[name="nabar_edit"]').val("");
                    // $('[name="harga_edit"]').val("");
                    // $('#ModalaEdit').modal('hide');
                    // tampil_data_barang();
                    location.reload();
                }
            });
            return false;
        });
    </script>

    <!-- Modal edit di form kerja_add.php -->