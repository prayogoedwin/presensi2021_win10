<div class="container">
	<div class="modal-header">
		<h5 class="modal-title"><?php echo $title ?></h5>
		<button type="button" class="close" data-dismiss="modal">&times;</button>
	</div>

	<form name="form" id="form_harian" method="post">
		<div class="modal-body">

			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label>Absen Masuk</label>
						<input id="tanggal_masuk" class="form-control" name="tanggal_masuk" placeholder="Tanggal Masuk" type="text" readonly="readonly" value="<?php echo $tanggal ?>">
					</div>

					<div class="col-sm-4">
						<label>Jam</label>
						<input id="absen_masuk_jam" data-length="2" class="form-control" name="absen_masuk_jam" placeholder="Jam" type="text" value="<?php echo substr($form_data['absen_masuk'], 11, 2) ?>">
					</div>

					<div class="col-sm-4">
						<label>Menit</label>
						<input id="absen_masuk_menit" data-length="2" class="form-control" name="absen_masuk_menit" placeholder="Menit" type="text" value="<?php echo substr($form_data['absen_masuk'], 14, 2) ?>">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label>Absen Pulang</label>
						<input id="tanggal_masuk" class="form-control" name="tanggal_masuk" placeholder="Tanggal Masuk" type="text" readonly="readonly" value="<?php echo $tanggal ?>">
					</div>

					<div class="col-sm-4">
						<label>Jam</label>
						<input id="absen_pulang_jam" data-length="2" class="form-control" name="absen_pulang_jam" placeholder="Jam" type="text" value="<?php echo substr($form_data['absen_pulang'], 11, 2) ?>">
					</div>

					<div class="col-sm-4">
						<label>Menit</label>
						<input id="absen_pulang_menit" data-length="2" class="form-control" name="absen_pulang_menit" placeholder="Menit" type="text" value="<?php echo substr($form_data['absen_pulang'], 14, 2) ?>">
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">

					<div class="col-sm-12">
						<label>Status</label>
						<select id="status" name="status" class="select">
							<?php foreach ($status->result() as $status) {

								$selected = '';
								if ($status->id == $form_data['status']) {
									$selected = 'selected="selected"';
								}
								echo '<option ' . $selected . ' value="' . $status->id . '">' . $status->nama . '</option>';
							} ?>
						</select>
					</div>


				</div>
			</div>



		</div>

		<!-- <div class="modal-footer">
			<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			<button type="button" id="btn_update" class="btn bg-primary">Update</button>
		</div> -->
	</form>

	<div class="modal-footer">
	<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
	  <button onclick="simpan_harian()" class="btn bg-success"><i class="fa fa-save"></i> Simpan</button>  
 </div>

</div>


<!-- Modal edit di form kerja_add.php -->








<script type="text/javascript">
	$(function() {
		$('.select').select2();

	});

	function progressbar(tipe) {
		var loader = $('#progressbar');
		switch (tipe) {
			case 'open':
				loader.modal({
					dismissible: false, // Modal can be dismissed by clicking outside of the modal
					opacity: .5, // Opacity of modal background
				});
				loader.modal('show');
				break;

			case 'close':
				loader.modal('hide');
				break;
		}
	}

	function simpan_harian() {
		$.ajax({
			url: '<?php echo $form_action ?>',
			data: $('#form_harian').serialize(),
			beforeSend: function() {
				progressbar('open');
			},
			success: function(result) {
				progressbar('close');
				if (result.success) {
					alert('success : ' + result.message);
					popup('close', 'medium');
					location.reload();
				} else {
					alert('error : ' + result.message);
				}

			},
			type: "post",
			dataType: "json"
		});

	}
</script>