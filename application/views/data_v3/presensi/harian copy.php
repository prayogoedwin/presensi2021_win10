<h5><?php echo $title ?></h5>
<form name="form" id="form_harian" method="post">
<div class="row">
<div class=" col s6">
	<label for="absen_masuk">Absen Masuk</label>
	<div class="row">
    <input id="tanggal_masuk" class="col s6" name="tanggal_masuk" placeholder="Tanggal Masuk" type="text" readonly="readonly" value="<?php echo $tanggal ?>">
	<div class="col s1">  </div>
	<input id="absen_masuk_jam" data-length="2" class="col s2" name="absen_masuk_jam" placeholder="Jam" type="text" value="<?php echo substr($form_data['absen_masuk'],11,2) ?>">
	<div class="col s1"> : </div>
	<input id="absen_masuk_menit" data-length="2" class="col s2" name="absen_masuk_menit" placeholder="Menit" type="text" value="<?php echo substr($form_data['absen_masuk'],14,2) ?>">
	<div class="col s1">  </div>
	</div>
</div>

<div class="col s6">
	<label for="absen_pulang">Pulang</label>
	<div class="row">
    <input id="tanggal_pulang" class="col s6" name="tanggal_pulang" placeholder="Tanggal Pulang" type="text" readonly="readonly" value="<?php echo date('Y-m-d', strtotime($tanggal.' +'.$form_data['selisih_hari'].' day')) ?>">
	<div class="col s1">  </div>
	<input id="absen_pulang_jam" data-length="2" class="col s2" name="absen_pulang_jam" placeholder="Jam" type="text" value="<?php echo substr($form_data['absen_pulang'],11,2) ?>">
	<div class="col s1"> : </div>
	<input id="absen_pulang_menit" data-length="2" class="col s2" name="absen_pulang_menit" placeholder="Menit" type="text" value="<?php echo substr($form_data['absen_pulang'],14,2) ?>">
	<div class="col s1">  </div>
	</div>
</div>

<div class="input-field col s12 m12 l12 xl12">
	<label for="status">Status</label>
     <select id="status" name="status" class="select">
		<?php foreach($status->result() as $status)
        {
			
			$selected = '';
			if($status->id==$form_data['status'])
            {
				$selected = 'selected="selected"';
            }
			echo '<option '.$selected.' value="'.$status->id.'">'.$status->nama.'</option>';
		 } ?>
    </select>
</div>

</div>




			
</form>

<div class="modal-footer">
      <button onclick="$('#main').fadeIn();popup('close','medium')" class="modal-action modal-close waves-effect waves-light btn blue"><i class="material-icons left">close</i> Tutup</button>
	  <button onclick="simpan_harian()" class="waves-effect waves-light btn green"><i class="material-icons left">save</i> Simpan</button>  
 </div>


	  

<script type="text/javascript">
$(function(){
	$('.select').select2();
	
});

function progressbar(tipe)
{
    var loader = $('#progressbar');
	switch (tipe) 
	{
	case 'open':
		loader.modal({
		dismissible: false, // Modal can be dismissed by clicking outside of the modal
		opacity: .5, // Opacity of modal background
		});
	loader.modal('show');
	break;
	
	case 'close':
	loader.modal('hide');
	break;
	}
}

function simpan_harian()
{
	$.ajax({
        url: '<?php echo $form_action?>', 
        data: $('#form_harian').serialize(),
		beforeSend: function() {
		progressbar('open');
		},
		success: function(result){
			progressbar('close');
			if (result.success)
			{
			alert('success : '+result.message);
			popup('close','medium');
			location.reload();
			} 
			
			else 
			{
			alert('error : '+result.message);
			}
			
            },
        type: "post", 
       dataType: "json"
    }); 
	
}
		
</script>

	