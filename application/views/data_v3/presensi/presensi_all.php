<?php require(__DIR__ . '/../../template/sesi.php') ?>
<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Data - <span class="font-weight-semibold">Presensi</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                 <div class="d-flex justify-content-center">
							<!-- <a href="#" class="btn btn-link btn-float text-default" data-toggle="modal" data-target="#keterangan" ><i class="fa fa-file"></i><span>Keterangan</span></a> -->
							<!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a> -->
						</div> 
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->
            <br />

        <?php     } ?>

        <!-- Form inputs -->
        <div class="row">
        <div class="card col-lg-8" style="margin-right:20px">


            <div class="card-body ">
                <!-- <form action="#"> -->
                <form action="<?php echo base_url(); ?>data_v3/presensi_save" method="get" accept-charset="utf-8">
                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Bulan</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" required name="bulan" id="bulan">
                            <?php for ($j = 1; $j <= 12; $j++) {
                                $selected = '';
                                $bln = sprintf("%02d", $j);
                                if ($month == $bln) {
                                    $selected = 'selected="selected"';
                                }
                                echo '<option ' . $selected . ' value="' . $bln . '">' . namaBulan($j) . '</option>';
                            } ?>

                        </select>
                    </div>



                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Tahun</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" required name="tahun" id="tahun">
                            <?php for ($tahun = 2018; $tahun <= date("Y") + 1; $tahun++) { ?>
                                <option value="<?php echo $tahun ?>" <?php if ($year == $tahun) echo 'selected="selected"' ?>><?php echo $tahun ?></option>
                            <?php } ?>

                        </select>
                    </div>



                </div>

                <!-- <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Set Dengan Jadwal Umum</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" required name="jadwal_umum" id="jadwal_umum">
                            <option value="">Pilih</option>
                            <option value="1">Ya</option>
                            <option value="0">Tidak</option>


                        </select>
                    </div>



                </div> -->




                <div class="text-right">
                <button type="button" class="btn btn-success pull-right" onclick="presensi_check()"> <i class="fa fa-search"></i>Cek </button>
                <button type="submit" class="btn btn-primary pull-right"> <i class="fa fa-save"></i> Simpan </button>
                </div>

                            </form>
              
            </div>
        </div>
        

        
        <!-- /form inputs -->
        </div>

        <div class="row">
            <div class="card col-lg-8" style="margin-right:20px">


                <div class="card-body ">
                    <div class="table-responsive table-result">
                        <table class="table table-bordered">
                            <tr>
                                <th colspan="3">Bulan <?= bulan($month) ?> Tahun <?= $year ?></th>
                            </tr>
                            <tr>
                                <th>No</th>
                                <th>OPD</th>
                                <th>Status</th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>



    </div>
    <!-- /content area -->

    <?php require(__DIR__ . '/../../template/footer.php') ?>

    <script>
        presensi_check();
        
        function presensi_check() {
            $.ajax({
                url: '<?php echo base_url(); ?>data_v3/presensi/check',
                data: {
                    'year' : $('#tahun').val(), 
                    'month' : $('#bulan').val(),  
                },
                beforeSend: function() {
                    $('.table-result').html('Generating table ...');
                },
                success: function(result) {
                    $('.table-result').html(result);
                },
                type: "post",
                dataType: "html"
            });

        }

        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });

        <?php if(isset($not_found)){ ?>
            alert('<?php echo $not_found; ?>');
            <?php } ?>
    </script>

    <script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#opd").change(function() { // Ketika user mengganti atau memilih Mutasi Ke
                $("#unit").hide(); // Sembunyikan dulu combobox kota nya
                $("#loading").show(); // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("fungsi/unit_kerja"); ?>", // Isi dengan url/path file php yang dituju
                    data: {
                        opd: $("#opd").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide(); // Sembunyikan loadingnya
                        // set isi dari combobox 
                        // lalu munculkan kembali combobox 
                        $("#unit").html(response.list_lokasis).show();
                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });
            });
        });

    </script>

</div>
<!-- /main content -->

</div>
<!-- /page content -->

</body>

</html>