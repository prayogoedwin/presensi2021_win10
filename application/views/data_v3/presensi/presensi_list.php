<?php require(__DIR__ . '/../../template/sesi.php') ?>

<style>
    table.datatable-button-html5-basic,
    table.datatable-button-html5-basic tr,
    table.datatable-button-html5-basic th,
    table.datatable-button-html5-basic td {
        border: 1px solid #000000;
        font-size: 14px;
    }

    table.datatable-button-html5-basic .col-date {
        width: 150px;
        max-width: 150px;
        min-width: 150px;
    }

    

    table.datatable-button-html5-basic .col-id {
        width: 130px;
        max-width: 1300px;
        min-width: 130px;
    }

    table.datatable-button-html5-basic .col-name {
        width: 200px;
        max-width: 200px;
        min-width: 200px;
    }

    .holiday {
        color: red;
    }

    .blue-text {
        color: #2196F3 !important;
    }
</style>

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Data - <span class="font-weight-semibold">Presensi Bulan <?= bulan($bulan) ?> Tahun <?= $tahun ?></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                <div class="d-flex justify-content-center">
                    <a href="#" class="btn btn-link btn-float text-default" data-toggle="modal" data-target="#keterangan"><i class="fa fa-file"></i><span>Keterangan</span></a>

                    <?php
                    $today = date('Y-m-d');
                    $close = date('Y-m-d', strtotime("+1 months", strtotime($tahun . '-' . $bulan . '-5')));
                    //echo date('Y-m-d');
                    //echo $close;
                    if ($today <= $close || $this->authentification->get_user('role') == '1' || $this->authentification->get_user('role') == '2') { ?>
                        <button id="simpan" class="btn btn-link btn-float text-default" href="javascript:void(0)" onclick="simpan()"><i class="fa fa-save"></i><span>Simpan (Kalkulasi)</span></button>

                    <?php }  ?>

                    <!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a> -->
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->
            <br />

        <?php     } ?>




        <div class="col-md-12">
            <div class="">
                <!-- <div class="card"> -->
                <!-- <div class="card-header header-elements-inline">
								<h6 class="card-title">Basic tabs</h6>
								<div class="header-elements">
									<div class="list-icons">
				                		<a class="list-icons-item" data-action="collapse"></a>
				                		<a class="list-icons-item" data-action="reload"></a>
				                		<a class="list-icons-item" data-action="remove"></a>
				                	</div>
			                	</div>
							</div> -->

                <div class="card-body">
                <form action="<?php echo base_url(); ?>portal/auth" id='form' method="post" accept-charset="utf-8">
                    <?php #echo form_open('portal/auth');  ?>
                    <table class="datatable-button-html5-basic" border="1" id="jadwal">
                        <thead>
                            <tr>
                                <!-- <th class="center">No.</th> -->
                                <th class="center ftl col-id" rowspan="3">NIP</th>
                                <th class="center ftl col-name" rowspan="3">Nama</th>
                                <?php
                                for ($tgl = 1; $tgl <=  jumlah_hari($tahun, $bulan); $tgl++) {
                                    echo '<th class="center col-date" colspan="2">' . $tgl . '<br>' . nama_hari($tahun . '-' . $bulan . '-' . sprintf("%02d", $tgl)) . '</th>';
                                } ?>
                                <th class="center col-date" rowspan="3"> KWK (jam:menit) </th>
                                <th class="center col-date" rowspan="3"> Alpha (hari) </th>
                                <th class="center col-date" rowspan="3"> Action </th>
                            </tr>
                            <tr>
                                <!-- <th colspan="3" rowspan="2">
<div class="input-field">
  <input name="keyword" id="keyword" type="search" placeholder="Cari Nama">
  <label class="label-icon" for="search"><i class="material-icons">search</i></label>
  <i class="material-icons">close</i>
</div>
 </th> -->
                                <?php
                                for ($tgl = 1; $tgl <=  jumlah_hari($tahun, $bulan); $tgl++) { ?>
                                    <th>Masuk</th>
                                    <th rowspan="2">Status</th>
                                <?php } ?>
                            </tr>
                            <?php
                            for ($tgl = 1; $tgl <=  jumlah_hari($tahun, $bulan); $tgl++) { ?>
                                <th>Pulang</th>
                            <?php } ?></tr>

                        </thead>
                        <tbody>

                            <?php
                            $no = 0;

                            foreach ($presensi_data as $key_nip => $presensi) {
                                $no++;
                            ?>
                                <tr>
                                    <!-- <td><?php echo $no ?></td> -->
                                    <td class="ftl col-id"><?php echo "'" . $key_nip ?></td>
                                    <td class="ftl col-name"><?php echo $presensi['data']->nama_lengkap ?></td>
                                    <?php
                                    foreach ($presensi['date'] as $key_date => $value_date) {

                                        $tanggal = sprintf("%02d", $key_date);
                                        if ($value_date['waktu_kerja'] > 0) {

                                    ?>
                                            <td class="center">
                                                <input placeholder="Masuk" id="absen_masuk_<?php echo $key_nip . '_' . $tanggal ?>" name="absen_masuk_<?php echo $key_nip . '_' . $tanggal ?>" type="hidden" value="<?php echo $value_date['absen_masuk'] ?>">
                                                <?php echo jam_menit($value_date['absen_masuk'])   ?><br>
                                                <input placeholder="Pulang" id="absen_pulang_<?php echo $key_nip . '_' . $tanggal ?>" name="absen_pulang_<?php echo $key_nip . '_' . $tanggal ?>" type="hidden" value="<?php echo $value_date['absen_pulang'] ?>">
                                                <?php echo jam_menit($value_date['absen_pulang']) ?>
                                            </td>
                                            <td class="center">
                                                <input placeholder="Status" id="status_<?php echo $key_nip . '_' . $tanggal ?>" name="status_<?php echo $key_nip . '_' . $tanggal ?>" type="hidden" value="<?php echo $value_date['status'] ?>" readonly="readonly">
                                                <a style="color:<?php echo $value_date['color'] ?>" class="<?php echo $value_date['color'] ?>-text" href="javascript:void(0)" onclick="ubah('<?php echo $key_nip ?>','<?php echo $tahun . '-' . $bulan . '-' . $tanggal ?>','harian')"><?php echo $value_date['status']; ?> </a>
                                                <!-- <?php if ((!$value_date['editable'] == TRUE) || ($this->authentification->get_user('role') != '1')) { ?> <a class="<?php echo $value_date['color'] ?>-text" href="javascript:void(0)"><?php echo $value_date['status']; ?> </a> <?php } else { ?> <a class="<?php echo $value_date['color'] ?>-text" href="javascript:void(0)" onclick="ubah('<?php echo $key_nip ?>','<?php echo $tahun . '-' . $bulan . '-' . $tanggal ?>','harian')"><?php echo $value_date['status'] ?></a> <?php } ?> -->
                                            </td>
                                        <?php
                                        } else {
                                            echo '<td class="center blue-text" colspan="2">' . $value_date['nama_singkat'] . '<input placeholder="Status" id="status_' . $key_nip . '_' . $tanggal . '" name="status_' . $key_nip . '_' . $tanggal . '" type="hidden" value="' . $value_date['nama_singkat'] . '" readonly="readonly"></td>';
                                        }

                                        ?>
                                    <?php }
                                    $class = "";
                                    if ($presensi['kwk'] > get_konfigurasi('batas_kwk')) {
                                        $class = "yellow";
                                    }
                                    ?>
                                    <td class="center <?php echo $class ?>">
                                        <?php
                                        echo sprintf("%02d : %02d", floor($presensi['kwk'] / 60), $presensi['kwk'] % 60);
                                        ?>
                                        <input placeholder="KWK" id="kwk_<?php echo $key_nip ?>" name="kwk_<?php echo $key_nip ?>" type="hidden" class="right-align <?php echo $class ?>" value="<?php echo $presensi['kwk']; ?>" readonly="readonly">
                                    </td>
                                    <?php
                                    $class = "";
                                    if ($presensi['alpha'] > get_konfigurasi('batas_alpha')) {
                                        $class = "yellow";
                                    }
                                    ?>
                                    <td class="right-align <?php echo $class ?>"><?php echo $presensi['alpha']; ?>
                                        <input placeholder="Alpha" id="alpha_<?php echo $key_nip ?>" name="alpha_<?php echo $key_nip ?>" type="hidden" class="right-align <?php echo $class ?>" value="<?php echo $presensi['alpha']; ?>" readonly="readonly">
                                    </td>

                                    <td class="center-align"> <a class="btn btn-danger" href="javascript:void(0)" onclick="hapus('<?php echo $presensi['data']->id ?>')" title="Hapus"><i class="fa fa-trash"></i> Hapus</a></td>

                                    <!-- <td onclick="javascript: return confirm('Anda yakin hapus?')"> <?php echo anchor('data/hapus' . $presensi->id, ' <div class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>Hapus</div>') ?> </td> -->
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <br />
                    <br />
                    <div class="text-left">
                        <!-- <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan </button> -->
                    </div>
                    <?php #echo form_close();  ?>
                </form>






                </div>
            </div>
        </div>

    </div>
    <!-- /content area -->

    <?php require(__DIR__ . '/../../template/footer.php') ?>

    <script src="https://cdn.datatables.net/fixedcolumns/3.3.3/js/dataTables.fixedColumns.min.js"></script>
    <script src="<?php echo base_url('assets/jquery.scrollabletable.js'); ?>"></script>



    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>



    <script>
        /* ------------------------------------------------------------------------------
         *
         *  # Buttons extension for Datatables. HTML5 examples
         *
         *  Demo JS code for datatable_extension_buttons_html5.html page
         *
         * ---------------------------------------------------------------------------- */


        // Setup module
        // ------------------------------

        var DatatableButtonsHtml5 = function() {


            //
            // Setup module components
            //

            // Basic Datatable examples
            var _componentDatatableButtonsHtml5 = function() {
                if (!$().DataTable) {
                    console.warn('Warning - datatables.min.js is not loaded.');
                    return;
                }

                // Setting datatable defaults
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Filter:</span> _INPUT_',
                        searchPlaceholder: 'Type to filter...',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {
                            'first': 'First',
                            'last': 'Last',
                            'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                            'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                        }
                    }
                });


                // Basic initialization


                var tabel = $('.datatable-button-html5-basic-old').DataTable({
                        buttons: {
                            dom: {
                                button: {
                                    className: 'btn btn-light'
                                }
                            },
                            buttons: [
                                // 'excelHtml5'
                            ]
                        },

                        'scrollY': "500px",
                        'scrollX': true,
                        'scrollCollapse': true,
                        'paging': false,
                        'fixedColumns': {
                            leftColumns: 1,
                        },

                    }

                );

                $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                    $.fn.dataTable.tables({
                        visible: true,
                        api: true
                    }).columns.adjust();
                });



            };

            // Select2 for length menu styling
            var _componentSelect2 = function() {
                if (!$().select2) {
                    console.warn('Warning - select2.min.js is not loaded.');
                    return;
                }

                // Initialize
                $('.dataTables_length select').select2({
                    minimumResultsForSearch: Infinity,
                    dropdownAutoWidth: true,
                    width: 'auto'
                });
            };


            //
            // Return objects assigned to module
            //

            return {
                init: function() {
                    _componentDatatableButtonsHtml5();
                    _componentSelect2();
                }
            }
        }();


        // Initialize module
        // ------------------------------

        document.addEventListener('DOMContentLoaded', function() {
            DatatableButtonsHtml5.init();
        });


        // $('#jadwal').scrollabletable({
        // 	'max_height_scrollable' : 500,
        // 	'scroll_horizontal' : 1,
        // 	'max_width' : $('#jadwal').parent().width(),
        // 	'padding_right' : 15,
        // 	'tambahan_top_left' : 50
        // });
        /*dan hapus ini*/
    </script>


    <!-- Modal edit di form kerja_add.php -->

    <div id="edit-data" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Jadwal</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form action="#">
                    <div class="modal-body">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>NIP</label>
                                    <input type="hidden" readonly id="id" name="id" class="form-control">
                                    <input type="text" readonly id="nip" name="nip" class="form-control">
                                </div>

                                <div class="col-sm-6">
                                    <label>Nama</label>
                                    <input type="text" readonly id="nama" name="nama" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Hari</label>
                                    <input type="text" readonly id="hari" name="hari" class="form-control">
                                </div>

                                <div class="col-sm-6">
                                    <label>Tanggal</label>
                                    <input type="text" readonly id="tanggal" name="tanggal" class="form-control">
                                    <input type="hidden" readonly id="tgl" name="tgl" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Jadwal Saat Ini</label>
                                    <input type="text" readonly id="jadwal" name="jadwal" class="form-control">
                                </div>
                                <div class="col-sm-6">
                                    <label>Ubah Jadwal</label>
                                    <select class="form-control select2" style="width: 100%;" name="jadwal_baru" id="jadwal_baru">
                                        <option value="">Pilih</option>
                                        <?php foreach ($jamkerja as $ja) : ?>
                                            <option value="<?= $ja->id ?>"><?= $ja->nama_singkat ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>


                            </div>
                        </div>



                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="button" id="btn_update" class="btn bg-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal edit di form kerja_add.php -->


    <script>
        $(document).ready(function() {
            // Untuk sunting
            $('#edit-data').on('show.bs.modal', function(event) {
                var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
                var modal = $(this)
                // Isi nilai pada field
                modal.find('#id').attr("value", div.data('id'));
                modal.find('#nip').attr("value", div.data('nip'));
                modal.find('#nama').attr("value", div.data('nama'));
                modal.find('#hari').attr("value", div.data('hari'));
                modal.find('#tanggal').attr("value", div.data('tanggal'));
                modal.find('#tgl').attr("value", div.data('tgl'));
                modal.find('#jadwal').attr("value", div.data('jadwal'));

            });
        });


        //Update Jadwak
        $('#btn_update').on('click', function() {
            // var id = modal.find('#id').attr("value",div.data('id'));
            var id = $('#id').val();
            var tanggal = $('#tgl').val();
            var jadwal_baru = $('#jadwal_baru').val();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('jadwal/update_jadwal_satuan') ?>",
                dataType: "JSON",
                data: {
                    id: id,
                    tanggal: tanggal,
                    jadwal_baru: jadwal_baru
                },
                success: function(data) {
                    // console.log(data);
                    // $('[name="kobar_edit"]').val("");
                    // $('[name="nabar_edit"]').val("");
                    // $('[name="harga_edit"]').val("");
                    // $('#ModalaEdit').modal('hide');
                    // tampil_data_barang();
                    location.reload();
                }
            });
            return false;
        });


        function load_page(page, div) {
            var progressbar = '<h5>Menampilkan Halaman ...</h5><div class="progress"><div class="indeterminate"></div></div>';
            $.ajax({
                url: page,
                beforeSend: function() {
                    $(div).html(progressbar);
                },
                success: function(response) {
                    $(div).html(response);
                },
                dataType: "html"
            });
            return
            false;
        }

        function popup(type, size, url = '') {
            var pop = $('#popup_' + size),
                body = pop.find('.modal-content');


            switch (type) {
                case 'open':
                    pop.modal({
                        dismissible: false,
                        opacity: .5,
                    });
                    pop.modal('show');
                    body.html('');
                    load_page(url, body);

                    break;

                case 'close':
                    body.html('');
                    pop.modal('hide');
                    break;
            }



        }



        function loading(type) {
            var loader = $('#loader');
            switch (type) {
                case 'open':
                    loader.modal({
                        dismissible: false, // Modal can be dismissed by clicking outside of the modal
                        opacity: .7, // Opacity of modal background 
                    });
                    loader.modal('show');
                    break;

                case 'close':
                    loader.modal('hide');
                    break;
            }

        }

        function confirm_action(heading, question, func) {

            var pop = $('#popup_small'),
                body = pop.find('.modal-content');
            body.html('');
            var konfirmasi =
                $('<div class="center"><h5>' + heading + '</h5>' +
                    '<p>' + question + '</p>' +
                    '<button id="ok" class="btn btn-info"><i class="fa fa-check"></i> Ya</button>&nbsp;&nbsp;' +
                    '<button onclick="popup("close","small")" data-dismiss="modal" class="modal-action modal-close btn btn-danger"><i class="fa fa-times left"></i> Batal</button>');

            pop.modal({
                dismissible: false,
                opacity: .5,
            });
            body.html(konfirmasi);
            pop.modal('show');
            $("#ok").click(function() {
                popup("close", "small");
                func(true);
                return func;
            });



        }

        function simpan() {
            confirm_action('Yakin Akan Disimpan (Usulkan kalkulasi TPP) ?', 'Data ini akan otomatis ke database TPP BKD Prov. Jateng, Untuk SKPD yang ada Jadwal sift Jangan diSAVE dulu sebelum data fix atau masih ada Revisi Jadwal', function(r) {
                if (r) {
                    $.ajax({
                        url: '<?php echo $form_action ?>',
                        data: $('#form').serialize(),
                        beforeSend: function() {
                            loading('open');
                        },
                        success: function(result) {
                            loading('close');
                            if (result.success) {
                                alert('success : ' + result.message);
                                //location.reload();
                            } else {
                                alert('error : ' + result.message);

                            }

                        },
                        type: "post",
                        dataType: "json"
                    });
                }
            });

        }



        function hapus(id) {
            if (id) {

                confirm_action('Konfirmasi', 'Yakin Akan Dihapus ?', function(r) {
                    if (r) {
                        loading('open');
                        $.post(site + 'data_v3/presensi/delete/' + id, function(result) {
                            loading('close');
                            if (result.success) {
                                alert('success : ' + result.message);
                                location.reload();
                            } else {
                                alert('error : ' + result.message);
                            }

                        }, 'json');

                    }

                });

            }
        }

        function ubah(nip, tanggal, tipe) {
            if (nip && tanggal && tipe) {
                //console.log(site + 'data_v3/presensi/harian/' + nip + '/' + tanggal);

                switch (tipe) {
                    case 'harian':
                        popup('open', 'medium', site + 'data_v3/presensi/harian/' + nip + '/' + tanggal);
                        break;
                    case 'bulanan':
                        popup('open', 'large', site + 'data_v3/presensi/bulanan/' + nip + '/' + tanggal);
                        break;
                }


            }
        }
    </script>

    <!-- Modal edit di form kerja_add.php -->