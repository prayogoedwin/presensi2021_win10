
<table class="table table-bordered">
    <tr>
        <th colspan="4">Bulan <?= bulan($month) ?> Tahun <?= $year ?></th>
    </tr>
    <tr>
        <th>No</th>
        <th>OPD</th>
        <th>Status</th>
        <th>Saved At</th>
    </tr>
    <?php  $no=1; ?>
    <?php foreach($allopd as $key_opd => $value_opd){ ?>
        <tr>
                <td><?php echo $no; ?>.</td>
            <td><?php echo $value_opd->NALOK; ?></td>
            <td><?php echo isset($table[$value_opd->KOLOK]) ? '✔' : '✖'; ?></td>
            <td><?php echo isset($table[$value_opd->KOLOK]) ? date('d M Y', strtotime($table[$value_opd->KOLOK]->saved_at)) : '' ; ?></td>
        </tr>
    <?php  $no++; ?>
    <?php } ?>
</table>