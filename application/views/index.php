<!-- Main content -->
<div class="content-wrapper">


<!-- Page header -->
<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4> HIBAH <span class="font-weight-semibold"><?=id_bidang_by('url',$this->uri->segment('3'))->nama?></span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<div class="header-elements d-none mb-3 mb-md-0">
						<!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
					</div>
				</div>
			</div>
            <!-- /page header -->


			<!-- Content area -->
			<div class="content pt-0">

				
				<!-- Basic initialization -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Tabel Data</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
                    <?php
						$message = $this->session->flashdata('message');
						$info = $this->session->flashdata('info');
                        if (isset($message)) { ?>
                           
						 <!-- Solid alert -->
						 <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
						 <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
						 <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
				    </div>
					<!-- /solid alert -->
					
					<?php 	} ?>
					</div>

					<table id="$datatable" class="table datatable-button-html5-basic">
						<thead style="text-align:left">
							<tr style="text-align:left">
								<th>No</th>
								<th>Tahun Anggaran</th>
								<th>Nomor Proposal</th>

								<?php if($this->uri->segment('3') == 'bpk'){
									echo '<th>No Statistik</th>';
								}
								
								if($this->uri->segment('3') == 'bk'){
									echo '<th>No Identitas</th>';
								}?>
								
								<?php if($this->uri->segment('3') != 'kpp'){
									echo '<th>Klasifikasi Lembaga</th>';
								}?>
								
								<?php if($this->uri->segment('3') != 'kpp'){
									echo '<th>Tipe</th>';
								}?>
								
                                <th>Nama Lembaga</th>
                                <th>Alamat</th>
                                <th>Desa</th>
                                <th>Kecamatan</th>
                                <th>Kota</th>
                                <th>Tanggal Proposal</th>
                                <th>Kepala Lembaga</th>
                                <th>HP/Telp</th>
                                <th>Kegunaan</th>
                                <th>RAB</th>
                                <th>Usulan APBD</th>
                                <?php if($this->uri->segment('3') != 'kpp'){
									echo '<th>Rekomendasi Kemenag</th>';
								}?>
								<th>Surat Domisili</th>

								<?php if($this->uri->segment('3') != 'kpp'){
									echo $this->uri->segment('3') == 'bpk'  ? '<th>Ijin Operasional</th>' : '<th>Simas</th>';
								}?>
                                
								<?php if($this->uri->segment('3') != 'kpp'){
									echo '<th>Susunan Pengurus</th>';
								}?>

								<?php if($this->uri->segment('3') == 'bpk'){
									echo '<th>Profil Lembaga</th>';
								}?>
								
                                <th>Status</th>
                                <th>Keterangan</th>
								<th>Input by</th>
                                <th>Tanggal Input</th>
                                <th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- /basic initialization -->

			</div>
			<!-- /content area -->


<?php require(__DIR__ . '/template/footer.php') ?>
<?php require(__DIR__ . '/template/datatables.php') ?>


            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>