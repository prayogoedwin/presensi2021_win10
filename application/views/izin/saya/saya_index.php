<?php require(__DIR__ . '/../../template/sesi.php') ?>
<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Izin - <span class="font-weight-semibold">Saya</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                <div class="d-flex justify-content-center">
                    <a href="#" class="btn btn-link btn-float text-default" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i><span>Tambah</span></a>
                    <!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a> -->
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->
            <br />

        <?php     } ?>

        <!-- Form inputs -->
        <div class="row">
            <div class="card col-lg-12" style="margin-right:20px">
                <div class="card-body ">
                    <table class="table table-striped datatable-button-html5-basic">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Tipe</th>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Deskripsi</th>
                                <th class="text-center">Status</th>
                                <!-- <th class="text-center">Action</th> -->
                               
                            </tr>
                        </thead>
                        <tbody class="text-center">


                        </tbody>

                    </table>

                </div>
            </div>




            <!-- /form inputs -->
        </div>





    </div>
    <!-- /content area -->

    <?php require(__DIR__ . '/../../template/footer.php') ?>

        <!-- Modal edit di form kerja_add.php -->

        <div id="tambah" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Izin Batu Saya</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <?php echo form_open('izin/add_izin') ?>

                <div class="modal-body">

                    <div class="form-group">
                        <div class="row">

                            <div class="col-sm-12">
                                <label>Pilih Tipe</label>
                                <select class="form-control select2" style="width: 100%;" name="tipe" id="tipe">
                                    <option value="">Pilih</option>
                                    <?php foreach ($data_tipe as $ti) : ?>
                                        <option value="<?= $ti->id ?>"><?= $ti->nama ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Tanggal Awal</label>
                                <input type="date" id="awal" name="awal" class="form-control">
                            </div>

                            <div class="col-sm-6">
                                <label>Tanggal Akhir</label>
                                <input type="date" id="akhir" name="akhir" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Keterangan</label>
                                <textarea rows="5" id="keterangan" name="keterangan" class="form-control"></textarea>

                            </div>


                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn bg-primary">Update</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <!-- Modal edit di form kerja_add.php -->

    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>

    <script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#opd").change(function() { // Ketika user mengganti atau memilih Mutasi Ke
                $("#unit").hide(); // Sembunyikan dulu combobox kota nya
                $("#loading").show(); // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("fungsi/unit_kerja"); ?>", // Isi dengan url/path file php yang dituju
                    data: {
                        opd: $("#opd").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide(); // Sembunyikan loadingnya
                        // set isi dari combobox 
                        // lalu munculkan kembali combobox 
                        $("#unit").html(response.list_lokasis).show();
                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });
            });
        });
    </script>

<script>
/* ------------------------------------------------------------------------------
 *
 *  # Buttons extension for Datatables. HTML5 examples
 *
 *  Demo JS code for datatable_extension_buttons_html5.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var DatatableButtonsHtml5 = function() {


//
// Setup module components
//

// Basic Datatable examples
var _componentDatatableButtonsHtml5 = function() {
    if (!$().DataTable) {
        console.warn('Warning - datatables.min.js is not loaded.');
        return;
    }

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
        }
    });

       
    // Basic initialization
    var tabel = $('.datatable-button-html5-basic').DataTable({
        buttons: {            
            dom: {
                button: {
                    className: 'btn btn-light'
                }
            },
            buttons: [
                'excelHtml5'
            ]
        },
        'scrollX'   : true,
        'data'      : <?=json_encode($datatable);?>,
        'columns'   : [
                        { data: null, sortable : false, searceable : false },
                        {
                            data: 'tipe'
                        },
                        {
                            data: 'tanggal'
                        },
                        {
                            data: 'description'
                        },
                        {
                            data: 'status'
                        },

                        // {
                        //     data: 'action'
                        // },

                    ]
                      
        });
        tabel.on( 'order.dt search.dt', function () {
            tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
  

};

// Select2 for length menu styling
var _componentSelect2 = function() {
    if (!$().select2) {
        console.warn('Warning - select2.min.js is not loaded.');
        return;
    }

    // Initialize
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        dropdownAutoWidth: true,
        width: 'auto'
    });
};


//
// Return objects assigned to module
//

return {
    init: function() {
        _componentDatatableButtonsHtml5();
        _componentSelect2();
    }
}
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
DatatableButtonsHtml5.init();
});

</script>

<script>
    $(document).ready(function() {
        // Untuk sunting
        $('#edit-data').on('show.bs.modal', function (event) {
            var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
            var modal          = $(this)
            // Isi nilai pada field
            modal.find('#id').attr("value",div.data('id'));
            modal.find('#tipe').attr("value",div.data('tipe'));
            modal.find('#tanggal_awal').attr("value",div.data('tanggal_awal'));
            modal.find('#tanggal_akhir').attr("value",div.data('tanggal_akhir'));
            modal.find('#status').attr("value",div.data('status'));
            modal.find('#deskripsi').attr("value",div.data('deskripsi'));

           
        });
    });
</script>


	<!-- Modal edit di form kerja_add.php -->
    <div id="edit-data" class="modal fade" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Edit Izin Saya</h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

							<form action="#">
								<div class="modal-body">

									<div class="form-group">
										<div class="row">
											<div class="col-sm-6">
												<label>Tipe</label>
                                                <input type="hidden" readonly id="id" name="id" class="form-control">
												<input type="text"  id="tipe" name="tipe" class="form-control">
											</div>

											<div class="col-sm-6">
												<label>Status</label>
												<input type="text"  id="status" name="status" class="form-control">
											</div>
										</div>
									</div>

                                    <div class="form-group">
										<div class="row">
											<div class="col-sm-6">
												<label>Tanggal Awal</label>
												<input type="text"  id="tanggal_awal" name="tanggal_awal" class="form-control">
											</div>

											<div class="col-sm-6">
												<label>Tanggal Akhir</label>
												<input type="text"  id="tanggal_akhir" name="tanggal_akhir" class="form-control">
                                               
											</div>
										</div>
									</div>

                                    <div class="form-group">
										<div class="row">
                                            <div class="col-sm-12">
												<label>Keterangan</label>
												<input type="text" readonly id="deskripsi" name="deskripsi" class="form-control">
											</div>
											

											 
										</div>
									</div>
                                    

									
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
									<button type="button" id="btn_update" class="btn bg-primary">Update</button>
								</div>
							</form>
						</div>
					</div>
				</div>

<!-- Modal edit di form kerja_add.php -->


    








</div>
<!-- /main content -->

</div>
<!-- /page content -->

</body>

</html>