<?php require(__DIR__ . '/../../template/sesi.php') ?>
<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Jadwal - <span class="font-weight-semibold">Kerja (<?=bulan($bulan)?> - <?=$tahun?>)</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                <div class="d-flex justify-content-center">
                    <a href="#" class="btn btn-link btn-float text-default" data-toggle="modal" data-target="#keterangan"><i class="fa fa-info"></i><span>Keterangan</span></a>
                    <a href="#" class="btn btn-link btn-float text-default" data-toggle="modal" data-target="#set"><i class="fa fa-copy"></i><span>Set Jadwal Umum</span></a>
                    <!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a> -->
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->
            <br />

        <?php     } ?>
        <div class="col-md-12">
            <div class="card">
                <!-- <div class="card-header header-elements-inline">
								<h6 class="card-title">Basic tabs</h6>
								<div class="header-elements">
									<div class="list-icons">
				                		<a class="list-icons-item" data-action="collapse"></a>
				                		<a class="list-icons-item" data-action="reload"></a>
				                		<a class="list-icons-item" data-action="remove"></a>
				                	</div>
			                	</div>
							</div> -->

                <div class="card-body">
                    <?php echo form_open('portal/auth');  ?>
                    <table class="table table-striped datatable-button-html5-basic">
                        <thead>
                            <tr>

                                <th class="center">
                                
                                NIP
                                </th>
                                <th class="center">Nama</th>

                                <?php
                                for ($tgl = 1; $tgl <=  jumlah_hari($tahun, $bulan); $tgl++) {
                                    echo '<th class="center" style="text-align:center">' . $tgl . '<br>' . nama_hari($tahun . '-' . $bulan . '-' . sprintf("%02d", $tgl)) . '</th>';
                                } ?>

                            </tr>

                        </thead>
                        <tbody>

                            <?php
                            $no = 0;
                            foreach ($data_pegawai as $presensi) :
                                $no++;
                                $arr_presensi[$presensi->nip][1] = $presensi->j_01;
                                $arr_presensi[$presensi->nip][2] = $presensi->j_02;
                                $arr_presensi[$presensi->nip][3] = $presensi->j_03;
                                $arr_presensi[$presensi->nip][4] = $presensi->j_04;
                                $arr_presensi[$presensi->nip][5] = $presensi->j_05;
                                $arr_presensi[$presensi->nip][6] = $presensi->j_06;
                                $arr_presensi[$presensi->nip][7] = $presensi->j_07;
                                $arr_presensi[$presensi->nip][8] = $presensi->j_08;
                                $arr_presensi[$presensi->nip][9] = $presensi->j_09;
                                $arr_presensi[$presensi->nip][10] = $presensi->j_10;
                                $arr_presensi[$presensi->nip][11] = $presensi->j_11;
                                $arr_presensi[$presensi->nip][12] = $presensi->j_12;
                                $arr_presensi[$presensi->nip][13] = $presensi->j_13;
                                $arr_presensi[$presensi->nip][14] = $presensi->j_14;
                                $arr_presensi[$presensi->nip][15] = $presensi->j_15;
                                $arr_presensi[$presensi->nip][16] = $presensi->j_16;
                                $arr_presensi[$presensi->nip][18] = $presensi->j_18;
                                $arr_presensi[$presensi->nip][17] = $presensi->j_17;
                                $arr_presensi[$presensi->nip][19] = $presensi->j_19;
                                $arr_presensi[$presensi->nip][20] = $presensi->j_20;
                                $arr_presensi[$presensi->nip][21] = $presensi->j_21;
                                $arr_presensi[$presensi->nip][22] = $presensi->j_22;
                                $arr_presensi[$presensi->nip][23] = $presensi->j_23;
                                $arr_presensi[$presensi->nip][24] = $presensi->j_24;
                                $arr_presensi[$presensi->nip][25] = $presensi->j_25;
                                $arr_presensi[$presensi->nip][26] = $presensi->j_26;
                                $arr_presensi[$presensi->nip][27] = $presensi->j_27;
                                $arr_presensi[$presensi->nip][28] = $presensi->j_28;
                                $arr_presensi[$presensi->nip][29] = $presensi->j_29;
                                $arr_presensi[$presensi->nip][30] = $presensi->j_30;
                                $arr_presensi[$presensi->nip][31] = $presensi->j_31;
                            ?>
                                <tr>

                                    <td class="center">
                                    <input name="id_presensi[]" value="<?= $presensi->id_presensi ?>">
                                        <?= $presensi->nip ?>
                                    </td>
                                    <td class="center"><?= $presensi->nama_lengkap ?></td>


                                    <?php
                                    for ($tgl = 1; $tgl <=  jumlah_hari($tahun, $bulan); $tgl++) { ?>
                                        <td>
                                        
                                        <a  href="javascript:;"
                                            data-id="<?php echo $presensi->id_presensi ?>"
                                            data-nip="<?php echo $presensi->nip ?>"
                                            data-nama="<?php echo $presensi->nama_lengkap ?>"
                                            data-hari="<?php echo nama_hari($tahun . '-' . $bulan . '-' . sprintf("%02d", $tgl)) ?>"
                                            data-tanggal="<?php echo $tgl.'-'. $bulan.'-'.$tahun ?>"
                                            data-tgl="<?php echo $tgl ?>"
                                            data-jadwal="<?php echo jam_kerjas($arr_presensi[$presensi->nip][$tgl])?>"
                                            data-toggle="modal" data-target="#edit-data">
                                            <?= jam_kerjas($arr_presensi[$presensi->nip][$tgl]) ?>
                                        </a>
                                        </td>
                                    <?php } ?>

                                </tr>
                            <?php endforeach; ?>



                        </tbody>
                    </table>
                    <br />
                    <br />
                    <div class="text-left">
                        <!-- <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan </button> -->
                    </div>
                    <?php echo form_close();  ?>






                </div>
            </div>
        </div>

    </div>
    <!-- /content area -->

    <?php require(__DIR__ . '/../../template/footer.php') ?>

    <?php $this->load->view('jadwal/kerja/kerja_modal.php') ?>

    
    
   

    <script src="https://cdn.datatables.net/fixedcolumns/3.3.3/js/dataTables.fixedColumns.min.js"></script>


    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>



    <script>
        /* ------------------------------------------------------------------------------
         *
         *  # Buttons extension for Datatables. HTML5 examples
         *
         *  Demo JS code for datatable_extension_buttons_html5.html page
         *
         * ---------------------------------------------------------------------------- */


        // Setup module
        // ------------------------------

        var DatatableButtonsHtml5 = function() {


            //
            // Setup module components
            //

            // Basic Datatable examples
            var _componentDatatableButtonsHtml5 = function() {
                if (!$().DataTable) {
                    console.warn('Warning - datatables.min.js is not loaded.');
                    return;
                }

                // Setting datatable defaults
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Filter:</span> _INPUT_',
                        searchPlaceholder: 'Type to filter...',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {
                            'first': 'First',
                            'last': 'Last',
                            'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                            'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                        }
                    }
                });


                // Basic initialization


                var tabel = $('.datatable-button-html5-basic').DataTable({
                        buttons: {
                            dom: {
                                button: {
                                    className: 'btn btn-light'
                                }
                            },
                            buttons: [
                                // 'excelHtml5'
                            ]
                        },

                        'scrollY': "500px",
                        'scrollX': true,
                        'scrollCollapse': true,
                        'paging': false,
                        'fixedColumns': {
                            leftColumns: 1,
                        },

                    }

                );

                $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                    $.fn.dataTable.tables({
                        visible: true,
                        api: true
                    }).columns.adjust();
                });



            };

            // Select2 for length menu styling
            var _componentSelect2 = function() {
                if (!$().select2) {
                    console.warn('Warning - select2.min.js is not loaded.');
                    return;
                }

                // Initialize
                $('.dataTables_length select').select2({
                    minimumResultsForSearch: Infinity,
                    dropdownAutoWidth: true,
                    width: 'auto'
                });
            };


            //
            // Return objects assigned to module
            //

            return {
                init: function() {
                    _componentDatatableButtonsHtml5();
                    _componentSelect2();
                }
            }
        }();


        // Initialize module
        // ------------------------------

        document.addEventListener('DOMContentLoaded', function() {
            DatatableButtonsHtml5.init();
        });
    </script>





<script>
    $(document).ready(function() {
        // Untuk sunting
        $('#edit-data').on('show.bs.modal', function (event) {
            var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
            var modal          = $(this)
            // Isi nilai pada field
            modal.find('#id').attr("value",div.data('id'));
            modal.find('#nip').attr("value",div.data('nip'));
            modal.find('#nama').attr("value",div.data('nama'));
            modal.find('#hari').attr("value",div.data('hari'));
            modal.find('#tanggal').attr("value",div.data('tanggal'));
            modal.find('#tgl').attr("value",div.data('tgl'));
            modal.find('#jadwal').attr("value",div.data('jadwal'));
           
        });
    });


    //Update Jadwak
    //  $('#btn_update').on('click',function(e){
     $(document).on('click', '.btn_update', function(e) {
            // var id = modal.find('#id').attr("value",div.data('id'));
            var id=$('#id').val();
            var tanggal=$('#tgl').val();
            var jadwal_baru=$('#jadwal_baru').val();
            
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('jadwal/update_jadwal_satuan')?>",
                dataType : "JSON",
                data : {id:id , tanggal:tanggal, jadwal_baru:jadwal_baru},
                success: function(data){
                    // console.log(data);
                    // $('[name="kobar_edit"]').val("");
                    // $('[name="nabar_edit"]').val("");
                    // $('[name="harga_edit"]').val("");
                    // $('#ModalaEdit').modal('hide');
                    // tampil_data_barang();
                    location.reload(true);
                }
            });
            return false;
        });
</script>

<!-- Modal edit di form kerja_add.php -->



