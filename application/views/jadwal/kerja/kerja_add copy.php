<?php require(__DIR__ . '/../../template/sesi.php') ?>
<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Jadwal - <span class="font-weight-semibold">Kerja</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                <div class="d-flex justify-content-center">
                    <a href="#" class="btn btn-link btn-float text-default" data-toggle="modal" data-target="#keterangan"><i class="fa fa-file"></i><span>Keterangan</span></a>
                    <!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a> -->
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->
            <br />

        <?php     } ?>
        <div class="col-md-12">
            <div class="card">
                <!-- <div class="card-header header-elements-inline">
								<h6 class="card-title">Basic tabs</h6>
								<div class="header-elements">
									<div class="list-icons">
				                		<a class="list-icons-item" data-action="collapse"></a>
				                		<a class="list-icons-item" data-action="reload"></a>
				                		<a class="list-icons-item" data-action="remove"></a>
				                	</div>
			                	</div>
							</div> -->

                <div class="card-body">
                <?php echo form_open('portal/auth');  ?>
                    <table class="table table-striped datatable-button-html5-basic">
                        <thead>
                            <tr>

                                <th class="center">NIP</th>
                                <th class="center">Nama</th>

                                <?php
                                for ($tgl = 1; $tgl <=  jumlah_hari($tahun, $bulan); $tgl++) {
                                    echo '<th class="center" style="text-align:center">' . $tgl . '<br>' . nama_hari($tahun . '-' . $bulan . '-' . sprintf("%02d", $tgl)) . '</th>';
                                } ?>

                            </tr>

                        </thead>
                        <tbody>

                            <?php
                            $no = 0;
                            foreach ($data_pegawai as $pegawai) :
                                $no++;
                            ?>
                                <tr>

                                    <td class="center">
                                    <?= $pegawai->nip ?>
                                    <?php if($status == 'update'){ 
                                    echo '<input type="hidden" name="id[]"  value="'.$pegawai->id_presensi.'">';
                                    }else{
                                    echo '<input type="hidden" name="id[]"  value="">';    
                                    } ?>
                                    <input type="hidden" name="nip[]" value="<?=$pegawai->nip?>">
                                    </td>
                                    <td class="center"><?= $pegawai->nama_lengkap ?></td>


                                    <?php
                                    for ($tgl = 1; $tgl <=  jumlah_hari($tahun, $bulan); $tgl++) { ?>
                                        <td>
                                            <select class="form-control select2 pull-center" style="width:75px" id="tanggal_<?php echo $tgl ?>" name="jam_kerja<?php echo $tgl ?>" class="jam" onchange="general_tanggal('<?php echo $tgl ?>')">

                                                <?php foreach ($jamkerja as $j) : ?>
                                                    <option value="<?= $j->id ?>"><?= $j->nama_singkat ?></option>
                                                <?php endforeach; ?>
                                            </select>

                                        </td>
                                    <?php } ?>

                                </tr>
                            <?php endforeach; ?>



                        </tbody>
                    </table>
                    <br/>
                    <br/>
                    <div class="text-left">
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan </button>
                    </div>
                    <?php echo form_close();  ?>






                </div>
            </div>
        </div>

    </div>
    <!-- /content area -->

    <?php require(__DIR__ . '/../../template/footer.php') ?>

    <script src="https://cdn.datatables.net/fixedcolumns/3.3.3/js/dataTables.fixedColumns.min.js"></script>


    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>



    <script>
        /* ------------------------------------------------------------------------------
         *
         *  # Buttons extension for Datatables. HTML5 examples
         *
         *  Demo JS code for datatable_extension_buttons_html5.html page
         *
         * ---------------------------------------------------------------------------- */


        // Setup module
        // ------------------------------

        var DatatableButtonsHtml5 = function() {


            //
            // Setup module components
            //

            // Basic Datatable examples
            var _componentDatatableButtonsHtml5 = function() {
                if (!$().DataTable) {
                    console.warn('Warning - datatables.min.js is not loaded.');
                    return;
                }

                // Setting datatable defaults
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Filter:</span> _INPUT_',
                        searchPlaceholder: 'Type to filter...',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {
                            'first': 'First',
                            'last': 'Last',
                            'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                            'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                        }
                    }
                });


                // Basic initialization


                var tabel = $('.datatable-button-html5-basic').DataTable({
                        buttons: {
                            dom: {
                                button: {
                                    className: 'btn btn-light'
                                }
                            },
                            buttons: [
                                // 'excelHtml5'
                            ]
                        },

                        'scrollY': "500px",
                        'scrollX': true,
                        'scrollCollapse': true,
                        'paging': false,
                        'fixedColumns': {
                            leftColumns: 1,
                        },

                    }

                );

                $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                    $.fn.dataTable.tables({
                        visible: true,
                        api: true
                    }).columns.adjust();
                });



            };

            // Select2 for length menu styling
            var _componentSelect2 = function() {
                if (!$().select2) {
                    console.warn('Warning - select2.min.js is not loaded.');
                    return;
                }

                // Initialize
                $('.dataTables_length select').select2({
                    minimumResultsForSearch: Infinity,
                    dropdownAutoWidth: true,
                    width: 'auto'
                });
            };


            //
            // Return objects assigned to module
            //

            return {
                init: function() {
                    _componentDatatableButtonsHtml5();
                    _componentSelect2();
                }
            }
        }();


        // Initialize module
        // ------------------------------

        document.addEventListener('DOMContentLoaded', function() {
            DatatableButtonsHtml5.init();
        });
    </script>

    <?php require(__DIR__ . '/kerja_modal.php') ?>