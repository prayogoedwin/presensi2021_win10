	<!-- Basic Keterangan di kerja_index.php -->
	<div id="keterangan" class="modal fade" tabindex="-1">
		<div class="modal-dialog modal-full">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Keterangan</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="card-body ">
					<table class="table table-striped datatable-button-html5-basicz">
						<thead>
							<tr>
								<td>Kode</td>
								<td>Nama</td>
								<td>Deskripsi</td>
								<td>Jam Masuk</td>
								<td>Jam Pulang</td>
							</tr>
						</thead>
						<?php foreach ($keterangan as $ket) {
						?>
							<tr>
								<td><?= $ket->nama_singkat ?></td>
								<td><?= $ket->nama ?></td>
								<td><?= $ket->deskripsi ?></td>
								<td><?= $ket->jadwal_masuk ?></td>
								<td><?= $ket->jadwal_keluar ?></td>
							</tr>
						<?php
						} ?>

					</table>
				</div>

			</div>
		</div>
	</div>
	<!-- Basic Keterangan di kerja_index.php -->


	<!-- Modal edit di form kerja_add.php -->
	<div id="edit-data" class="modal fade" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Edit Jadwal</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<form action="#">
					<div class="modal-body">

						<div class="form-group">
							<div class="row">
								<div class="col-sm-6">
									<label>NIP</label>
									<input type="hidden" readonly id="id" name="id" class="form-control">
									<input type="text" readonly id="nip" name="nip" class="form-control">
								</div>

								<div class="col-sm-6">
									<label>Nama</label>
									<input type="text" readonly id="nama" name="nama" class="form-control">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-sm-6">
									<label>Hari</label>
									<input type="text" readonly id="hari" name="hari" class="form-control">
								</div>

								<div class="col-sm-6">
									<label>Tanggal</label>
									<input type="text" readonly id="tanggal" name="tanggal" class="form-control">
									<input type="hidden" readonly id="tgl" name="tgl" class="form-control">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-sm-6">
									<label>Jadwal Saat Ini</label>
									<input type="text" readonly id="jadwal" name="jadwal" class="form-control">
								</div>
								<div class="col-sm-6">
									<label>Ubah Jadwal</label>
									<select class="form-control select2" style="width: 100%;" name="jadwal_baru" id="jadwal_baru">
										<option value="">Pilih</option>
										<?php foreach ($jamkerja as $ja) : ?>
											<option value="<?= $ja->id ?>"><?= $ja->nama_singkat ?></option>
										<?php endforeach; ?>
									</select>
								</div>


							</div>
						</div>



					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
						<button type="button" id="btn_update" class="btn_update btn bg-primary">Update</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Modal edit di form kerja_add.php -->

	<!-- Modal set di form kerja_add.php -->
	<?php echo form_open('jadwal/set_jadwal_umum') ?>
	<div id="set" class="modal fade" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Set Jadwal Umum</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>


				<div class="modal-body">

					<h5> Anda Yakin Ingin Set Jadwal Menggunakan Jadwal Umum ?

						<!-- Form inputs -->
						<div class="row">



							<div class="card-body ">
								<!-- <form action="#"> -->



								<div class="form-group row" hidden>
									<div class="col-lg-3">
										<label class="col-form-label ">OPD</label>
									</div>

									<div class="col-lg-9">
										<input name="opd" value="<?= $opd ?>">


									</div>

								</div>

								<div class="form-group row" hidden>
									<div class="col-lg-3">
										<label class="col-form-label ">Unit Kerja</label>
									</div>

									<div class="col-lg-9">
										<input name="unit" value="<?= $unit ?>">

									</div>

								</div>

								<div class="form-group row" hidden>
									<div class="col-lg-3">
										<label class="col-form-label ">Bulan</label>
									</div>

									<div class="col-lg-9">
										<input name="bulan" value="<?= $bulan ?>">
									</div>
								</div>

								<div class="form-group row" hidden>
									<div class="col-lg-3">
										<label class="col-form-label ">Tahun</label>
									</div>

									<div class="col-lg-9">
										<input name="tahun" value="<?= $tahun ?>">
									</div>
								</div>


								<div class="form-group row">
									<div class="col-lg-3">
										<label class="col-form-label ">Minggu</label>
									</div>

									<div class="col-lg-9">
										<input name="minggu" value="1">
									</div>
								</div>

								<div class="form-group row">
									<div class="col-lg-3">
										<label class="col-form-label ">Senin</label>
									</div>

									<div class="col-lg-9">
										<input name="minggu" value="3">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-lg-3">
										<label class="col-form-label ">Selasa</label>
									</div>

									<div class="col-lg-9">
										<input name="minggu" value="3">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-lg-3">
										<label class="col-form-label ">Rabu</label>
									</div>

									<div class="col-lg-9">
										<input name="minggu" value="3">
									</div>
								</div>v>
								</div>
								<div class="form-group row">
									<div class="col-lg-3">
										<label class="col-form-label ">Kamis</label>
									</div>

									<div class="col-lg-9">
										<input name="minggu" value="3">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-lg-3">
										<label class="col-form-label ">Jumat</label>
									</div>

									<div class="col-lg-9">
										<input name="minggu" value="458">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-lg-3">
										<label class="col-form-label ">Sabtu</label>
									</div>

									<div class="col-lg-9">
										<input name="minggu" value="1">
									</div>
								</div>


								











							</div>


						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							<button type="button" class="btn bg-primary">Ya</button>
						</div>

				</div>
			</div>
		</div>
		<?php echo form_close() ?>

		<!-- Modal set di form kerja_add.php -->