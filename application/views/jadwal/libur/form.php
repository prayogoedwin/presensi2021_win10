<style>
.picker__date-display 
{ 
display :none;
}

</style>
<h5><?php echo $title?></h5>

<form name="form" id="form" method="post" class="col s12">


<div class="col s12">
	<label for="nama">Nama</label>
    <input placeholder="Nama" id="nama" name="nama" type="text" data-length="128" class="validate" value="<?php echo $form_data['nama'] ?>"> 
</div>
<div class="row">		
<div class=" col s6">
	<label for="tanggal_awal">Tanggal Awal</label>
    <input id="tanggal_awal" name="tanggal_awal" type="text" value="<?php echo $tanggal_awal ?>" <?php if($act=='add') echo 'readonly="readonly"';?> class="validate">
    
</div>

<div class="col s6">
	<label for="tanggal_akhir">Tanggal Akhir</label>
    <input id="tanggal_akhir" name="tanggal_akhir" type="text" value="<?php echo $tanggal_akhir ?>" class="datepicker validate">
</div>


</div>
<div class="col s12">
          <label for="keterangan">Keterangan</label>
		  <textarea id="keterangan" name="keterangan" class="materialize-textarea" data-length="255"><?php echo $form_data['keterangan'] ?></textarea>
        </div>
		
</form>

<div class="modal-footer">
      <button onclick="$('#main').fadeIn();popup('close','large')" class="modal-action modal-close waves-effect waves-light btn blue"><i class="material-icons left">close</i> Tutup</button>
	  <?php if($act=='edit') { ?>
	  <button onclick="hapus()" class="waves-effect waves-light btn red"><i class="material-icons left">delete</i> Hapus</button>
	  <?php } ?>
	  <button onclick="simpan()" class="waves-effect waves-light btn green"><i class="material-icons left">save</i> Simpan</button>  
 </div>

<script type="text/javascript">

$(function(){
	$('.datepicker').pickadate({
	monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
	monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
	weekdaysFull: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
	weekdaysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
	weekdaysLetter: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
    showMonthsFull: true,
	selectMonths: false,
    selectYears: false,
	format: 'yyyy-mm-dd',
	closeOnSelect: true,
	closeOnClear: true,
	today: 'Hari Ini',
	clear: 'Bersihkan',
	close: 'Tutup',
	labelMonthNext: 'Bulan Berikutnya',
	labelMonthPrev: 'Bulan Sebelumnya',
	labelMonthSelect: 'Pilih Bulan',
	labelYearSelect: 'Pilih Tahun',
	
	});
  
	$('.select').select2();
	
});

<?php if($act=='edit') { ?>

function hapus()
{
	confirm_action('Konfirmasi','Yakin Akan Dihapus ?',function(r)
				{
				if (r){
					loading('open');
					$.post('<?php echo site_url($group_module.'/'.$module.'/delete/'.$id)?>',function(result){
					loading('close');
					if (result.success)
					{
					messager('success',result.message);
					popup('close','large');
					location.reload();
					} 
					else 
					{
					messager('error',result.message);
					}
							
					},'json');
					
				}
				
				});
}
<?php } ?>

function simpan()
{
	$.ajax({
        url: '<?php echo $form_action?>', 
        data: $('#form').serialize(),
		beforeSend: function() {
		loading('open');
		},
		success: function(result){
			loading('close');
			if (result.success)
			{
			messager('success',result.message);
			popup('close','large');
			location.reload();
			} 
			else 
			{
			messager('error',result.message);
			
			}
			
            },
        type: "post", 
       dataType: "json"
    }); 
	
}
	
</script>