<!-- Main content -->
<div class="content-wrapper">


	<!-- Page header -->
	<div class="page-header border-bottom-0">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4> Jadwal - <span class="font-weight-semibold">Libur</span></h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>

			<div class="header-elements d-none mb-3 mb-md-0">
				<!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Content area -->
	<div class="content pt-0">

		<?php
		$message = $this->session->flashdata('message');
		$info = $this->session->flashdata('info');
		if (isset($message)) { ?>

			<!-- Solid alert -->
			<div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
				<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
				<span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
			</div>
			<!-- /solid alert -->
			<br />

		<?php 	} ?>

		<div class="form-group row">
			<div class="col-lg-3">
				<select id="bulan" name="bulan" onchange="cari()" class="form-control select2">
					<?php for ($j = 1; $j <= 12; $j++) {
						$selected = '';
						$bln = sprintf("%02d", $j);
						if ($month == $bln) {
							$selected = 'selected="selected"';
						}
						echo '<option ' . $selected . ' value="' . $bln . '">' . namaBulan($j) . '</option>';
					} ?>


				</select>
			</div>

			<div class="col-lg-3">
				<select id="tahun" name="tahun" onchange="cari()" class="form-control select2">
					<?php for ($tahun = 2018; $tahun <= date("Y") + 1; $tahun++) { ?>
						<option value="<?php echo $tahun ?>" <?php if ($year == $tahun) echo 'selected="selected"' ?>><?php echo $tahun ?></option>
					<?php } ?>
				</select>

			</div>
			<!-- <div class="col-lg-3">
				<button onclick="cari()" class="btn btn-success"><i class="fa fa-search"></i> Cari </button>
			</div> -->
		</div>

		<!-- Basic initialization -->
		<div class="card">







			<?php echo $this->calendar->generate($year, $month, $calendar); ?>

		</div>
		<!-- /basic initialization -->

	</div>
	<!-- /content area -->


	<?php require(__DIR__ . '/../../template/footer.php') ?>

	<script>

	

		var year = <?php echo $year ?>;
		var month = <?php echo $month ?>;

		function pad2(number) {
			return (number < 10 ? '0' : '') + number;
		}


		function cari() {
			var bulan = $('#bulan').val();
			var tahun = $('#tahun').val();
			if (bulan && tahun) {
				// window.location.replace(location+'/jadwal/libur/'+tahun+'/'+bulan);
				window.location.href = "http://localhost:81/presensi2021/jadwal/libur/" + tahun + '/' + bulan;
			} else {
				alert('salah');
			}
		}

		

		function tambah(day) {
			var windowName = 'ABC';
			var style = "top=center, left=center, width=500, height=500, status=no, menubar=no, toolbar=no, scrollbars=no, resize=no";
			var url = "http://localhost:81/presensi2021" + '/jadwal/libur/add/' + year + '-' + pad2(month) + '-' + pad2(day);
			// newwindow=window.open(url,windowName,'height=200,width=150,toolbar=0,menubar=0,location=0');
			// if (window.focus) {newwindow.focus()}
			// return false;
			var title = 'abc';
			// return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no');
			
			popupWindow(url, 'test', windowName, 200, 100);
           


		    // popup('open', 'large', "http://localhost/presensi2021" + '/jadwal/libur/add/' + year + '-' + pad2(month) + '-' + pad2(day));
	
		}

		function popupWindow(url, windowName, win, w, h) {
			const y = win.top.outerHeight / 2 + win.top.screenY - ( h / 2);
			const x = win.top.outerWidth / 2 + win.top.screenX - ( w / 2);
			return window.open(url, windowName, `toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=${w}, height=${h}, top=${y}, left=${x}`);
		}

		function ubah(day) {
			// alert('test');
			// $('#main').fadeOut();
			popup('open', 'large', "http://localhost:81/presensi2021/" + '/jadwal/libur/edit/' + year + '-' + pad2(month) + '-' + pad2(day));
		}
	</script>



