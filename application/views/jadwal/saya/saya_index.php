<?php error_reporting(0); ?>
<script type="text/javascript">
	var year = <?php echo $year ?>;
	var month = <?php echo $month ?>;

	function pad2(number) {
		return (number < 10 ? '0' : '') + number;
	}

	function tambah(day) {
		$('#main').fadeOut();
		popup('open', 'large', site + '/jadwal/saya/add/' + year + '-' + pad2(month) + '-' + pad2(day));

	}

	function ubah(day) {
		$('#main').fadeOut();
		popup('open', 'large', site + '/jadwal/saya/edit/' + year + '-' + pad2(month) + '-' + pad2(day));
	}

	// function cari()
	// {

	// 	var bulan= $('#bulan').val();
	// 	var tahun = $('#tahun').val();

	// 	// var bulan = document.getElementById("bulan");
	// 	// var tahun = document.getElementById("tahun");

	// 	// if(bulan && tahun)
	// 	// {
	// 		window.location.href=<?= base_url('presensi2021/jadwal/saya/') ?>+tahun+'/'+bulan);
	// 		// window.location.replace(<?= base_url('presensi2021/jadwal/saya/') ?>+tahun+'/'+bulan);
	// 	// }

	// }

	function cari() {

		var bulan = $('#bulan').val();
		var tahun = $('#tahun').val();

		// var bulan = document.getElementById("bulan");
		// var tahun = document.getElementById("tahun");

		// if(bulan && tahun)
		// {
		window.location.href = <?= base_url('presensi2021/jadwal/saya/') ?> + tahun + '/' + bulan);
	// window.location.replace(<?= base_url('presensi2021/jadwal/saya/') ?>+tahun+'/'+bulan);
	// }

	}
</script>

<!-- Main content -->
<div class="content-wrapper">


	<!-- Page header -->
	<div class="page-header border-bottom-0">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4> Jadwal - <span class="font-weight-semibold">Saya</span></h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>

			<div class="header-elements d-none mb-3 mb-md-0">
				<!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Content area -->
	<div class="content pt-0">

		<?php
		$message = $this->session->flashdata('message');
		$info = $this->session->flashdata('info');
		if (isset($message)) { ?>

			<!-- Solid alert -->
			<div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
				<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
				<span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
			</div>
			<!-- /solid alert -->
			<br />

		<?php 	} ?>

		<div class="form-group row">
			<div class="col-lg-3">
				<select id="bulan" name="bulan" onchange="cari()" class="form-control select2">
					<?php for ($j = 1; $j <= 12; $j++) {
						$selected = '';
						$bln = sprintf("%02d", $j);
						if ($month == $bln) {
							$selected = 'selected="selected"';
						}
						echo '<option ' . $selected . ' value="' . $bln . '">' . namaBulan($j) . '</option>';
					} ?>


				</select>
			</div>

			<div class="col-lg-3">
				<select id="tahun" name="tahun" onchange="cari()" class="form-control select2">
					<?php for ($tahun = 2018; $tahun <= date("Y") + 1; $tahun++) { ?>
						<option value="<?php echo $tahun ?>" <?php if ($year == $tahun) echo 'selected="selected"' ?>><?php echo $tahun ?></option>
					<?php } ?>
				</select>

			</div>
			<!-- <div class="col-lg-3">
				<button onclick="cari()" class="btn btn-success"><i class="fa fa-search"></i> Cari </button>
			</div> -->
		</div>

		<!-- Basic initialization -->
		<div class="card">

			<div class="card-body">
				<table class="table table-striped datatable-button-html5-basic">
					<?php
					if ($jadwal_data->num_rows() > 0) {

						foreach ($jam_kerja as $jam) {


							if ($jam->waktu_kerja > 0) {
								$jadwal_masuk[$jam->id] = $jam->jadwal_masuk;
								$jadwal_keluar[$jam->id] = $jam->jadwal_keluar;
							} else {
								$jadwal_masuk[$jam->id] = '-';
								$jadwal_keluar[$jam->id] = '-';
							}
						}
						foreach ($jam_kerja as $jam) {


							if ($jam->waktu_kerja > 0) {
								$jadwal_masuk[$jam->id] = $jam->jadwal_masuk;
								$jadwal_keluar[$jam->id] = $jam->jadwal_keluar;
							} else {
								$jadwal_masuk[$jam->id] = '-';
								$jadwal_keluar[$jam->id] = '-';
							}
						}

						$no = 0;
						foreach ($jadwal_data->result() as $jadwal) {
							$no++;
							$arr_masuk[1] = $jadwal_masuk[$jadwal->j_01];
							$arr_masuk[2] = $jadwal_masuk[$jadwal->j_02];
							$arr_masuk[3] = $jadwal_masuk[$jadwal->j_03];
							$arr_masuk[4] = $jadwal_masuk[$jadwal->j_04];
							$arr_masuk[5] = $jadwal_masuk[$jadwal->j_05];
							$arr_masuk[6] = $jadwal_masuk[$jadwal->j_06];
							$arr_masuk[7] = $jadwal_masuk[$jadwal->j_07];
							$arr_masuk[8] = $jadwal_masuk[$jadwal->j_08];
							$arr_masuk[9] = $jadwal_masuk[$jadwal->j_09];
							$arr_masuk[10] = $jadwal_masuk[$jadwal->j_10];
							$arr_masuk[11] = $jadwal_masuk[$jadwal->j_11];
							$arr_masuk[12] = $jadwal_masuk[$jadwal->j_12];
							$arr_masuk[13] = $jadwal_masuk[$jadwal->j_13];
							$arr_masuk[14] = $jadwal_masuk[$jadwal->j_14];
							$arr_masuk[15] = $jadwal_masuk[$jadwal->j_15];
							$arr_masuk[16] = $jadwal_masuk[$jadwal->j_16];
							$arr_masuk[18] = $jadwal_masuk[$jadwal->j_18];
							$arr_masuk[17] = $jadwal_masuk[$jadwal->j_17];
							$arr_masuk[19] = $jadwal_masuk[$jadwal->j_19];
							$arr_masuk[20] = $jadwal_masuk[$jadwal->j_20];
							$arr_masuk[21] = $jadwal_masuk[$jadwal->j_21];
							$arr_masuk[22] = $jadwal_masuk[$jadwal->j_22];
							$arr_masuk[23] = $jadwal_masuk[$jadwal->j_23];
							$arr_masuk[24] = $jadwal_masuk[$jadwal->j_24];
							$arr_masuk[25] = $jadwal_masuk[$jadwal->j_25];
							$arr_masuk[26] = $jadwal_masuk[$jadwal->j_26];
							$arr_masuk[27] = $jadwal_masuk[$jadwal->j_27];
							$arr_masuk[28] = $jadwal_masuk[$jadwal->j_28];
							$arr_masuk[29] = $jadwal_masuk[$jadwal->j_29];
							$arr_masuk[30] = $jadwal_masuk[$jadwal->j_30];
							$arr_masuk[31] = $jadwal_masuk[$jadwal->j_31];

							$arr_pulang[1] = $jadwal_keluar[$jadwal->j_01];
							$arr_pulang[2] = $jadwal_keluar[$jadwal->j_02];
							$arr_pulang[3] = $jadwal_keluar[$jadwal->j_03];
							$arr_pulang[4] = $jadwal_keluar[$jadwal->j_04];
							$arr_pulang[5] = $jadwal_keluar[$jadwal->j_05];
							$arr_pulang[6] = $jadwal_keluar[$jadwal->j_06];
							$arr_pulang[7] = $jadwal_keluar[$jadwal->j_07];
							$arr_pulang[8] = $jadwal_keluar[$jadwal->j_08];
							$arr_pulang[9] = $jadwal_keluar[$jadwal->j_09];
							$arr_pulang[10] = $jadwal_keluar[$jadwal->j_10];
							$arr_pulang[11] = $jadwal_keluar[$jadwal->j_11];
							$arr_pulang[12] = $jadwal_keluar[$jadwal->j_12];
							$arr_pulang[13] = $jadwal_keluar[$jadwal->j_13];
							$arr_pulang[14] = $jadwal_keluar[$jadwal->j_14];
							$arr_pulang[15] = $jadwal_keluar[$jadwal->j_15];
							$arr_pulang[16] = $jadwal_keluar[$jadwal->j_16];
							$arr_pulang[18] = $jadwal_keluar[$jadwal->j_18];
							$arr_pulang[17] = $jadwal_keluar[$jadwal->j_17];
							$arr_pulang[19] = $jadwal_keluar[$jadwal->j_19];
							$arr_pulang[20] = $jadwal_keluar[$jadwal->j_20];
							$arr_pulang[21] = $jadwal_keluar[$jadwal->j_21];
							$arr_pulang[22] = $jadwal_keluar[$jadwal->j_22];
							$arr_pulang[23] = $jadwal_keluar[$jadwal->j_23];
							$arr_pulang[24] = $jadwal_keluar[$jadwal->j_24];
							$arr_pulang[25] = $jadwal_keluar[$jadwal->j_25];
							$arr_pulang[26] = $jadwal_keluar[$jadwal->j_26];
							$arr_pulang[27] = $jadwal_keluar[$jadwal->j_27];
							$arr_pulang[28] = $jadwal_keluar[$jadwal->j_28];
							$arr_pulang[29] = $jadwal_keluar[$jadwal->j_29];
							$arr_pulang[30] = $jadwal_keluar[$jadwal->j_30];
							$arr_pulang[31] = $jadwal_keluar[$jadwal->j_31];
						}
					?>

						<thead>
							<tr>
								<th class="text-center">Tanggal</th>
								<th class="text-center">Masuk</th>
								<th class="text-center">Pulang</th>
							</tr>
						</thead>
						<tbody>
							<?php
							for ($tgl = 1; $tgl <=  jumlah_hari($year, $month); $tgl++) { ?>
								<tr>
									<td class="text-center"><?php echo $tgl . ' ' . bulan($month) . ' ' . $year ?></td>
									<td class="text-center"><?php echo $arr_masuk[$tgl] ?></td>
									<td class="text-center"><?php echo $arr_pulang[$tgl] ?></td>
								</tr>
							<?php } ?>
						</tbody>

					<?php } else {
						echo '<h4>Jadwal Tidak Ditemukan</h4>';
					}
					?>

				</table>
			</div>

		</div>
		<!-- /basic initialization -->

	</div>
	<!-- /content area -->


	<?php require(__DIR__ . '/../../template/footer.php') ?>

	<!-- <script>
function myFunction() {
  var x = document.getElementById("mySelect").value;
  var x = document.getElementById("mySelect").value;

  document.getElementById("demo").innerHTML = "You selected: " + x;
}
</script> -->

	<!-- <script>



		function cari() {

			var myURL = document.location;
			document.location = myURL + "?a=parameter";

			console.log(myURL);
			console.log(document.location)

			var bulan = $('#bulan').val();
			var tahun = $('#tahun').val();
			// var pageURL = $(location).attr('href');
			var pageURL = window.location.hostname+'/presensi2020/jadwal/saya';
			// console.log(pageURL+'/'+tahun+'/'+bulan);
			// console.log(pageURL);
			var x = pageURL+'/'+tahun+'/'+bulan;
			// console.log(site+'/jadwal/saya/'+tahun+'/'+bulan);
			if(bulan && tahun)
			{
			// window.location.href=x;
			// window.location.replace(x);
			// $(location).attr('href',x);
			
			}
			
		}
	</script> -->

	<script>
		function cari() {
			var bulan = $('#bulan').val();
			var tahun = $('#tahun').val();
			if (bulan && tahun) {
				// window.location.replace(location+'/jadwal/saya/'+tahun+'/'+bulan);
				window.location.href = "http://localhost:81/presensi2021/jadwal/saya/" + tahun + '/' + bulan;
			} else {
				alert('salah');
			}
		}
	</script>