<?php require(__DIR__ . '/../../template/sesi.php') ?>
<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Rekap - <span class="font-weight-semibold">Bulanan</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                 <div class="d-flex justify-content-center">
							<!-- <a href="#" class="btn btn-link btn-float text-default" data-toggle="modal" data-target="#keterangan" ><i class="fa fa-file"></i><span>Keterangan</span></a> -->
							<!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a> -->
						</div> 
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->
            <br />

        <?php     } ?>

        <!-- Form inputs -->
        <div class="row">
        <div class="card col-lg-8" style="margin-right:20px">


            <div class="card-body ">
                <!-- <form action="#"> -->
                <?php echo form_open('rekap/bulanan/cari') ?>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">OPD</label>
                    </div>

                    <div class="col-lg-9">
                        <?php

                        $unituser = $dataPns->A_01 . '00000000';
                        foreach ($allopd as $opd) {

                            $s[$opd->KOLOK] = ($opd->NALOK);
                        }
                        echo form_dropdown('opd', $s, '', 'class="form-control select2" data-fouc id="opd"');
                        ?>


                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Unit Kerja</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" name="unit" id="unit">
                            <option value="">Pilih</option>

                        </select>

                    </div>

                </div>


				<div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Eselon</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" name="eselon" id="eselon">
						<option value="all">Semua</option>
						<option value="str">Struktural</option>
						<option value="1">Eselon 1</option>
						<option value="2">Eselon 2</option>
						<option value="3">Eselon 3</option>
        				<option value="4">Eselon 4</option>

                        </select>

                    </div>

                </div>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Bulan</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" required name="bulan" id="bulan">
                            
                            <<option value="all">Semua Bulan</option>
                            <option value="smt1">Semester I</option>
                            <option value="smt2">Semester II</option>
                            <option value="caturwulan1">Catur Wulan I</option>
                            <option value="caturwulan2">Catur Wulan II</option>
                            <option value="caturwulan3">Catur Wulan III</option>
                            <?php for ($j = 1; $j <= 12; $j++) {
                                $selected = '';
                                $bln = sprintf("%02d", $j);
                                if ($month == $bln) {
                                    $selected = 'selected="selected"';
                                }
                                echo '<option ' . $selected . ' value="' . $bln . '">' . namaBulan($j) . '</option>';
                            } ?>

                        </select>
                    </div>



                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Tahun</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" required name="tahun" id="tahun">
                            <?php for ($tahun = 2018; $tahun <= date("Y") + 1; $tahun++) { ?>
                                <option value="<?php echo $tahun ?>" <?php if ($year == $tahun) echo 'selected="selected"' ?>><?php echo $tahun ?></option>
                            <?php } ?>

                        </select>
                    </div>



                </div>

                <!-- <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Set Dengan Jadwal Umum</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" required name="jadwal_umum" id="jadwal_umum">
                            <option value="">Pilih</option>
                            <option value="1">Ya</option>
                            <option value="0">Tidak</option>


                        </select>
                    </div>



                </div> -->




                <div class="text-right">
                    <button  type="submit" class="btn btn-primary pull-right"> <i class="fa fa-search"></i> Cari </button>
                    <button onclick="cetak()" type="button" class="btn btn-info pull-right"> <i class="fa fa-print"></i> Cetak </button>
                    <button onclick="excel()" type="button" class="btn btn-success pull-right"> <i class="fa fa-file"></i> Excel </button>
                </div>

               
                <?php echo form_close() ?>
              
            </div>
        </div>
        

        
        <!-- /form inputs -->
        </div>
        <div class="row">
            <div class="card col-lg-12">
                <div class="card-body ">
                    <div id="result" class="table-responsive">
                        <h5 class="center"><?php echo strtoupper($title) ?></h5>
                        <?php
                        error_reporting(0);
                        foreach($pegawai_data->result() as $pegawai)
                        {
                            foreach($tipe_data->result() as $tipe) 
                            {
                                $rekap[$pegawai->nip][$tipe->id] = 0;		
                            }
                        }

                        foreach($presensi_data->result() as $presensi)
                        {
                        $rekap[$presensi->nip][$presensi->s_01]++;
                        $rekap[$presensi->nip][$presensi->s_02]++;
                        $rekap[$presensi->nip][$presensi->s_03]++;
                        $rekap[$presensi->nip][$presensi->s_04]++;
                        $rekap[$presensi->nip][$presensi->s_05]++;
                        $rekap[$presensi->nip][$presensi->s_06]++;
                        $rekap[$presensi->nip][$presensi->s_07]++;
                        $rekap[$presensi->nip][$presensi->s_08]++;
                        $rekap[$presensi->nip][$presensi->s_09]++;
                        $rekap[$presensi->nip][$presensi->s_10]++;
                        $rekap[$presensi->nip][$presensi->s_11]++;
                        $rekap[$presensi->nip][$presensi->s_12]++;
                        $rekap[$presensi->nip][$presensi->s_13]++;
                        $rekap[$presensi->nip][$presensi->s_14]++;
                        $rekap[$presensi->nip][$presensi->s_15]++;
                        $rekap[$presensi->nip][$presensi->s_16]++;
                        $rekap[$presensi->nip][$presensi->s_17]++;
                        $rekap[$presensi->nip][$presensi->s_18]++;
                        $rekap[$presensi->nip][$presensi->s_19]++;
                        $rekap[$presensi->nip][$presensi->s_20]++;
                        $rekap[$presensi->nip][$presensi->s_21]++;
                        $rekap[$presensi->nip][$presensi->s_22]++;
                        $rekap[$presensi->nip][$presensi->s_23]++;
                        $rekap[$presensi->nip][$presensi->s_24]++;
                        $rekap[$presensi->nip][$presensi->s_25]++;
                        $rekap[$presensi->nip][$presensi->s_26]++;
                        $rekap[$presensi->nip][$presensi->s_27]++;
                        $rekap[$presensi->nip][$presensi->s_28]++;
                        $rekap[$presensi->nip][$presensi->s_29]++;
                        $rekap[$presensi->nip][$presensi->s_30]++;
                        $rekap[$presensi->nip][$presensi->s_31]++;
                        }

                        #echo '<pre>';
                        #print_r($rekap);
                        #echo '</pre>';
                        ?>

                        <table class="table table-bordered" id="tabel">
                        <thead>
                        <tr>
                        <th class="center" rowspan="2">No.</th>
                        <th class="center" rowspan="2">NIP</th>
                        <th class="center" rowspan="2">Nama</th>
                        <th class="center" colspan="<?php echo $tipe_data->num_rows()?>">Tipe</th>
                        </tr>
                        <?php
                        foreach($tipe_data->result() as $tipe) { ?>
                        <th class="center"><?php echo $tipe->nama ?></th>
                        <?php } ?>
                        <tr>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 0;
                        foreach($pegawai_data->result() as $pegawai) {
                        $no++; ?>
                        <tr>
                        <td><?php echo $no ?></td>
                        <td><?php echo $pegawai->nip?></td>
                        <td><?php echo $pegawai->nama?></td>
                        <?php
                        foreach($tipe_data->result() as $tipe) { ?>
                        <td class="right-align"><?php 
                        if($tipe->id =='H')
                        {
                            echo rupiah($rekap[$pegawai->nip]['H']+$rekap[$pegawai->nip]['T']+$rekap[$pegawai->nip]['TAP']+$rekap[$pegawai->nip]['TAM']+$rekap[$pegawai->nip]['TAMP']+$rekap[$pegawai->nip]['TAPT']+$rekap[$pegawai->nip]['TP']+$rekap[$pegawai->nip]['P']);
                        }
                        else
                        {
                            echo rupiah($rekap[$pegawai->nip][$tipe->id]);
                        }


                        ?></td>
                        <?php }?>
                        </tr>
                        <?php } ?>
                        </tbody>
                        </table>

                        <script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/table2excel.js"></script>
                        <script>
                        function excel()
                        {
                            $('#result').table2excel({
                                name: "<?php echo $title ?>",
                                filename: "<?php echo $title ?>",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });
                        }

                        
    function cetak()
    {
        var DocumentContainer = document.getElementById('result');
        var WindowObject = window.open('', 'Cetak', 'width='+screen.width+', height='+screen.height+', top=0, left=0, toolbars=no, scrollbars=yes, status=no, resizable=yes');
        WindowObject.document.writeln('<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/materialize.css')?>">');
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        setTimeout(function(){
        WindowObject.print()},500);
    }
                        
                        
                        </script>

                    </div>
                </div>
        </div>

        



    </div>
    <!-- /content area -->

    <?php require(__DIR__ . '/../../template/footer.php') ?>

    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>


    <script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#opd").change(function() { // Ketika user mengganti atau memilih Mutasi Ke
                $("#unit").hide(); // Sembunyikan dulu combobox kota nya
                $("#loading").show(); // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("fungsi/unit_kerja"); ?>", // Isi dengan url/path file php yang dituju
                    data: {
                        opd: $("#opd").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide(); // Sembunyikan loadingnya
                        // set isi dari combobox 
                        // lalu munculkan kembali combobox 
                        $("#unit").html(response.list_lokasis).show();
                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });
            });
        });
    </script>



</div>
<!-- /main content -->

</div>
<!-- /page content -->

</body>

</html>