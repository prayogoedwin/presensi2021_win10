<?php require(__DIR__ . '/../../template/sesi.php') ?>
<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Rekap - <span class="font-weight-semibold">Bulanan</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                 <div class="d-flex justify-content-center">
							<!-- <a href="#" class="btn btn-link btn-float text-default" data-toggle="modal" data-target="#keterangan" ><i class="fa fa-file"></i><span>Keterangan</span></a> -->
							<!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a> -->
						</div> 
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->
            <br />

        <?php     } ?>

        <!-- Form inputs -->
        <div class="row">
        <div class="card col-lg-8" style="margin-right:20px">


            <div class="card-body ">
			<style>
#result {
    overflow-x: visible;
}
</style>
<div class="row">
<?php $role = $this->authentification->get_user('role'); 
#echo $role;die();
if($role == '1' || $role == '2' || $role == '5')
{ ?>
<div class="input-field col s12 m12 l12 xl12">
     <select id="skpd" name="skpd">
		<option value="<?php echo encode('all')?>">Semua</option>
    </select>
</div>

<div class="input-field col s12 m12 l12 xl12">
     <select id="upt" name="upt">
		<option value=""></option>
    </select>
</div>

<div class="input-field col s12 m12 l12 xl12">
	<label for="eselon">Eselon</label>
     <select id="eselon" name="eselon" class="select">
		<option value="all">Semua</option>
        <option value="str">Struktural</option>
		<option value="1">Eselon 1</option>
        <option value="2">Eselon 2</option>
        <option value="3">Eselon 3</option>
        <option value="4">Eselon 4</option>
    </select>
</div>
	
<?php } else { ?>


<?php if($role == '8' || $role == '9') { ?>
<div class="input-field col s12 m12 l12 xl12">
     <select id="skpd" name="skpd" readonly="readonly">
		<option value="<?php echo encode($profile['opd'])?>" selected="selected"><?php echo $profile['instansi'] ?></option>
    </select>
</div>

<div class="input-field col s12 m12 l12 xl12">
     <select id="upt" name="upt">
		<option value=""></option>
    </select>
</div>

<div class="input-field col s12 m12 l12 xl12">
	<label for="eselon">Eselon</label>
     <select id="eselon" name="eselon" class="select">
		<option value="all">Semua</option>
        <option value="str">Struktural</option>
		<option value="1">Eselon 1</option>
        <option value="2">Eselon 2</option>
        <option value="3">Eselon 3</option>
        <option value="4">Eselon 4</option>
    </select>
</div>

<?php } } ?>
<div class="input-field col s12 m6 l6 xl6">
	<label for="bulan">Bulan</label>
     <select id="bulan" name="bulan" class="select">
	 	<<option value="all">Semua Bulan</option>
        <option value="smt1">Semester I</option>
		<option value="smt2">Semester II</option>
		<option value="caturwulan1">Catur Wulan I</option>
		<option value="caturwulan2">Catur Wulan II</option>
		<option value="caturwulan3">Catur Wulan III</option>
		
		<?php for($j=1;$j<=12;$j++)
        {
			$selected = '';
			$bln = sprintf("%02d",$j);
			if($bln==date("m"))
            {
				$selected = 'selected="selected"';
            }
			echo '<option '.$selected.' value="'.$bln.'">'.bulan($j).'</option>';
		 } ?>
    </select>
</div>


<div class="input-field col s12 m6 l6 xl6">
	<label for="tahun">Tahun</label>
     <select id="tahun" name="tahun" class="select">
		<?php for($tahun=$this->config->item('start_year');$tahun<=date("Y")+1;$tahun++) { ?>
		<option value="<?php echo $tahun ?>" <?php if($tahun == date("Y")) echo 'selected="selected"' ?>><?php echo $tahun ?></option>
		<?php } ?>
    </select>
</div>

</div>

<div class="row">
<div class="col s6">
	<button onclick="cari()" class="waves-effect waves-light btn green"><i class="material-icons left">search</i> Cari</button>
</div>
<div class="col s6 right-align">
	<button onclick="cetak()" id="cetak" class="waves-effect waves-light btn blue"><i class="material-icons">print</i> Cetak</button>
	<button onclick="excel()" id="excel" class="waves-effect waves-light btn red"><i class="material-icons">file_download</i> Excel</button>
</div>
</div>

<div id="result">

</div>


              
            </div>
        </div>
        

        
        <!-- /form inputs -->
        </div>

        



    </div>
    <!-- /content area -->

    <?php require(__DIR__ . '/../../template/footer.php') ?>
<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/table2excel.js"></script>

<script>
    function FormatResult(data) {
        var markup= "<option value="+ data.id+">"+data.text+"</option>";
        return markup;
    }

    function FormatSelection(data) {
        return data.text;
    }

    function cetak()
    {
        var DocumentContainer = document.getElementById('result');
        var WindowObject = window.open('', 'Cetak', 'width='+screen.width+', height='+screen.height+', top=0, left=0, toolbars=no, scrollbars=yes, status=no, resizable=yes');
        WindowObject.document.writeln('<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/materialize.css')?>">');
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        setTimeout(function(){
        WindowObject.print()},500);
    }

    function cari()
    {
        eselon = $("#eselon :selected").val();
        <?php 
        if($role == '1' || $role == '2' || $role == '5')
        { ?>
        if($("#upt :selected").val().length > 0)
        {
        unit_kerja = $("#upt :selected").val();
        }
        else 
        {
        if($("#skpd :selected").val().length > 0)
        {
        unit_kerja = $("#skpd :selected").val();
        }
        else
        {
        unit_kerja = '<?php echo encode($profile['opd']) ?>';
        }
        }	
        <?php }
        if($role == '8' || $role == '9')
        { ?>
        if($("#upt :selected").val().length > 0)
        {
        unit_kerja = $("#upt :selected").val();
        }
        else 
        {
        unit_kerja = '<?php echo encode($profile['opd']) ?>';
        }		
        <?php }
        if($role == '8.1' || $role == '9.1')
        { ?>
        unit_kerja = '<?php echo encode($profile['upt']) ?>';
        <?php }
        if($role == '77')
        { ?>
        unit_kerja = '<?php echo encode($profile['satker']) ?>';
        <?php } ?>

        tahun = $("#tahun :selected").val();
        bulan = $("#bulan :selected").val();
        if(unit_kerja && tahun && bulan)
        {
            $('#cetak').fadeIn();
            $('#excel').fadeIn();
            $("#result").fadeOut();
            $("#result").html('');

            progressbar('open');

            load_page(site+'rekapitulasi/bulanan/result/'+unit_kerja+'/'+tahun+'-'+bulan+'/'+eselon,'#result');	
            progressbar('close');
            $("#result").fadeIn();

        }
        else
        {
            messager('warning','Periksa Form');
        }

    }

    $(function(){
        $('#cetak').hide();
        $('#excel').hide();
        $('.select').select2();
        <?php if($role == '1' || $role == '2' || $role == '5') { ?>	
        $('#skpd').select2({
            placeholder: 'SKPD',
            minimumInputLength: 0,
            allowClear: true,
            quietMillis: 100,
            multiple: false,
            ajax: {
                dataType: "json",
                url: "<?php echo site_url('service/get_unit_kerja') ?>",
                type:'POST',
                data: function (params) {
                    return {
                        keyword: params.term, 
                        per_page: 10, 
                        page: params.page, 
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 10) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, 
            templateResult: FormatResult, 
            templateSelection: FormatSelection 
        });
        <?php } ?>	

        $('#upt').select2({
            placeholder: 'Bidang / UPT',
            minimumInputLength: 0,
            allowClear: true,
            quietMillis: 100,
            multiple: false,
            ajax: {
                dataType: "json",
                url: "<?php echo site_url('service/get_upt') ?>",
                type:'POST',
                data: function (params) {
                    return {
                        unit_kerja: $("#skpd :selected").val(),
                        keyword: params.term, 
                        per_page: 10, 
                        page: params.page, 
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                    results: data.items,
                    pagination: {
                    more: (params.page * 10) < data.total_count
                    }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            templateResult: FormatResult, // omitted for brevity, see the source of this page
            templateSelection: FormatSelection // omitted for brevity, see the source of this page
        });

    });
</script>





</div>
<!-- /main content -->

</div>
<!-- /page content -->

</body>

</html>