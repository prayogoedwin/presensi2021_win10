<h5 class="center"><?php echo strtoupper($title) ?></h5>
<?php
error_reporting(0);
foreach($pegawai_data->result() as $pegawai)
{
	foreach($tipe_data->result() as $tipe) 
	{
		$rekap[$pegawai->nip][$tipe->id] = 0;		
	}
}

foreach($presensi_data->result() as $presensi)
{
$rekap[$presensi->nip][$presensi->s_01]++;
$rekap[$presensi->nip][$presensi->s_02]++;
$rekap[$presensi->nip][$presensi->s_03]++;
$rekap[$presensi->nip][$presensi->s_04]++;
$rekap[$presensi->nip][$presensi->s_05]++;
$rekap[$presensi->nip][$presensi->s_06]++;
$rekap[$presensi->nip][$presensi->s_07]++;
$rekap[$presensi->nip][$presensi->s_08]++;
$rekap[$presensi->nip][$presensi->s_09]++;
$rekap[$presensi->nip][$presensi->s_10]++;
$rekap[$presensi->nip][$presensi->s_11]++;
$rekap[$presensi->nip][$presensi->s_12]++;
$rekap[$presensi->nip][$presensi->s_13]++;
$rekap[$presensi->nip][$presensi->s_14]++;
$rekap[$presensi->nip][$presensi->s_15]++;
$rekap[$presensi->nip][$presensi->s_16]++;
$rekap[$presensi->nip][$presensi->s_17]++;
$rekap[$presensi->nip][$presensi->s_18]++;
$rekap[$presensi->nip][$presensi->s_19]++;
$rekap[$presensi->nip][$presensi->s_20]++;
$rekap[$presensi->nip][$presensi->s_21]++;
$rekap[$presensi->nip][$presensi->s_22]++;
$rekap[$presensi->nip][$presensi->s_23]++;
$rekap[$presensi->nip][$presensi->s_24]++;
$rekap[$presensi->nip][$presensi->s_25]++;
$rekap[$presensi->nip][$presensi->s_26]++;
$rekap[$presensi->nip][$presensi->s_27]++;
$rekap[$presensi->nip][$presensi->s_28]++;
$rekap[$presensi->nip][$presensi->s_29]++;
$rekap[$presensi->nip][$presensi->s_30]++;
$rekap[$presensi->nip][$presensi->s_31]++;
}

//echo '<pre>';
//print_r($rekap);
//echo '</pre>';
?>

<table class="bordered responsive-table" id="tabel">
<thead>
<tr>
<th class="center" rowspan="2">No.</th>
<th class="center" rowspan="2">NIP</th>
<th class="center" rowspan="2">Nama</th>
<th class="center" colspan="<?php echo $tipe_data->num_rows()?>">Tipe</th>
</tr>
<?php
foreach($tipe_data->result() as $tipe) { ?>
<th class="center"><?php echo $tipe->nama ?></th>
<?php } ?>
<tr>
</tr>
</thead>
<tbody>
<?php
$no = 0;
foreach($pegawai_data->result() as $pegawai) {
$no++; ?>
<tr>
<td><?php echo $no ?></td>
<td><?php echo $pegawai->nip?></td>
<td><?php echo $pegawai->nama?></td>
<?php
foreach($tipe_data->result() as $tipe) { ?>
<td class="right-align"><?php 
if($tipe->id =='H')
{
	echo $this->maestro->rupiah($rekap[$pegawai->nip]['H']+$rekap[$pegawai->nip]['T']+$rekap[$pegawai->nip]['TAP']+$rekap[$pegawai->nip]['TAM']+$rekap[$pegawai->nip]['TAMP']+$rekap[$pegawai->nip]['TAPT']+$rekap[$pegawai->nip]['TP']+$rekap[$pegawai->nip]['P']);
}
else
{
	echo $this->maestro->rupiah($rekap[$pegawai->nip][$tipe->id]);
}


?></td>
<?php }?>
</tr>
<?php } ?>
</tbody>
</table>

<script>
function excel()
  {
	  $('#result').table2excel({
		name: "<?php echo $title ?>",
		filename: "<?php echo $title ?>",
		exclude_img: true,
		exclude_links: true,
		exclude_inputs: true
	});
  }
  
 
</script>
