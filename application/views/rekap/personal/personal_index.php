<?php require(__DIR__ . '/../../template/sesi.php') ?>
<style>
	.select2-selection--single{
		padding: 1.1rem 0;
	}
</style>

<?php
error_reporting(0);
foreach($tipe_data->result() as $tipe)
{
$count_tipe[$tipe->id] = 0;	
$group[$tipe->id] = $tipe->group;	

}

foreach($jam_kerja as $jam)
{
	$absen_buka[$jam->id] = $jam->absen_buka;
	$nama_singkat[$jam->id] = $jam->nama_singkat;
	$jadwal_masuk[$jam->id] = $jam->jadwal_masuk;
	$jadwal_tengah[$jam->id] = $jam->jadwal_tengah;
	$jadwal_keluar[$jam->id] = $jam->jadwal_keluar;
	$absen_tutup[$jam->id] = $jam->absen_tutup;
	$selisih_hari[$jam->id] = $jam->selisih_hari;
	$waktu_kerja[$jam->id] = $jam->waktu_kerja;
}

foreach($izin_data as $izin)
{
	$arr_izin[$izin->tanggal] = $izin->tipe;
	
}


?>

<!-- Main content -->
<div class="content-wrapper">


	<!-- Page header -->
	<div class="page-header border-bottom-0">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4> Rekap - <span class="font-weight-semibold">Saya</span></h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>

			<div class="header-elements d-none mb-3 mb-md-0">
				<!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Content area -->
	<div class="content pt-0">

		<?php
		$message = $this->session->flashdata('message');
		$info = $this->session->flashdata('info');
		if (isset($message)) { ?>

			<!-- Solid alert -->
			<div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
				<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
				<span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
			</div>
			<!-- /solid alert -->
			<br />

		<?php 	} ?>

		<?php echo form_open('rekap/personal') ?>

		<div class="form-group row">
			<div class="col-lg-4">
				<input type="hidden" name="lok_A_01" id="lok_A_01" value="<?=$dataPns->A_01;?>">
				<!-- <select id="bulan" name="bulan" onchange="cari()" class="form-control select2"> -->
				<select class="form-control h-auto" name="nipbos" id="nipbos" style="width:100%!important; height:100%!important;" required></select>
			</div>

			<div class="col-lg-4">
				<input type="text" readonly class="form-control" name="namabos" id="namabos" >
		
				
			</div>

			
		</div>

		<div class="form-group row">
			<div class="col-lg-4">
				<!-- <select id="bulan" name="bulan" onchange="cari()" class="form-control select2"> -->
				<select id="bulan" name="bulan" class="form-control select2">
					<?php for ($j = 1; $j <= 12; $j++) {
						$selected = '';
						$bln = sprintf("%02d", $j);
						if ($month == $bln) {
							$selected = 'selected="selected"';
						}
						echo '<option ' . $selected . ' value="' . $bln . '">' . namaBulan($j) . '</option>';
					} ?>


				</select>
			</div>

			<div class="col-lg-4">
				<!-- <select id="tahun" name="tahun" onchange="cari()" class="form-control select2"> -->
				<select id="tahun" name="tahun"  class="form-control select2">
					<?php for ($y = 2018; $y <= date("Y") + 1; $y++) { ?>
						<option value="<?php echo $y ?>" <?php if ($y == $tahun) echo 'selected="selected"' ?>><?php echo $y ?></option>
					<?php } ?>
				</select>

			</div>
			<div class="col-lg-4">
				<button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari </button>
			</div>
		</div>
		<?php echo form_close() ?>

		<!-- Basic initialization -->
		<div class="card">

			<div class="card-body">
				<table class="table table-striped table-bordered datatable-button-html5-basic">

						<thead>
						<tr>
						<th class="text-center" rowspan="2">No.</th>
						<th class="text-center" rowspan="2">Tanggal</th>
						<th class="text-center" colspan="2">Absen</th>
						<th class="text-center" rowspan="2">Status</th>
						<th class="text-center" rowspan="2">KWK<br>(menit)</th>
						</tr>
						<tr>
						<th class="text-center">Masuk</th>
						<th class="text-center">Pulang</th> 
						</tr>
						</thead>

					<?php	if ($presensi_data_cek->num_rows() > 0) { ?>
						<tbody>

						<?php
						
	for ($tgl = 1; $tgl <=  jumlah_hari($tahun,$bulan); $tgl++)
	{ 
	$tanggal = sprintf("%02d",$tgl);
	// $tanggal = $tgl;
	$count_tipe[$presensi_data['s_'.$tanggal]]++;

	$jadwal = $presensi_data['j_'.$tanggal];

//echo $jadwal;

if($waktu_kerja[$jadwal] > 0)
{ 
	$periode[$tanggal] = $tahun.'-'.$bulan.'-'.$tanggal;
	
	if(strlen($arr_izin[$tahun.'-'.$bulan.'-'.$tanggal]) > 0)
	{
		$kwk[$tanggal] = 0;
	}
	else
	{
	
	$absen_buka[$tanggal] = $tahun.'-'.$bulan.'-'.$tanggal.' '.$absen_buka[$jadwal].':00';
	$jadwal_masuk[$tanggal] = $tahun.'-'.$bulan.'-'.$tanggal.' '.$jadwal_masuk[$jadwal].':00';
	$jadwal_tengah[$tanggal] = $tahun.'-'.$bulan.'-'.$tanggal.' '.$jadwal_tengah[$jadwal].':00';
	
	$jadwal_keluar[$tanggal] = date('Y-m-d H:i:s', strtotime($tahun.'-'.$bulan.'-'.$tanggal.' '.$jadwal_keluar[$jadwal].':00 +'.$selisih_hari[$jadwal].' day'));
	$absen_tutup[$tanggal] = date('Y-m-d H:i:s', strtotime($tahun.'-'.$bulan.'-'.$tanggal.' '.$absen_tutup[$jadwal].':00 +'.$selisih_hari[$jadwal].' day'));
	
		$absen_masuk[$tanggal] = $presensi_data['am_'.$tanggal];
		$absen_pulang[$tanggal] = $presensi_data['ap_'.$tanggal];
		
		if(strlen($absen_masuk[$tanggal]) > 0)
		{
			$absen_masuk[$tanggal] = $presensi_data['am_'.$tanggal];
		}
		
		if(strlen($absen_pulang[$tanggal]) > 0)
		{
			$absen_pulang[$tanggal] = $presensi_data['ap_'.$tanggal];
		}
		
			if((strlen($absen_masuk[$tanggal]) == 0) && (strlen($absen_pulang[$tanggal]) == 0) )
			{
				//alpha
				$kwk[$tanggal] = '0';
			}
			else
			{
				if((strlen($absen_masuk[$tanggal]) >= 0) && (strlen($absen_pulang[$tanggal]) >= 0))
				{
					if(($absen_masuk[$tanggal] <= $jadwal_masuk[$tanggal]) && ($absen_pulang[$tanggal] >= $jadwal_keluar[$tanggal]))
					{
						//hadir
						$kwk[$tanggal] = 0;
					}
					if(($absen_masuk[$tanggal] > $jadwal_masuk[$tanggal]) && ($absen_pulang[$tanggal] > $jadwal_keluar[$tanggal]))
					{
						//terlambat
						$kwk[$tanggal]  = floor((strtotime($absen_masuk[$tanggal]) - strtotime($jadwal_masuk[$tanggal]) ) / 60);
					}
					if(($absen_masuk[$tanggal] < $jadwal_masuk[$tanggal]) && ($absen_pulang[$tanggal] < $jadwal_keluar[$tanggal]))
					{
						//pulang awal
						$kwk[$tanggal]  = floor((strtotime($jadwal_keluar[$tanggal])- strtotime($absen_pulang[$tanggal]) ) / 60);
					}
					if(($absen_masuk[$tanggal] > $jadwal_masuk[$tanggal]) && ($absen_pulang[$tanggal] < $jadwal_keluar[$tanggal]))
					{
						//terlambat dan pulang awal
						$kwk_awal[$tanggal]  = floor((strtotime($absen_masuk[$tanggal]) - strtotime($jadwal_masuk[$tanggal]) ) / 60);					
						$kwk_akhir[$tanggal]  = floor((strtotime($jadwal_keluar[$tanggal]) - strtotime($absen_pulang[$tanggal]) ) / 60);
						$kwk[$tanggal] = $kwk_awal[$tanggal] + $kwk_akhir[$tanggal];
					}
				}
				if((strlen($absen_masuk[$tanggal]) == 0) && (strlen($absen_pulang[$tanggal]) >= 0))
				{
					if($jadwal_keluar[$tanggal] <= $absen_pulang[$tanggal]) 
					{
					//tidak absen masuk
					$kwk[$tanggal] = floor($waktu_kerja[$jadwal] / 2 );
					}
					if($jadwal_keluar[$tanggal] >= $absen_pulang[$tanggal]) 
					{
					//tidak absen masuk dan pulang cepat
					$kwk_awal[$tanggal]  = floor($waktu_kerja[$jadwal] / 2);					
					$kwk_akhir[$tanggal]  = floor((strtotime($jadwal_keluar[$tanggal]) - strtotime($absen_pulang[$tanggal]) ) / 60);
					$kwk[$tanggal] = $kwk_awal[$tanggal] + $kwk_akhir[$tanggal];
					}
					
				}
				
				if((strlen($absen_masuk[$tanggal]) >= 0) && (strlen($absen_pulang[$tanggal]) == 0))
				{
					if($jadwal_masuk[$tanggal] >= $absen_masuk[$tanggal]) 
					{
					//tidak absen pulang
					$kwk[$tanggal] = floor($waktu_kerja[$jadwal] / 2 );
					}
					if($jadwal_masuk[$tanggal] <= $absen_masuk[$tanggal]) 
					{
					//tidak absen pulang dan terlambat
					$kwk_awal[$tanggal]  = floor((strtotime($absen_masuk[$tanggal]) - strtotime($jadwal_masuk[$tanggal]) ) / 60);					
					$kwk_akhir[$tanggal]  = floor($waktu_kerja[$jadwal] / 2 );
					$kwk[$tanggal] = $kwk_awal[$tanggal] + $kwk_akhir[$tanggal];
					}			
				}		
			}	
	}
}
else
{
	$kwk[$tanggal] = 0;
					
}





?>
<tr>
<td class="text-center"><?php echo $tgl.'.' ?></td>
<td class="left-align"><?php echo namahari_tanggal($tahun,$bulan,$tanggal) ?></td>
<td class="text-center"><?php echo jam_menit($presensi_data['am_'.$tanggal]) ?></td>
<td class="text-center"><?php echo jam_menit($presensi_data['ap_'.$tanggal]) ?></td>
<td class="text-center"><?php echo $presensi_data['s_'.$tanggal] ?></td>
<td class="text-center"><?php 
if($group[$presensi_data['s_'.$tanggal]] == '3')
{
	echo '0';
}
else
{
	echo $kwk[$tanggal];
}

 ?></td>

</tr>	
<?php }	
?>

							
						</tbody>

						<?php } else {
						echo '<tr><td class="text-center" colspan=6><h4>Rekap Tidak Ditemukan</h4></td></tr>';
					}
					?>

					

				</table>
			</div>

		</div>
		<!-- /basic initialization -->

	</div>
	<!-- /content area -->


	<?php require(__DIR__ . '/../../template/footer.php') ?>



	<script>
    $(function () {
        var lokasi = $('#lok_A_01').val();
        var fixurl = '<?=base_url('fungsi/cari_pns/');?>' + lokasi;
        $('#nipbos').select2({
            ajax: {
                url: fixurl,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        nipnya: params.term
                    }
                    
                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
			
            placeholder: "Masukkan NIP pada aplikasi",
            minimumInputLength: 5,
            escapeMarkup: function (markup) { return markup; },
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,
			
        });
        
        function formatRepo (repo) {
            if (repo.loading) {
                return repo.text;
            }
            
            var markup = "<div>" + repo.name + "</div><div>" + repo.id + "</div>";
            
            return markup;
        }
        
        function formatRepoSelection (repo) {
            return repo.id;
        }
        
        $('#nipbos').change(function(){
            var nip = $('#nipbos').select2('data')[0].id;
            var name = $('#nipbos').select2('data')[0].name;
            $('#id').val(nip);
            $('#namabos').val(name);

        });

        
    });
	</script>