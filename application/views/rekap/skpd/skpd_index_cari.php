<?php require(__DIR__ . '/../../template/sesi.php');
error_reporting(0);
?>
<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Rekap - <span class="font-weight-semibold">SKPD CARI</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                 <div class="d-flex justify-content-center">
							<!-- <a href="#" class="btn btn-link btn-float text-default" data-toggle="modal" data-target="#keterangan" ><i class="fa fa-file"></i><span>Keterangan</span></a> -->
							<!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a> -->
						</div> 
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->
            <br />

        <?php     } ?>

        <!-- Form inputs -->
        <div class="row">
        <div class="card col-lg-8" style="margin-right:20px">


            <div class="card-body ">
                <!-- <form action="#"> -->
                <?php echo form_open('rekap/skpd/cari') ?>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">OPD</label>
                    </div>

                    <div class="col-lg-9">
                        <?php

                        $unituser = $dataPns->A_01 . '00000000';
                        foreach ($allopd as $opd) {

                            $s[$opd->KOLOK] = ($opd->NALOK);
                        }
                        echo form_dropdown('opd', $s, '', 'class="form-control select2" data-fouc id="opd"');
                        ?>


                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Unit Kerja</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" name="unit" id="unit">
                            <option value="">Pilih</option>

                        </select>

                    </div>

                </div>


				<div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Eselon</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" name="eselon" id="eselon">
						<option value="all">Semua</option>
						<option value="str">Struktural</option>
						<option value="1">Eselon 1</option>
						<option value="2">Eselon 2</option>
						<option value="3">Eselon 3</option>
        				<option value="4">Eselon 4</option>

                        </select>

                    </div>

                </div>


                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Bulan</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" required name="bulan" id="bulan">
                            <?php for ($j = 1; $j <= 12; $j++) {
                                $selected = '';
                                $bln = sprintf("%02d", $j);
                                if ($month == $bln) {
                                    $selected = 'selected="selected"';
                                }
                                echo '<option ' . $selected . ' value="' . $bln . '">' . namaBulan($j) . '</option>';
                            } ?>

                        </select>
                    </div>



                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Tahun</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" required name="tahun" id="tahun">
                            <?php for ($tahun = 2018; $tahun <= date("Y") + 1; $tahun++) { ?>
                                <option value="<?php echo $tahun ?>" <?php if ($year == $tahun) echo 'selected="selected"' ?>><?php echo $tahun ?></option>
                            <?php } ?>

                        </select>
                    </div>



                </div>

                <!-- <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label ">Set Dengan Jadwal Umum</label>
                    </div>

                    <div class="col-lg-9">
                        <select class="form-control select2" style="width: 100%;" required name="jadwal_umum" id="jadwal_umum">
                            <option value="">Pilih</option>
                            <option value="1">Ya</option>
                            <option value="0">Tidak</option>


                        </select>
                    </div>



                </div> -->




                <div class="text-right">
                    <button type="submit" class="btn btn-primary pull-right"> <i class="fa fa-save"></i> Cari </button>
                </div>

               
                <?php echo form_close() ?>
              
            </div>
        </div>
        

      
       


        <?php

foreach($tipe_data->result() as $tipe) 
	{
		$jumlah[$tipe->id][1] = 0;
		$jumlah[$tipe->id][2] = 0;
		$jumlah[$tipe->id][3] = 0;
		$jumlah[$tipe->id][4] = 0;
		$jumlah[$tipe->id][5] = 0;
		$jumlah[$tipe->id][6] = 0;
		$jumlah[$tipe->id][7] = 0;
		$jumlah[$tipe->id][8] = 0;
		$jumlah[$tipe->id][9] = 0;
		$jumlah[$tipe->id][10] = 0;
		$jumlah[$tipe->id][11] = 0;
		$jumlah[$tipe->id][12] = 0;
		$jumlah[$tipe->id][13] = 0;
		$jumlah[$tipe->id][14] = 0;
		$jumlah[$tipe->id][15] = 0;
		$jumlah[$tipe->id][16] = 0;
		$jumlah[$tipe->id][17] = 0;
		$jumlah[$tipe->id][18] = 0;
		$jumlah[$tipe->id][19] = 0;
		$jumlah[$tipe->id][20] = 0;
		$jumlah[$tipe->id][21] = 0;
		$jumlah[$tipe->id][22] = 0;
		$jumlah[$tipe->id][23] = 0;
		$jumlah[$tipe->id][24] = 0;
		$jumlah[$tipe->id][25] = 0;
		$jumlah[$tipe->id][26] = 0;
		$jumlah[$tipe->id][27] = 0;
		$jumlah[$tipe->id][28] = 0;
		$jumlah[$tipe->id][29] = 0;
		$jumlah[$tipe->id][30] = 0;
		$jumlah[$tipe->id][31] = 0;	 
	}


foreach($presensi_data->result() as $presensi)
{
	$jumlah[$presensi->s_01][1]++;
	$jumlah[$presensi->s_02][2]++;
	$jumlah[$presensi->s_03][3]++;
	$jumlah[$presensi->s_04][4]++;
	$jumlah[$presensi->s_05][5]++;
	$jumlah[$presensi->s_06][6]++;
	$jumlah[$presensi->s_07][7]++;
	$jumlah[$presensi->s_08][8]++;
	$jumlah[$presensi->s_09][9]++;
	$jumlah[$presensi->s_10][10]++;
	$jumlah[$presensi->s_11][11]++;
	$jumlah[$presensi->s_12][12]++;
	$jumlah[$presensi->s_13][13]++;
	$jumlah[$presensi->s_14][14]++;
	$jumlah[$presensi->s_15][15]++;
	$jumlah[$presensi->s_16][16]++;
	$jumlah[$presensi->s_17][17]++;
	$jumlah[$presensi->s_18][18]++;
	$jumlah[$presensi->s_19][19]++;
	$jumlah[$presensi->s_20][20]++;
	$jumlah[$presensi->s_21][21]++;
	$jumlah[$presensi->s_22][22]++;
	$jumlah[$presensi->s_23][23]++;
	$jumlah[$presensi->s_24][24]++;
	$jumlah[$presensi->s_25][25]++;
	$jumlah[$presensi->s_26][26]++;
	$jumlah[$presensi->s_27][27]++;
	$jumlah[$presensi->s_28][28]++;
	$jumlah[$presensi->s_29][29]++;
	$jumlah[$presensi->s_30][30]++;
	$jumlah[$presensi->s_31][31]++;
}


?>

<!-- Basic initialization -->
<div class="card container">

<div class="card-body">
<table class="table table-striped table-bordered datatable-button-html5-basic">
<thead>
<tr>
<th class="center" rowspan="2">No.</th>
<th class="center" rowspan="2">Tipe</th>
<th class="left" colspan="<?php echo jumlah_hari($year,$month)?>">Tanggal</th>
</tr>
<tr>
<?php
for ($tgl = 1; $tgl <=  jumlah_hari($year,$month); $tgl++)
{ 
?>
<th class="center"><?php echo $tgl ?></th>
<?php } ?>
</tr>
</thead>
<tbody>
<?php
$no = 0;
foreach($tipe_data->result() as $tipe) {
$no++; ?>
<tr>
<td><?php echo $no ?></td>
<td><?php echo $tipe->nama ?></td>
<?php
for ($tgl = 1; $tgl <=  jumlah_hari($year,$month); $tgl++)
{
//$tanggal = sprintf("%02d",$tgl);	
?>
<td class="right-align"><?php
if($tipe->id =='H')
{
	echo rupiah($jumlah['H'][$tgl]+$jumlah['T'][$tgl]+$jumlah['TAP'][$tgl]+$jumlah['TAM'][$tgl]+$jumlah['TAMP'][$tgl]+$jumlah['TAMPT'][$tgl]+$jumlah['TP'][$tgl]+$jumlah['P'][$tgl]);
}
else
 echo rupiah($jumlah[$tipe->id][$tgl])?></td>
<?php } ?>
</tr>
<?php } ?>


</tbody>
</table>

</div>
</div>

</div>






        



    </div>
    <!-- /content area -->

    <?php require(__DIR__ . '/../../template/footer.php') ?>
    <script src="https://cdn.datatables.net/fixedcolumns/3.3.3/js/dataTables.fixedColumns.min.js"></script>

    <script>
        /* ------------------------------------------------------------------------------
         *
         *  # Buttons extension for Datatables. HTML5 examples
         *
         *  Demo JS code for datatable_extension_buttons_html5.html page
         *
         * ---------------------------------------------------------------------------- */


        // Setup module
        // ------------------------------

        var DatatableButtonsHtml5 = function() {


            //
            // Setup module components
            //

            // Basic Datatable examples
            var _componentDatatableButtonsHtml5 = function() {
                if (!$().DataTable) {
                    console.warn('Warning - datatables.min.js is not loaded.');
                    return;
                }

                // Setting datatable defaults
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Filter:</span> _INPUT_',
                        searchPlaceholder: 'Type to filter...',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {
                            'first': 'First',
                            'last': 'Last',
                            'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                            'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                        }
                    }
                });


                // Basic initialization


                var tabel = $('.datatable-button-html5-basic').DataTable({
                        buttons: {
                            dom: {
                                button: {
                                    className: 'btn btn-light'
                                }
                            },
                            buttons: [
                                // 'excelHtml5'
                            ]
                        },

                        'scrollY': "500px",
                        'scrollX': true,
                        'scrollCollapse': true,
                        'paging': false,
                        'searching': false,
                        'fixedColumns': {
                            leftColumns: 1,
                        },

                    }

                );

                $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                    $.fn.dataTable.tables({
                        visible: true,
                        api: true
                    }).columns.adjust();
                });



            };

            // Select2 for length menu styling
            var _componentSelect2 = function() {
                if (!$().select2) {
                    console.warn('Warning - select2.min.js is not loaded.');
                    return;
                }

                // Initialize
                $('.dataTables_length select').select2({
                    minimumResultsForSearch: Infinity,
                    dropdownAutoWidth: true,
                    width: 'auto'
                });
            };


            //
            // Return objects assigned to module
            //

            return {
                init: function() {
                    _componentDatatableButtonsHtml5();
                    _componentSelect2();
                }
            }
        }();


        // Initialize module
        // ------------------------------

        document.addEventListener('DOMContentLoaded', function() {
            DatatableButtonsHtml5.init();
        });
    </script>


    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>


    <script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#opd").change(function() { // Ketika user mengganti atau memilih Mutasi Ke
                $("#unit").hide(); // Sembunyikan dulu combobox kota nya
                $("#loading").show(); // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("fungsi/unit_kerja"); ?>", // Isi dengan url/path file php yang dituju
                    data: {
                        opd: $("#opd").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide(); // Sembunyikan loadingnya
                        // set isi dari combobox 
                        // lalu munculkan kembali combobox 
                        $("#unit").html(response.list_lokasis).show();
                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });
            });
        });
    </script>



</div>
<!-- /main content -->

</div>
<!-- /page content -->

</body>

</html>