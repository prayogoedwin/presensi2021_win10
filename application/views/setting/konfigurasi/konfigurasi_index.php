<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Setting - <span class="font-weight-semibold">Konfigurasi</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                <!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->
            <br />

        <?php     } ?>

        <!-- Form inputs -->
        <div class="card col-lg-10">


            <div class="card-body ">
                <!-- <form action="#"> -->
                <?php echo form_open('setting/konfigurasi_edit_act') ?>

                <?php foreach ($config as $conn) : ?>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="col-form-label "><?= $conn->nama ?></label>
                        </div>

                        <div class="col-lg-4">
                            <input type="text" width="100%" required id="email" name="value_<?= $conn->id ?>" value="<?= $conn->value ?>" class="form-control">

                        </div>
                        <div class="col-lg-3">
                            <label class="col-form-label "><?= $conn->keterangan ?></label>
                        </div>
                    </div>
                <?php endforeach; ?>

                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="col-form-label ">URL Cron Job</label>
                        </div>

                        <div class="col-lg-4">
                            <input type="text" width="100%" required id="email" name="value_" value="http://localhost:81/presensi2021/data_v3/presensi/presensi_save" class="form-control">
                            <!-- http://localhost:81/presensi2021/data_v3/presensi/presensi_save?year=2021&month=09 -->
                        </div>
                        <div class="col-lg-3">
                            <label class="col-form-label "></label>
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="col-form-label ">Tanggal Cron Job</label>
                        </div>

                        <div class="col-lg-4">
                            <input type="text" width="100%" required id="email" name="value_" value="10" class="form-control">
                           
                        </div>
                        <div class="col-lg-3">
                            <label class="col-form-label ">Tanggal Tiap Bulan</label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="col-form-label ">Waktu Cron Job</label>
                        </div>

                        <div class="col-lg-4">
                            <input type="text" width="100%" required id="email" name="value_" value="00:00" class="form-control">
                           
                        </div>
                        <div class="col-lg-3">
                            <label class="col-form-label ">Jam,Menit Tiap Action</label>
                        </div>
                    </div>



                <div class="text-left">
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan </button>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
        <!-- /form inputs -->

    </div>
    <!-- /content area -->

    <?php require(__DIR__ . '/../../template/footer.php') ?>

    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    </script>


</div>
<!-- /main content -->

</div>
<!-- /page content -->

</body>

</html>