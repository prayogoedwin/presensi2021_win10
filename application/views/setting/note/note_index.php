<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<!-- Main content -->
<div class="content-wrapper">


    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> Setting - <span class="font-weight-semibold">Note</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none mb-3 mb-md-0">
                <!-- <div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
						</div> -->
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content pt-0">

        <?php
        $message = $this->session->flashdata('message');
        $info = $this->session->flashdata('info');
        if (isset($message)) { ?>

            <!-- Solid alert -->
            <div class="alert bg-<?= $info ?> alert-styled-left alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                <span class="font-weight-semibold"><?= strtoupper($info) ?>! &nbsp;</span><?= $message ?>
            </div>
            <!-- /solid alert -->
            <br />

        <?php     } ?>


        <!-- Solid tabs -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">


                    <div class="card-body">
                        <ul class="nav nav-tabs nav-tabs-solid border-0">
                            <?php foreach ($notes as $note) :
                                if ($note->id == 'privat') {
                                    $act = 'active';
                                } else {
                                    $act = NULL;
                                }
                            ?>
                                <li class="nav-item"><a href="#<?= $note->id ?>" class="nav-link <?= $act ?>" data-toggle="tab"><?= strtoupper($note->id) ?></a></li>
                                <!-- <li class="nav-item"><a href="#solid-tab2" class="nav-link" data-toggle="tab">Private</a></li> -->
                            <?php endforeach; ?>
                        </ul>

                        <?php echo form_open('setting/note_edit_act') ?>
                        <div class="tab-content">
                            
                            <?php foreach ($notes as $note) :
                                if ($note->id == 'privat') {
                                    $fsa = 'show active';
                                } else {
                                    $fsa = NULL;
                                }
                            ?>
                                    <div class="tab-pane fade <?= $fsa ?>" id="<?= $note->id ?>">
                                    <!-- <input type="text" name="" value="<?= $note->id ?>"> -->
                                    <textarea id="text_<?= $note->id ?>" name="teks_<?= $note->id ?>"><?= $note->teks ?></textarea>
                                    <br />
                                    <div class="text-left">
                                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan </button>
                                    </div>

                                    

                                    </div>
                            <?php endforeach; ?>
                            
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>


        </div>
        <!-- /solid tabs -->


    </div>
    <!-- /content area -->


    <?php require(__DIR__ . '/../../template/footer.php') ?>


    <script>
        $(document).ready(function() {
            <?php foreach ($notes as $note) : ?>
                $('#text_<?= $note->id ?>').summernote();
            <?php endforeach; ?>
        });
    </script>









</div>
<!-- /main content -->
</div>
<!-- /page content -->

</body>

</html>