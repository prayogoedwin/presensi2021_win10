<!-- Footer -->
<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2021. <a href="#"><a href="<?=base_url('dashboard')?>" target="_blank">Badan Kepegawaian Daerah Jawa Tengah</a>
					</span>

					
					<span class="navbar-text ml-lg-auto">
						Version 1.0
					</span>
				</div>
</div>
<!-- /footer -->

<div class="modal bottom-sheet" id="progressbar">
	<div class="modal-content">
	<h5>Harap Menunggu !</h5><div class="progress"><div class="indeterminate"></div></div>
	<p>Proses sedang berlangsung ...</p>
	</div></div>

<div id="popup_large" class="modal modal-large">
    <div class="modal-content"></div>
    </div>
	
	<div id="popup_medium" class="modal modal-medium">
    <div class="modal-content"></div>
    </div>
	
	<div id="popup_small" class="modal modal-small">
    <div class="modal-content"></div>
    </div>


	<div class="modal transparent scroll-hidden z-depth-0" id="loader">
	<div class="modal-content center">
	<div class="preloader-wrapper big active">
	<div class="spinner-layer spinner-blue-only">
		<div class="circle-clipper left">
			<div class="circle"></div>
		</div><div class="gap-patch">
		<div class="circle"></div>
		</div><div class="circle-clipper right">
			<div class="circle"></div>
		</div>
	</div>
	</div>
	</div>
	</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>