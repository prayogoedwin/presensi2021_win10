<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>ePresensi Provinsi Jawa Tengah</title>
	<!-- App Icons -->
	<link rel="shortcut icon" href="<?=base_url()?>assets/jateng.ico">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/global_assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/light/full/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/light/full/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/light/full/assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/light/full/assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url()?>assets/limitless/light/full/assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?=base_url()?>assets/limitless/global_assets/js/main/jquery.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/pickers/daterangepicker.js"></script>

	<script src="<?=base_url()?>assets/limitless/light/full/assets/js/app.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/demo_pages/dashboard.js"></script>
    <script src="<?=base_url()?>assets/limitless/global_assets/js/demo_pages/layout_fixed_sidebar_custom.js"></script>

    <!-- Theme JS files Form -->
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/forms/selects/select2.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/demo_pages/form_select2.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/demo_pages/form_inputs.js"></script>
	<!-- /theme JS files Form -->
   
    <!-- Theme JS files Table -->
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/forms/selects/select2.min.js"></script>
	<!-- <link href="https:////cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
	<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script> -->
	

    <script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
	
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url()?>assets/limitless/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <!-- <script src="assets/limitless/full/assets/global_assets/js/demo_pages/datatables_extension_buttons_html5.js"></script> -->
	<!-- <script src="assets/datatables.js"></script>  -->

	<style>
	.center{
		text-align:center
	}
	</style>

	<script> 
		var site = "<?php echo site_url()?>";
	</script>

	

</head>