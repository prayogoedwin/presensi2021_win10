<?php include('sesi.php');?>

<!-- <body class="navbar-top sidebar-xs"> -->
<body class="navbar-top"> 

	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-light fixed-top">
		<div class="navbar-brand">
			<a href="<?=base_url('dashboard')?>" class="d-inline-block">
				<img src="<?=base_url()?>assets/presensi_black.png" alt="" width="90px" >
			</a> 
			
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>

				
			</ul>

			<span class="badge bg-success my-3 my-md-0 ml-md-3 mr-md-auto">Online</span>

			<ul class="navbar-nav">
				
					<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						<img src="<?=base_url()?>assets/jateng.png" class="rounded-circle mr-2" height="34" alt="">
						<span><?=$this->formatter->getDateTimeFormatUser(date('Y-m-d H:i:s'))?></span>
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<!-- <a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
						<a href="#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>
						<a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a>
						<div class="dropdown-divider"></div>
						<a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a> -->
						<a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_default"><i class="icon-user"></i> Profil Saya</a>
						<a href="<?=base_url('portal')?>" class="dropdown-item"><i class="icon-switch2"></i> Keluar</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->

	<!-- Basic modal -->
	<div id="modal_default" class="modal fade" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Profil Saya</h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

							<div class="modal-body ">
							<table>
								<tr>
								<td>
								<img src="<?=$urlFoto?>" width="100" height="130" alt="">
								</td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td>
								<table width="100%" style="padding:10px">
									<tr><td>Nama Lengkap</td><td>:</td><td> <?=$dataPns->B_03A?> <?=$dataPns->B_03?> <?=$dataPns->B_03B?></td></tr>
									<tr><td>NIP</td><td>:</td><td> <?=$dataPns->B_02B?></td></tr>
									<tr><td>Tempat, Tanggal Lahir</td><td>:</td><td> <?=$dataPns->B_04?>, <?=$this->formatter->getDateMonthFormatUser($dataPns->B_05)?></td></tr>
									<tr><td>Jabatan</td><td>:</td><td> <?=$dataPns->I_JB?></td></tr>
									<tr><td>Lokasi Kerja</td><td>:</td><td> <?=$lokasiKerja->NALOKP?></td></tr>
									<tr><td>Unit Kerja</td><td>:</td><td> <?=$unitKerja->NALOKP?></td></tr>	
								</table>
								</td>
								
								</tr>


							</table>
								
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
								<!-- <button type="button" class="btn bg-primary">Save changes</button> -->
							</div>
						</div>
					</div>
				</div>
				<!-- /basic modal -->