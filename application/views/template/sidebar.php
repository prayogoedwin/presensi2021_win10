<?php include('sesi.php');?>

<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->

    <!-- <div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md"> -->
    <div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- User menu -->
            <div class="sidebar-user">
                <div class="card-body">
                    <div class="media">
                        <div class="mr-3">
                            <!-- <a href="#"><img src="<?= base_url() ?>assets/jateng.png" width="60" height="60" class="rounded-circle" alt=""></a> -->
                            <a href="#"><img src="<?=$urlFoto?>" width="50" height="60" class="rounded-circle" alt=""></a>
                        </div>

                        <div class="media-body">
                            <div class="media-title font-weight-semibold"><?= strtoupper($this->session->userdata('username')) ?></div>
                            <div class="font-size-xs opacity-80">
                                <?=$dataPns->B_03?> <br />
                                <u><?=$dataPns->B_02B?></u>
                                <br />
                                <?= $roleUser ?>
                            </div>
                        </div>

                        <!-- <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div> -->
                    </div>
                </div>
            </div>
            <!-- /user menu -->


            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <!-- Main -->
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i>
                    </li>
                    <!-- <li class="nav-item">
                    <a href="<?= base_url('dashboard') ?>" class="nav-link active">
                        <i class="icon-home4"></i>
                        <span>
                            Kota
                        </span>
                    </a>
                </li>
                

                <li class="nav-item">
                    <a href="tablex.html" class="nav-link">
                        <i class="icon-design"></i>
                        <span>
                            Table
                        </span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="formx.html" class="nav-link">
                        <i class="icon-pencil5"></i>
                        <span>
                            Form
                        </span>
                    </a>
                </li> -->



                    <li class="nav-item">
                        <a href="<?= base_url('dashboard') ?>" class="nav-link">
                            <i class="fa fa-desktop"></i>
                            <span>
                                Dashboard
                            </span>
                        </a>
                    </li>

                    <li class="nav-item nav-item-submenu <?=active_menu($controller,'setting')?>">
                        <a href="#" class="nav-link"><i class="fa fa-cogs"></i> <span>Setting </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('setting/konfigurasi') ?>" class="nav-link ">
                                    <i class="fa fa-cog"></i>
                                    <span>
                                        Konfigurasi
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('setting/privilege') ?>" class="nav-link ">
                                    <i class="fa fa-users-cog"></i>
                                    <span>
                                        Hak Akses
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('setting/note') ?>" class="nav-link ">
                                    <i class="fa fa-sticky-note"></i>
                                    <span>
                                        Note
                                    </span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item nav-item-submenu <?=active_menu($controller,'jadwal')?>">
                        <a href="#" class="nav-link"><i class="fa fa-calendar-alt"></i> <span>Jadwal </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('jadwal/saya') ?>" class="nav-link ">
                                    <i class="fa fa-clipboard-list"></i>
                                    <span>
                                        Jadwal Saya
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('jadwal/libur') ?>" class="nav-link ">
                                    <i class="fa fa-calendar-times"></i>
                                    <span>
                                        Hari Libur
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('jadwal/jam') ?>" class="nav-link ">
                                    <i class="fa fa-clock"></i>
                                    <span>
                                        Jam Kerja
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('jadwal/kerja') ?>" class="nav-link ">
                                    <i class="fa fa-calendar-check"></i>
                                    <span>
                                        Jadwal Kerja
                                    </span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item nav-item-submenu  <?=active_menu($controller,'izin')?>">
                        <a href="#" class="nav-link"><i class="fa fa-envelope"></i> <span>Izin </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('izin/saya') ?>" class="nav-link ">
                                    <i class="fa fa-envelope-open-text"></i>
                                    <span>
                                        Izin Saya
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('izin/data') ?>" class="nav-link ">
                                    <i class="fa fa-mail-bulk"></i>
                                    <span>
                                        Data Izin
                                    </span>
                                </a>
                            </li>

                            
                        </ul>
                    </li>

                    <!-- <li class="nav-item nav-item-submenu <?=active_menu($controller,'data')?>" >
                        <a href="#" class="nav-link"><i class="fa fa-database"></i> <span>Data </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('data/presensi') ?>" class="nav-link ">
                                    <i class="fa fa-fingerprint"></i>
                                    <span>
                                        Presensi
                                    </span>
                                </a>
                            </li> 
                        </ul>
                    </li> -->

                    <li class="nav-item nav-item-submenu  <?=active_menu($controller,'data_v3')?>">
                        <a href="#" class="nav-link"><i class="fa fa-database"></i> <span>Data </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('data_v3/presensi') ?>" class="nav-link ">
                                    <i class="fa fa-fingerprint"></i>
                                    <span>
                                    Presensi
                                    </span>
                                </a>
                            </li> 

                            <li class="nav-item">
                                <a href="<?= base_url('data_v3/presensi/all') ?>" class="nav-link ">
                                    <i class="fa fa-fingerprint"></i>
                                    <span>
                                    Bulk Save 
                                    </span>
                                </a>
                            </li> 

                        </ul>
                    </li>



                    <li class="nav-item nav-item-submenu  <?=active_menu($controller,'rekap')?>">
                        <a href="#" class="nav-link"><i class="fa fa-file-alt"></i> <span>Rekap Absen </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                            <li class="nav-item">
                                <a href="<?= base_url('rekap/saya') ?>" class="nav-link ">
                                    <i class="fa fa-user"></i>
                                    <span>
                                         Saya
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('rekap/personal') ?>" class="nav-link ">
                                    <i class="fa fa-user"></i>
                                    <span>
                                         Personal
                                    </span>
                                </a>
                            </li> 
                            <li class="nav-item">
                                <a href="<?= base_url('rekap/skpd') ?>" class="nav-link ">
                                    <i class="fa fa-user"></i>
                                    <span>
                                         SKPD
                                    </span>
                                </a>
                            </li> 

                            <li class="nav-item">
                                <a href="<?= base_url('rekap/bulanan') ?>" class="nav-link ">
                                    <i class="fa fa-user"></i>
                                    <span>
                                         Bulanan
                                    </span>
                                </a>
                            </li> 
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="<?= base_url('manual') ?>" class="nav-link ">
                            <i class="fa fa-book-reader"></i>
                            <span>
                                Manual Book
                            </span>
                        </a>
                    </li>

                    



                    <!-- <li class="nav-item nav-item-submenu nav-item-expanded nav-item-open ">
							<a href="#" class="nav-link"><i class="icon-office"></i> <span>Setting </span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Sidebars">
                                <li class="nav-item">
                                <a href="<?= base_url('p') ?>/tambah/ /reguler" class="nav-link ">
                                    <i class="icon-office"></i>
                                    <span>
                                        Konfigurasi
                                </span>
                                </a>
                                </li>

                                <li class="nav-item">
                                <a href="<?= base_url('p') ?>/tambah/ /reguler" class="nav-link ">
                                    <i class="icon-office"></i>
                                    <span>
                                        Hak Akses
                                </span>
                                </a>
                                </li>

                                <li class="nav-item">
                                <a href="<?= base_url('p') ?>/tambah/ /reguler" class="nav-link ">
                                    <i class="icon-office"></i>
                                    <span>
                                        Note
                                </span>
                                </a>
                                </li>


                            
								 <li class="nav-item nav-item-submenu nav-item-expanded nav-item-open">
									<a href="#" class="nav-link"><i class="icon-office"></i>Tambah Data</a>
									<ul class="nav nav-group-sub">
                                       
										<li class="nav-item"><a href="<?= base_url('p') ?>/tambah/ /reguler" class="nav-link active">Reguler</a></li>
                                        <li class="nav-item"><a href="<?= base_url('p') ?>/tambah/ /aspirasi" class="nav-link ">Aspirasi</a></li>
                                    
									</ul>
                                </li> 
                        
								
							</ul>
						</li>  -->




















                    <!-- <li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-tree5"></i> <span>Menu levels</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Menu levels">
								
								<li class="nav-item nav-item-submenu">
									<a href="#" class="nav-link"><i class="icon-firefox"></i> Second level with child</a>
									<ul class="nav nav-group-sub">
										
										<li class="nav-item nav-item-submenu">
											<a href="#" class="nav-link"><i class="icon-apple2"></i> Third level with child</a>
											<ul class="nav nav-group-sub">
												<li class="nav-item"><a href="#" class="nav-link"><i class="icon-html5"></i> Fourth level</a></li>
												<li class="nav-item"><a href="#" class="nav-link"><i class="icon-css3"></i> Fourth level</a></li>
											</ul>
										</li>
										
									</ul>
								</li>
								
							</ul>
						</li> -->

                    <!-- /main -->

                    <!-- Forms -->
                    <!-- <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Forms</div> <i class="icon-menu" title="Forms"></i></li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-pencil3"></i> <span>Form components</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Form components">
                        <li class="nav-item"><a href="form_inputs.html" class="nav-link">Basic inputs</a></li>
                        <li class="nav-item"><a href="form_checkboxes_radios.html" class="nav-link">Checkboxes &amp; radios</a></li>
                        
                    </ul>
                </li> -->


                    <!-- /forms -->





                </ul>
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->